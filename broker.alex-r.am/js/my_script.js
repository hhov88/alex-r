if(page=='add_post'){
    var area_tree;
    $(document).ready(function(){
        jQuery.post('http://broker.alex-r.am/ajax.php', {
            action 	: 'get_location_tree'
        },function(data){
            area_tree = $.parseJSON(data);
            area_tree.forEach(function(region, i, area_tree) {
                if(window.edit_region !== undefined && edit_region==region.region.settings.region_id){
                    var sel = 'selected';
                }
                else{
                    var sel = '';
                }
                $(".region").append('<option value="'+region.region.settings.region_id+'" '+sel+'>'+region.region.settings.region_ru+'</option>');
            });
            region_change(".region");
        })
    })
    var regSelector = 0;
    function region_change(elm){
        var regId = $(elm).val();
        var citySelect = '<option value="0">-- Город --</option>';
        var streetSelect = '<option value="0">-- Улица --</option>';
        area_tree.forEach(function(region, i, area_tree) {
            if(region.region.settings.region_id==regId){
                var regStreets = region.region.streets;
                regStreets.forEach(function(street,j,regStreets){
                    if(window.edit_street !== undefined && edit_street==street.street_id){
                        var sel = 'selected';
                    }
                    else{
                        var sel = '';
                    }
                    streetSelect += '<option value="'+street.street_id+'" '+sel+'>'+street.street_ru+'</option>';
                })
                regSelector = i;
            }
        });
        var cities = area_tree[regSelector].region.cities;
        var counter = 0;
        cities.forEach(function(city, i, cities) {
            if(window.edit_city !== undefined && edit_city==city.settings.city_id){
                var sel = 'selected';
            }
            else{
                var sel ='';
            }
            citySelect += '<option value="'+city.settings.city_id+'" '+sel+'>'+city.settings.city_ru+'</option>';
            counter++;
        });
        $(".city").html(citySelect);
        $(".street").html(streetSelect);
        if(window.subpage !== undefined && subpage == 'view' && window.edit_city !== undefined && edit_city>0){
            city_change(".city");
        }
    }
    var citySelector = 0;
    function city_change(elm){
        var cityId = $(elm).val();
        var streetSelect = '<option value="0">-- Улица --</option>';
        var areaSelect = '<option value="0">-- Район --</option>';
        var cities = area_tree[regSelector].region.cities;
        cities.forEach(function(city,i,cities){
            if(city.settings.city_id==cityId){
                var cityStreets = city.streets;
                cityStreets.forEach(function(street,j,cityStreets){
                    if(window.edit_street !== undefined && edit_street==street.street_id){
                        var sel = 'selected';
                    }
                    else{
                        var sel = '';
                    }
                    streetSelect += '<option value="'+street.street_id+'" '+sel+'>'+street.street_ru+'</option>';
                })
                citySelector = i;
            }
        });
        var areas = cities[citySelector].areas;
        var counter = 0;
        areas.forEach(function(area, i, areas){
            if(window.edit_area !== undefined && edit_area== area.settings.area_id){
                var sel = 'selected';
            }
            else{
                var sel = '';
            }
            areaSelect += '<option value="'+area.settings.area_id+'" '+sel+'>'+area.settings.area_ru+'</option>';
            counter++;
        });
        $(".area").html(areaSelect);
        $(".street").html(streetSelect);
        if(window.subpage !== undefined && subpage == 'view' && window.edit_area !== undefined && edit_area>0){
            area_change(".area");
        }
    }
    var areaSelector = 0;
    function area_change(elm){
        var areaId = $(elm).val();
        var streetSelect = '<option value="0">-- Улица --</option>';
        var microareaSelect = '<option value="0">-- Микрорайон --</option>';
        var areas = area_tree[regSelector].region.cities[citySelector].areas;
        areas.forEach(function(area, i, areas){
            if(area.settings.area_id==areaId){
                var areaStreets = area.streets;
                areaStreets.forEach(function(street, j, areaStreets){
                    if(window.edit_street !== undefined && edit_street==street.street_id){
                        var sel = 'selected';
                    }
                    else{
                        var sel = '';
                    }
                    streetSelect += '<option value="'+street.street_id+'" '+sel+'>'+street.street_ru+'</option>';
                })
                areaSelector = i;
            }
        });
        var microareas = areas[areaSelector].microareas;
        var counter = 0;
        microareas.forEach(function(microarea, i, microareas){
            if(window.edit_microarea !== undefined && edit_microarea== microarea.settings.area_id){
                var sel = 'selected';
            }
            else{
                var sel = '';
            }
            microareaSelect += '<option value="'+microarea.settings.area_id+'" '+sel+'>'+microarea.settings.area_ru+'</option>';
            counter++;
        })
        $(".microarea").html(microareaSelect);
        $(".street").html(streetSelect);
        if(window.subpage !== undefined && subpage == 'view' && window.edit_microarea !== undefined && edit_microarea>0){
            microarea_change(".microarea");
        }
    }
    var microareaSelector = 0;
    function microarea_change(elm){
        var microareaId = $(elm).val();
        var streetSelect = '<option value="0">-- Улица --</option>';
        var microareas = area_tree[regSelector].region.cities[citySelector].areas[areaSelector].microareas;
        microareas.forEach(function(microarea, i, microareas){
            if(microarea.settings.area_id==microareaId){
                var areaStreets = microarea.streets;
                areaStreets.forEach(function(street, j, areaStreets){
                    if(window.edit_street !== undefined && edit_street==street.street_id){
                        var sel = 'selected';
                    }
                    else{
                        var sel = '';
                    }
                    streetSelect += '<option value="'+street.street_id+'" '+sel+'>'+street.street_ru+'</option>';
                })
                microareaSelector = i;
            }
        });
        $(".street").html(streetSelect);
    }
    $("div#mydropzone").dropzone({ url: "http://broker.alex-r.am/upload_images.php" });
    if(window.subpage === undefined || subpage != 'view'){
        $("#mydropzone").html('<div class="dz-default dz-message"><span>Загрузить изображения</span><img src="http://user.alex-r.am/images/no-image.png"></div>');
    }
    else{
        function delete_image(elm,image,id){
            if(confirm('Хотите удалить изображение?')){
                jQuery.post('http://broker.alex-r.am/ajax.php', {
                    action 	: 'delete_image_in_edit',
                    image   : image,
                    id      : id
                },function(data){

                })
                $(elm).parent("div").remove();
            }
        }
    }
    function get_category_attributes(elm){
        var category = $(elm).val();
        jQuery.post('http://broker.alex-r.am/ajax.php', {
            action 	 : 'get_category_attributes',
            category : category
        },function(data){
            $(".category_attributes").html(data);
        })
    }
    $(".sale").click(function(){
        if($(this).is(":checked")){
            $(".sale_options").show();
        }
        else{
            $(".sale_options").hide();
        }
    })
    $(".rent").click(function(){
        if($(this).is(":checked")){
            $(".rent_options").show();
        }
        else{
            $(".rent_options").hide();
        }
    })
    function select_order_input(elm){
        $(elm).children(".dz_home_ch").children("input").select();
    }
    function find_real_estates(){
        $(".overlay").show();
        var json = {};
        json['type'] = $('input[name="type"]:checked').val();
        json['price'] = {};
        json['price']['from'] = $('input[name="price_from"]').val();
        json['price']['to'] = $('input[name="price_to"]').val();
        json['price']['currency'] = $('select[name="price_currency"]').val();
        json['location'] = {};
        json['location']['region'] = $(".region").val();
        json['location']['city'] = [];
        var h=0;
        $(".city_val").each(function(){
            if($(this).is(":checked")){
                json['location']['city'][h] = $(this).val();
                h++;
            }
        })
        var j=0;
        json['location']['area'] = [];
        $(".area_val").each(function(){
            if($(this).is(":checked")){
                json['location']['area'][j] = $(this).val();
                j++;
            }
        })
        json['location']['microarea'] = $(".microarea").val();
        json['location']['street'] = $(".street").val();
        json['category'] = $(".category").val();
        json['attributes'] = {};
        switch(json['category']){
            case '5':
                json['attributes']['area'] = {};
                json['attributes']['area']['from'] = parseFloat($('input[name="area_from"]').val());
                json['attributes']['area']['to'] = parseFloat($('input[name="area_to"]').val());
                json['attributes']['rooms'] = {};
                var i=0;
                $('input[name="rooms"]').each(function(){
                    if($(this).is(":checked")){
                        json['attributes']['rooms'][i] = parseInt($(this).val());
                        i++;
                    }
                });
                json['attributes']['floorer'] = {};
                json['attributes']['floorer']['from'] = parseFloat($('select[name="floorer_from"]').val());
                json['attributes']['floorer']['to'] = parseFloat($('select[name="floorer_to"]').val());
                json['attributes']['floor'] = {};
                json['attributes']['floor']['from'] = parseFloat($('select[name="floor_from"]').val());
                json['attributes']['floor']['to'] = parseFloat($('select[name="floor_to"]').val());
                json['attributes']['floor_name'] = {};
                var i=0;
                $('input[name="floor_name"]').each(function(){
                    if($(this).is(":checked")){
                        json['attributes']['floor_name'][i] = $(this).val();
                        i++;
                    }
                });
                json['attributes']['repair'] = {};
                var i=0;
                $('input[name="repair"]').each(function(){
                    if($(this).is(":checked")){
                        json['attributes']['repair'][i] = $(this).val();
                        i++;
                    }
                });
                json['attributes']['balcony'] = {};
                var i=0;
                $('input[name="balcony"]').each(function(){
                    if($(this).is(":checked")){
                        json['attributes']['balcony'][i] = $(this).val();
                        i++;
                    }
                });
                json['attributes']['garage'] = {};
                var i = 0;
                $('input[name="garage"]').each(function(){
                    if($(this).is(":checked")){
                        json['attributes']['garage'][i]=$(this).val();
                        i++;
                    }
                });
                json['attributes']['basement'] = $('input[name="basement"]:checked').val();
                json['attributes']['water'] = $('input[name="water"]:checked').val();
                json['attributes']['gas'] = $('input[name="gas"]:checked').val();
                json['attributes']['building_project'] = {};
                var i=0;
                $('input[name="building_project"]').each(function(){
                    if($(this).is(":checked")){
                        json['attributes']['building_project'][i] = $(this).val();
                        i++;
                    }
                });
                json['attributes']['building_position'] = {};
                var i=0;
                $('input[name="building_position"]').each(function(){
                    if($(this).is(":checked")){
                        json['attributes']['building_position'][i] = $(this).val();
                        i++;
                    }
                });
                json['attributes']['building_type'] = {};
                var i=0;
                $('input[name="building_type"]').each(function(){
                    if($(this).is(":checked")){
                        json['attributes']['building_type'][i] = $(this).val();
                        i++;
                    }
                });
                json['attributes']['housetop'] = {};
                var i=0;
                $('input[name="housetop"]').each(function(){
                    if($(this).is(":checked")){
                        json['attributes']['housetop'][i] = $(this).val();
                        i++;
                    }
                });
                break;
            case '6':{
                json['attributes']['total_area'] = {};
                json['attributes']['total_area']['from'] = parseFloat($('input[name="total_area_from"]').val());
                json['attributes']['total_area']['to'] = parseFloat($('input[name="total_area_to"]').val());
                json['attributes']['home_area'] = {};
                json['attributes']['home_area']['from'] = parseFloat($('input[name="home_area_from"]').val());
                json['attributes']['home_area']['to'] = parseFloat($('input[name="home_area_to"]').val());
                json['attributes']['rooms'] = {};
                var i=0;
                $('input[name="rooms"]').each(function(){
                    if($(this).is(":checked")){
                        json['attributes']['rooms'][i] = parseInt($(this).val());
                        i++;
                    }
                });
                json['attributes']['floorer'] = {};
                json['attributes']['floorer']['from'] = parseFloat($('select[name="floorer_from"]').val());
                json['attributes']['floorer']['to'] = parseFloat($('select[name="floorer_to"]').val());
                json['attributes']['repair'] = {};
                var i=0;
                $('input[name="repair"]').each(function(){
                    if($(this).is(":checked")){
                        json['attributes']['repair'][i] = $(this).val();
                        i++;
                    }
                });
                json['attributes']['balcony'] = {};
                var i=0;
                $('input[name="balcony"]').each(function(){
                    if($(this).is(":checked")){
                        json['attributes']['balcony'][i] = $(this).val();
                        i++;
                    }
                });
                json['attributes']['garage'] = {};
                var i = 0;
                $('input[name="garage"]').each(function(){
                    if($(this).is(":checked")){
                        json['attributes']['garage'][i]=$(this).val();
                        i++;
                    }
                });
                json['attributes']['basement'] = $('input[name="basement"]:checked').val();
                json['attributes']['water'] = $('input[name="water"]:checked').val();
                json['attributes']['gas'] = $('input[name="gas"]:checked').val();
                json['attributes']['heating_system'] = $('input[name="heating_system"]:checked').val();
                break;
            }
            case '7':
                json['attributes']['type'] = {};
                var i=0;
                $('input[name="typer"]').each(function(){
                    if($(this).is(":checked")){
                        json['attributes']['type'][i] = $(this).val();
                        i++;
                    }
                });
                json['attributes']['area'] = {};
                json['attributes']['area']['from'] = parseFloat($('input[name="area_from"]').val());
                json['attributes']['area']['to'] = parseFloat($('input[name="area_to"]').val());
                json['attributes']['rooms'] = {};
                var i=0;
                $('input[name="rooms"]').each(function(){
                    if($(this).is(":checked")){
                        json['attributes']['rooms'][i] = parseInt($(this).val());
                        i++;
                    }
                });
                json['attributes']['floor'] = {};
                json['attributes']['floor']['from'] = parseFloat($('select[name="floor_from"]').val());
                json['attributes']['floor']['to'] = parseFloat($('select[name="floor_to"]').val());
                json['attributes']['floor_name'] = {};
                var i=0;
                $('input[name="floor_name"]').each(function(){
                    if($(this).is(":checked")){
                        json['attributes']['floor_name'][i] = $(this).val();
                        i++;
                    }
                });
                json['attributes']['showcases'] = $('input[name="showcases"]:checked').val();
                json['attributes']['entrance'] = $('input[name="entrance"]:checked').val();
                json['attributes']['repair'] = $('input[name="repair"]:checked').val();
                json['attributes']['building_type'] = {};
                var i=0;
                $('input[name="building_type"]').each(function(){
                    if($(this).is(":checked")){
                        json['attributes']['building_type'][i] = $(this).val();
                        i++;
                    }
                });
                json['attributes']['building_position'] = {};
                var i=0;
                $('input[name="building_position"]').each(function(){
                    if($(this).is(":checked")){
                        json['attributes']['building_position'][i] = $(this).val();
                        i++;
                    }
                });
                break;
            case '8':{
                json['attributes']['area'] = {};
                json['attributes']['area']['from'] = parseFloat($('input[name="area_from"]').val());
                json['attributes']['area']['to'] = parseFloat($('input[name="area_to"]').val());
                json['attributes']['buildings'] = {};
                json['attributes']['buildings']['from'] = parseFloat($('input[name="buildings_from"]').val());
                json['attributes']['buildings']['to'] = parseFloat($('input[name="buildings_to"]').val());
                json['attributes']['water_drink'] = $('input[name="water_drink"]:checked').val();
                json['attributes']['water_irrigation'] = $('input[name="water_irrigation"]:checked').val();
                json['attributes']['gas'] = $('input[name="gas"]:checked').val();
                json['attributes']['electricity'] = $('input[name="electricity"]:checked').val();
                json['attributes']['high_voltage_pillars'] = $('input[name="high_voltage_pillars"]:checked').val();
                json['attributes']['sewage'] = $('input[name="sewage"]:checked').val();
                json['attributes']['fence'] = $('input[name="fence"]:checked').val();
                json['attributes']['fruit_trees'] = $('input[name="fruit_trees"]:checked').val();
                break;
            }
            case '9':
                json['attributes']['area'] = {};
                json['attributes']['area']['from'] = parseFloat($('input[name="area_from"]').val());
                json['attributes']['area']['to'] = parseFloat($('input[name="area_to"]').val());
                json['attributes']['building_type'] = $('input[name="building_type"]:checked').val();
                break;
            default:
                break;
        }
        jQuery.post('http://broker.alex-r.am/ajax.php', {
            action 	 : 'get_client_search_result',
            search_options : JSON.stringify(json),
            client : $(".client_id").val(),
            user : $(".user_id").val(),
            level : $(".level").val()
        },function(data){
           document.location.href="http://broker.alex-r.am/index.php?action=buyer&subaction=view&id="+$(".client_id").val();
        })
    }
}
function offer_to_user(post_id,client_id,elm){
    jQuery.post('http://broker.alex-r.am/ajax.php', {
        action 	 : 'ad_offer',
        client : client_id,
        post_id : post_id
    },function(data){
        $(".block_offers").html(data);
        $(elm).removeClass("btn-success");
        $(elm).addClass("btn-warning");
        $(elm).html('<i class="fa fa-check-square-o"></i> Выбран');
    })
}
function edit_offer_text(offer_id,elm){
    jQuery.post('http://broker.alex-r.am/ajax.php', {
        action 	 : 'edit_offer_text_form',
        offer_id : offer_id
    },function(data){
        $(elm).parent("td").html(data);
    })
}
function save_offer_text(offer_id,elm){
    var text = $(elm).prev(".offer_text").val();
    jQuery.post('http://broker.alex-r.am/ajax.php', {
        action 	 : 'save_offer_text',
        offer_id : offer_id,
        text : text
    },function(data){
        $(elm).parent("td").html(data);
    })
}
function filter_offers(id,elm){
    if($(elm).val()!=''){
        document.location.href="http://broker.alex-r.am/index.php?action=buyer&subaction=offers&id="+id+"&offer_status="+$(elm).val();
    }
    else{
        document.location.href="http://broker.alex-r.am/index.php?action=buyer&subaction=offers&id="+id;
    }
}
function edit_regions(elm){
    $(elm).parent().parent().parent().find(".my_show").hide();
    $(elm).parent().parent().parent().find(".my_hide").css("display","inline-block");
}
function save_region(elm){
    var id = $(elm).data("id");
    var tr = $(elm).parent().parent().parent();
    var hy = tr.find(".hy").val();
    var ru = tr.find(".ru").val();
    var en = tr.find(".en").val();
    jQuery.post('http://broker.alex-r.am/ajax.php', {
        action 	 : 'save_region',
        id : id,
        hy : hy,
        ru : ru,
        en : en
    },function(data){
        document.location.href="http://broker.alex-r.am/index.php?action=locations&subaction=region";
    })
}
function delete_region(elm){
    if(confirm('Вы уверены?')){
        var id = $(elm).data("id");
        var tr = $(elm).parent().parent().parent();
        jQuery.post('http://broker.alex-r.am/ajax.php', {
            action 	 : 'delete_region',
            id : id
        },function(data){
            document.location.href="http://broker.alex-r.am/index.php?action=locations&subaction=region";
        })
    }
}
function save_city(elm,reg){
    var id = $(elm).data("id");
    var tr = $(elm).parent().parent().parent();
    var hy = tr.find(".hy").val();
    var ru = tr.find(".ru").val();
    var en = tr.find(".en").val();
    jQuery.post('http://broker.alex-r.am/ajax.php', {
        action 	 : 'save_city',
        id : id,
        hy : hy,
        ru : ru,
        en : en
    },function(data){
        document.location.href="http://broker.alex-r.am/index.php?action=locations&subaction=city&region="+reg;
    })
}
function save_area(elm,reg){
    var id = $(elm).data("id");
    var tr = $(elm).parent().parent().parent();
    var hy = tr.find(".hy").val();
    var ru = tr.find(".ru").val();
    var en = tr.find(".en").val();
    jQuery.post('http://broker.alex-r.am/ajax.php', {
        action 	 : 'save_area',
        id : id,
        hy : hy,
        ru : ru,
        en : en
    },function(data){
        document.location.href="http://broker.alex-r.am/index.php?action=locations&subaction=area&region="+reg;
    })
}
function save_microarea(elm,reg){
    var id = $(elm).data("id");
    var tr = $(elm).parent().parent().parent();
    var hy = tr.find(".hy").val();
    var ru = tr.find(".ru").val();
    var en = tr.find(".en").val();
    jQuery.post('http://broker.alex-r.am/ajax.php', {
        action 	 : 'save_microarea',
        id : id,
        hy : hy,
        ru : ru,
        en : en
    },function(data){
        document.location.href="http://broker.alex-r.am/index.php?action=locations&subaction=microarea&region="+reg;
    })
}
function save_street(elm,reg,page){
    var id = $(elm).data("id");
    var tr = $(elm).parent().parent().parent();
    var hy = tr.find(".hy").val();
    var ru = tr.find(".ru").val();
    var en = tr.find(".en").val();
    jQuery.post('http://broker.alex-r.am/ajax.php', {
        action 	 : 'save_street',
        id : id,
        hy : hy,
        ru : ru,
        en : en
    },function(data){
        document.location.href=page;
    })
}
function delete_street(elm,reg){
    if(confirm('Вы уверены?')){
        var id = $(elm).data("id");
        var tr = $(elm).parent().parent().parent();
        jQuery.post('http://broker.alex-r.am/ajax.php', {
            action 	 : 'delete_street',
            id : id
        },function(data){
            document.location.href="http://broker.alex-r.am/index.php?action=locations&subaction=street&region="+reg;
        })
    }
}
function add_street(elm,reg){
    var hy = $("input[name='street_hy']").val();
    var ru = $("input[name='street_ru']").val();
    var en = $("input[name='street_en']").val();
    var region = $("select[name='region']").val();
    var city = $("select[name='city']").val();
    var area = $("select[name='area']").val();
    var microarea = $("select[name='microarea']").val();
    jQuery.post('http://broker.alex-r.am/ajax.php', {
        action 	 : 'add_street',
        hy : hy,
        ru : ru,
        en : en,
        region : region,
        city : city,
        area : area,
        microarea : microarea
    },function(data){
        document.location.href="http://broker.alex-r.am/index.php?action=locations&subaction=street&region="+reg;
    })
}
function print_list_offers(elm){
    Popup($(elm).html());
}
function Popup(data){
    var mywindow = window.open('', 'Print');
    mywindow.document.write('<html><head><title>Print</title>');
    /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
    mywindow.document.write('</head><body >');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    mywindow.print();
    mywindow.close();

    return true;
}
function search_client_by_name(elm,user_level,post_id){
    $(".client_phone_search").val("");
    $(".client_id_search").val("");
    var name = $(elm).val();
    jQuery.post('http://broker.alex-r.am/ajax.php', {
        action 	 : 'search_client_by_name',
        name : name,
        user_level : user_level,
        post_id : post_id
    },function(data){
        $(".search_result_clients").html(data);
    })
}
function search_client_by_phone(elm,user_level,post_id){
    $(".client_name_search").val("");
    $(".client_id_search").val("");
    var name = $(elm).val();
    jQuery.post('http://broker.alex-r.am/ajax.php', {
        action 	 : 'search_client_by_phone',
        phone : name,
        user_level : user_level,
        post_id : post_id
    },function(data){
        $(".search_result_clients").html(data);
    })
}
function search_client_by_id(elm,user_level,post_id){
    $(".client_name_search").val("");
    $(".client_phone_search").val("");
    var id = $(elm).val();
    jQuery.post('http://broker.alex-r.am/ajax.php', {
        action 	 : 'search_client_by_id',
        id : id,
        user_level : user_level,
        post_id : post_id
    },function(data){
        $(".search_result_clients").html(data);
    })
}
function add_phone1_row(elm){
    var html = '<input type="text" name="phone1[]" class="form-control" style="margin-top: 10px">';
    $(html).insertBefore(elm);
}
function add_phone2_row(elm){
    var html = '<input type="text" name="phone2[]" class="form-control" style="margin-top: 10px">';
    $(html).insertBefore(elm);
}
function delete_area(elm){
    if(confirm('Вы уверены?')) {
        var id = $(elm).data("id");
        jQuery.post('http://broker.alex-r.am/ajax.php', {
            action: 'delete_area',
            id: id
        }, function (data) {
            document.location.href = "http://broker.alex-r.am/index.php?action=locations&subaction=area&region=8"
        })
    }
}
function get_area_microarea_data(elm){
    var area = $(elm).val();
    jQuery.post('http://broker.alex-r.am/ajax.php', {
        action: 'get_area_microareas',
        area : area
    }, function (data) {
        $(".area_microarea").html(data);
    })
}
function delete_microarea(elm){
    if(confirm('Вы уверены?')) {
        var id = $(elm).data("id");
        jQuery.post('http://broker.alex-r.am/ajax.php', {
            action: 'delete_microarea',
            id: id
        }, function (data) {
            document.location.href = "http://broker.alex-r.am/index.php?action=locations&subaction=microarea&region=8"
        })
    }
}
function save_client_settings(id){
    var name = $(".client_name").val();
    var phone1 = $(".client_phone1").val();
    var phone2 = $(".client_phone2").val();
    var mail = $(".client_email").val();
    var txt = $(".client_text").val();
    jQuery.post('http://broker.alex-r.am/ajax.php', {
        action: 'client_settings_save',
        id: id,
        name : name,
        phone1 : phone1,
        phone2 : phone2,
        mail : mail,
        txt : txt
    }, function (data) {
        document.location.href = "http://broker.alex-r.am/index.php?action=buyer&subaction=view&id="+id;
    })

}
function change_position_in_list(elm){
    if($(elm).is(":checked")) {
        $(elm).attr("checked","checked");
    }
    else{
        $(elm).removeAttr("checked");
    }
    var htm = '';
    for(var i=1;i<21;i++){
        if($(".a_hide").find("input[value='"+i+"']").is(":checked")){
            htm += '<div style="display: inline-block;width: 80%;padding: 0 10%;">'+ $(".a_hide").find("input[value='"+i+"']").parent().html() +'</div>';
        }
    }
    for(var i=1;i<21;i++){
        if(!($(".a_hide").find("input[value='"+i+"']").is(":checked"))){
            htm += '<div style="display: inline-block;width: 80%;padding: 0 10%;">'+ $(".a_hide").find("input[value='"+i+"']").parent().html() +'</div>';
        }
    }
    $(".a_hide").html(htm);
}
function do_image_hide_action(elm){
    if($(elm).is(":checked")){
        $(elm).next("input").val('1');
    }
    else{
        $(elm).next("input").val('0');
    }
}
function find_category_filter_settings(elm){
    var cat = $(elm).val();
    jQuery.post('http://broker.alex-r.am/ajax.php', {
        action: 'get_all_filter_cat_settings',
        cat: cat
    }, function (data) {
        $(".for_all_filter_settings").html(data);
    })
}
function get_f_city(elm){
    $(".f_street_res").html("");
    $(".f_street_input").val("");
    var city = $(elm).val();
    jQuery.post('http://broker.alex-r.am/ajax.php', {
        action: 'get_f_city',
        region: city
    }, function (data) {
        $(".f_city").html(data);
    })
}
function get_f_area(elm){
    $(".f_street_res").html("");
    $(".f_street_input").val("");
    var city = $(elm).val();
    jQuery.post('http://broker.alex-r.am/ajax.php', {
        action: 'get_f_area',
        city: city
    }, function (data) {
        $(".f_area").html(data);
    })
}
function get_f_microarea(elm){
    $(".f_street_res").html("");
    $(".f_street_input").val("");
    var area = $(elm).val();
    jQuery.post('http://broker.alex-r.am/ajax.php', {
        action: 'get_f_microarea',
        area: area
    }, function (data) {
        $(".f_microarea").html(data);
    })
}
function get_f_streets(elm){
    var latter = $(elm).val();
	if(latter!=''){
    var region = $(".f_region").val();
    var city = $(".f_city").val();
    var area = $(".f_area").val();
    var microarea = $(".f_microarea").val();
    jQuery.post('http://broker.alex-r.am/ajax.php', {
        action: 'get_f_streets',
        latter: latter,
        region: region,
        city : city,
        area : area,
        microarea: microarea
    }, function (data) {
        $(".f_street_res").html(data);
    })
	}
	else{
		$(".f_street").val('');
		$(".f_street_res").html('');
	}
}
function put_street(id,elm){
    $(".f_street_input").val($(elm).text());
    $(".f_street").val(id);
    $(".f_street_res").html("");
}
if(window.count_esim !== undefined && count_esim!=''){
    $(".count_posts_all_in").html(count_esim);
}
function get_client_search_attributes(elm){
    var category = $(elm).val();
    jQuery.post('http://broker.alex-r.am/ajax.php', {
        action 	 : 'get_client_search_attributes',
        category : category
    },function(data){
        $(".search_attributes").html(data);
    })
}
function delete_buyer_ajax(elm,id){
	if(confirm('Вы уверены?')){
		jQuery.post('http://broker.alex-r.am/ajax.php', {
			action 	 : 'delete_buyer_ajax',
			id : id
		},function(data){
			$(elm).parent().parent().parent().fadeOut(300, 
				function() { 
					$(this).remove(); 
			});
		})
	}
}
function open_modal(elm,id){
	$(".off_id").val(id);
	$("#soldModal").show();
}
function close_dialog(){
	$("#soldModal").removeAttr("style");
}
function ajax_remove_fromoffer(elm,id){
	if(confirm('Вы уверены?')){
		jQuery.post('http://broker.alex-r.am/ajax.php', {
			action 	 : 'ajax_remove_fromoffer',
			id : id
		},function(data){
			$(elm).parent().parent().parent().fadeOut(300, 
				function() { 
					$(this).remove(); 
			});
		})
	}
}
function open_br_post_comment(elm,id){
	$(".example-modal").removeClass("hide");
	jQuery.post('http://broker.alex-r.am/ajax.php', {
		action 	 : 'open_br_post_comment',
		id : id
	},function(data){
		$(".modal-body").html(data);
		var element = document.getElementById("yourDivID");
        element.scrollTop = element.scrollHeight;
	})
}
function send_post_comment(elm){
	var message = $(elm).parent().parent().find(".message").val();
	var id = $(elm).parent().parent().find(".pid").val();
	if(message==''){
		$(elm).parent().parent().find(".message").css("border-color","red");
	}
	else{
		$(elm).parent().parent().find(".message").removeAttr("style");
		jQuery.post('http://broker.alex-r.am/ajax.php', {
			action 	 : 'send_post_comment',
			id : id,
			message : message
		},function(data){
			open_br_post_comment(elm,id)
		})
	}
}
function send_post_comment1(elm,e){
	if (e.which == 13) {
    var message = $(elm).val();
	var id = $(elm).parent().find(".pid").val();
	if(message==''){
		$(elm).parent().parent().find(".message").css("border-color","red");
	}
	else{
		$(elm).parent().parent().find(".message").removeAttr("style");
		jQuery.post('http://broker.alex-r.am/ajax.php', {
			action 	 : 'send_post_comment',
			id : id,
			message : message
		},function(data){
			open_br_post_comment(elm,id)
		})
	}
    return false;    //<---- Add this line
  }
}