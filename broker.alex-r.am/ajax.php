<?php
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
if(!IS_AJAX){
    {header('HTTP/1.0 403 Forbidden');exit;}
}
if($_POST['action'] == ''){
    echo "bad request!";
}
else{
    require_once( "config.php" );
    $func_name = $_POST['action'];
    $func_name();
}
function get_location_tree(){
    $regions = mwdb_select("SELECT region_id,region_ru FROM region WHERE {var} ORDER BY region_ru",array(1));
    $array_response = array();
    $i=0;
    foreach($regions as $region){
        $array_response[$i] = array();
        $array_response[$i]['region'] = array();
        $array_response[$i]['region']['settings'] = $region;
        $array_response[$i]['region']['streets'] = array();
        $region_streets = mwdb_select("SELECT street_id,street_ru FROM streets WHERE region={var} ORDER BY street_ru",array($region->region_id));
        foreach($region_streets as $region_street){
            array_push($array_response[$i]['region']['streets'],$region_street);
        }
        $cities = mwdb_select("SELECT city_id,city_ru FROM city WHERE region={var} ORDER BY city_ru",array($region->region_id));
        $j = 0;
        $array_response[$i]['region']['cities'] = array();
        foreach($cities as $city){
            $array_response[$i]['region']['cities'][$j] = array();
            $array_response[$i]['region']['cities'][$j]['settings'] = $city;
            $array_response[$i]['region']['cities'][$j]['streets'] = array();
            $city_streets = mwdb_select("SELECT street_id,street_ru FROM streets WHERE city={var} ORDER BY street_ru",array($city->city_id));
            foreach($city_streets as $city_street){
                array_push($array_response[$i]['region']['cities'][$j]['streets'],$city_street);
            }
            $array_response[$i]['region']['cities'][$j]['areas'] = array();
            $k = 0;
            $areas = mwdb_select("SELECT area_id,area_ru FROM area WHERE city={var} ORDER BY area_ru",array($city->city_id));
            foreach($areas as $area){
                $array_response[$i]['region']['cities'][$j]['areas'][$k] = array();
                $array_response[$i]['region']['cities'][$j]['areas'][$k]['streets'] = array();
                $area_streets = mwdb_select("SELECT street_id,street_ru FROM streets WHERE area={var} ORDER BY street_ru",array($area->area_id));
                foreach($area_streets as $area_street){
                    array_push($array_response[$i]['region']['cities'][$j]['areas'][$k]['streets'],$area_street);
                }
                $array_response[$i]['region']['cities'][$j]['areas'][$k]['settings'] = $area;
                $array_response[$i]['region']['cities'][$j]['areas'][$k]['microareas'] = array();
                $microares = mwdb_select("SELECT area_id,area_ru FROM microarea WHERE area={var} ORDER BY area_ru",array($area->area_id));
                $m = 0;
                foreach($microares as $microarea){
                    $array_response[$i]['region']['cities'][$j]['areas'][$k]['microareas'][$m] = array();
                    $array_response[$i]['region']['cities'][$j]['areas'][$k]['microareas'][$m]['settings'] = $microarea;
                    $array_response[$i]['region']['cities'][$j]['areas'][$k]['microareas'][$m]['streets'] = array();
                    $microarea_streets = mwdb_select("SELECT street_id,street_ru FROM streets WHERE microarea={var} ORDER BY street_ru",array($microarea->area_id));
                    foreach($microarea_streets as $microarea_street){
                        array_push($array_response[$i]['region']['cities'][$j]['areas'][$k]['microareas'][$m]['streets'],$microarea_street);
                    }
                    $m++;
                }
                $k++;
            }
            $j++;
        }
        $i++;
    }
    echo json_encode($array_response);
}
function get_category_attributes(){
    $cat_id = $_POST['category'];
    switch($cat_id){
        case '5':{
            ?>
            <div class="col-md-6" style="background: #F9FAFC;padding: 23px 25px;">
                <div class="col-md-12" style="height: 190px">
                    <div class="col-md-3" >
                        <div class="col-md-8">Площадь</div>
                        <div class="col-md-8">
                            <input type="text" name="ap_area" class="apartment_attr ap_area form-control">
                        </div>
                        <div class="col-md-4">м2</div>
                    </div>
                    <div class="col-md-3">
                        <div class="col-md-8">Комнаты</div>
                        <div class="col-md-11">
                            <select name="ap_rooms" class="apartment_attr ap_rooms form-control">
                                <?php
                                for($i=1;$i<10;$i++){
                                    ?>
                                    <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                    <?php
                                }
                                ?>
                                <option value="10">10+</option>
                            </select>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                    <div class="col-md-3" >
                        <div class="col-md-8">Этажность</div>
                        <div class="col-md-11">
                            <select name="ap_floorer" class="apartment_attr ap_floorer form-control">
                                <?php
                                for($i=1;$i<=20;$i++){
                                    ?>
                                    <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                    <div class="col-md-3" >
                        <div class="col-md-8">Этаж</div>
                        <div class="col-md-11 a_hide" style="height: 160px;overflow: hidden;position: absolute;z-index: 1001;line-height: 10px;margin-top: 24px;background: #fff url(http://user.alex-r.am/images/select.png) no-repeat 96% 10px;border: 1px solid #D2D6DE;">
                            <?php
                            for($i=1;$i<=20;$i++){
                                ?>
                                <div style="display: inline-block;width: 80%;padding: 0 10%;">
                                    <input type="checkbox" onclick="change_position_in_list(this)" name="ap_floor" class="apartment_attr ap_floor" value="<?php echo $i ?>" style="float: left"><span style="float: left;margin-top: 6px;margin-left: 6px;"><?php echo $i ?></span>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-5" >
                        <div class="col-md-8">Проект</div>
                        <div class="col-md-11">
                            <select name="ap_building_project" class="apartment_attr ap_building_project form-control">
                                <option value="building_project1">Сталинский проект</option>
                                <option value="building_project2">Хрущевский</option>
                                <option value="building_project3">Пост хрущевский</option>
                                <option value="building_project4">Каменный спецпроект</option>
                                <!--<option value="building_project5">Каркасно панельный</option>-->
                                <option value="building_project6">Бадалян</option>
                                <option value="building_project7">Московский ДСК</option>
                                <option value="building_project8">Ереванский ДСК</option>
                                <option value="building_project9">Грузинский</option>
                                <option value="building_project10">Ленточный</option>
                                <option value="building_project11">Монолитый сейсмоустойчивый</option>
                                <option value="building_project12">Трехъярусный</option>
                                <option value="building_project13">Четырехъярусный</option>
                                <option value="building_project14">элитарная монолитная новостройка</option>
                                <option value="building_project16">чешский проект</option>
                                <option value="building_project15">Спецпроект</option>
                                <option value="building_project17">Югославский</option>
                                <option value="building_project18">Финский</option>
                                <option value="">-</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-5" >
                        <div class="col-md-8">Тип здания</div>
                        <div class="col-md-11">
                            <select name="ap_building_type" class="apartment_attr ap_building_type form-control">
                                <option value="building_type1">монолитный</option>
                                <option value="building_type2">каменный</option>
                                <option value="building_type3">крупнопанельный</option>
                                <option value="building_type4">каркаснопанельный</option>
                            </select>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-5" >
                        <div class="col-md-12">Расположение здания</div>
                        <div class="col-md-11">
                            <select name="ap_building_position" class="apartment_attr ap_building_position form-control">
                                <option value="building_position1">Первая  линия</option>
                                <option value="building_position2">Вторая  линия</option>
                                <option value="">-</option>
                            </select>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                    <div class="col-md-5" >
                        <div class="col-md-12">Тип покрытия</div>
                        <div class="col-md-11">
                            <select name="ap_housetop" class="apartment_attr ap_housetop form-control">
                                <option value="housetop1">Деревянный</option>
                                <option value="housetop2">Панельный</option>
                                <option value="housetop3">Монолитный бетон</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="height: 55px;"></div>
            </div>
            <div class="col-md-6" style="background: #F9FAFC;padding: 23px 25px;">
                <div class="col-md-12" style="height: 190px;">
                    <div class="col-md-3">
                        <div class="col-md-12">Название этажа</div>
                        <div class="col-md-11" style="height: 130px;overflow: hidden;position: absolute;z-index: 1003;line-height: 10px;margin-top: 24px;background: #fff url(http://user.alex-r.am/images/select.png) no-repeat 96% 10px;border: 1px solid #D2D6DE;">
                            <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                <input type="checkbox" name="ap_floor_name" class="apartment_attr ap_floor_name" value="basement" style="float: left"><span style="float: left;margin-top: 6px;">подвал</span>
                            </div>
                            <div style="display: inline-block;width: 100%;padding: 0 1%;">
                                <input type="checkbox" name="ap_floor_name" class="apartment_attr ap_floor_name" value="semibasement" style="float: left"><span style="float: left;margin-top: 6px;">полуподвал</span>
                            </div>
                            <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                <input type="checkbox" name="ap_floor_name" class="apartment_attr ap_floor_name" value="first" style="float: left"><span style="float: left;margin-top: 6px;">первый</span>
                            </div>
                            <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                <input type="checkbox" name="ap_floor_name" class="apartment_attr ap_floor_name" value="high_first" style="float: left"><span style="float: left;margin-top: 6px;">белэтаж</span>
                            </div>
                            <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                <input type="checkbox" name="ap_floor_name" class="apartment_attr ap_floor_name" value="average" style="float: left"><span style="float: left;margin-top: 6px;">средний</span>
                            </div>
                            <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                <input type="checkbox" name="ap_floor_name" class="apartment_attr ap_floor_name" value="high_" style="float: left"><span style="float: left;margin-top: 6px;">высокий</span>
                            </div>
                            <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                <input type="checkbox" name="ap_floor_name" class="apartment_attr ap_floor_name" value="last" style="float: left"><span style="float: left;margin-top: 6px;">последний</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9" >
                        <div class="col-md-8">Отделка</div>
                        <div class="col-md-9 a_hide4" style="height: 165px;overflow: hidden;position: absolute;z-index: 1003;line-height: 10px;margin-top: 24px;background: #fff url(http://user.alex-r.am/images/select.png) no-repeat 96% 10px;border: 1px solid #D2D6DE;">
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="ap_repair" class="apartment_attr ap_repair" value="repair1"></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Нулевое состояние</div>
                                <div class="col-md-3" style="margin-top: 1px"></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="ap_repair" class="apartment_attr ap_repair" value="repair2"></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Гос. состояние</div>
                                <div class="col-md-3" style="margin-top: 1px"></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="ap_repair" class="apartment_attr ap_repair" value="repair3"></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Капитальный ремонт</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="repair3_dop" class="apartment_attr ap_repair_dop form-control" placeholder="год" style="height: 20px"></div>

                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="ap_repair" class="apartment_attr ap_repair" value="repair4"></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Стильный ремонт</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="repair4_dop" class="apartment_attr ap_repair_dop form-control" placeholder="год" style="height: 20px"></div>

                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="ap_repair" class="apartment_attr ap_repair" value="repair5"></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Среднее состояние</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="repair5_dop" class="apartment_attr ap_repair_dop form-control" placeholder="год" style="height: 20px"></div>

                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="ap_repair" class="apartment_attr ap_repair" value="repair6"></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Косметический ремонт</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="repair6_dop" class="apartment_attr ap_repair_dop form-control" placeholder="год" style="height: 20px"></div>

                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="ap_repair" class="apartment_attr ap_repair" value="repair7"></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">В процессе ремонта</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="repair7_dop" class="apartment_attr ap_repair_dop form-control" placeholder="год" style="height: 20px"></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="ap_repair" class="apartment_attr ap_repair" value="repair8"></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Частичный ремонт</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="repair8_dop" class="apartment_attr ap_repair_dop form-control" placeholder="год" style="height: 20px"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="height: 55px;">
                    <div class="col-md-3" >
                        <div class="col-md-8">Вода</div>
                        <div class="col-md-11">
                            <select name="ap_water" class="apartment_attr ap_water form-control">
                                <option value="ap_water1">Постоянная</option>
                                <option value="ap_water2">Временная</option>
                            </select>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                    <div class="col-md-3" >
                        <div class="col-md-8">Газ</div>
                        <div class="col-md-11">
                            <select name="ap_gas" class="apartment_attr ap_gas form-control">
                                <option value="ap_gas1">В здании</option>
                                <option value="ap_gas2">В квартире</option>
                            </select>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                <div class="col-md-12" style="height: 115px;">
                    <div class="col-md-6" >
                        <div class="col-md-12">Балкон</div>
                        <div class="col-md-11 a_hide2" style="height: 87px;overflow: hidden;position: absolute;z-index: 1001;line-height: 10px;margin-top: 24px;background: #fff url(http://user.alex-r.am/images/select.png) no-repeat 96% 10px;border: 1px solid #D2D6DE;">
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="ap_balcony[]" class="apartment_attr ap_balcony" value="balcony1"></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Висячий</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="balcony1_dop" class="apartment_attr ap_balcony_dop form-control" placeholder="шт." style="height: 20px"></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="ap_balcony[]" class="apartment_attr ap_balcony" value="balcony2"></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Француский</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="balcony2_dop" class="apartment_attr ap_balcony_dop form-control" placeholder="шт." style="height: 20px"></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="ap_balcony[]" class="apartment_attr ap_balcony" value="balcony3"></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Открытая Лоджия</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="balcony3_dop" class="apartment_attr ap_balcony_dop form-control" placeholder="шт." style="height: 20px"></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="ap_balcony[]" class="apartment_attr ap_balcony" value="balcony4"></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Закрытая Лоджия</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="balcony4_dop" class="apartment_attr ap_balcony_dop form-control" placeholder="шт." style="height: 20px"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3" >
                        <div class="col-md-12">Гараж</div>
                        <div class="col-md-11 a_hide3" style="height: 40px;overflow: hidden;position: absolute;z-index: 1001;line-height: 10px;margin-top: 24px;background: #fff url(http://user.alex-r.am/images/select.png) no-repeat 96% 10px;border: 1px solid #D2D6DE;">
                            <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                <input type="checkbox" name="ap_garage" class="apartment_attr ap_garage" value="ap_garage1" style="float: left"><span style="float: left;margin-top: 6px;">Каменный</span>
                            </div>
                            <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                <input type="checkbox" name="ap_garage" class="apartment_attr ap_garage" value="ap_garage2" style="float: left"><span style="float: left;margin-top: 6px;">Железный</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3" >
                        <div class="col-md-8">Подвал</div>
                        <div class="col-md-9">
                            <div class="col-md-12">
                                <div class="col-md-3"><input type="checkbox" name="ap_basement" value="1" class="apartment_attr ap_basement"></div>
                                <div class="col-md-9">Есть</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            break;
        }
        case '6':{
            ?>
            <div class="col-md-4" style="background: #F9FAFC;padding: 23px 25px;">
                <div class="col-md-12" style="height: 73px;">
                    <div class="col-md-5">
                        <div class="col-md-8">Общая площадь</div>
                        <div class="col-md-8">
                            <input type="text" name="hm_total_area" class="home_attr hm_total_area form-control">
                        </div>
                        <div class="col-md-4">m2</div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-8">Площадь дома</div>
                        <div class="col-md-8">
                            <input type="text" name="hm_home_area" class="home_attr hm_home_area form-control">
                        </div>
                        <div class="col-md-4">m2</div>
                    </div>
                    <div class="col-md-3">
                        <div class="col-md-12">Количество комнат</div>
                        <div class="col-md-11">
                            <select name="hm_rooms" class="home_attr hm_rooms form-control">
                                <?php
                                for($i=1;$i<10;$i++){
                                    ?>
                                    <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                    <?php
                                }
                                ?>
                                <option value="10">10+</option>
                            </select>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                <div class="col-md-12" style="height: 125px">
                    <div class="col-md-6">
                        <div class="col-md-8">Этажность</div>
                        <div class="col-md-11">
                            <select name="hm_floorer" class="home_attr hm_floorer form-control">
                                <?php
                                for($i=1;$i<6;$i++){
                                    ?>
                                    <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12">Продаваемый этаж</div>
                        <div class="col-md-11 h_hide" style="height: 95px;overflow: hidden;position: absolute;z-index: 1001;line-height: 10px;margin-top: 24px;background: #fff url(http://user.alex-r.am/images/select.png) no-repeat 96% 10px;border: 1px solid #D2D6DE;">
                            <?php
                            for($i=1;$i<6;$i++){
                                ?>
                                <div style="display: inline-block;width: 80%;padding: 0 10%;">
                                    <input type="checkbox" onclick="change_position_in_list(this)" name="hm_floor" class="home_attr hm_floor" value="<?php echo $i ?>" style="float: left"><span style="float: left;margin-top: 6px;margin-left: 6px;"><?php echo $i ?></span>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="height: 104px;">
                    <div class="col-md-6">
                        <div class="col-md-8">Тип здания</div>
                        <div class="col-md-11">
                            <select name="hm_building_type" class="home_attr hm_building_type form-control">
                                <option value="building_type2">каменный</option>
                                <option value="building_type1">монолитный</option>
                                <option value="building_type3">крупнопанельный</option>
                                <option value="building_type4">каркаснопанельный</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12">Положение здания</div>
                        <div class="col-md-11">
                            <select name="hm_building_position" class="home_attr hm_building_position form-control">
                                <option value="building_position1">Первая линия</option>
                                <option value="building_position2">Вторая линия</option>
                                <!--<option value="building_position3">Третья линия</option>
                                <option value="building_position4">Четвертая линия</option>-->
                                <option value="">-</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8" style="background: #F9FAFC;padding: 23px 25px;">
                <div class="col-md-12" style="height: 115px;">
                    <div class="col-md-4">
                        <div class="col-md-8">Балкон</div>
                        <div class="col-md-11 h_hide1" style="height: 87px;overflow: hidden;position: absolute;z-index: 1001;line-height: 10px;margin-top: 24px;background: #fff url(http://user.alex-r.am/images/select.png) no-repeat 96% 10px;border: 1px solid #D2D6DE;">
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_balcony" class="home_attr hm_balcony" value="balcony1"></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Висячий</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="balcony1_dop" class="home_attr hm_balcony_dop form-control" placeholder="шт." style="height: 20px"></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_balcony" class="home_attr hm_balcony" value="balcony2"></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Француский</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="balcony2_dop" class="home_attr hm_balcony_dop form-control" placeholder="шт." style="height: 20px"></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_balcony" class="home_attr hm_balcony" value="balcony3"></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Открытая Лоджия</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="balcony3_dop" class="home_attr hm_balcony_dop form-control" placeholder="шт." style="height: 20px"></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_balcony" class="home_attr hm_balcony" value="balcony4"></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Закрытая Лоджия</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="balcony4_dop" class="home_attr hm_balcony_dop form-control" placeholder="шт." style="height: 20px"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-8">Гараж</div>
                        <div class="col-md-11" style="height: 67px;overflow: hidden;position: absolute;z-index: 1001;line-height: 10px;margin-top: 24px;background: #fff url(http://user.alex-r.am/images/select.png) no-repeat 96% 10px;border: 1px solid #D2D6DE;">
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_garage" class="home_attr hm_garage" value="garage1"></div>
                                <div class="col-md-6" style="margin-top: 6px;margin-left: 5px;">Пристройка</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="garage1_dop2" class="home_attr hm_garage_dop1 form-control" style="height: 20px"></div>
                                <div class="col-md-1 no-padding" style="margin-top: 6px" >m2</div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_garage" class="home_attr hm_garage" value="garage2"></div>
                                <div class="col-md-6" style="margin-top: 6px;margin-left: 5px;">Под зданием</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="garage2_dop2" class="home_attr hm_garage_dop1 form-control" style="height: 20px"></div>
                                <div class="col-md-1 no-padding" style="margin-top: 6px">m2</div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_garage" class="home_attr hm_garage" value="garage3"></div>
                                <div class="col-md-6" style="margin-top: 6px;margin-left: 5px;">Отдельный</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="garage3_dop2" class="home_attr hm_garage_dop1 form-control" style="height: 20px"></div>
                                <div class="col-md-1 no-padding" style="margin-top: 6px">m2</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-8">Подвал</div>
                        <div class="col-md-11">
                            <div class="col-md-12">
                                <div class="col-md-1" style="margin-top: 1px;"><input type="checkbox" name="hm_basement" class="home_attr hm_basement" value="basement1"></div>
                                <div class="col-md-3">Есть</div>
                                <div class="col-md-4">Площадь</div>
                                <div class="col-md-3"><input type="text" name="basement1_dop" class="home_attr hm_basement_dop form-control" style="height: 20px"></div>
                                <div class="col-md-1">м2</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="height: 190px;">
                    <div class="col-md-6">
                        <div class="col-md-8">Отделка</div>
                        <div class="col-md-11" style="height: 165px;overflow: hidden;position: absolute;z-index: 1003;line-height: 10px;margin-top: 24px;background: #fff url(http://user.alex-r.am/images/select.png) no-repeat 96% 10px;border: 1px solid #D2D6DE;">
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_repair" class="home_attr hm_repair" value="repair1"></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Нулевое состояние</div>
                                <div class="col-md-3" style="margin-top: 1px"></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_repair" class="home_attr hm_repair" value="repair2"></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Гос. состояние</div>
                                <div class="col-md-3" style="margin-top: 1px"></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_repair" class="home_attr hm_repair" value="repair3"></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Капитальный ремонт</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="repair3_dop" class="home_attr hm_repairr_dop form-control" placeholder="год" style="height: 20px"></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_repair" class="home_attr hm_repair" value="repair4"></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Стильный ремонт</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="repair4_dop" class="home_attr hm_repair_dop form-control" placeholder="год" style="height: 20px"></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_repair" class="home_attr hm_repair" value="repair5"></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Среднее состояние</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="repair5_dop" class="home_attr hm_repair_dop form-control" placeholder="год" style="height: 20px"></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_repair" class="home_attr hm_repair" value="repair6"></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Косметический ремонт</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="repair6_dop" class="home_attr hm_repair_dop form-control" placeholder="год" style="height: 20px"></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_repair" class="home_attr hm_repair" value="repair7"></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">В процессе ремонта</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="repair7_dop" class="home_attr hm_repair_dop form-control" placeholder="год" style="height: 20px"></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_repair" class="home_attr hm_repair" value="repair8"></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Частичный ремонт</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="repair8_dop" class="home_attr hm_repair_dop form-control" placeholder="год" style="height: 20px"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="col-md-12">Вода:</div>
                        <div class="col-md-11">
                            <select name="hm_water" class="home_attr hm_water form-control">
                                <option value="hm_water1">Постоянный</option>
                                <option value="hm_water2">Временный</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="col-md-12">Газ</div>
                        <div class="col-md-11">
                            <select name="hm_gas" class="home_attr hm_gas form-control">
                                <option value="hm_gas1">Есть</option>
                                <option value="hm_gas2">Возможность</option>
                                <option value="hm_gas3">Нет</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="col-md-12">Отопление:</div>
                        <div class="col-md-11">
                            <select name="hm_heating_system" class="home_attr hm_heating_system form-control">
                                <option value="hm_heating_system1">Есть</option>
                                <option value="hm_heating_system2">Нет</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            break;
        }
        case '7':{
            ?>
            <div class="col-md-6" style="background: #F9FAFC;padding: 23px 25px;">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="col-md-12">Тип</div>
                        <div class="col-md-11">
                            <select name="cm_type" class="commercial_attr cm_type form-control">
                                <option value="cm_type2">гостиница</option>
                                <option value="cm_type1">оффис</option>
                                <option value="cm_type3">развлекательный</option>
                                <option value="cm_type4">магазин</option>
                                <option value="cm_type5">парикмахерская</option>
                                <option value="cm_type6">автотехобслуживание</option>
                                <option value="cm_type7">склад</option>
                                <option value="cm_type8">сельскохозяйственный</option>
                                <option value="cm_type9">бизнес центр</option>
                                <option value="cm_type10">производственный</option>
                                <option value="cm_type11">учебный</option>
                                <option value="cm_type12">медицинские заведения</option>
                                <option value="cm_type13">универсальный</option>
                                <option value="cm_type14">другое</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12">Тип здания</div>
                        <div class="col-md-11">
                            <select name="ap_building_type" class="apartment_attr ap_building_type form-control">
                                <option value="building_type1">монолитный</option>
                                <option value="building_type2">каменный</option>
                                <option value="building_type3">крупнопанельный</option>
                                <option value="building_type4">каркаснопанельный</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12">Расположение здания</div>
                        <div class="col-md-11">
                            <select name="ap_building_position" class="apartment_attr ap_building_position form-control">
                                <option value="building_position1">Первая линия</option>
                                <option value="building_position2">Вторая линия</option>
                                <option value="building_position3">Третья линия</option>
                                <option value="building_position4">Четвертая линия</option>
                                <option value="building_position5">Другое</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="height: 140px">
                    <div class="col-md-3">
                        <div class="col-md-12">Площадь</div>
                        <div class="col-md-9">
                            <input type="text" name="cm_area" class="commercial_attr cm_area form-control">
                        </div>
                        <div class="col-md-3">m2</div>
                    </div>
                    <div class="col-md-2">
                        <div class="col-md-12">Комнаты</div>
                        <div class="col-md-11">
                            <select name="cm_rooms" class="commercial_attr cm_rooms form-control">
                                <?php
                                for($i=1;$i<10;$i++){
                                    ?>
                                    <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                    <?php
                                }
                                ?>
                                <option value="10">10+</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2" >
                        <div class="col-md-8">Этажность</div>
                        <div class="col-md-11">
                            <select name="cm_floorer" class="commercial_attr cm_floor form-control">
                                <?php
                                for($i=1;$i<=20;$i++){
                                    ?>
                                    <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                    <div class="col-md-2">
                        <div class="col-md-12">Этаж</div>
                        <div class="col-md-11" style="height: 130px;overflow: hidden;position: absolute;z-index: 1003;line-height: 10px;margin-top: 24px;background: #fff url(http://user.alex-r.am/images/select.png) no-repeat 96% 10px;border: 1px solid #D2D6DE;">
                            <?php
                            for($i=-3;$i<4;$i++){
                                ?>
                                <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                    <input type="checkbox" name="cm_floor" class="commercial_attr cm_floor" value="<?php echo $i; ?>" style="float: left"><span style="float: left;margin-top: 6px;"><?php echo $i; ?></span>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                    <div class="col-md-3">
                        <div class="col-md-12">Название этажa</div>
                        <div class="col-md-11 a_hide1" style="height: 95px;overflow: hidden;position: absolute;z-index: 1003;line-height: 10px;margin-top: 24px;background: #fff url(http://user.alex-r.am/images/select.png) no-repeat 96% 10px;border: 1px solid #D2D6DE;">
                            <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                <input type="checkbox" name="cm_floor_name" class="commercial_attr cm_floor_name" value="basement" style="float: left"><span style="float: left;margin-top: 6px;">подвал</span>
                            </div>
                            <div style="display: inline-block;width: 100%;padding: 0 1%;">
                                <input type="checkbox" name="cm_floor_name" class="commercial_attr cm_floor_name" value="semibasement" style="float: left"><span style="float: left;margin-top: 6px;">полуподвал</span>
                            </div>
                            <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                <input type="checkbox" name="cm_floor_name" class="commercial_attr cm_floor_name" value="first" style="float: left"><span style="float: left;margin-top: 6px;">первый</span>
                            </div>
                            <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                <input type="checkbox" name="cm_floor_name" class="commercial_attr cm_floor_name" value="average" style="float: left"><span style="float: left;margin-top: 6px;">средний</span>
                            </div>
                            <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                <input type="checkbox" name="cm_floor_name" class="commercial_attr cm_floor_name" value="last" style="float: left"><span style="float: left;margin-top: 6px;">последний</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6" style="background: #F9FAFC;padding: 23px 25px;">
                <div class="col-md-12" style="height: 198px;">
                    <div class="col-md-4">
                        <div class="col-md-12">Наличие витрин:</div>
                        <div class="col-md-12">
                            <div class="col-md-1" style="margin-top: 1px"><input type="radio" name="cm_showcases" class="commercial_attr cm_showcases" value="cm_showcases1"></div>
                            <div class="col-md-10" style="margin-left: 5px">Есть</div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-1" style="margin-top: 1px"><input type="radio" name="cm_showcases" class="commercial_attr cm_showcases" value="cm_showcases2"></div>
                            <div class="col-md-10" style="margin-left: 5px">Нет</div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12">Вход:</div>
                        <div class="col-md-10">
                            <select name="cm_entrance" class="commercial_attr cm_entrance form-control">
                                <option value="cm_entrance1">С улицы</option>
                                <option value="cm_entrance2">Со двора</option>
                                <option value="cm_entrance3">Главный вход</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12">Состояние:</div>
                        <div class="col-md-12">
                            <div class="col-md-1" style="margin-top: 1px"><input type="radio" name="cm_repair" class="commercial_attr cm_repair" value="cm_repair1"></div>
                            <div class="col-md-10" style="margin-left: 5px">Отремонтированный</div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-1" style="margin-top: 1px"><input type="radio" name="cm_repair" class="commercial_attr cm_repair" value="cm_repair2"></div>
                            <div class="col-md-10" style="margin-left: 5px">Не отремонтировано</div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            break;
        }
        case '8':{
            ?>
            <div class="col-md-4" style="background: #F9FAFC;padding: 23px 25px;">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="col-md-12">Размеры участок</div>
                        <div class="col-md-8">
                            <input type="text" name="ld_area" class="land_attr ld_area form-control">
                        </div>
                        <div class="col-md-4">m2</div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12">Наличие постройки</div>
                        <div class="col-md-8">
                            <input type="text" name="ld_buildings" class="land_attr ld_buildings form-control">
                        </div>
                        <div class="col-md-4">m2</div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="col-md-12">Вода (питьевая)</div>
                        <div class="col-md-11">
                            <select name="ld_water_drink" class="land_attr ld_water_drink form-control">
                                <option value="water_drink1">Есть</option>
                                <option value="water_drink2">Нет</option>
                                <option value="water_drink3">Возможность</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12">Вода (ирригационная)</div>
                        <div class="col-md-11">
                            <select name="ld_water_irrigation" class="land_attr ld_water_irrigation form-control">
                                <option value="water_irrigation1">Есть</option>
                                <option value="water_irrigation2">Нет</option>
                                <option value="water_irrigation3">Возможность</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="col-md-12">Газ</div>
                        <div class="col-md-11">
                            <select name="ld_gas" class="land_attr ld_gas form-control">
                                <option value="ld_gas1">Есть</option>
                                <option value="ld_gas2">Нет</option>
                                <option value="ld_gas3">Возможность</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12">Ток</div>
                        <div class="col-md-11">
                            <select name="ld_electricity" class="land_attr ld_electricity form-control">
                                <option value="ld_electricity1">Есть</option>
                                <option value="ld_electricity2">Нет</option>
                                <option value="ld_electricity3">Возможность</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8" style="background: #F9FAFC;padding: 23px 25px;">
                <div class="col-md-12" style="height: 95px">
                    <div class="col-md-6">
                        <div class="col-md-12">Высоковольтные столбы</div>
                        <div class="col-md-11" style="height: 45px;overflow: hidden;position: absolute;z-index: 1003;line-height: 10px;margin-top: 24px;background: #fff url(http://user.alex-r.am/images/select.png) no-repeat 96% 10px;border: 1px solid #D2D6DE;">
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="radio" name="ld_high_voltage_pillars" class="land_attr ld_high_voltage_pillars" value="high_voltage_pillars1"></div>
                                <div class="col-md-2" style="margin-top: 6px;margin-left: 5px;">Есть</div>
                                <div class="col-md-4" style="margin-top: 6px;margin-left: 5px;">Количество</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="high_voltage_pillars1_dop" class="home_attr ld_high_voltage_pillars_dop form-control" style="height: 20px"></div>
                                <div class="col-md-1" style="margin-top: 6px;margin-left: 5px;">шт.</div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="radio" name="ld_high_voltage_pillars" class="home_attr ld_high_voltage_pillars" value="high_voltage_pillars2"></div>
                                <div class="col-md-2" style="margin-top: 6px;margin-left: 5px;">Нет</div>
                                <div class="col-md-4" style="margin-top: 6px;margin-left: 5px;">Расстояние</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="high_voltage_pillars2_dop" class="home_attr ld_high_voltage_pillars_dop form-control" style="height: 20px"></div>
                                <div class="col-md-1" style="margin-top: 6px;margin-left: 5px;">м</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12">Канализация</div>
                        <div class="col-md-11" style="height: 60px;overflow: hidden;position: absolute;z-index: 1003;line-height: 10px;margin-top: 24px;background: #fff url(http://user.alex-r.am/images/select.png) no-repeat 96% 10px;border: 1px solid #D2D6DE;">
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="radio" name="ld_sewage" class="land_attr ld_sewage" value="sewage1"></div>
                                <div class="col-md-4" style="margin-top: 6px;margin-left: 5px;">Есть</div>
                                <div class="col-md-3" style="margin-top: 1px"></div>
                                <div class="col-md-2"></div>
                                <div class="col-md-1"></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="radio" name="ld_sewage" class="home_attr ld_sewage" value="sewage2"></div>
                                <div class="col-md-4" style="margin-top: 6px;margin-left: 5px;">Нет</div>
                                <div class="col-md-3"></div>
                                <div class="col-md-2"></div>
                                <div class="col-md-1"></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="radio" name="ld_sewage" class="home_attr ld_sewage" value="sewage3"></div>
                                <div class="col-md-4" style="margin-top: 6px;margin-left: 5px;">Возможность</div>
                                <div class="col-md-3" style="margin-top: 6px;margin-left: 5px;">Расстояние</div>
                                <div class="col-md-2" style="margin-top: 1px"><input type="text" name="sewage3_dop" class="home_attr ld_sewage_dop form-control" style="height: 20px"></div>
                                <div class="col-md-1" style="margin-top: 6px;margin-left: 5px;">м</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="height: 73px">
                    <div class="col-md-6">
                        <div class="col-md-12">Ограждение</div>
                        <div class="col-md-6">
                            <select name="ld_fence" class="land_attr ld_fence form-control">
                                <option value="ld_fence1">Камень</option>
                                <option value="ld_fence2">Проволочные сетки</option>
                                <option value="ld_fence3">Нет</option>
                            </select>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12">Плодовые деревья</div>
                        <div class="col-md-1"><input type="radio" name="fruit_trees" class="land_attr fruit_trees" value="fruit_trees1"></div>
                        <div class="col-md-3" style="margin-top: 5px">Есть</div>
                        <div class="col-md-1"><input type="radio" name="fruit_trees" class="land_attr fruit_trees" value="fruit_trees2"></div>
                        <div class="col-md-3" style="margin-top: 5px">Нет</div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
            </div>
            <?php
            break;
        }
        case '9':{
            ?>
            <div class="col-md-6" style="background: #F9FAFC;padding: 23px 25px;">
                <div class="col-md-6">
                    <div class="col-md-12">Площадь</div>
                    <div class="col-md-6">
                        <input type="text" name="gg_area" class="garage_attr gg_area form-control">
                    </div>
                    <div class="col-md-4">m2</div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-12">Тип здания</div>
                    <div class="col-md-1"><input type="radio" name="gg_building_type" class="garage_attr gg_building_type" value="gg_building1"></div>
                    <div class="col-md-3">Камень</div>
                    <div class="col-md-1"><input type="radio" name="gg_building_type" class="garage_attr gg_building_type" value="gg_building2"></div>
                    <div class="col-md-3">Металл</div>
                </div>
            </div>
            <div class="col-md-6" style="background: #F9FAFC;padding: 50px 25px;">

            </div>
            <?php
            break;
        }
    }
}
function delete_image_in_edit(){
    $image = $_POST['image'];
    $image_dir1 = $_SERVER['DOCUMENT_ROOT']."/images/ads/".$image;
    if(file_exists($image_dir1)){
        unlink($image_dir1);
    }
    $image_dir2 = $_SERVER['DOCUMENT_ROOT']."/images/100/".$image;
    if(file_exists($image_dir2)){
        unlink($image_dir2);
    }
    $image_dir3 = $_SERVER['DOCUMENT_ROOT']."/images/700/".$image;
    if(file_exists($image_dir3)){
        unlink($image_dir3);
    }
    $id = $_POST['id'];
    mwdb_query("DELETE FROM post_images WHERE image_name='{var}' AND post_id={var}",array($image,$id));
}
function get_client_search_attributes(){
    $category = (int)$_POST['category'];
    switch($category){
        case '5':{
            ?>
            <div class="col-md-12">
                <div class="col-md-3">
                    <div class="col-md-12">Площадь</div>
                    <div class="col-md-11">
                        <div class="col-md-4"><input type="text" name="area_from" class="form-control" placeholder="От"></div>
                        <div class="col-md-4"><input type="text" name="area_to" class="form-control" placeholder="До"></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-12">Комнаты</div>
                    <div class="col-md-11">
                        <div class="col-md-12">
                            <?php
                            for($i=1;$i<11;$i++){
								if($i==6){echo '<div class="col-md-2" style="height:22px"></div>';}
                                ?>
                                <div class="col-md-2"><input type="checkbox" name="rooms[]" value="<?php echo $i; ?>"><?php echo $i;if($i==10){echo '+';} ?></div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-3">Этажность</div>
                    <div class="col-md-11">
                        <div class="col-md-4">
                            <select name="floorer_from" class="form-control">
                                <option value="0">От</option>
                                <?php
                                for($i=1;$i<21;$i++){
                                    ?>
                                    <option value="<?php echo $i ?>"><?php echo $i; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <select name="floorer_to" class="form-control">
                                <option value="0">До</option>
                                <?php
                                for($i=1;$i<21;$i++){
                                    ?>
                                    <option value="<?php echo $i ?>"><?php echo $i; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-12">Этаж</div>
                    <div class="col-md-11">
                        <div class="col-md-4">
                            <select name="floor_from" class="form-control">
                                <option value="0">От</option>
                                <?php
                                for($i=1;$i<21;$i++){
                                    ?>
                                    <option value="<?php echo $i ?>"><?php echo $i; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <select name="floor_to" class="form-control">
                                <option value="0">До</option>
                                <?php
                                for($i=1;$i<21;$i++){
                                    ?>
                                    <option value="<?php echo $i ?>"><?php echo $i; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="col-md-12">Проект здания</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 10px;">
                        <div class="col-md-4">
                            <div class="col-md-12"><input type="checkbox" name="building_project[]" value="building_project14">Новостройка</div>
                            <div class="col-md-12"><input type="checkbox" name="building_project[]" value="building_project1">Сталинский</div>
                            <div class="col-md-12"><input type="checkbox" name="building_project[]" value="building_project2">Хрущевский</div>
                            <div class="col-md-12"><input type="checkbox" name="building_project[]" value="building_project3">Пост хрущевский</div>
                            <div class="col-md-12"><input type="checkbox" name="building_project[]" value="building_project4">Каменный спецпроект</div>
                            <div class="col-md-12"><input type="checkbox" name="building_project[]" value="building_project6">Бадалян</div>
                        </div>
                        <div class="col-md-4">
                            <div class="col-md-12"><input type="checkbox" name="building_project[]" value="building_project7">Московский ДСК</div>
                            <div class="col-md-12"><input type="checkbox" name="building_project[]" value="building_project8">Ереванский ДСК</div>
                            <div class="col-md-12"><input type="checkbox" name="building_project[]" value="building_project9">Грузинский</div>
                            <div class="col-md-12"><input type="checkbox" name="building_project[]" value="building_project10">Ленточный</div>
                            <div class="col-md-12"><input type="checkbox" name="building_project[]" value="building_project11">Монолитый сейсмоустойчивый</div>
                            <div class="col-md-12"><input type="checkbox" name="building_project[]" value="building_project12">Трехъярусный</div>
                        </div>
                        <div class="col-md-4">
                            <div class="col-md-12"><input type="checkbox" name="building_project[]" value="building_project13">Четырехъярусный</div>
                            <div class="col-md-12"><input type="checkbox" name="building_project[]" value="building_project16">Чешский проект</div>
                            <div class="col-md-12"><input type="checkbox" name="building_project[]" value="building_project15">Спецпроект</div>
                            <div class="col-md-12"><input type="checkbox" name="building_project[]" value="building_project17">Югославский</div>
                            <div class="col-md-12"><input type="checkbox" name="building_project[]" value="building_project18">Финский</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-12">Тип здания</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="checkbox" name="building_type[]" value="building_type1">монолитный
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="building_type[]" value="building_type2">каменный
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="building_type[]" value="building_type3">крупнопанельный
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="building_type[]" value="building_type4">каркаснопанельный
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-12">Расположение здания</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <input type="checkbox" name="building_position[]" value="building_position1">Первая линия
                            </div>
                            <div class="col-md-12">
                                <input type="checkbox" name="building_position[]" value="building_position2">Вторая линия
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-12">Тип покрытия</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <input type="checkbox" name="housetop[]" value="housetop1">Деревянный
                            </div>
                            <div class="col-md-12">
                                <input type="checkbox" name="housetop[]" value="housetop2">Панельный
                            </div>
                            <div class="col-md-12">
                                <input type="checkbox" name="housetop[]" value="housetop3">Монолитный бетон
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-3">
                    <div class="col-md-12">Название этажа</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="checkbox" name="floor_name[]" value="semibasement">полуподвал
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="floor_name[]" value="basement">подвал
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="floor_name[]" value="first">первый
                        </div>
						<div class="col-md-12">
                            <input type="checkbox" name="floor_name[]" value="high_first">белэтаж
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="floor_name[]" value="average">средний
                        </div>
						<div class="col-md-12">
                            <input type="checkbox" name="floor_name[]" value="high_">высокий
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="floor_name[]" value="last">последний
                        </div>
						<div class="col-md-12">
                            <input type="checkbox" name="floor_name[]" value="duplex">дуплекс
                        </div>
						<div class="col-md-12">
                            <input type="checkbox" name="floor_name[]" value="mansard">мансард
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-12">Отделка</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="checkbox" name="repair[]" value="repair1">Нулевое состояние
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="repair[]" value="repair2">Гос. состояние
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="repair[]" value="repair3">Капитальный ремонт
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="repair[]" value="repair4">Стильный ремонт
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="repair[]" value="repair5">Среднее состояние
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="repair[]" value="repair6">Косметический ремонт
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="repair[]" value="repair7">В процессе ремонта
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="repair[]" value="repair8">Частичный ремонт
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-12">Вода</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="radio" name="water" value="ap_water1">Постоянная
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="water" value="ap_water2">Временная
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="water" value="0" checked>Не важно
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-12">Газ</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="radio" name="gas" value="ap_gas1">В здании
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="gas" value="ap_gas2">В квартире
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="gas" value="0" checked>Не важно
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-3">
                    <div class="col-md-12">Балкон</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="checkbox" name="balcony[]" value="balcony1">Висячий
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="balcony[]" value="balcony2">Француский
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="balcony[]" value="balcony3">Открытая Лоджия
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="balcony[]" value="balcony4">Закрытая Лоджия
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-12">Гараж</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="checkbox" name="garage[]" value="ap_garage1">Каменный
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="garage[]" value="ap_garage2" >Железный
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-12">Подвал</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="radio" name="basement" value="1">Да
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="basement" value="">Нет
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="basement" value="0" checked>Не важно
                        </div>
                    </div>
                </div>
            </div>
            <?php
            break;
        }
        case '6':{
            ?>
            <div class="col-md-12">
                <div class="col-md-3" >
                    <div class="col-md-12">Общая площадь</div>
                    <div class="col-md-11">
                        <div class="col-md-4"><input type="text" name="total_area_from" class="form-control" placeholder="От"></div>
                        <div class="col-md-4"><input type="text" name="total_area_to" class="form-control" placeholder="До"></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-12">Площадь дома</div>
                    <div class="col-md-11">
                        <div class="col-md-4"><input type="text" name="home_area_from" class="form-control" placeholder="От"></div>
                        <div class="col-md-4"><input type="text" name="home_area_to" class="form-control" placeholder="До"></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-12">Комнаты</div>
                    <div class="col-md-11">
                        <div class="col-md-12">
                            <?php
                            for($i=1;$i<11;$i++){
								if($i==6){echo '<div class="col-md-2" style="height:22px"></div>';}
                                ?>
                                <div class="col-md-2"><input type="checkbox" name="rooms[]" value="<?php echo $i; ?>"><?php echo $i;if($i==10){echo '+';} ?></div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-12">Этажность</div>
                    <div class="col-md-11">
                        <div class="col-md-4">
                            <select name="floorer_from" class="form-control">
                                <option value="0">От</option>
                                <?php
                                for($i=1;$i<21;$i++){
                                    ?>
                                    <option value="<?php echo $i ?>"><?php echo $i; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <select name="floorer_to" class="form-control">
                                <option value="0">До</option>
                                <?php
                                for($i=1;$i<21;$i++){
                                    ?>
                                    <option value="<?php echo $i ?>"><?php echo $i; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-2">
                    <div class="col-md-12">Тип здания</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="checkbox" name="building_type[]" value="building_type1">монолитный
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="building_type[]" value="building_type2">каменный
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="building_type[]" value="building_type3">крупнопанельный
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="building_type[]" value="building_type4">каркаснопанельный
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-12">Расположение здания</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <input type="checkbox" name="building_position[]" value="building_position1">Первая линия
                            </div>
                            <div class="col-md-12">
                                <input type="checkbox" name="building_position[]" value="building_position2">Вторая линия
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-12">Отделка</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="checkbox" name="repair[]" value="repair1">Нулевое состояние
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="repair[]" value="repair2">Гос. состояние
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="repair[]" value="repair3">Капитальный ремонт
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="repair[]" value="repair4">Стильный ремонт
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="repair[]" value="repair5">Среднее состояние
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="repair[]" value="repair6">Косметический ремонт
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="repair[]" value="repair7">В процессе ремонта
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="repair[]" value="repair8">Частичный ремонт
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-12">Вода</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="radio" name="water" value="hm_water1">Постоянная
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="water" value="hm_water2">Временная
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="water" value="0" checked>Не важно
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-12">Газ</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="radio" name="gas" value="hm_gas1">Есть
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="gas" value="hm_gas2">Возможность
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="gas" value="hm_gas3">Нет
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="gas" value="0" checked>Не важно
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-12">Система отопления</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="radio" name="heating_system" value="hm_heating_system1">Есть
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="heating_system" value="hm_heating_system2">Нет
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="heating_system" value="0" checked>Не важно
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-2">
                    <div class="col-md-12">Балкон</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="checkbox" name="balcony[]" value="balcony1">Висячий
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="balcony[]" value="balcony2">Француский
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="balcony[]" value="balcony3">Открытая Лоджия
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="balcony[]" value="balcony4">Закрытая Лоджия
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-12">Гараж</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="checkbox" name="garage[]" value="garage1">Соседние
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="garage[]" value="garage2">Под зданием
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="garage[]" value="garage3">Отдельный
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-12">Подвал</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="radio" name="basement" value="1">Да
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="basement" value="">Нет
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="basement" value="0" checked>Не важно
                        </div>
                    </div>
                </div>
            </div>
            <?php
            break;
        }
        case '7':{
            ?>
            <div class="col-md-12">
                <div class="col-md-7">
                    <div class="col-md-12">Тип</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-4">
                            <div class="col-md-12">
                                <input type="checkbox" name="typer[]" value="cm_type2">гостиница
                            </div>
                            <div class="col-md-12">
                                <input type="checkbox" name="typer[]" value="cm_type1">оффис
                            </div>
                            <div class="col-md-12">
                                <input type="checkbox" name="typer[]" value="cm_type3">развлекательный
                            </div>
                            <div class="col-md-12">
                                <input type="checkbox" name="typer[]" value="cm_type4">магазин
                            </div>
                            <div class="col-md-12">
                                <input type="checkbox" name="typer[]" value="cm_type5">парикмахерская
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="col-md-12">
                                <input type="checkbox" name="typer[]" value="cm_type6">автотехобслуживание
                            </div>
                            <div class="col-md-12">
                                <input type="checkbox" name="typer[]" value="cm_type7">склад
                            </div>
                            <div class="col-md-12">
                                <input type="checkbox" name="typer[]" value="cm_type8">сельскохозяйственный
                            </div>
                            <div class="col-md-12">
                                <input type="checkbox" name="typer[]" value="cm_type9">бизнес центр
                            </div>
                            <div class="col-md-12">
                                <input type="checkbox" name="typer[]" value="cm_type10">производственный
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="col-md-12">
                                <input type="checkbox" name="typer[]" value="cm_type11">учебный
                            </div>
                            <div class="col-md-12">
                                <input type="checkbox" name="typer[]" value="cm_type12">медицинские заведения
                            </div>
                            <div class="col-md-12">
                                <input type="checkbox" name="typer[]" value="cm_type13">универсальный
                            </div>
                            <div class="col-md-12">
                                <input type="checkbox" name="typer[]" value="cm_type14">другое
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-12">Площадь</div>
                    <div class="col-md-11">
                        <div class="col-md-12">
                            <div class="col-md-4"><input type="text" name="area_from" class="form-control" placeholder="От"></div>
                            <div class="col-md-4"><input type="text" name="area_to" class="form-control" placeholder="До"></div>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-top: 10px">Этаж</div>
                    <div class="col-md-11">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <select name="floor_from" class="form-control" style="padding: 0">
                                    <option value="0">От</option>
                                    <?php
                                    for($i=-3;$i<6;$i++){
                                        ?>
                                        <option value="<?php echo $i ?>"><?php echo $i; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <select name="floor_to" class="form-control" style="padding: 0">
                                    <option value="0">До</option>
                                    <?php
                                    for($i=-3;$i<6;$i++){
                                        ?>
                                        <option value="<?php echo $i ?>"><?php echo $i; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-12">Комнаты</div>
                    <div class="col-md-11">
                        <div class="col-md-12">
                            <?php
                            for($i=1;$i<11;$i++){
								if($i==6){echo '<div class="col-md-2" style="height:22px"></div>';}
                                ?>
                                <div class="col-md-2"><input type="checkbox" name="rooms[]" value="<?php echo $i; ?>"><?php echo $i;if($i==10){echo '+';} ?></div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-top: 10px">Название этажа</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="checkbox" name="floor_name[]" value="cm_floor_name1">Подвал
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="floor_name[]" value="cm_floor_name2">Полуподвал
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="floor_name[]" value="cm_floor_name3">Цокольный этаж
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="floor_name[]" value="cm_floor_name4">Первый этаж
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-2">
                    <div class="col-md-12">Тип здания</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="checkbox" name="building_type[]" value="building_type1">монолитный
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="building_type[]" value="building_type2">каменный
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="building_type[]" value="building_type3">крупнопанельный
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="building_type[]" value="building_type4">каркаснопанельный
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-12">Расположение здания</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="checkbox" name="building_position[]" value="building_position1">Первая линия
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="building_position[]" value="building_position2">Вторая линия
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="building_position[]" value="building_position3">Третья линия
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="building_position[]" value="building_position4">Четвертая линия
                        </div>
                        <div class="col-md-12">
                            <input type="checkbox" name="building_position[]" value="building_position5">Другое
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-12">Наличие витрин</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="radio" name="showcases" value="cm_showcases1">Есть
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="showcases" value="cm_showcases2">Нет
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="showcases" value="0" checked>Не важно
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-12">Вход</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="radio" name="entrance" value="cm_entrance1">С улицы
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="entrance" value="cm_entrance2">Со двора
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="entrance" value="cm_entrance3">Главный вход
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="entrance" value="0" checked>Не важно
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-12">Состояние</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="radio" name="repair" value="cm_repair1">Отремонтированный
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="repair" value="cm_repair2">Не отремонтировано
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="repair" value="0" checked>Не важно
                        </div>
                    </div>
                </div>
            </div>
            <?php
            break;
        }
        case '8':{
            ?>
            <div class="col-md-12">
                <div class="col-md-4">
                    <div class="col-md-12">Площадь</div>
                    <div class="col-md-11">
                        <div class="col-md-5"><input type="text" name="area_from" class="form-control" placeholder="От"></div>
                        <div class="col-md-5"><input type="text" name="area_to" class="form-control" placeholder="До"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-12">Наличие постройки(Площадь)</div>
                    <div class="col-md-11">
                        <div class="col-md-4"><input type="text" name="buildings_from" class="form-control" placeholder="От"></div>
                        <div class="col-md-4"><input type="text" name="buildings_to" class="form-control" placeholder="До"></div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-12">Вода (питьевая)</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="radio" name="water_drink" value="water_drink1">Есть
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="water_drink" value="water_drink2">Нет
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="water_drink" value="water_drink3">Возможность
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="water_drink" value="0" checked>Не важно
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-12">Вода (ирригационная)</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="radio" name="water_irrigation" value="water_irrigation1">Есть
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="water_irrigation" value="water_irrigation2">Нет
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="water_irrigation" value="water_irrigation3">Возможность
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="water_irrigation" value="0" checked>Не важно
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-2">
                    <div class="col-md-12">Газ</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="radio" name="gas" value="ld_gas1">Есть
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="gas" value="ld_gas2">Нет
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="gas" value="ld_gas3">Возможность
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="gas" value="0" checked>Не важно
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-12">Ток</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="radio" name="electricity" value="ld_electricity1">Есть
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="electricity" value="ld_electricity2">Нет
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="electricity" value="ld_electricity3">Возможность
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="electricity" value="0" checked>Не важно
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-12">Высоковольтные столбы</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="radio" name="high_voltage_pillars" value="high_voltage_pillars1">Есть
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="high_voltage_pillars" value="high_voltage_pillars2">Нет
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="high_voltage_pillars" value="0" checked>Не важно
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-12">Канализация</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="radio" name="sewage" value="sewage1">Есть
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="sewage" value="sewage2">Нет
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="sewage" value="sewage3">Возможность
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="sewage" value="0" checked>Не важно
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-12">Ограждение</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="radio" name="fence" value="ld_fence1">Камень
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="fence" value="ld_fence2">Проволочные сетки
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="fence" value="ld_fence3">Нет
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="fence" value="0" checked>Не важно
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-12">Плодовые деревья</div>
                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                        <div class="col-md-12">
                            <input type="radio" name="fruit_trees" value="fruit_trees1">Есть
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="fruit_trees" value="fruit_trees2">Нет
                        </div>
                        <div class="col-md-12">
                            <input type="radio" name="fruit_trees" value="0" checked>Не важно
                        </div>
                    </div>
                </div>
            </div>
            <?php
            break;
        }
        case '9':{
            ?>
            <div class="col-md-3">
                <div class="col-md-12">Площадь</div>
                <div class="col-md-11">
                    <div class="col-md-4"><input type="text" name="area_from" class="form-control" placeholder="От"></div>
                    <div class="col-md-4"><input type="text" name="area_to" class="form-control" placeholder="До"></div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="col-md-12">Тип здания</div>
                <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                    <div class="col-md-12">
                        <input type="radio" name="building_type" value="gg_building1">Камень
                    </div>
                    <div class="col-md-12">
                        <input type="radio" name="building_type" value="gg_building2">Металл
                    </div>
                    <div class="col-md-12">
                        <input type="radio" name="building_type" value="0" checked>Не важно
                    </div>
                </div>
            </div>
            <?php
            break;
        }
        default:{
            break;
        }
    }
}
function get_client_search_result(){
    session_start();
    $user = (int)$_POST['user'];
    $operator = (int)$_POST['operator'];
    $search_params = mwdb_get_row("SELECT * FROM client_search WHERE client_id={var}",array($user));
    if($operator=='1') {
        $query = "post_status NOT IN (5,6,7)";
    }
    else{
        $result_broker_operator = mwdb_get_var("SELECT user_type FROM users WHERE user_id={var}",array($operator));
        if($result_broker_operator=='2'){
            $query = "post_status NOT IN (5,6,7) AND post_user=$operator";
        }
        else{
            $mutq_operator = mwdb_get_var("SELECT city FROM users WHERE user_id={var}",array($operator));
            $query = "post_status NOT IN (5,6,7) AND post_user IN ($mutq_operator)";
        }
    }
    if($search_params->category!='0'){
        $query .= " AND post_category=".$search_params->category." ";
    }
    if($search_params->region!='0'){
        $query .= " AND post_region=".$search_params->region." ";
    }
    if($search_params->city!=''){
        $query .= " AND post_city IN (".$search_params->city.") ";
    }
    if($search_params->area!=''){
        $query .= " AND post_area IN (".$search_params->area.") ";
    }
    if($search_params->microarea!=''){
        $query .= " AND post_microarea IN (".$search_params->microarea.") ";
    }
    $result_posts = mwdb_select("SELECT post_id FROM posts WHERE $query",array());
    $post_filtered = array();
    foreach($result_posts as $row_post){
        array_push($post_filtered,$row_post->post_id);
    }
    $post_filtered_meta = array();
    if($search_params->type_search=='sale'){
        $result_posts_from_meta = mwdb_select("SELECT post_id FROM post_meta WHERE meta_key='{var}' AND meta_value='{var}'",array('sale',1));
        foreach($result_posts_from_meta as $row_post_from_meta){
            if(in_array($row_post_from_meta->post_id,$post_filtered)){
                array_push($post_filtered_meta,$row_post_from_meta->post_id);
            }
        }
        $post_filtered_price_from = array();
        if($search_params->price_from>0){
            $result_posts_from_price_from =  mwdb_select("SELECT post_id FROM post_meta WHERE meta_key='{var}' AND meta_value>={var}",array('price_number',$search_params->price_from));
            foreach($result_posts_from_price_from as $row_post_from_price_from){
                if(in_array($row_post_from_price_from->post_id,$post_filtered_meta)){
                    array_push($post_filtered_price_from,$row_post_from_price_from->post_id);
                }
            }
            $post_filtered_meta = $post_filtered_price_from;
        }
        //if(count($post_filtered_price_from)>0) {

        //}
        $post_filtered_price_to = array();
        if($search_params->price_to>0){
            $result_posts_from_price_to =  mwdb_select("SELECT post_id FROM post_meta WHERE meta_key='{var}' AND meta_value<={var}",array('price_number',$search_params->price_to));
            foreach($result_posts_from_price_to as $row_post_from_price_to){
                if(in_array($row_post_from_price_to->post_id,$post_filtered_meta)){
                    array_push($post_filtered_price_to,$row_post_from_price_to->post_id);
                }
            }
            $post_filtered_meta = $post_filtered_price_to;
        }
        //if(count($post_filtered_price_to)>0) {

        //}
    }
    elseif($search_params->type_search=='rent'){
        $result_posts_from_meta = mwdb_select("SELECT post_id FROM post_meta WHERE meta_key='{var}' AND meta_value='{var}'",array('rent',1));
        foreach($result_posts_from_meta as $row_post_from_meta){
            if(in_array($row_post_from_meta->post_id,$post_filtered)){
                array_push($post_filtered_meta,$row_post_from_meta->post_id);
            }
        }
        $post_filtered_price_from = array();
        if($search_params->price_from>0){
            $result_posts_from_price_from = mwdb_select("SELECT DISTINCT post_id FROM post_meta WHERE (meta_key='{var}' AND meta_value>={var}) OR (meta_key='{var}' AND meta_value>={var}) OR (meta_key='{var}' AND meta_value>={var})",array('rent_cost1',$search_params->price_from,'rent_cost2',$search_params->price_from,'rent_cost3',$search_params->price_from));
            foreach($result_posts_from_price_from as $row_post_from_price_from){
                if(in_array($row_post_from_price_from->post_id,$post_filtered_meta)){
                    array_push($post_filtered_price_from,$row_post_from_price_from->post_id);
                }
            }
            $post_filtered_meta = $post_filtered_price_from;
        }
        $post_filtered_price_to = array();
        if($search_params->price_to>0){
            $result_posts_from_price_to = mwdb_select("SELECT DISTINCT post_id FROM post_meta WHERE (meta_key='{var}' AND meta_value<={var}) OR (meta_key='{var}' AND meta_value<={var}) OR (meta_key='{var}' AND meta_value<={var})",array('rent_cost1',$search_params->price_to,'rent_cost2',$search_params->price_to,'rent_cost3',$search_params->price_to));
            foreach($result_posts_from_price_to as $row_post_from_price_to){
                if(in_array($row_post_from_price_to->post_id,$post_filtered_meta)){
                    array_push($post_filtered_price_to,$row_post_from_price_to->post_id);
                }
            }
            $post_filtered_meta = $post_filtered_price_to;
        }
    }
    elseif($search_params->type_search=='day'){
        $result_posts_from_meta = mwdb_select("SELECT post_id FROM post_meta WHERE meta_key='{var}' AND meta_value='{var}'",array('rent_type3',1));
        foreach($result_posts_from_meta as $row_post_from_meta){
            if(in_array($row_post_from_meta->post_id,$post_filtered)){
                array_push($post_filtered_meta,$row_post_from_meta->post_id);
            }
        }
        $post_filtered_price_from = array();
        if($search_params->price_from>0){
            $result_posts_from_price_from = mwdb_select("SELECT post_id FROM post_meta WHERE meta_key='{var}' AND meta_value>={var}",array('rent_cost3',$search_params->price_from));
            foreach($result_posts_from_price_from as $row_post_from_price_from){
                if(in_array($row_post_from_price_from->post_id,$post_filtered_meta)){
                    array_push($post_filtered_price_from,$row_post_from_price_from->post_id);
                }
            }
            $post_filtered_meta = $post_filtered_price_from;
        }
        $post_filtered_price_to = array();
        if($search_params->price_to>0){
            $result_posts_from_price_to = mwdb_select("SELECT post_id FROM post_meta WHERE meta_key='{var}' AND meta_value<={var}",array('rent_cost3',$search_params->price_to));
            foreach($result_posts_from_price_to as $row_post_from_price_to){
                if(in_array($row_post_from_price_to->post_id,$post_filtered_meta)){
                    array_push($post_filtered_price_to,$row_post_from_price_to->post_id);
                }
            }
            $post_filtered_meta = $post_filtered_price_to;
        }
    }
    $posts = array();
    switch($search_params->category){
        case '5':{
            $row_search_attrs = mwdb_get_row("SELECT * FROM client_apartments WHERE search_id={var}",array($search_params->search_id));
            $queried = '1=1 ';
            if($row_search_attrs->area!=''){
                $array_area = explode('-',$row_search_attrs->area);
                if($array_area[0]>0){
                    $queried .= " AND area>=".$array_area[0]." ";
                }
                if($array_area[1]>0){
                    $queried .= " AND area<=".$array_area[1]." ";
                }
            }
            if($row_search_attrs->rooms!=''){
                $queried .= " AND rooms IN (".$row_search_attrs->rooms.") ";
            }
            if($row_search_attrs->floorer!=''){
                $array_floorer = explode('-',$row_search_attrs->floorer);
                if($array_floorer[0]>0){
                    $queried .= " AND floorer>=".$array_floorer[0]." ";
                }
                if($array_floorer[1]>0){
                    $queried .= " AND floorer<=".$array_floorer[1]." ";
                }
            }
            if($row_search_attrs->floor!=''){
                $array_floor = explode('-',$row_search_attrs->floor);
                if($array_floor[0]>0){
                    $queried .= " AND floor>=".$array_floor[0]." ";
                }
                if($array_floor[1]>0){
                    $queried .= " AND floor<=".$array_floor[1]." ";
                }
            }
            if($row_search_attrs->floor_name!=''){
                $f_n = '';
                $array_floor_name = explode(',',$row_search_attrs->floor_name);
                foreach($array_floor_name as $fl_nm){
                    $f_n .= "'".$fl_nm."',";
                }
                $f_n = substr($f_n,0,-1);
                $queried .= " AND floor_name IN (".$f_n.") ";
            }
            if($row_search_attrs->repair!=''){
                $f_n = '';
                $array_floor_name = explode(',',$row_search_attrs->repair);
                foreach($array_floor_name as $fl_nm){
                    $f_n .= " repair LIKE '%".$fl_nm."%' OR ";
                }
                $f_n = substr($f_n,0,-3);
                $queried .= " AND (".$f_n.") ";
            }
            if($row_search_attrs->balcony!=''){
                $f_n = '';
                $array_floor_name = explode(',',$row_search_attrs->balcony);
                foreach($array_floor_name as $fl_nm){
                    $f_n .= " balcony LIKE '%".$fl_nm."%' OR ";
                }
                $f_n = substr($f_n,0,-1);
                $queried .= " AND (".$f_n.") ";
            }
            if($row_search_attrs->garage!=''){
                $f_n = '';
                $array_floor_name = explode(',',$row_search_attrs->garage);
                foreach($array_floor_name as $fl_nm){
                    $f_n .= "'".$fl_nm."',";
                }
                $f_n = substr($f_n,0,-1);
                $queried .= " AND garage IN (".$f_n.") ";
            }
            if($row_search_attrs->basement!='0'){
                $queried .= " AND basement='".$row_search_attrs->basement."' ";
            }
            if($row_search_attrs->water!='0'){
                $queried .= " AND water='".$row_search_attrs->water."' ";
            }
            if($row_search_attrs->gas!='0'){
                $queried .= " AND gas='".$row_search_attrs->gas."' ";
            }
            if($row_search_attrs->building_project!=''){
                $f_n = '';
                $array_floor_name = explode(',',$row_search_attrs->building_project);
                foreach($array_floor_name as $fl_nm){
                    $f_n .= "'".$fl_nm."',";
                }
                $f_n = substr($f_n,0,-1);
                $queried .= " AND building_project IN (".$f_n.") ";
            }
            if($row_search_attrs->building_position!=''){
                $f_n = '';
                $array_floor_name = explode(',',$row_search_attrs->building_position);
                foreach($array_floor_name as $fl_nm){
                    $f_n .= "'".$fl_nm."',";
                }
                $f_n = substr($f_n,0,-1);
                $queried .= " AND building_position IN (".$f_n.") ";
            }
            if($row_search_attrs->building_type!=''){
                $f_n = '';
                $array_floor_name = explode(',',$row_search_attrs->building_type);
                foreach($array_floor_name as $fl_nm){
                    $f_n .= "'".$fl_nm."',";
                }
                $f_n = substr($f_n,0,-1);
                $queried .= " AND building_type IN (".$f_n.") ";
            }
            if($row_search_attrs->housetop!=''){
                $f_n = '';
                $array_floor_name = explode(',',$row_search_attrs->housetop);
                foreach($array_floor_name as $fl_nm){
                    $f_n .= "'".$fl_nm."',";
                }
                $f_n = substr($f_n,0,-1);
                $queried .= " AND housetop IN (".$f_n.") ";
            }
            $result_posts_in_attr = mwdb_select("SELECT post_id FROM settings_apartments WHERE $queried ");
            foreach($result_posts_in_attr as $row_in_attr){
                if(in_array($row_in_attr->post_id,$post_filtered_meta)){
                    array_push($posts,$row_in_attr->post_id);
                }
            }
            break;
        }
        case '6':{
            $row_search_attrs = mwdb_get_row("SELECT * FROM client_houses WHERE search_id={var}",array($search_params->search_id));
            $queried = '1=1 ';
            if($row_search_attrs->total_area!=''){
                $array_total_area = explode('-',$row_search_attrs->total_area);
                if($array_total_area[0]>0){
                    $queried .= " AND total_area>=".$array_total_area[0]." ";
                }
                if($array_total_area[1]>0){
                    $queried .= " AND total_area<=".$array_total_area[1]." ";
                }
            }
            if($row_search_attrs->home_area!=''){
                $array_home_area = explode('-',$row_search_attrs->home_area);
                if($array_home_area[0]>0){
                    $queried .= " AND home_area>=".$array_home_area[0]." ";
                }
                if($array_home_area[1]>0){
                    $queried .= " AND home_area<=".$array_home_area[1]." ";
                }
            }
            if($row_search_attrs->rooms!=''){
                $queried .= " AND rooms IN (".$row_search_attrs->rooms.") ";
            }
            if($row_search_attrs->floorer!=''){
                $array_floorer = explode('-',$row_search_attrs->floorer);
                if($array_floorer[0]>0){
                    $queried .= " AND floorer>=".$array_floorer[0]." ";
                }
                if($array_floorer[1]>0){
                    $queried .= " AND floorer<=".$array_floorer[1]." ";
                }
            }
            if($row_search_attrs->repair!=''){
                $f_n = '';
                $array_floor_name = explode(',',$row_search_attrs->repair);
                foreach($array_floor_name as $fl_nm){
                    $f_n .= " repair LIKE '%".$fl_nm."%' OR ";
                }
                $f_n = substr($f_n,0,-3);
                $queried .= " AND (".$f_n.") ";
            }
            if($row_search_attrs->balcony!=''){
                $f_n = '';
                $array_floor_name = explode(',',$row_search_attrs->balcony);
                foreach($array_floor_name as $fl_nm){
                    $f_n .= " balcony LIKE '%".$fl_nm."%' OR ";
                }
                $f_n = substr($f_n,0,-3);
                $queried .= " AND (".$f_n.") ";
            }
            if($row_search_attrs->garage!=''){
                $f_n = '';
                $array_floor_name = explode(',',$row_search_attrs->garage);
                foreach($array_floor_name as $fl_nm){
                    $f_n .= " garage LIKE '%".$fl_nm."%' OR ";
                }
                $f_n = substr($f_n,0,-3);
                $queried .= " AND (".$f_n.") ";
            }
            if($row_search_attrs->basement=='1'){
                $queried .= " AND basement!='' ";
            }
            if($row_search_attrs->water!='0'){
                $queried .= " AND water='".$row_search_attrs->water."' ";
            }
            if($row_search_attrs->gas!='0'){
                $queried .= " AND gas='".$row_search_attrs->gas."' ";
            }
            if($row_search_attrs->heating_system!='0'){
                $queried .= " AND heating_system='".$row_search_attrs->heating_system."' ";
            }
            if($row_search_attrs->building_position!=''){
                $f_n = '';
                $array_floor_name = explode(',',$row_search_attrs->building_position);
                foreach($array_floor_name as $fl_nm){
                    $f_n .= "'".$fl_nm."',";
                }
                $f_n = substr($f_n,0,-1);
                $queried .= " AND building_position IN (".$f_n.") ";
            }
            if($row_search_attrs->building_type!=''){
                $f_n = '';
                $array_floor_name = explode(',',$row_search_attrs->building_type);
                foreach($array_floor_name as $fl_nm){
                    $f_n .= "'".$fl_nm."',";
                }
                $f_n = substr($f_n,0,-1);
                $queried .= " AND building_type IN (".$f_n.") ";
            }
            $result_posts_in_attr = mwdb_select("SELECT post_id FROM settings_houses WHERE $queried ");
            foreach($result_posts_in_attr as $row_in_attr){
                if(in_array($row_in_attr->post_id,$post_filtered_meta)){
                    array_push($posts,$row_in_attr->post_id);
                }
            }
            break;
        }
        case '7':{
            $row_search_attrs = mwdb_get_row("SELECT * FROM client_commercial WHERE search_id={var}",array($search_params->search_id));
            $queried = '1=1 ';
            if($row_search_attrs->type!=''){
                $f_n = '';
                $array_floor_name = explode(',',$row_search_attrs->type);
                foreach($array_floor_name as $fl_nm){
                    $f_n .= "'".$fl_nm."',";
                }
                $f_n = substr($f_n,0,-1);
                $queried .= " AND type IN (".$f_n.") ";
            }
            if($row_search_attrs->area!=''){
                $array_area = explode('-',$row_search_attrs->area);
                if($array_area[0]>0){
                    $queried .= " AND area>=".$array_area[0]." ";
                }
                if($array_area[1]>0){
                    $queried .= " AND area<=".$array_area[1]." ";
                }
            }
            if($row_search_attrs->rooms!=''){
                $queried .= " AND rooms IN (".$row_search_attrs->rooms.") ";
            }
            if($row_search_attrs->floor!=''){
                $a = explode('-',$row_search_attrs->floor);
                $array_floor = array();
                if(count($a)>2){
                    $array_floor[0] = '-'.$a[1];
                    $array_floor[1] = $a[2];
                }
                else{
                    $array_floor[0] = $a[0];
                    $array_floor[1] = $a[1];
                }
                if($array_floor[0]>0){
                    $queried .= " AND floor>=".$array_floor[0]." ";
                }
                if($array_floor[1]>0){
                    $queried .= " AND floor<=".$array_floor[1]." ";
                }
            }
            if($row_search_attrs->floor_name!=''){
                $f_n = '';
                $array_floor_name = explode(',',$row_search_attrs->floor_name);
                foreach($array_floor_name as $fl_nm){
                    $f_n .= "'".$fl_nm."',";
                }
                $f_n = substr($f_n,0,-1);
                $queried .= " AND floor_name IN (".$f_n.") ";
            }
            if($row_search_attrs->showcases!='0'){
                $queried .= " AND showcases='".$row_search_attrs->showcases."' ";
            }
            if($row_search_attrs->entrance!='0'){
                $queried .= " AND entrance='".$row_search_attrs->entrance."' ";
            }
            if($row_search_attrs->repair!='0'){
                $queried .= " AND repair='".$row_search_attrs->repair."' ";
            }
            if($row_search_attrs->building_position!=''){
                $f_n = '';
                $array_floor_name = explode(',',$row_search_attrs->building_position);
                foreach($array_floor_name as $fl_nm){
                    $f_n .= "'".$fl_nm."',";
                }
                $f_n = substr($f_n,0,-1);
                $queried .= " AND building_position IN (".$f_n.") ";
            }
            if($row_search_attrs->building_type!=''){
                $f_n = '';
                $array_floor_name = explode(',',$row_search_attrs->building_type);
                foreach($array_floor_name as $fl_nm){
                    $f_n .= "'".$fl_nm."',";
                }
                $f_n = substr($f_n,0,-1);
                $queried .= " AND building_type IN (".$f_n.") ";
            }
            $result_posts_in_attr = mwdb_select("SELECT post_id FROM settings_commercial WHERE $queried ");
            foreach($result_posts_in_attr as $row_in_attr){
                if(in_array($row_in_attr->post_id,$post_filtered_meta)){
                    array_push($posts,$row_in_attr->post_id);
                }
            }
            break;
        }
        case '8':{
            $row_search_attrs = mwdb_get_row("SELECT * FROM  client_land WHERE search_id={var}",array($search_params->search_id));
            $queried = '1=1 ';
            if($row_search_attrs->area!=''){
                $array_area = explode('-',$row_search_attrs->area);
                if($array_area[0]>0){
                    $queried .= " AND area>=".$array_area[0]." ";
                }
                if($array_area[1]>0){
                    $queried .= " AND area<=".$array_area[1]." ";
                }
            }
            if($row_search_attrs->buildings!=''){
                $array_buildings = explode('-',$row_search_attrs->buildings);
                if($array_buildings[0]>0){
                    $queried .= " AND buildings>=".$array_buildings[0]." ";
                }
                if($array_buildings[1]>0){
                    $queried .= " AND buildings<=".$array_buildings[1]." ";
                }
            }
            if($row_search_attrs->water_drink!='0'){
                $queried .= " AND water_drink='".$row_search_attrs->water_drink."' ";
            }
            if($row_search_attrs->water_irrigation!='0'){
                $queried .= " AND water_irrigation='".$row_search_attrs->water_irrigation."' ";
            }
            if($row_search_attrs->gas!='0'){
                $queried .= " AND gas='".$row_search_attrs->gas."' ";
            }
            if($row_search_attrs->electricity!='0'){
                $queried .= " AND electricity='".$row_search_attrs->electricity."' ";
            }
            if($row_search_attrs->high_voltage_pillars!='0'){
                $queried .= " AND high_voltage_pillars='".$row_search_attrs->high_voltage_pillars."' ";
            }
            if($row_search_attrs->sewage!='0'){
                $queried .= " AND sewage='".$row_search_attrs->sewage."' ";
            }
            if($row_search_attrs->fence!='0'){
                $queried .= " AND fence='".$row_search_attrs->fence."' ";
            }
            if($row_search_attrs->fruit_trees!='0'){
                $queried .= " AND fruit_trees='".$row_search_attrs->fruit_trees."' ";
            }
            $result_posts_in_attr = mwdb_select("SELECT post_id FROM settings_land WHERE $queried ");
            foreach($result_posts_in_attr as $row_in_attr){
                if(in_array($row_in_attr->post_id,$post_filtered_meta)){
                    array_push($posts,$row_in_attr->post_id);
                }
            }
            break;
        }
        case '9':{
            $row_search_attrs = mwdb_get_row("SELECT * FROM client_garages WHERE search_id={var}",array($search_params->search_id));
            $queried = '1=1 ';
            if($row_search_attrs->area!=''){
                $array_area = explode('-',$row_search_attrs->area);
                if($array_area[0]>0){
                    $queried .= " AND area>=".$array_area[0]." ";
                }
                if($array_area[1]>0){
                    $queried .= " AND area<=".$array_area[1]." ";
                }
            }
            if($row_search_attrs->building_type!='0'){
                $queried .= " AND building_type='".$row_search_attrs->building_type."' ";
            }
            $result_posts_in_attr = mwdb_select("SELECT post_id FROM settings_garages WHERE $queried ");
            foreach($result_posts_in_attr as $row_in_attr){
                if(in_array($row_in_attr->post_id,$post_filtered_meta)){
                    array_push($posts,$row_in_attr->post_id);
                }
            }
            break;
        }
        default:{
            $posts = $post_filtered_meta;
            break;
        }
    }
    ?>
    <table class="table table-hover table-bordered" style="text-align:center">
        <tr>
            <th>ID</th>
            <th>Название</th>
            <th>Площадь</th>
            <th>Этаж</th>
            <th>Цена</th>
            <th>Адрес</th>
            <th>Владелец</th>
			<th>Доп. инфо</th>
            <th>Изображение</th>
            <th>Категория</th>
            <?php if($operator=='1'){ ?>
                <th>Оператор</th>
            <?php } ?>
            <th>Дествия</th>
        </tr>
        <?php
        $offers_class = new Offer($user);
        $offers = $offers_class->get();
        $array_offered_posts = array();
        foreach($offers as $offer){
            array_push($array_offered_posts,$offer->post_id);
        }
        foreach($posts as $row_post){
            $post_class = new Post('ru',$row_post);
            $post = $post_class->get();
            $location = '';
            if($post->post_region>0){
                $location .= mwdb_get_var("SELECT region_ru FROM region WHERE region_id={var}",array($post->post_region)).', ';
            }
            if($post->post_city>0){
                $location .= mwdb_get_var("SELECT city_ru FROM city WHERE city_id={var}",array($post->post_city)).', ';
            }
            if($post->post_area>0){
                $location .= mwdb_get_var("SELECT area_ru FROM area WHERE area_id={var}",array($post->post_area)).', ';
            }
            if($post->post_microarea>0){
                $location .= mwdb_get_var("SELECT area_ru FROM microarea WHERE area_id={var}",array($post->post_microarea)).', ';
            }
            if($post->post_street>0){
                $location .= mwdb_get_var("SELECT street_ru FROM streets WHERE street_id={var}",array($post->post_street)).' ';
            }
            $location .= $post->post_address;
            $owner = json_decode($post->post_owner);
            $owner_show = $owner->owner_name.'<br>'.$owner->owner_phone1.'<br>'.$owner->owner_phone2.'<br>'.$owner->owner_email;
            $images = json_decode($post->post_images,true);
            if(isset($images[0]['img']) && $images[0]['img']!=''){
                $image_primary = 'http://user.alex-r.am/images/100/'.$images[0]['img'];
            }
            else{
                $image_primary = 'http://user.alex-r.am/LOGO.png';
            }
            if($post->post_status=='1'){
                $style_row = 'style="background:rgba(255, 255, 0, 0.15)"';
            }
            elseif($post->post_status=='2'){
                $style_row = 'style="background:rgba(210, 236, 161, 0.45);"';
            }
            else{
                $style_row = '';
            }
            $settings = json_decode($post->post_meta);
            $price= '';
            if(isset($settings->sale) && $settings->sale=='1'){
                $price = $settings->price_number.' '.$settings->price_currency;
            }
            if(isset($settings->rent) && $settings->rent=='1'){
                $price .= '<br>Аренда: ';
                if(isset($settings->rent_cost1) && (double)$settings->rent_cost1>0){
                    $price .= $settings->rent_cost1.' '.$settings->rent_currency1;
                }
                elseif(isset($settings->rent_cost2) && (double)$settings->rent_cost2>0){
                    $price .= $settings->rent_cost2.' '.$settings->rent_currency2;
                }
                elseif(isset($settings->rent_cost3) && (double)$settings->rent_cost3>0){
                    $price .= $settings->rent_cost3.' '.$settings->rent_currency3;
                }
            }
            $attributes = json_decode($post->settings);
            if(isset($attributes->area)){
                $area = $attributes->area;
            }
            elseif($attributes->home_area){
                $area = $attributes->home_area;
            }
            else{
                $area = '';
            }
            $fl = '';
            if(isset($attributes->floor) && $attributes->floor!=''){
                $fl .= $attributes->floor;
            }
            if(isset($attributes->floorer) && $attributes->floorer!=''){
                $fl .= '/'.$attributes->floorer;
            }
            if(isset($attributes->rooms) && $attributes->rooms!=''){
                $rooms = $attributes->rooms;
            }
            ?>
            <tr <?php echo $style_row; ?>>
                <td><?php echo $post->post_id; ?></td>
                <td><?php echo $rooms; ?></td>
                <td><?php echo $area; ?> м2</td>
                <td><?php echo $fl; ?></td>
                <td><?php echo $price; ?></td>
                <td><?php echo $location; ?></td>
                <td><?php echo $owner_show; ?></td>
				<td style="width:8%"><?php echo nl2br($post->post_dop); ?></td>
                <td><img src="<?php echo $image_primary; ?>" width="100"></td>
                <td><?php echo mwdb_get_var("SELECT category_title_ru FROM category WHERE category_id={var}",array($post->post_category)); ?></td>
                <?php if($operator=='1'){ ?>
                    <td><?php echo mwdb_get_var("SELECT user_name FROM users WHERE user_id={var}",array($post->post_user)); ?></td>
                <?php } ?>
                <td>
                    <div class="btn-group">
                        <a href="http://alex-r.am/estate/<?php echo $post->post_id; ?>/?user_id=<?php echo $_SESSION['user_id'] ?>" target="_blank" class="btn btn-default">
                            <i class="fa fa-eye" data-toggle="tooltip" title="" data-original-title="Просмотр"></i>
                        </a>
                        <a onclick="offer_to_user(<?php echo $post->post_id; ?>,<?php echo $user; ?>,this)" class="btn <?php if(in_array($post->post_id,$array_offered_posts)){echo 'btn-warning';} else{echo 'btn-success';} ?>">
                            <i class="fa <?php if(in_array($post->post_id,$array_offered_posts)){echo 'fa-check-square-o';} else{echo 'fa-check';} ?>" data-toggle="tooltip" title="" data-original-title="<?php if(in_array($post->post_id,$array_offered_posts)){echo 'Выбран';} else{echo 'Предложить';} ?>"></i>
                        </a>
                    </div>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
    <?php
}
function ad_offer(){
    $client_id = (int)$_POST['client'];
    $post_id = (int)$_POST['post_id'];
    $offer = new Offer($client_id,0,$post_id);
    $offer->insert();
    $offers = $offer->get();
    $i=0;
    foreach($offers as $offer){
        $i++;
    }
    ?>
    <div>
        <div>Количество выбранных недвижимостей для даннего пользователя: <strong style="font-size: 20px"><?php echo $i; ?></strong></div>
        <a href="http://broker.alex-r.am/index.php?action=buyer&subaction=offers&id=<?php echo $client_id; ?>" class="btn btn-block btn-primary" style="float: right;padding: 3px 12px;width: 30%;">Список выбранных</a>
    </div>
    <?php
}
function edit_offer_text_form(){
    $offer_id = (int)$_POST['offer_id'];
    $offer_class = new Offer(0,$offer_id);
    $offer_class->get_by_offer_id();
    ?>
    <textarea class="form-control offer_text" name="offer_text"><?php echo $offer_class->offer_text; ?></textarea>
    <button type="submit" class="btn btn-primary" onclick="save_offer_text(<?php echo $offer_id; ?>,this)" style="margin-top: 10px;">Сохранить</button>
    <?php
}
function save_offer_text(){
    $offer_id = (int)$_POST['offer_id'];
    $text = (string)$_POST['text'];
    $offer = new Offer(0,$offer_id);
    $offer->update_text($text);
    ?>
    <a onclick="edit_offer_text(<?php echo $offer->offer_id; ?>,this)" style="display: inline-block;float: right;cursor: pointer">
        <span class="glyphicon glyphicon-pencil" style="font-size: 20px"></span>
    </a>
    <div class="offer_text_block" style="float: left;">
        <?php echo $offer->offer_text; ?>
    </div>
    <?php
}
function save_region(){
    $id = (int)$_POST['id'];
    $hy = (string)$_POST['hy'];
    $ru = (string)$_POST['ru'];
    $en = (string)$_POST['en'];
    $region = new Region($id,$hy,$ru,$en);
    $region->update();
}
function delete_region(){
    $id = (int)$_POST['id'];
    mwdb_query("DELETE FROM region WHERE region_id={var}",array($id));
}
function delete_city(){
    $id = (int)$_POST['id'];
    mwdb_query("DELETE FROM city WHERE city_id={var}",array($id));
}
function save_city(){
    $id = (int)$_POST['id'];
    $hy = (string)$_POST['hy'];
    $ru = (string)$_POST['ru'];
    $en = (string)$_POST['en'];
    $region = new City($id,$hy,$ru,$en);
    $region->update();
}
function save_area(){
    $id = (int)$_POST['id'];
    $hy = (string)$_POST['hy'];
    $ru = (string)$_POST['ru'];
    $en = (string)$_POST['en'];
    $region = new Area($id,$hy,$ru,$en);
    $region->update();
}
function save_microarea(){
    $id = (int)$_POST['id'];
    $hy = (string)$_POST['hy'];
    $ru = (string)$_POST['ru'];
    $en = (string)$_POST['en'];
    $region = new Microarea($id,$hy,$ru,$en);
    $region->update();
}
function save_street(){
    $id = (int)$_POST['id'];
    $hy = (string)$_POST['hy'];
    $ru = (string)$_POST['ru'];
    $en = (string)$_POST['en'];
    $reg = (int)$_POST['region'];
    $city = (int)$_POST['city'];
    $area = (int)$_POST['area'];
    $microarea = (int)$_POST['microarea'];
    $region = new Street($id,$hy,$ru,$en,$reg,$city,$area,$microarea);
    $region->update();
}
function delete_street(){
    $id = (int)$_POST['id'];
    $street = new Street($id);
    $street->delete();
}
function add_street(){
    $hy = (string)$_POST['hy'];
    $ru = (string)$_POST['ru'];
    $en = (string)$_POST['en'];
    $region = (int)$_POST['region'];
    $city = (int)$_POST['city'];
    $area = (int)$_POST['area'];
    $microarea = (int)$_POST['microarea'];
    if($hy!='' && $ru!='' && $en!='' && $region>0 && $city>0){
        $street = new Street(0,$hy,$ru,$en,$region,$city,$area,$microarea);
        $street->insert();
    }
}
function search_client_by_name(){
    session_start();
    $name = (string)$_POST['name'];
    $user_level = (int)$_POST['user_level'];
    $post_id = (int)$_POST['post_id'];
    if($name!=''){
        if($user_level=='1'){
            $result_clients = mwdb_select("SELECT * FROM post_client WHERE client_status!={var} AND client_name LIKE '%$name%' ORDER BY client_type DESC,client_status,client_time DESC LIMIT 20",array(6));
        }
        else{
            $result_clients = mwdb_select("SELECT * FROM post_client WHERE client_status!={var} AND user_id={var} AND client_name LIKE '%$name%' ORDER BY client_type DESC,client_status,client_time DESC LIMIT 20",array(6,$_SESSION['user_id']));
        }
        get_client_Search_table($result_clients,$post_id);
    }
    else{
        echo '';
    }
}
function search_client_by_phone(){
    session_start();
    $name = (string)$_POST['phone'];
    $user_level = (int)$_POST['user_level'];
    $post_id = (int)$_POST['post_id'];
    if($name!=''){
        if($user_level=='1'){
            $result_clients = mwdb_select("SELECT * FROM post_client WHERE client_status!={var} AND (client_phone1 LIKE '%$name%' OR client_phone2 LIKE '%$name%') ORDER BY client_type DESC,client_status,client_time DESC LIMIT 20",array(6));
        }
        else{
            $result_clients = mwdb_select("SELECT * FROM post_client WHERE client_status!={var} AND user_id={var} AND (client_phone1 LIKE '%$name%' OR client_phone2 LIKE '%$name%') ORDER BY client_type DESC,client_status,client_time DESC LIMIT 20",array(6,$_SESSION['user_id']));
        }
        get_client_Search_table($result_clients,$post_id);
    }
    else{
        echo '';
    }
}
function search_client_by_id(){
    session_start();
    $name = (int)$_POST['id'];
    $user_level = (int)$_POST['user_level'];
    $post_id = (int)$_POST['post_id'];
    if($name!=''){
        if($user_level=='1'){
            $result_clients = mwdb_select("SELECT * FROM post_client WHERE client_status!={var} AND client_id LIKE '$name%' ORDER BY client_type DESC,client_status,client_time DESC LIMIT 20",array(6));
        }
        else{
            $result_clients = mwdb_select("SELECT * FROM post_client WHERE client_status!={var} AND user_id={var} AND client_id LIKE '$name%' ORDER BY client_type DESC,client_status,client_time DESC LIMIT 20",array(6,$_SESSION['user_id']));
        }
        get_client_Search_table($result_clients,$post_id);
    }
    else{
        echo '';
    }
}
function delete_area(){
    $id = (int)$_POST['id'];
    mwdb_query("DELETE FROM area WHERE area_id={var}",array($id));
}
function get_area_microareas(){
    $area = (int)$_POST['area'];
    $result_microareas = mwdb_select("SELECT area_id,area_ru FROM area WHERE city={var} ORDER BY area_order",array($area));
    ?>
    <option value="0">-- Выберите Район --</option>
    <?php
    foreach($result_microareas as $row_micro){
        ?>
        <option value="<?php echo $row_micro->area_id; ?>"><?php echo $row_micro->area_ru; ?></option>
        <?php
    }
}
function delete_microarea(){
    $id = (int)$_POST['id'];
    mwdb_query("DELETE FROM microarea WHERE area_id={var}",array($id));
}
function client_settings_save(){
    $id = (int)$_POST['id'];
    $name = (string)$_POST['name'];
    $phone1 = (string)$_POST['phone1'];
    $phone2 = (string)$_POST['phone2'];
    $mail = (string)$_POST['mail'];
    $text = (string)$_POST['txt'];
    mwdb_query("UPDATE post_client SET client_name='{var}',client_phone1='{var}',client_phone2='{var}',client_email='{var}',client_settings='{var}' WHERE client_id={var}",array($name,$phone1,$phone2,$mail,$text,$id));
}
function get_location_microares(){
    $area = (int)$_POST['area_id'];
    $micro = $_POST['micro'];
    $array_micro = explode(',',$micro);
    $result_microareas = mwdb_select("SELECT area_id,area_ru FROM  microarea WHERE area={var} ORDER BY area_order",array($area));
    foreach($result_microareas as $microarea){
        ?>
        <div class="col-md-12"><input type="checkbox" name="micro_val[]" <?php if(in_array($microarea->area_id,$array_micro)){echo 'checked';} ?> class="micro_val" value="<?php echo $microarea->area_id ?>"><?php echo $microarea->area_ru; ?></div>
        <?php
    }
}
function get_all_filter_cat_settings(){
    $cat = (int)$_POST['cat'];
    switch($cat){
        case '5':{
            ?>
            <div class="col-md-12">
                <div class="col-md-2">Комнаты</div>
                <div class="col-md-9">
                    <input type="checkbox" name="rooms[]" value="1">1
                    <input type="checkbox" name="rooms[]" value="2" style="margin-left: 9px">2
                    <input type="checkbox" name="rooms[]" value="3" style="margin-left: 9px">3
                    <input type="checkbox" name="rooms[]" value="4" style="margin-left: 9px">4
                    <input type="checkbox" name="rooms[]" value="5" style="margin-left: 9px">5
					<input type="checkbox" name="rooms[]" value="6" style="margin-left: 9px">6
					<input type="checkbox" name="rooms[]" value="7" style="margin-left: 9px">7
					<input type="checkbox" name="rooms[]" value="8" style="margin-left: 9px">8
					<input type="checkbox" name="rooms[]" value="9" style="margin-left: 9px">9
					<input type="checkbox" name="rooms[]" value="10" style="margin-left: 9px">10+
                </div>
            </div>
			<div class="col-md-6">
            <div class="col-md-12">
                <div class="col-md-2" style="padding-top: 4px;">Этаж</div>
                <div class="col-md-9">
                    <div class="col-md-4">
                        <select name="floor_from" class="form-control" style="height: 20px">
                            <option value="">От</option>
                            <?php
                            for($i=1;$i<21;$i++){
                                ?>
                                <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-1" style="text-align: center">_</div>
                    <div class="col-md-4">
                        <select name="floor_to" class="form-control" style="height: 20px">
                            <option value="">До</option>
                            <?php
                            for($i=1;$i<21;$i++){
                                ?>
                                <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-2" style="padding-top: 4px;">Площадь</div>
                <div class="col-md-9">
                    <div class="col-md-4">
                        <input type="text" name="area_from" class="form-control" style="height: 20px">
                    </div>
                    <div class="col-md-1" style="text-align: center">_</div>
                    <div class="col-md-4">
                        <input type="text" name="area_to" class="form-control" style="height: 20px">
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-2" style="padding-top: 4px;">Цена</div>
                <div class="col-md-9">
                    <div class="col-md-4">
                        <input type="text" name="price_from" class="form-control" style="height: 20px" placeholder="USD">
                    </div>
                    <div class="col-md-1" style="text-align: center">_</div>
                    <div class="col-md-4">
                        <input type="text" name="price_to" class="form-control" style="height: 20px" placeholder="USD">
                    </div>
                </div>
            </div>
			</div>
			<div class="col-md-6">
				<div class="col-md-12"><input type="checkbox" name="building_type[]" value="building_type1"> монолитный</div>
				<div class="col-md-12"><input type="checkbox" name="building_type[]" value="building_type2"> каменный</div>
				<div class="col-md-12"><input type="checkbox" name="building_type[]" value="building_type3"> крупнопанельный</div>
				<div class="col-md-12"><input type="checkbox" name="building_type[]" value="building_type4"> каркаснопанельный</div>
			</div>
            <?php
            break;
        }
        case '6':{
            ?>
            <div class="col-md-12">
                <div class="col-md-2">Комнаты</div>
                <div class="col-md-9">
                    <input type="checkbox" name="rooms[]" value="1">1
                    <input type="checkbox" name="rooms[]" value="2" style="margin-left: 9px">2
                    <input type="checkbox" name="rooms[]" value="3" style="margin-left: 9px">3
                    <input type="checkbox" name="rooms[]" value="4" style="margin-left: 9px">4
                    <input type="checkbox" name="rooms[]" value="5" style="margin-left: 9px">5
					<input type="checkbox" name="rooms[]" value="6" style="margin-left: 9px">6
					<input type="checkbox" name="rooms[]" value="7" style="margin-left: 9px">7
					<input type="checkbox" name="rooms[]" value="8" style="margin-left: 9px">8
					<input type="checkbox" name="rooms[]" value="9" style="margin-left: 9px">9
					<input type="checkbox" name="rooms[]" value="10" style="margin-left: 9px">10+
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-2" style="padding-top: 4px;">Этажность</div>
                <div class="col-md-9">
                    <div class="col-md-2">
                        <select name="floor_from" class="form-control" style="height: 20px">
                            <option value="">От</option>
                            <?php
                            for($i=1;$i<6;$i++){
                                ?>
                                <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-1" style="text-align: center">_</div>
                    <div class="col-md-2">
                        <select name="floor_to" class="form-control" style="height: 20px">
                            <option value="">До</option>
                            <?php
                            for($i=1;$i<6;$i++){
                                ?>
                                <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-2" style="padding-top: 4px;">Площадь</div>
                <div class="col-md-9">
                    <div class="col-md-2">
                        <input type="text" name="area_from" class="form-control" style="height: 20px">
                    </div>
                    <div class="col-md-1" style="text-align: center">_</div>
                    <div class="col-md-2">
                        <input type="text" name="area_to" class="form-control" style="height: 20px">
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-2" style="padding-top: 4px;">Цена</div>
                <div class="col-md-9">
                    <div class="col-md-2">
                        <input type="text" name="price_from" class="form-control" style="height: 20px" placeholder="USD">
                    </div>
                    <div class="col-md-1" style="text-align: center">_</div>
                    <div class="col-md-2">
                        <input type="text" name="price_to" class="form-control" style="height: 20px" placeholder="USD">
                    </div>
                </div>
            </div>
            <?php
            break;
        }
        case '7':{
            ?>
            <div class="col-md-12">
                <div class="col-md-2" style="padding-top: 4px;">Тип</div>
                <div class="col-md-9">
                    <div class="col-md-4">
                        <select name="type" class="form-control" style="height: 20px">
                            <option value="">-- Выбрать --</option>
                            <option value="cm_type2">гостиница</option>
                            <option value="cm_type1">оффис</option>
                            <option value="cm_type3">развлекательный</option>
                            <option value="cm_type4">магазин</option>
                            <option value="cm_type5">парикмахерская</option>
                            <option value="cm_type6">автотехобслуживание</option>
                            <option value="cm_type7">склад</option>
                            <option value="cm_type8">сельскохозяйственный</option>
                            <option value="cm_type9">бизнес центр</option>
                            <option value="cm_type10">производственный</option>
                            <option value="cm_type11">учебный</option>
                            <option value="cm_type12">медицинские заведения</option>
                            <option value="cm_type13">универсальный</option>
                            <option value="cm_type14">другое</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-2" style="padding-top: 4px;">Площадь</div>
                <div class="col-md-9">
                    <div class="col-md-2">
                        <input type="text" name="area_from" class="form-control" style="height: 20px">
                    </div>
                    <div class="col-md-1" style="text-align: center">_</div>
                    <div class="col-md-2">
                        <input type="text" name="area_to" class="form-control" style="height: 20px">
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-2" style="padding-top: 4px;">Цена</div>
                <div class="col-md-9">
                    <div class="col-md-2">
                        <input type="text" name="price_from" class="form-control" style="height: 20px" placeholder="USD">
                    </div>
                    <div class="col-md-1" style="text-align: center">_</div>
                    <div class="col-md-2">
                        <input type="text" name="price_to" class="form-control" style="height: 20px" placeholder="USD">
                    </div>
                </div>
            </div>
            <?php
            break;
        }
        case '8':{
            ?>
            <div class="col-md-12">
                <div class="col-md-2" style="padding-top: 4px;">Площадь</div>
                <div class="col-md-9">
                    <div class="col-md-2">
                        <input type="text" name="area_from" class="form-control" style="height: 20px">
                    </div>
                    <div class="col-md-1" style="text-align: center">_</div>
                    <div class="col-md-2">
                        <input type="text" name="area_to" class="form-control" style="height: 20px">
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-2" style="padding-top: 4px;">Цена</div>
                <div class="col-md-9">
                    <div class="col-md-2">
                        <input type="text" name="price_from" class="form-control" style="height: 20px" placeholder="USD">
                    </div>
                    <div class="col-md-1" style="text-align: center">_</div>
                    <div class="col-md-2">
                        <input type="text" name="price_to" class="form-control" style="height: 20px" placeholder="USD">
                    </div>
                </div>
            </div>
            <?php
            break;
        }
        case '9':{
            ?>
            <div class="col-md-12">
                <div class="col-md-2" style="padding-top: 4px;">Площадь</div>
                <div class="col-md-9">
                    <div class="col-md-2">
                        <input type="text" name="area_from" class="form-control" style="height: 20px">
                    </div>
                    <div class="col-md-1" style="text-align: center">_</div>
                    <div class="col-md-2">
                        <input type="text" name="area_to" class="form-control" style="height: 20px">
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-2" style="padding-top: 4px;">Цена</div>
                <div class="col-md-9">
                    <div class="col-md-2">
                        <input type="text" name="price_from" class="form-control" style="height: 20px" placeholder="USD">
                    </div>
                    <div class="col-md-1" style="text-align: center">_</div>
                    <div class="col-md-2">
                        <input type="text" name="price_to" class="form-control" style="height: 20px" placeholder="USD">
                    </div>
                </div>
            </div>
            <?php
            break;
        }
    }
}
function get_f_city(){
    $region = $_POST['region'];
    ?>
    <option value="0">-- Город/община --</option>
    <?php
    $result_cities = mwdb_select("SELECT city_id,city_ru FROM city WHERE region={var} ORDER BY city_order",array($region));
    foreach ($result_cities as $result_city) {
        ?>
        <option value="<?php echo $result_city->city_id; ?>"><?php echo $result_city->city_ru; ?></option>
        <?php
    }
}
function get_f_area(){
    $city = $_POST['city'];
    ?>
    <option value="0">-- Район --</option>
    <?php
    $result_areas = mwdb_select("SELECT area_id,area_ru FROM area WHERE city={var} ORDER BY area_order",array($city));
    foreach($result_areas as $row_area){
        ?>
        <option value="<?php echo $row_area->area_id; ?>"><?php echo $row_area->area_ru; ?></option>
        <?php
    }
}
function get_f_microarea(){
    $area = $_POST['area'];
    ?>
    <option value="0">-- Микрорайон --</option>
    <?php
    $result_areas = mwdb_select("SELECT area_id,area_ru FROM microarea WHERE area={var} ORDER BY area_order",array($area));
    foreach($result_areas as $row_area){
        ?>
        <option value="<?php echo $row_area->area_id; ?>"><?php echo $row_area->area_ru; ?></option>
        <?php
    }
}
function get_f_streets(){
    $l = $_POST['latter'];
    $region = (int)$_POST['region'];
    $city = (int)$_POST['city'];
    $area = (int)$_POST['area'];
    $microarea = (int)$_POST['microarea'];
    $qu = '';
    if($region!='0'){
        $qu .= " region=".$region." AND ";
    }
    if($city!='0'){
        $qu .= " city=".$city." AND ";
    }
    if($area!='0'){
        $qu .= " area=".$area." AND ";
    }
    if($microarea!='0'){
        $qu .= " microarea=".$microarea." AND ";
    }
    if($l!='') {
        $result_streets = mwdb_select("SELECT street_ru FROM streets WHERE $qu (street_hy LIKE '%{var}%' OR street_ru LIKE '%{var}%' OR street_en LIKE '%{var}%') GROUP BY street_ru", array($l, $l, $l));
        foreach($result_streets as $row_street){
            $result_ids = mwdb_select("SELECT street_id FROM streets WHERE street_ru='{var}'",array($row_street->street_ru));
            $id = '';
            foreach($result_ids as $row_id){
                $id .= $row_id->street_id.',';
            }
            $id = substr($id,0,-1);
            ?>
            <div class="col-md-12">
                <a style="cursor: pointer;" onclick="put_street('<?php echo $id; ?>',this);"><?php echo $row_street->street_ru; ?></a>
            </div>
            <?php
        }
    }
}
function delete_buyer_ajax(){
	$id = (int)$_POST['id'];
	$post = new Client($id);
	$post->delete();
}
function ajax_remove_fromoffer(){
	$offer_id = (int)$_POST['id'];
	$offer = new Offer(0,$offer_id);
	$offer->get_by_offer_id();
	$client_id = $offer->client_id;
	$offer->delete();
}
function open_br_post_comment(){
	session_start();
	$result_messages = mwdb_select("SELECT * FROM broker_note_post WHERE post_id={var} ORDER BY note_id",array($_POST['id']));
	?>
	<div class="direct-chat-messages" id="yourDivID">
	<?php
	$i=0;
	foreach($result_messages as $row_message){
		$row_user = mwdb_get_row("SELECT user_id,user_name,user_type FROM users WHERE user_id={var}",array($row_message->user_id));
		if($row_user->user_id==$_SESSION['user_id']){
			?>
			<div class="direct-chat-msg">
			  <div class="direct-chat-info clearfix">
				<span class="direct-chat-name pull-left"><?=$row_user->user_name?></span>
				<span class="direct-chat-timestamp pull-right"><?=date("H:i d.m.Y",strtotime($row_message->note_time)+10800)?></span>
			  </div><!-- /.direct-chat-info -->
			  <img class="direct-chat-img" src="http://broker.alex-r.am/images/<?=$row_user->user_type?>.png" alt="message user image"><!-- /.direct-chat-img -->
			  <div class="direct-chat-text">
				<?=$row_message->note_text?>
			  </div><!-- /.direct-chat-text -->
			</div><!-- /.direct-chat-msg -->
			<?php
		}
		else{
			?>
			<div class="direct-chat-msg right">
			  <div class="direct-chat-info clearfix">
				<span class="direct-chat-name pull-right"><?=$row_user->user_name?></span>
				<span class="direct-chat-timestamp pull-left"><?=date("H:i d.m.Y",strtotime($row_message->note_time)+10800)?></span>
			  </div><!-- /.direct-chat-info -->
			  <img class="direct-chat-img" src="http://broker.alex-r.am/images/<?=$row_user->user_type?>.png" alt="message user image"><!-- /.direct-chat-img -->
			  <div class="direct-chat-text">
				<?=$row_message->note_text?>
			  </div><!-- /.direct-chat-text -->
			</div><!-- /.direct-chat-msg -->  
			<?php
		}
		$i++;
	}
	?>
	<div class="input-group">
	  <input type="text" name="message" placeholder="Комментария" class="form-control message" onkeypress="send_post_comment1(this,event)">
	  <input type="hidden" name="pid" value="<?=$_POST['id']?>" class="pid">
	  <span class="input-group-btn">
		<button type="button" onclick="send_post_comment(this)" class="btn btn-primary btn-flat">Отправить</button>
	  </span>
	</div>
	  </div>
	<?php
}
function send_post_comment(){
	session_start();
	mwdb_query("INSERT INTO broker_note_post(user_id,post_id,note_text) VALUES ('{var}','{var}','{var}')",array($_SESSION['user_id'],(int)$_POST['id'],(string)$_POST['message']));
}
?>