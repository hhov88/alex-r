<?php
function base_url(){
    return "http://" . $_SERVER['SERVER_NAME']."/";
}
function search_result_for_all($user_level,$search_options,$user_id,$client_id=0){
    $translations = new Translations();
    $translation = $translations->get_lang_translations('ru');
    $options = json_decode($search_options);
    ?>
    <div class="col-md-12 no-padding">
        <div class="box collapsed-box">
            <div class="box-header with-border">
                <h3 class="box-title" style="color: #199AC0;">Параметры поиска</h3>
                <div class="box-tools pull-right">
                    <a onclick="return confirm('Прежние параметры поиска будут удалены! Вы действительно хотите изменить параметры поиска?')" href="http://user.alex-r.am/index.php?action=buyer&subaction=delete_search_parametrs&id=<?php echo $client_id; ?>">Новые параметры поиска <i class="fa fa-edit"></i></a>
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title=""><i class="fa fa-plus"></i></button>
                </div>
            </div>
            <div class="box-body" style="display: none">
                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                    <div class="col-md-3" style="font-size: 18px;">Тип предложений:</div>
                    <div class="col-md-9">
                        <?php
                        if($options->type=='sale'){
                            echo 'Продажа';
                        }
                        else{
                            echo 'Аренда';
                        }
                        ?>
                    </div>
                </div>
                <?php if((double)$options->price->from>0 || (double)$options->price->to>0){ ?>
                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                    <div class="col-md-3" style="font-size: 18px;">Цена:</div>
                    <div class="col-md-9">
                        <div class="col-md-12">
                            <div class="col-md-3" style="text-align: center">От:</div>
                            <div class="col-md-3" style="text-align: center">До:</div>
                            <div class="col-md-3" style="text-align: center">Валюта:</div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-3" style="text-align: center"><?php echo $options->price->from; ?></div>
                            <div class="col-md-3" style="text-align: center"><?php echo $options->price->to; ?></div>
                            <div class="col-md-3" style="text-align: center"><?php echo $options->price->currency; ?></div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if($options->location->region>0){ ?>
                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                    <div class="col-md-3" style="font-size: 18px;">Местонахождение:</div>
                    <div class="col-md-9">
                        <?php
                        $location = '';
                        if($options->location->region>0){
                            //$location .= mwdb_get_var("SELECT region_ru FROM region WHERE region_id={var}",array($options->location->region)).', ';
                        }
                        if(count((array)$options->location->city)>0){
                            foreach($options->location->city as $city){
                                $location .= mwdb_get_var("SELECT city_ru FROM city WHERE city_id={var}",array($city)).', ';
                            }
                        }
                        if(count((array)$options->location->area)>0){
                            foreach($options->location->area as $area) {
                                $location .= mwdb_get_var("SELECT area_ru FROM area WHERE area_id={var}", array($area)) . ', ';
                            }
                        }
                        if(count((array)$options->location->microarea)>0){
                            foreach($options->location->microarea as $area) {
                                $location .= mwdb_get_var("SELECT area_ru FROM  microarea WHERE area_id={var}", array($area)) . ', ';
                            }
                        }
                        if($options->location->street>0){
                            //$location .= mwdb_get_var("SELECT street_ru FROM streets WHERE street_id={var}",array($options->location->street)).' ';
                        }
                        echo substr($location,0,-2);
                        ?>
                    </div>
                </div>
                <?php } ?>
                <?php if($options->category>0){ ?>
                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                    <div class="col-md-3" style="font-size: 18px;">Категория:</div>
                    <div class="col-md-9">
                        <?php echo mwdb_get_var("SELECT category_title_ru FROM category WHERE category_id={var}",array($options->category)); ?>
                    </div>
                </div>
                <?php
                    switch($options->category){
                        case '5':{
                            if((double)$options->attributes->area->from>0 || (double)$options->attributes->area->to>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Площадь:</div>
                                    <div class="col-md-9">
                                        <div class="col-md-12">
                                            <div class="col-md-3" style="text-align: center">От:</div>
                                            <div class="col-md-3" style="text-align: center">До:</div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-3" style="text-align: center"><?php echo $options->attributes->area->from; ?></div>
                                            <div class="col-md-3" style="text-align: center"><?php echo $options->attributes->area->to; ?></div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            if(count((array)$options->attributes->rooms)>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Количество комнат:</div>
                                    <div class="col-md-9">
                                        <?php
                                        $rooms = '';
                                        foreach($options->attributes->rooms as $room){
                                            $rooms .= $room.', ';
                                        }
                                        echo substr($rooms,0,-2);
                                        ?>
                                    </div>
                                </div>
                                <?php
                            }
                            if((double)$options->attributes->floorer->from>0 || (double)$options->attributes->floorer->to>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Количество этажей:</div>
                                    <div class="col-md-9">
                                        <div class="col-md-12">
                                            <div class="col-md-3" style="text-align: center">От:</div>
                                            <div class="col-md-3" style="text-align: center">До:</div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-3" style="text-align: center"><?php echo $options->attributes->floorer->from; ?></div>
                                            <div class="col-md-3" style="text-align: center"><?php echo $options->attributes->floorer->to; ?></div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                            if((double)$options->attributes->floor->from>0 || (double)$options->attributes->floor->to>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">На каком этаже:</div>
                                    <div class="col-md-9">
                                        <div class="col-md-12">
                                            <div class="col-md-3" style="text-align: center">От:</div>
                                            <div class="col-md-3" style="text-align: center">До:</div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-3" style="text-align: center"><?php echo $options->attributes->floor->from; ?></div>
                                            <div class="col-md-3" style="text-align: center"><?php echo $options->attributes->floor->to; ?></div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                            if(count((array)$options->attributes->floor_name)>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Название этажа:</div>
                                    <div class="col-md-9">
                                        <?php
                                        $rooms = '';
                                        foreach($options->attributes->floor_name as $floor_name){
                                            $rooms .= $translation[$floor_name].', ';
                                        }
                                        echo substr($rooms,0,-2);
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if($options->attributes->basement=='1'){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Подвал:</div>
                                    <div class="col-md-9">
                                        Да
                                    </div>
                                </div>
                            <?php
                            }
                            if(isset($options->attributes->water) && $options->attributes->water!='0'){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Вода:</div>
                                    <div class="col-md-9">
                                        <?php
                                        echo $translation[$options->attributes->water];
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if(isset($options->attributes->gas) && $options->attributes->gas!='0'){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Газ:</div>
                                    <div class="col-md-9">
                                        <?php
                                        echo $translation[$options->attributes->gas];
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if(count((array)$options->attributes->repair)>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Отделка:</div>
                                    <div class="col-md-9">
                                        <?php
                                        $rooms = '';
                                        foreach($options->attributes->repair as $floor_name){
                                            $rooms .= $translation[$floor_name].', ';
                                        }
                                        echo substr($rooms,0,-2);
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if(count((array)$options->attributes->balcony)>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Балкон:</div>
                                    <div class="col-md-9">
                                        <?php
                                        $rooms = '';
                                        foreach($options->attributes->balcony as $floor_name){
                                            $rooms .= $translation[$floor_name].', ';
                                        }
                                        echo substr($rooms,0,-2);
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if(count((array)$options->attributes->garage)>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Гараж:</div>
                                    <div class="col-md-9">
                                        <?php
                                        $rooms = '';
                                        foreach($options->attributes->garage as $floor_name){
                                            $rooms .= $translation[$floor_name].', ';
                                        }
                                        echo substr($rooms,0,-2);
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if(count((array)$options->attributes->building_project)>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Проект здания:</div>
                                    <div class="col-md-9">
                                        <?php
                                        $rooms = '';
                                        foreach($options->attributes->building_project as $floor_name){
                                            $rooms .= $translation[$floor_name].', ';
                                        }
                                        echo substr($rooms,0,-2);
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if(count((array)$options->attributes->building_type)>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Тип здания:</div>
                                    <div class="col-md-9">
                                        <?php
                                        $rooms = '';
                                        foreach($options->attributes->building_type as $floor_name){
                                            $rooms .= $translation[$floor_name].', ';
                                        }
                                        echo substr($rooms,0,-2);
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if(count((array)$options->attributes->building_position)>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Положение здания к улице:</div>
                                    <div class="col-md-9">
                                        <?php
                                        $rooms = '';
                                        foreach($options->attributes->building_position as $floor_name){
                                            $rooms .= $translation[$floor_name].', ';
                                        }
                                        echo substr($rooms,0,-2);
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if(count((array)$options->attributes->housetop)>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Тип покрытия:</div>
                                    <div class="col-md-9">
                                        <?php
                                        $rooms = '';
                                        foreach($options->attributes->housetop as $floor_name){
                                            $rooms .= $translation[$floor_name].', ';
                                        }
                                        echo substr($rooms,0,-2);
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            break;
                        }
                        case '6':{
                            if((double)$options->attributes->total_area->from>0 || (double)$options->attributes->total_area->to>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Общая площадь:</div>
                                    <div class="col-md-9">
                                        <div class="col-md-12">
                                            <div class="col-md-3" style="text-align: center">От:</div>
                                            <div class="col-md-3" style="text-align: center">До:</div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-3" style="text-align: center"><?php echo $options->attributes->total_area->from; ?></div>
                                            <div class="col-md-3" style="text-align: center"><?php echo $options->attributes->total_area->to; ?></div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                            if((double)$options->attributes->home_area->from>0 || (double)$options->attributes->home_area->to>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Площадь дома:</div>
                                    <div class="col-md-9">
                                        <div class="col-md-12">
                                            <div class="col-md-3" style="text-align: center">От:</div>
                                            <div class="col-md-3" style="text-align: center">До:</div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-3" style="text-align: center"><?php echo $options->attributes->home_area->from; ?></div>
                                            <div class="col-md-3" style="text-align: center"><?php echo $options->attributes->home_area->to; ?></div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                            if(count((array)$options->attributes->rooms)>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Количество комнат:</div>
                                    <div class="col-md-9">
                                        <?php
                                        $rooms = '';
                                        foreach($options->attributes->rooms as $room){
                                            $rooms .= $room.', ';
                                        }
                                        echo substr($rooms,0,-2);
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if((double)$options->attributes->floorer->from>0 || (double)$options->attributes->floorer->to>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Количество этажей:</div>
                                    <div class="col-md-9">
                                        <div class="col-md-12">
                                            <div class="col-md-3" style="text-align: center">От:</div>
                                            <div class="col-md-3" style="text-align: center">До:</div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-3" style="text-align: center"><?php echo $options->attributes->floorer->from; ?></div>
                                            <div class="col-md-3" style="text-align: center"><?php echo $options->attributes->floorer->to; ?></div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                            if(count((array)$options->attributes->repair)>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Отделка:</div>
                                    <div class="col-md-9">
                                        <?php
                                        $rooms = '';
                                        foreach($options->attributes->repair as $floor_name){
                                            $rooms .= $translation[$floor_name].', ';
                                        }
                                        echo substr($rooms,0,-2);
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if(count((array)$options->attributes->balcony)>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Балкон:</div>
                                    <div class="col-md-9">
                                        <?php
                                        $rooms = '';
                                        foreach($options->attributes->balcony as $floor_name){
                                            $rooms .= $translation[$floor_name].', ';
                                        }
                                        echo substr($rooms,0,-2);
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if(count((array)$options->attributes->garage)>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Гараж:</div>
                                    <div class="col-md-9">
                                        <?php
                                        $rooms = '';
                                        foreach($options->attributes->garage as $floor_name){
                                            $rooms .= $translation[$floor_name].', ';
                                        }
                                        echo substr($rooms,0,-2);
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if(isset($options->attributes->basement) && $options->attributes->basement=='1'){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Подвал:</div>
                                    <div class="col-md-9">
                                        Да
                                    </div>
                                </div>
                            <?php
                            }
                            if(isset($options->attributes->water) && $options->attributes->water!='0'){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Вода:</div>
                                    <div class="col-md-9">
                                        <?php
                                        echo $translation[$options->attributes->water];
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if(isset($options->attributes->gas) && $options->attributes->gas!='0'){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Газ:</div>
                                    <div class="col-md-9">
                                        <?php
                                        echo $translation[$options->attributes->gas];
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if(isset($options->attributes->heating_system) && $options->attributes->heating_system!='0'){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Система отопления:</div>
                                    <div class="col-md-9">
                                        <?php
                                        echo $translation[$options->attributes->heating_system];
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            break;
                        }
                        case '7':{
                            if(count((array)$options->attributes->type)>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Тип:</div>
                                    <div class="col-md-9">
                                        <?php
                                        $rooms = '';
                                        foreach($options->attributes->type as $floor_name){
                                            $rooms .= $translation[$floor_name].', ';
                                        }
                                        echo substr($rooms,0,-2);
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if((double)$options->attributes->area->from>0 || (double)$options->attributes->area->to>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Площадь:</div>
                                    <div class="col-md-9">
                                        <div class="col-md-12">
                                            <div class="col-md-3" style="text-align: center">От:</div>
                                            <div class="col-md-3" style="text-align: center">До:</div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-3" style="text-align: center"><?php echo $options->attributes->area->from; ?></div>
                                            <div class="col-md-3" style="text-align: center"><?php echo $options->attributes->area->to; ?></div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                            if(count((array)$options->attributes->rooms)>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Количество комнат:</div>
                                    <div class="col-md-9">
                                        <?php
                                        $rooms = '';
                                        foreach($options->attributes->rooms as $room){
                                            $rooms .= $room.', ';
                                        }
                                        echo substr($rooms,0,-2);
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if((double)$options->attributes->floor->from>-4 || (double)$options->attributes->floor->to>-4){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">На каком этаже:</div>
                                    <div class="col-md-9">
                                        <div class="col-md-12">
                                            <div class="col-md-3" style="text-align: center">От:</div>
                                            <div class="col-md-3" style="text-align: center">До:</div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-3" style="text-align: center"><?php echo $options->attributes->floor->from; ?></div>
                                            <div class="col-md-3" style="text-align: center"><?php echo $options->attributes->floor->to; ?></div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                            if(count((array)$options->attributes->floor_name)>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Название этажа:</div>
                                    <div class="col-md-9">
                                        <?php
                                        $rooms = '';
                                        foreach($options->attributes->floor_name as $floor_name){
                                            $rooms .= $translation[$floor_name].', ';
                                        }
                                        echo substr($rooms,0,-2);
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if(isset($options->attributes->showcases) && $options->attributes->showcases!='0'){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Наличие витрин:</div>
                                    <div class="col-md-9">
                                        <?php
                                        echo $translation[$options->attributes->showcases];
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if(isset($options->attributes->entrance) && $options->attributes->entrance!='0'){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Вход:</div>
                                    <div class="col-md-9">
                                        <?php
                                        echo $translation[$options->attributes->entrance];
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if(isset($options->attributes->repair) && $options->attributes->repair!='0'){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Состояние:</div>
                                    <div class="col-md-9">
                                        <?php
                                        echo $translation[$options->attributes->repair];
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if(count((array)$options->attributes->building_type)>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Тип здания:</div>
                                    <div class="col-md-9">
                                        <?php
                                        $rooms = '';
                                        foreach($options->attributes->building_type as $floor_name){
                                            $rooms .= $translation[$floor_name].', ';
                                        }
                                        echo substr($rooms,0,-2);
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if(count((array)$options->attributes->building_position)>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Положение здания к улице:</div>
                                    <div class="col-md-9">
                                        <?php
                                        $rooms = '';
                                        foreach($options->attributes->building_position as $floor_name){
                                            $rooms .= $translation[$floor_name].', ';
                                        }
                                        echo substr($rooms,0,-2);
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            break;
                        }
                        case '8':{
                            if((double)$options->attributes->area->from>0 || (double)$options->attributes->area->to>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Площадь:</div>
                                    <div class="col-md-9">
                                        <div class="col-md-12">
                                            <div class="col-md-3" style="text-align: center">От:</div>
                                            <div class="col-md-3" style="text-align: center">До:</div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-3" style="text-align: center"><?php echo $options->attributes->area->from; ?></div>
                                            <div class="col-md-3" style="text-align: center"><?php echo $options->attributes->area->to; ?></div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                            if((double)$options->attributes->buildings->from>0 || (double)$options->attributes->buildings->to>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Наличие постройки(Площадь):</div>
                                    <div class="col-md-9">
                                        <div class="col-md-12">
                                            <div class="col-md-3" style="text-align: center">От:</div>
                                            <div class="col-md-3" style="text-align: center">До:</div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-3" style="text-align: center"><?php echo $options->attributes->buildings->from; ?></div>
                                            <div class="col-md-3" style="text-align: center"><?php echo $options->attributes->buildings->to; ?></div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                            if(isset($options->attributes->water_drink) && $options->attributes->water_drink!='0'){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Вода (питьевая):</div>
                                    <div class="col-md-9">
                                        <?php
                                        echo $translation[$options->attributes->water_drink];
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if(isset($options->attributes->water_irrigation) && $options->attributes->water_irrigation!='0'){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Вода (ирригационная):</div>
                                    <div class="col-md-9">
                                        <?php
                                        echo $translation[$options->attributes->water_irrigation];
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if(isset($options->attributes->gas) && $options->attributes->gas!='0'){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Газ:</div>
                                    <div class="col-md-9">
                                        <?php
                                        echo $translation[$options->attributes->gas];
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if(isset($options->attributes->electricity) && $options->attributes->electricity!='0'){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Ток:</div>
                                    <div class="col-md-9">
                                        <?php
                                        echo $translation[$options->attributes->electricity];
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if(isset($options->attributes->high_voltage_pillars) && $options->attributes->high_voltage_pillars!='0'){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Высоковольтные столбы:</div>
                                    <div class="col-md-9">
                                        <?php
                                        echo $translation[$options->attributes->high_voltage_pillars];
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if(isset($options->attributes->sewage) && $options->attributes->sewage!='0'){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Канализация:</div>
                                    <div class="col-md-9">
                                        <?php
                                        echo $translation[$options->attributes->sewage];
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if(isset($options->attributes->fence) && $options->attributes->fence!='0'){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Ограждение:</div>
                                    <div class="col-md-9">
                                        <?php
                                        echo $translation[$options->attributes->fence];
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            if(isset($options->attributes->fruit_trees) && $options->attributes->fruit_trees!='0'){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Плодовые деревья:</div>
                                    <div class="col-md-9">
                                        <?php
                                        echo $translation[$options->attributes->fruit_trees];
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            break;
                        }
                        case '9':{
                            if((double)$options->attributes->area->from>0 || (double)$options->attributes->area->to>0){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Площадь:</div>
                                    <div class="col-md-9">
                                        <div class="col-md-12">
                                            <div class="col-md-3" style="text-align: center">От:</div>
                                            <div class="col-md-3" style="text-align: center">До:</div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-3" style="text-align: center"><?php echo $options->attributes->area->from; ?></div>
                                            <div class="col-md-3" style="text-align: center"><?php echo $options->attributes->area->to; ?></div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                            if(isset($options->attributes->building_type) && $options->attributes->building_type!='0'){
                                ?>
                                <div class="col-md-12" style="border-bottom: 1px solid #82A0C0;margin-bottom: 20px;padding-bottom: 20px">
                                    <div class="col-md-3" style="font-weight: bold;">Тип здания:</div>
                                    <div class="col-md-9">
                                        <?php
                                        echo $translation[$options->attributes->building_type];
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                            break;
                        }
                    }
                }
                ?>
            </div><!-- /.box-body -->
            <div class="box-footer" style="display: none">
            </div><!-- /.box-footer-->
        </div>
    </div>
    <h4 style="color: #199AC0;">Результат поиска</h4>
    <table class="table table-hover table-bordered" style="text-align:center">
        <tr>
            <th>ID</th>
            <th>Название</th>
            <?php if($user_level=='1'){ ?>
                <th>Оператор</th>
            <?php } ?>
            <th>Адрес</th>
            <th>Владелец</th>
            <th>Изображение</th>
            <th>Категория</th>
            <th>Дествия</th>
        </tr>
        <?php
        if($user_level=='1'){
            $query_part = '';
        }
        else{
            $query_part = "posts.post_user=$user_id AND ";
        }
        $array_post = '';
        if($options->type=='sale'){
            $result_post_sales = mwdb_select("SELECT post_id FROM post_meta WHERE meta_key='{var}' AND meta_value='{var}'",array('sale',1));
            foreach($result_post_sales as $post_sale){
                $array_post .= $post_sale->post_id.',';
            }
            $array_post = substr($array_post,0,-1);
            if(isset($options->price->from) && (double)$options->price->from>0){
                $result_post_price_from = mwdb_select("SELECT post_id FROM post_meta WHERE meta_key='{var}' AND meta_value>={var} AND post_id IN ($array_post)",array('price_number',$options->price->from));
                $array_post = '';
                foreach($result_post_price_from as $post_price_from){
                    $array_post .= $post_price_from->post_id.',';
                }
                $array_post = substr($array_post,0,-1);
            }
            if(isset($options->price->to) && (double)$options->price->to>0){
                $result_post_price_to = mwdb_select("SELECT post_id FROM post_meta WHERE meta_key='{var}' AND meta_value<={var} AND post_id IN ($array_post)",array('price_number',$options->price->to));
                $array_post = '';
                foreach($result_post_price_to as $post_price_to){
                    $array_post .= $post_price_to->post_id.',';
                }
                $array_post = substr($array_post,0,-1);
            }
            if(isset($options->price->currency)){
                $result_post_price_currency = mwdb_select("SELECT post_id FROM post_meta WHERE meta_key='{var}' AND meta_value='{var}' AND post_id IN ($array_post)",array('price_currency',$options->price->currency));
                $array_post = '';
                foreach($result_post_price_currency as $post_price_currency){
                    $array_post .= $post_price_currency->post_id.',';
                }
                $array_post = substr($array_post,0,-1);
            }
        }
        elseif($options->type=='rent'){
            $result_post_rents = mwdb_select("SELECT post_id FROM post_meta WHERE meta_key='{var}' AND meta_value='{var}'",array('rent',1));
            foreach($result_post_rents as $post_rent){
                $array_post .= $post_rent->post_id.',';
            }
            $array_post = substr($array_post,0,-1);
            if(isset($options->price->from) && (double)$options->price->from>0){
                $result_post_price_from = mwdb_select("SELECT DISTINCT post_id FROM post_meta WHERE ((meta_key='{var}' AND meta_value>={var}) OR (meta_key='{var}' AND meta_value>={var}) OR (meta_key='{var}' AND meta_value>={var})) AND post_id IN ($array_post)",array('rent_cost1',$options->price->from,'rent_cost2',$options->price->from,'rent_cost3',$options->price->from));
                $array_post = '';
                foreach($result_post_price_from as $post_price_from){
                    $array_post .= $post_price_from->post_id.',';
                }
                $array_post = substr($array_post,0,-1);
            }
            if(isset($options->price->to) && (double)$options->price->to>0){
                $result_post_price_to = mwdb_select("SELECT DISTINCT post_id FROM post_meta WHERE ((meta_key='{var}' AND meta_value<={var}) OR (meta_key='{var}' AND meta_value<={var}) OR (meta_key='{var}' AND meta_value<={var})) AND post_id IN ($array_post)",array('rent_cost1',$options->price->from,'rent_cost2',$options->price->from,'rent_cost3',$options->price->from));
                $array_post = '';
                foreach($result_post_price_to as $post_price_to){
                    $array_post .= $post_price_to->post_id.',';
                }
                $array_post = substr($array_post,0,-1);
            }
            if(isset($options->price->currency)){
                $result_post_price_currency = mwdb_select("SELECT DISTINCT post_id FROM post_meta WHERE ((meta_key='{var}' AND meta_value='{var}') OR (meta_key='{var}' AND meta_value='{var}') OR (meta_key='{var}' AND meta_value='{var}')) AND post_id IN ($array_post)",array('rent_currency1',$options->price->currency,'rent_currency2',$options->price->currency,'rent_currency3',$options->price->currency));
                $array_post = '';
                foreach($result_post_price_currency as $post_price_currency){
                    $array_post .= $post_price_currency->post_id.',';
                }
                $array_post = substr($array_post,0,-1);
            }
        }
        if(isset($options->location->region) && $options->location->region>0){
            $query_part .= "post_region=".$options->location->region." AND ";
        }
        if(isset($options->location->city) && count((array)$options->location->city)>0){
            $query_part .= " (";
            foreach($options->location->city as $city){
                $query_part .= "post_city=".$city." OR ";
            }
            $query_part = substr($query_part,0,-4).') AND ';
        }
        if(isset($options->location->area) && count((array)$options->location->area)>0){
            $query_part .= " (";
            foreach($options->location->area as $area){
                $query_part .= "post_area=".$area." OR ";
            }
            $query_part = substr($query_part,0,-4).') AND ';
        }
        if(isset($options->location->microarea) && count((array)$options->location->microarea)>0){
            $query_part .= " (";
            foreach($options->location->microarea as $area){
                $query_part .= "post_microarea=".$area." OR ";
            }
            $query_part = substr($query_part,0,-4).') AND ';
        }
        if(isset($options->location->street) && $options->location->street>0){
            $query_part .= "post_street=".$options->location->street." AND ";
        }
        if(isset($options->category) && $options->category>0){
            $query_part .= "post_category=".$options->category." AND ";
            if(isset($options->attributes)){
                switch($options->category){
                    case '5':{
                        $attr_query = '1=1 AND ';
                        if(isset($options->attributes->area->from) && (double)$options->attributes->area->from>0){
                            $attr_query .= "area>=".$options->attributes->area->from." AND ";
                        }
                        if(isset($options->attributes->area->to) && (double)$options->attributes->area->to>0){
                            $attr_query .= "area<=".$options->attributes->area->to." AND ";
                        }
                        if(isset($options->attributes->rooms) && count((array)$options->attributes->rooms)>0){
                            $a_query = '';
                            foreach($options->attributes->rooms as $room_count){
                                $a_query .= "rooms=".$room_count." OR";
                            }
                            $a_query = substr($a_query,0,-3);
                            $attr_query .= "(".$a_query.") AND";
                        }
                        if(isset($options->attributes->floorer->from) && (double)$options->attributes->floorer->from>0){
                            $attr_query .= "floorer>=".$options->attributes->floorer->from." AND ";
                        }
                        if(isset($options->attributes->floorer->to) && (double)$options->attributes->floorer->to>0){
                            $attr_query .= "floorer<=".$options->attributes->floorer->to." AND ";
                        }
                        if(isset($options->attributes->floor->from) && (double)$options->attributes->floor->from>0){
                            $attr_query .= "floor>=".$options->attributes->floor->from." AND ";
                        }
                        if(isset($options->attributes->floor->to) && (double)$options->attributes->floor->to>0){
                            $attr_query .= "floor<=".$options->attributes->floor->to." AND ";
                        }
                        if(isset($options->attributes->floor_name) && count((array)$options->attributes->floor_name)>0){
                            $a_query = '';
                            foreach($options->attributes->floor_name as $floor_name){
                                $a_query .= "floor_name='".$floor_name."' OR";
                            }
                            $a_query = substr($a_query,0,-3);
                            $attr_query .= "(".$a_query.") AND";
                        }
                        if(isset($options->attributes->repair) && count((array)$options->attributes->repair)>0){
                            $a_query = '';
                            foreach($options->attributes->repair as $repair){
                                $a_query .= "repair LIKE '%".$repair."%' OR";
                            }
                            $a_query = substr($a_query,0,-3);
                            $attr_query .= "(".$a_query.") AND";
                        }
                        if(isset($options->attributes->balcony) && count((array)$options->attributes->balcony)>0){
                            $a_query = '';
                            foreach($options->attributes->balcony as $balcony){
                                $a_query .= "balcony LIKE '%".$balcony."%' OR";
                            }
                            $a_query = substr($a_query,0,-3);
                            $attr_query .= "(".$a_query.") AND";
                        }
                        if(isset($options->attributes->garage) && count((array)$options->attributes->garage)>0){
                            $a_query = '';
                            foreach($options->attributes->garage as $garage){
                                $a_query .= "garage='".$garage."' OR";
                            }
                            $a_query = substr($a_query,0,-3);
                            $attr_query .= "(".$a_query.") AND";
                        }
                        if(isset($options->attributes->basement) && $options->attributes->basement=='1'){
                            $attr_query .= "basement='1' AND ";
                        }
                        if(isset($options->attributes->water) && $options->attributes->water!='0'){
                            $attr_query .= "water='".$options->attributes->water."' AND ";
                        }
                        if(isset($options->attributes->gas) && $options->attributes->gas!='0'){
                            $attr_query .= "gas='".$options->attributes->gas."' AND ";
                        }
                        if(isset($options->attributes->building_project) && count((array)$options->attributes->building_project)>0){
                            $a_query = '';
                            foreach($options->attributes->building_project as $building_project){
                                $a_query .= "building_project='".$building_project."' OR";
                            }
                            $a_query = substr($a_query,0,-3);
                            $attr_query .= "(".$a_query.") AND";
                        }
                        if(isset($options->attributes->building_position) && count((array)$options->attributes->building_position)>0){
                            $a_query = '';
                            foreach($options->attributes->building_position as $building_position){
                                $a_query .= "building_position='".$building_position."' OR";
                            }
                            $a_query = substr($a_query,0,-3);
                            $attr_query .= "(".$a_query.") AND";
                        }
                        if(isset($options->attributes->building_type) && count((array)$options->attributes->building_type)>0){
                            $a_query = '';
                            foreach($options->attributes->building_type as $building_type){
                                $a_query .= "building_type='".$building_type."' OR";
                            }
                            $a_query = substr($a_query,0,-3);
                            $attr_query .= "(".$a_query.") AND";
                        }
                        if(isset($options->attributes->housetop) && count((array)$options->attributes->housetop)>0){
                            $a_query = '';
                            foreach($options->attributes->housetop as $housetop){
                                $a_query .= "housetop='".$housetop."' OR";
                            }
                            $a_query = substr($a_query,0,-3);
                            $attr_query .= "(".$a_query.") AND";
                        }
                        $result_posts_by_attribute = mwdb_select("SELECT post_id FROM settings_apartments WHERE $attr_query post_id IN ($array_post)",array());
                        break;
                    }
                    case '6':{
                        $attr_query = '1=1 AND ';
                        if(isset($options->attributes->total_area->from) && (double)$options->attributes->total_area->from>0){
                            $attr_query .= "total_area>=".$options->attributes->total_area->from." AND ";
                        }
                        if(isset($options->attributes->total_area->to) && (double)$options->attributes->total_area->to>0){
                            $attr_query .= "total_area<=".$options->attributes->total_area->to." AND ";
                        }
                        if(isset($options->attributes->home_area->from) && (double)$options->attributes->home_area->from>0){
                            $attr_query .= "home_area>=".$options->attributes->home_area->from." AND ";
                        }
                        if(isset($options->attributes->home_area->to) && (double)$options->attributes->home_area->to>0){
                            $attr_query .= "home_area<=".$options->attributes->home_area->to." AND ";
                        }
                        if(isset($options->attributes->rooms) && count((array)$options->attributes->rooms)>0){
                            $a_query = '';
                            foreach($options->attributes->rooms as $room_count){
                                $a_query .= "rooms=".$room_count." OR";
                            }
                            $a_query = substr($a_query,0,-3);
                            $attr_query .= "(".$a_query.") AND";
                        }
                        if(isset($options->attributes->floorer->from) && (double)$options->attributes->floorer->from>0){
                            $attr_query .= "floorer>=".$options->attributes->floorer->from." AND ";
                        }
                        if(isset($options->attributes->floorer->to) && (double)$options->attributes->floorer->to>0){
                            $attr_query .= "floorer<=".$options->attributes->floorer->to." AND ";
                        }
                        if(isset($options->attributes->repair) && count((array)$options->attributes->repair)>0){
                            $a_query = '';
                            foreach($options->attributes->repair as $repair){
                                $a_query .= "repair LIKE '%".$repair."%' OR";
                            }
                            $a_query = substr($a_query,0,-3);
                            $attr_query .= "(".$a_query.") AND";
                        }
                        if(isset($options->attributes->balcony) && count((array)$options->attributes->balcony)>0){
                            $a_query = '';
                            foreach($options->attributes->balcony as $balcony){
                                $a_query .= "balcony LIKE '%".$balcony."%' OR";
                            }
                            $a_query = substr($a_query,0,-3);
                            $attr_query .= "(".$a_query.") AND";
                        }
                        if(isset($options->attributes->garage) && count((array)$options->attributes->garage)>0){
                            $a_query = '';
                            foreach($options->attributes->garage as $garage){
                                $a_query .= "garage LIKE '%".$garage."%' OR";
                            }
                            $a_query = substr($a_query,0,-3);
                            $attr_query .= "(".$a_query.") AND";
                        }
                        if(isset($options->attributes->basement) && $options->attributes->basement=='1'){
                            $attr_query .= "basement!='' AND ";
                        }
                        if(isset($options->attributes->gas) && $options->attributes->gas!='0'){
                            $attr_query .= "gas='".$options->attributes->gas."' AND ";
                        }
                        if(isset($options->attributes->water) && $options->attributes->water!='0'){
                            $attr_query .= "water='".$options->attributes->water."' AND ";
                        }
                        if(isset($options->attributes->heating_system) && $options->attributes->heating_system!='0'){
                            $attr_query .= "heating_system='".$options->attributes->heating_system."' AND ";
                        }
                        $result_posts_by_attribute = mwdb_select("SELECT post_id FROM settings_houses WHERE $attr_query post_id IN ($array_post)",array());
                        break;
                    }
                    case '7':{
                        $attr_query = '1=1 AND ';
                        if(isset($options->attributes->type) && count((array)$options->attributes->type)>0){
                            $a_query = '';
                            foreach($options->attributes->type as $type){
                                $a_query .= "type='".$type."' OR";
                            }
                            $a_query = substr($a_query,0,-3);
                            $attr_query .= "(".$a_query.") AND";
                        }
                        if(isset($options->attributes->area->from) && (double)$options->attributes->area->from>0){
                            $attr_query .= "area>=".$options->attributes->area->from." AND ";
                        }
                        if(isset($options->attributes->area->to) && (double)$options->attributes->area->to>0){
                            $attr_query .= "area<=".$options->attributes->area->to." AND ";
                        }
                        if(isset($options->attributes->rooms) && count((array)$options->attributes->rooms)>0){
                            $a_query = '';
                            foreach($options->attributes->rooms as $room_count){
                                $a_query .= "rooms=".$room_count." OR";
                            }
                            $a_query = substr($a_query,0,-3);
                            $attr_query .= "(".$a_query.") AND";
                        }
                        if(isset($options->attributes->floor->from) && (double)$options->attributes->floor->from>0){
                            $attr_query .= "floor>=".$options->attributes->floor->from." AND ";
                        }
                        if(isset($options->attributes->floor->to) && (double)$options->attributes->floor->to>0){
                            $attr_query .= "floor<=".$options->attributes->floor->to." AND ";
                        }
                        if(isset($options->attributes->floor_name) && count((array)$options->attributes->floor_name)>0){
                            $a_query = '';
                            foreach($options->attributes->floor_name as $floor_name){
                                $a_query .= "floor_name='".$floor_name."' OR";
                            }
                            $a_query = substr($a_query,0,-3);
                            $attr_query .= "(".$a_query.") AND";
                        }
                        if(isset($options->attributes->showcases) && $options->attributes->showcases!='0'){
                            $attr_query .= "showcases='".$options->attributes->showcases."' AND ";
                        }
                        if(isset($options->attributes->entrance) && $options->attributes->entrance!='0'){
                            $attr_query .= "entrance='".$options->attributes->entrance."' AND ";
                        }
                        if(isset($options->attributes->repair) && $options->attributes->repair!='0'){
                            $attr_query .= "repair='".$options->attributes->repair."' AND ";
                        }
                        if(isset($options->attributes->building_type) && count((array)$options->attributes->building_type)>0){
                            $a_query = '';
                            foreach($options->attributes->building_type as $building_type){
                                $a_query .= "building_type='".$building_type."' OR";
                            }
                            $a_query = substr($a_query,0,-3);
                            $attr_query .= "(".$a_query.") AND";
                        }
                        if(isset($options->attributes->building_position) && count((array)$options->attributes->building_position)>0){
                            $a_query = '';
                            foreach($options->attributes->building_position as $building_position){
                                $a_query .= "building_position='".$building_position."' OR";
                            }
                            $a_query = substr($a_query,0,-3);
                            $attr_query .= "(".$a_query.") AND";
                        }
                        $result_posts_by_attribute = mwdb_select("SELECT post_id FROM settings_commercial WHERE $attr_query post_id IN ($array_post)",array());
                        break;
                    }
                    case '8':{
                        $attr_query = '1=1 AND ';
                        if(isset($options->attributes->area->from) && (double)$options->attributes->area->from>0){
                            $attr_query .= "area>=".$options->attributes->area->from." AND ";
                        }
                        if(isset($options->attributes->area->to) && (double)$options->attributes->area->to>0){
                            $attr_query .= "area<=".$options->attributes->area->to." AND ";
                        }
                        if(isset($options->attributes->buildings->from) && (double)$options->attributes->buildings->from>0){
                            $attr_query .= "buildings>=".$options->attributes->buildings->from." AND ";
                        }
                        if(isset($options->attributes->buildings->to) && (double)$options->attributes->buildings->to>0){
                            $attr_query .= "buildings<=".$options->attributes->buildings->to." AND ";
                        }
                        if(isset($options->attributes->water_drink) && $options->attributes->water_drink!='0'){
                            $attr_query .= "water_drink='".$options->attributes->water_drink."' AND ";
                        }
                        if(isset($options->attributes->water_irrigation) && $options->attributes->water_irrigation!='0'){
                            $attr_query .= "water_irrigation='".$options->attributes->water_irrigation."' AND ";
                        }
                        if(isset($options->attributes->gas) && $options->attributes->gas!='0'){
                            $attr_query .= "gas='".$options->attributes->gas."' AND ";
                        }
                        if(isset($options->attributes->electricity) && $options->attributes->electricity!='0'){
                            $attr_query .= "electricity='".$options->attributes->electricity."' AND ";
                        }
                        if(isset($options->attributes->high_voltage_pillars) && $options->attributes->high_voltage_pillars!='0'){
                            $attr_query .= "high_voltage_pillars LIKE '%".$options->attributes->high_voltage_pillars."%' AND ";
                        }
                        if(isset($options->attributes->sewage) && $options->attributes->sewage!='0'){
                            $attr_query .= "sewage LIKE '%".$options->attributes->sewage."%' AND ";
                        }
                        if(isset($options->attributes->fence) && $options->attributes->fence!='0'){
                            $attr_query .= "fence='".$options->attributes->fence."' AND ";
                        }
                        if(isset($options->attributes->fruit_trees) && $options->attributes->fruit_trees!='0'){
                            $attr_query .= "fruit_trees='".$options->attributes->fruit_trees."' AND ";
                        }
                        $result_posts_by_attribute = mwdb_select("SELECT post_id FROM settings_land WHERE $attr_query post_id IN ($array_post)",array());
                        break;
                    }
                    case '9':{
                        $attr_query = '1=1 AND ';
                        if(isset($options->attributes->area->from) && (double)$options->attributes->area->from>0){
                            $attr_query .= "area>=".$options->attributes->area->from." AND ";
                        }
                        if(isset($options->attributes->area->to) && (double)$options->attributes->area->to>0){
                            $attr_query .= "area<=".$options->attributes->area->to." AND ";
                        }
                        if(isset($options->attributes->building_type) && $options->attributes->building_type!='0'){
                            $attr_query .= "building_type='".$options->attributes->building_type."' AND ";
                        }
                        $result_posts_by_attribute = mwdb_select("SELECT post_id FROM settings_garages WHERE $attr_query post_id IN ($array_post)",array());
                        break;
                    }
                    default:{
                        $result_posts_by_attribute = mwdb_select("SELECT post_id FROM posts WHERE post_status!=6",array());
                        break;
                    }
                }
                $array_post = '';
                foreach($result_posts_by_attribute as $posts_by_attribute){
                    $array_post .= $posts_by_attribute->post_id.',';
                }
                $array_post = substr($array_post,0,-1);
            }
        }
        $offers_class = new Offer($client_id);
        $offers = $offers_class->get();
        $array_offered_posts = array();
        foreach($offers as $offer){
            array_push($array_offered_posts,$offer->post_id);
        }
        $result_posts = mwdb_select("SELECT post_id FROM posts WHERE $query_part post_id IN ($array_post) AND post_status!=6 ORDER BY post_status DESC,post_id DESC",array());
        foreach($result_posts as $row_post){
            $post_class = new Post('ru',$row_post->post_id);
            $post = $post_class->get();
            $location = '';
            if($post->post_region>0){
                $location .= mwdb_get_var("SELECT region_ru FROM region WHERE region_id={var}",array($post->post_region)).', ';
            }
            if($post->post_city>0){
                $location .= mwdb_get_var("SELECT city_ru FROM city WHERE city_id={var}",array($post->post_city)).', ';
            }
            if($post->post_area>0){
                $location .= mwdb_get_var("SELECT area_ru FROM area WHERE area_id={var}",array($post->post_area)).', ';
            }
            if($post->post_microarea>0){
                $location .= mwdb_get_var("SELECT area_ru FROM microarea WHERE area_id={var}",array($post->post_microarea)).', ';
            }
            if($post->post_street>0){
                $location .= mwdb_get_var("SELECT street_ru FROM streets WHERE street_id={var}",array($post->post_street)).' ';
            }
            $location .= $post->post_address;
            $owner = json_decode($post->post_owner);
            $owner_show = 'Имя: '.$owner->owner_name.'<br>Телефон1: '.$owner->owner_phone1.'<br>Телефон2: '.$owner->owner_phone2.'<br>E-mail: '.$owner->owner_email;
            $images = json_decode($post->post_images,true);
            if(isset($images[0]['img']) && $images[0]['img']!=''){
                $image_primary = 'http://user.alex-r.am/images/100/'.$images[0]['img'];
            }
            else{
                $image_primary = 'http://user.alex-r.am/LOGO.png';
            }
            if($post->post_status=='1'){
                $style_row = 'style="background:rgba(255, 255, 0, 0.15)"';
            }
            elseif($post->post_status=='2'){
                $style_row = 'style="background:rgba(210, 236, 161, 0.45);"';
            }
            else{
                $style_row = '';
            }
            ?>
            <tr <?php echo $style_row; ?>>
                <td><?php echo $post->post_id; ?></td>
                <td><?php echo $post->post_title_ru; ?></td>
                <?php if($user_level=='1'){ ?>
                    <td><?php echo mwdb_get_var("SELECT user_name FROM users WHERE user_id={var}",array($post->post_user)); ?></td>
                <?php } ?>
                <td><?php echo $location; ?></td>
                <td><?php echo $owner_show; ?></td>
                <td><img src="<?php echo $image_primary; ?>" width="100"></td>
                <td><?php echo mwdb_get_var("SELECT category_title_ru FROM category WHERE category_id={var}",array($post->post_category)); ?></td>
                <td>
                    <div class="btn-group">
                        <a href="http://alex-r.am/estate/<?php echo $post->post_id; ?>" target="_blank" class="btn btn-default">
                            <i class="fa fa-eye"></i>Просмотр
                        </a>
                        <a onclick="offer_to_user(<?php echo $post->post_id; ?>,<?php echo $client_id; ?>,this)" class="btn <?php if(in_array($post->post_id,$array_offered_posts)){echo 'btn-warning';} else{echo 'btn-success';} ?>">
                            <i class="fa <?php if(in_array($post->post_id,$array_offered_posts)){echo 'fa-check-square-o';} else{echo 'fa-check';} ?>"></i>
                            <?php if(in_array($post->post_id,$array_offered_posts)){echo 'Выбран';} else{echo 'Предложить';} ?>
                        </a>
                    </div>
                </td>
            </tr>
        <?php
        }
        ?>
    </table>
    <?php
}
function client_search_region($client_id){
    $client_re = mwdb_get_row("SELECT region,city,area,microarea FROM client_search WHERE client_id={var}",array($client_id));
    $region_name = mwdb_get_var("SELECT region_ru FROM region WHERE region_id={var}",array($client_re->region));
    $cities = explode(',',$client_re->city);
    $city_name = '';
    foreach($cities as $city){
        $city_name.= mwdb_get_var("SELECT city_ru FROM city WHERE city_id={var}",array($city)).', ';
    }
    $areas = explode(',',$client_re->area);
    $area_name = '';
    foreach($areas as $area){
        $area_name.= mwdb_get_var("SELECT area_ru FROM area WHERE area_id={var}",array($area)).', ';
    }
    $microareas = explode(',',$client_re->microarea);
    $microarea_name = '';
    foreach($microareas as $microarea){
        $microarea_name.= mwdb_get_var("SELECT area_ru FROM microarea WHERE area_id={var}",array($microarea)).', ';
    }
    return $region_name.'<br>'.substr($city_name,0,-2).'<br>'.substr($area_name,0,-2).'<br>'.substr($microarea_name,0,-2);
}
function get_client_Search_table($result,$post_id){
    ?>
    <style>
        th{text-align: center}
    </style>
    <table class="table table-hover table-bordered" style="text-align: center">
        <tr>
            <th>ID</th>
            <th>Имя клиента</th>
            <th>Телефон</th>
            <th>Район</th>
            <th>E-mail</th>
            <th>Дата регистрации</th>
            <th>Статус</th>
            <th>Дествия</th>
        </tr>
    <?php
    foreach($result as $row_client){
        switch($row_client->client_status){
            case '0':{
                $status = '<span class="label label-warning">Новый</span>';
                break;
            }
            case '1':{
                $status = '<span class="label label-primary">В процессе</span>';
                break;
            }
            case '2':{
                $status = '<span class="label label-success">Завершен</span>';
                break;
            }
        }
        if($row_client->client_type=='1'){
            $style_tr = 'style="background:rgba(255, 255, 0, 0.15)"';
        }
        elseif($row_client->client_type=='2'){
            $style_tr = 'style="background:rgba(210, 236, 161, 0.45);"';
        }
        else{
            $style_tr = '';
        }
        ?>
        <tr <?php echo $style_tr; ?>>
            <td><?php echo $row_client->client_id; ?></td>
            <td><?php echo $row_client->client_name; ?></td>
            <td><?php echo $row_client->client_phone1;if(isset($row_client->client_phone2) && $row_client->client_phone2!=''){echo ', '.$row_client->client_phone2;} ?></td>
            <td><?php echo client_search_region($row_client->client_id); ?></td>
            <td><?php echo $row_client->client_email; ?></td>
            <td><?php echo $row_client->client_time; ?></td>
            <td><?php echo $status; ?></td>
            <td>
                <div class="btn-group">
                    <?php
                    $offers_class = new Offer($row_client->client_id);
                    $offers = $offers_class->get();
                    $array_offered_posts = array();
                    if($offers){
                       foreach($offers as $offer){
                           array_push($array_offered_posts,$offer->post_id);
                       }
                    }
                    ?>
                    <a onclick="offer_to_user(<?php echo $post_id; ?>,<?php echo $row_client->client_id; ?>,this)" class="btn <?php if(in_array($post_id,$array_offered_posts)){echo 'btn-warning';} else{echo 'btn-success';} ?>">
                        <i class="fa <?php if(in_array($post_id,$array_offered_posts)){echo 'fa-check-square-o';} else{echo 'fa-check';} ?>"></i>
                        <?php if(in_array($post_id,$array_offered_posts)){echo 'Выбран';} else{echo 'Предложить';} ?>
                    </a>
                    <a href="http://broker.alex-r.am/index.php?action=buyer&subaction=offers&id=<?php echo $row_client->client_id; ?>" class="btn btn-default">
                        <i class="fa fa-list"></i> Список предложений
                    </a>
                </div>
            </td>
        </tr>
    <?php
    }
    ?>
    </table>
    <?php
}
?>