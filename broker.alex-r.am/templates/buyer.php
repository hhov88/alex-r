<?php
require( TEMPLATE_PATH."/header.php" );
if(isset($_GET['subaction'])){
    switch($_GET['subaction']){
        case 'all':{
            require( TEMPLATE_PATH."/buyer/all.php" );
            break;
        }
        case 'add':{
            require( TEMPLATE_PATH."/buyer/add.php" );
            break;
        }
        case 'view':{
            require( TEMPLATE_PATH."/buyer/view.php" );
            break;
        }
        case 'edit':{
            require( TEMPLATE_PATH."/buyer/edit.php" );
            break;
        }
        case 'end':{
            $id = (int)$_GET['id'];
            mwdb_query("UPDATE post_client SET client_status='2' WHERE client_id={var}",array($id));
            echo '<script>document.location.href="http://broker.alex-r.am/index.php?action=buyer&subaction=all"</script>';
            break;
        }
        case 'delete':{
            $id = (int)$_GET['id'];
            $post = new Client($id);
            $post->delete();
            echo '<script>document.location.href="http://broker.alex-r.am/index.php?action=buyer&subaction=all"</script>';
            break;
        }
        case 'archive':{
            require( TEMPLATE_PATH."/buyer/archive.php" );
            break;
        }
        case 'offers':{
            require( TEMPLATE_PATH."/buyer/offers.php" );
            break;
        }
        case 'remove_from_offer':{
            $offer_id = (int)$_GET['id'];
            $offer = new Offer(0,$offer_id);
            $offer->get_by_offer_id();
            $client_id = $offer->client_id;
            $offer->delete();
            echo '<script>document.location.href="http://broker.alex-r.am/index.php?action=buyer&subaction=offers&id='.$client_id.'"</script>';
            break;
        }
        case 'change_status':{
            $status = (int)$_GET['status'];
            $offer_id = (int)$_GET['id'];
            $offer = new Offer(0,$offer_id);
            $offer->update_status($status);
            $offer->get_by_offer_id();
            $client_id = $offer->client_id;
            if($status=='2'){
                mwdb_query("UPDATE post_client SET client_status='{var}' WHERE client_id={var}",array(2,$client_id));
            }
            echo '<script>document.location.href="http://broker.alex-r.am/index.php?action=buyer&subaction=offers&id='.$client_id.'"</script>';
            break;
        }
        case 'delete_search_parametrs':{
            $id = (int)$_GET['id'];
            mwdb_query("DELETE FROM client_search WHERE client_id={var}",array($id));
            mwdb_query("DELETE FROM client_offers WHERE client_id={var}",array($id));
            echo '<script>document.location.href="http://broker.alex-r.am/index.php?action=buyer&subaction=view&id='.$id.'"</script>';
            break;
        }
    }
}
require( TEMPLATE_PATH."/footer.php" );
?>