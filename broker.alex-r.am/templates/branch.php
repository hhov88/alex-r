<?php
require( TEMPLATE_PATH."/header.php" );
if(isset($_GET['subaction']) && $_GET['subaction']=='delete'){
    $id = (int)$_GET['id'];
    mwdb_query("DELETE FROM branch WHERE branch_id={var}",array($id));
    echo '<script>document.location.href="http://user.alex-r.am/index.php?action=branch"</script>';
}
?>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZ2D02mcYa-GES5qXKTrVOHIabQirrDi4&libraries=places"></script>
    <style>
        #map{
            height: 560px;
            width: 100%;
        }
    </style>
<section class="content-header">
    <h1>Наши офисы</h1>
</section>
<section class="content">
    <?php
    if(isset($_POST['backend_ad_user']) && $_POST['backend_ad_user']=='Добавить'){
        $branch_name_hy = (string)$_POST['branch_name_hy'];
        $branch_name_ru = (string)$_POST['branch_name_ru'];
        $branch_name_en = (string)$_POST['branch_name_en'];
        $branch_address_hy = (string)$_POST['branch_address_hy'];
        $branch_address_ru = (string)$_POST['branch_address_ru'];
        $branch_address_en = (string)$_POST['branch_address_en'];
        $email_br = (string)$_POST['email_br'];
        $skype_br = (string)$_POST['skype_br'];
        $map = (string)$_POST['map_address'];
        if(isset($_POST['phone1'])){
            $phone1 = array();
            foreach($_POST['phone1'] as $phone){
                array_push($phone1,$phone);
            }
        }
        if(isset($_POST['phone2'])){
            $phone2 = array();
            foreach($_POST['phone2'] as $phone){
                array_push($phone2,$phone);
            }
        }
        $branch = new Branch(0,$branch_name_hy,$branch_name_ru,$branch_name_en,$branch_address_hy,$branch_address_ru,$branch_address_en,$map,json_encode($phone1),json_encode($phone2),$email_br,$skype_br);
        $branch->insert();
        echo 'Офис добавлен!';
    }
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Добавить офис</h3>
                    <div class="pull-right box-tools">
                        <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Скрыть" style="margin-right: 5px;"><i class="fa fa-minus"></i></button>
                    </div><!-- /. tools -->
                </div>
                <div class="box-body">
                    <div class="col-md-6">
                        <form action="" method="post" enctype="multipart/form-data" autocomplete="off">
                            <table class="table table-hover table-bordered" style="text-align:center">
                                <tr>
                                    <th>На армянском</th>
                                    <th>На русском</th>
                                    <th>На английском</th>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" name="branch_name_hy" class="form-control" placeholder="Название филиала">
                                    </td>
                                    <td>
                                        <input type="text" name="branch_name_ru" class="form-control" placeholder="Название филиала">
                                    </td>
                                    <td>
                                        <input type="text" name="branch_name_en" class="form-control" placeholder="Название филиала">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <textarea name="branch_address_hy" class="form-control" placeholder="Адрес филиала" style="height: 120px"></textarea>
                                    </td>
                                    <td>
                                        <textarea name="branch_address_ru" class="form-control" placeholder="Адрес филиала" style="height: 120px"></textarea>
                                    </td>
                                    <td>
                                        <textarea name="branch_address_en" class="form-control" placeholder="Адрес филиала" style="height: 120px"></textarea>
                                    </td>
                                </tr>
                            </table>
                            <div class="col-md-12">
                                <label>Телефон офиса</label>
                                <input type="text" name="phone1[]" class="form-control"><a style="cursor: pointer;display: inline-block;margin:10px 0" onclick="add_phone1_row(this)"><i class="fa fa-plus"></i></a>
                            </div>
                            <div class="col-md-12">
                                <label>Мобильный телефон</label>
                                <input type="text" name="phone2[]" class="form-control"><a style="cursor: pointer;display: inline-block;margin:10px 0" onclick="add_phone2_row(this)"><i class="fa fa-plus"></i></a>
                            </div>
                            <div class="col-md-12">
                                <label>E-mail</label>
                                <input type="text" name="email_br" class="form-control">
                            </div>
                            <div class="col-md-12">
                                <label>Skype</label>
                                <input type="text" name="skype_br" class="form-control">
                            </div>
                            <input type="hidden" name="map_address" class="map_address" value="">
                            <input type="submit" name="backend_ad_user" value="Добавить" class="backend_ad_user btn btn-primary" style="margin-top: 30px">
                        </form>
                    </div>
                    <div class="col-md-6">
                        <div id="map"></div>
                        <script type="text/javascript">
                            function initialize() {
                                var mapOptions = {
                                    center: { lat: 40.177134, lng: 44.515690},
                                    zoom: 13
                                };
                                var map = new google.maps.Map(document.getElementById('map'),
                                    mapOptions);
                                var marker = new google.maps.Marker({
                                    map: map,
                                    draggable: true,
                                    position: mapOptions.center
                                });
                                google.maps.event.addListener(marker, 'dragend', function() {
                                    var position = marker.position;
                                    var lat = position.lat();
                                    var lng = position.lng();
                                    $(".map_address").val(lat+','+lng);
                                });
                            }
                            google.maps.event.addDomListener(window, 'load', initialize);
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>th{text-align: center !important;}</style>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Все офисы</h3>
                    <div class="box-tools">
                        <div class="input-group">
                            <input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                            <div class="input-group-btn">
                                <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover table-bordered" style="text-align:center">
                        <tr>
                            <th>Название</th>
                            <th>Адрес</th>
                            <th>Телефон</th>
                            <th>Мобильный</th>
                            <th>E-mail</th>
                            <th>Skype</th>
                            <th>действие</th>
                        </tr>
                        <?php
                        $users = mwdb_select("SELECT * FROM branch WHERE {var}",array(1));
                        if($users){
                            foreach($users as $user){
                                $tel1 = json_decode($user->branch_phone1);
                                $ph1 = '';
                                foreach($tel1 as $tl1){
                                    $ph1 .= $tl1.'<br>';
                                }
                                $tel2 = json_decode($user->branch_phone2);
                                $ph2 = '';
                                foreach($tel2 as $tl2){
                                    $ph2 .= $tl2.'<br>';
                                }
                                ?>
                                <tr>
                                    <td><?php echo $user->branch_name_hy; ?></td>
                                    <td><?php echo $user->branch_address_hy; ?></td>
                                    <td><?php echo $ph1; ?></td>
                                    <td><?php echo $ph2; ?></td>
                                    <td><?php echo $user->branch_email; ?></td>
                                    <td><?php echo $user->branch_skype; ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a onclick="return confirm('Вы уверены?')" href="http://user.alex-r.am/index.php?action=branch&subaction=delete&id=<?php echo $user->branch_id; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i> Удалить</a>
                                        </div>
                                    </td>
                                </tr>
                            <?php
                            }
                        }
                        ?>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>
<?php
require( TEMPLATE_PATH."/footer.php" );
?>