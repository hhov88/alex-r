<?php
$id = (string)$_GET['id'];
$array_ids = explode(',',$id);
require_once($_SERVER['DOCUMENT_ROOT'].'/PHPExcel/PHPExcel.php');
// Подключаем класс для вывода данных в формате excel
require_once($_SERVER['DOCUMENT_ROOT'].'/PHPExcel/PHPExcel/Writer/Excel5.php');

// Создаем объект класса PHPExcel
$xls = new PHPExcel();
// Устанавливаем индекс активного листа
$xls->setActiveSheetIndex(0);
// Получаем активный лист
$sheet = $xls->getActiveSheet();
// Подписываем лист
$sheet->setTitle('недвижимости');
$sheet->getColumnDimension('A')->setWidth(10);
$sheet->getColumnDimension('B')->setWidth(10);
$sheet->getColumnDimension('C')->setWidth(14);
$sheet->getColumnDimension('D')->setWidth(10);
$sheet->getColumnDimension('E')->setWidth(14);
$sheet->getColumnDimension('F')->setWidth(40);
$sheet->getColumnDimension('G')->setWidth(100);
// Вставляем текст в ячейку A1
/*$sheet->setCellValue("A1", $client_class->client_name.'  '.$client_class->client_phone1.($client_class->client_phone2!=''?', '.$client_class->client_phone2:''));
$sheet->getStyle('A1')->getFill()->setFillType(
	PHPExcel_Style_Fill::FILL_SOLID);
$sheet->getStyle('A1')->getFill()->getStartColor()->setRGB('EEEEEE');

// Объединяем ячейки
$sheet->mergeCells('A1:G1');

// Выравнивание текста
$sheet->getStyle('A1')->getAlignment()->setHorizontal(
	PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	*/
$sheet->setCellValue("A1", "ID");
$sheet->getStyle('A1')->getFill()->setFillType(
	PHPExcel_Style_Fill::FILL_SOLID);
$sheet->getStyle('A1')->getFill()->getStartColor()->setRGB('CCCCCC');
$sheet->getStyle('A1')->getAlignment()->setHorizontal(
	PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$sheet->setCellValue("B1", "Кол-во комнат");
$sheet->getStyle('B1')->getFill()->setFillType(
	PHPExcel_Style_Fill::FILL_SOLID);
$sheet->getStyle('B1')->getFill()->getStartColor()->setRGB('CCCCCC');
$sheet->getStyle('B1')->getAlignment()->setHorizontal(
	PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
$sheet->setCellValue("C1", "Площадь");
$sheet->getStyle('C1')->getFill()->setFillType(
	PHPExcel_Style_Fill::FILL_SOLID);
$sheet->getStyle('C1')->getFill()->getStartColor()->setRGB('CCCCCC');
$sheet->getStyle('C1')->getAlignment()->setHorizontal(
	PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
$sheet->setCellValue("D1", "Этаж");
$sheet->getStyle('D1')->getFill()->setFillType(
	PHPExcel_Style_Fill::FILL_SOLID);
$sheet->getStyle('D1')->getFill()->getStartColor()->setRGB('CCCCCC');
$sheet->getStyle('D1')->getAlignment()->setHorizontal(
	PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
$sheet->setCellValue("E1", "Цена");
$sheet->getStyle('E1')->getFill()->setFillType(
	PHPExcel_Style_Fill::FILL_SOLID);
$sheet->getStyle('E1')->getFill()->getStartColor()->setRGB('CCCCCC');
$sheet->getStyle('E1')->getAlignment()->setHorizontal(
	PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
$sheet->setCellValue("F1", "Адрес, Владелец");
$sheet->getStyle('F1')->getFill()->setFillType(
	PHPExcel_Style_Fill::FILL_SOLID);
$sheet->getStyle('F1')->getFill()->getStartColor()->setRGB('CCCCCC');
$sheet->getStyle('F1')->getAlignment()->setHorizontal(
	PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$sheet->setCellValue("G1", "Заметка");
$sheet->getStyle('G1')->getFill()->setFillType(
	PHPExcel_Style_Fill::FILL_SOLID);
$sheet->getStyle('G1')->getFill()->getStartColor()->setRGB('CCCCCC');
$sheet->getStyle('G1')->getAlignment()->setHorizontal(
	PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
/*<th>ID</th>
<th>Кол-во комнат</th>
<th>Площадь</th>
<th>Этаж</th>
<th>Цена</th>
<th>Адрес,Тел.</th>
<th style="width: 30%">Заметка</th>*/
$row_my = 2;
							foreach($array_ids as $row_post){
                            $post_class = new Post('ru',$row_post);
                            $post = $post_class->get();
                            $location = '';
                            /*if($post->post_region>0){
                                $location .= mwdb_get_var("SELECT region_ru FROM region WHERE region_id={var}",array($post->post_region)).', ';
                            }*/
                            if($post->post_city>0){
                                $location .= mwdb_get_var("SELECT city_ru FROM city WHERE city_id={var}",array($post->post_city)).', ';
                            }
                            /*if($post->post_area>0){
                                $location .= mwdb_get_var("SELECT area_ru FROM area WHERE area_id={var}",array($post->post_area)).', ';
                            }*/
                            /*if($post->post_microarea>0){
                                $location .= '<small>'.mwdb_get_var("SELECT area_ru FROM microarea WHERE area_id={var}",array($post->post_microarea)).', </small>';
                            }*/
                            if($post->post_street>0){
                                $location .= mwdb_get_var("SELECT street_ru FROM streets WHERE street_id={var}",array($post->post_street)).' ';
                            }
                            $location .= $post->post_address;
                            $owner = json_decode($post->post_owner);
                            $owner_show = ''.$owner->owner_name.', '.$owner->owner_phone1.', '.($owner->owner_phone2!=''?$owner->owner_phone2.', ':'').($owner->owner_phone3!=''?$owner->owner_phone3.', ':'').($owner->owner_email!=''?$owner->owner_email.',':'').($post->arajarkam!=''?'Arajark.am: '.$post->arajarkam:'');
                            $images = json_decode($post->post_images,true);
                            if(isset($images[0]['img']) && $images[0]['img']!=''){
                                $image_primary = 'http://user.alex-r.am/images/100/'.$images[0]['img'];
                            }
                            else{
                                $image_primary = 'http://user.alex-r.am/LOGO.png';
                            }
                            if($post->post_status=='1'){
                                $style_row = 'style="background:rgb(222, 212, 139)"';
                            }
                            elseif($post->post_status=='2'){
                                $style_row = 'style="background:rgb(190, 222, 139);"';
                            }
                            else{
                                $style_row = '';
                            }
							if($post->published=='0'){
								$style_row = 'style="background: rgba(255, 0, 0, 0.28);"';
							}
                            $settings = json_decode($post->post_meta);
                            if(isset($settings->sale) && $settings->sale=='1'){
                                $price = $settings->price_number.' '.$settings->price_currency;
                            }
                            if(isset($settings->rent) && $settings->rent=='1'){
                                $price .= '<br><small>Аренда: </small>';
                                if(isset($settings->rent_cost1) && (double)$settings->rent_cost1>0){
                                    $price .= $settings->rent_cost1.' '.$settings->rent_currency1;
                                }
                                elseif(isset($settings->rent_cost2) && (double)$settings->rent_cost2>0){
                                    $price .= $settings->rent_cost2.' '.$settings->rent_currency2;
                                }
                                elseif(isset($settings->rent_cost3) && (double)$settings->rent_cost3>0){
                                    $price .= $settings->rent_cost3.' '.$settings->rent_currency3;
                                }
                            }
                            $attributes = json_decode($post->settings);
                            if(isset($attributes->area)){
                                $area = $attributes->area;
                            }
                            elseif($attributes->home_area){
                                $area = $attributes->home_area;
                            }
                            else{
                                $area = '';
                            }
                            $fl = '';
                            if(isset($attributes->floor) && $attributes->floor!=''){
                                $fl .= $attributes->floor;
                            }
                            if(isset($attributes->floorer) && $attributes->floorer!=''){
                                $fl .= '/'.$attributes->floorer;
                            }
                            if(isset($attributes->rooms) && $attributes->rooms!=''){
                                $rooms = $attributes->rooms;
                            }
							$sheet->setCellValueByColumnAndRow(
										  0,
										  $row_my,
										  $post->post_id);
		// Применяем выравнивание
		$sheet->getStyleByColumnAndRow(0, $row_my)->getAlignment()->
				setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		$sheet->setCellValueByColumnAndRow(
										  1,
										  $row_my,
										  $rooms);
		// Применяем выравнивание
		$sheet->getStyleByColumnAndRow(1, $row_my)->getAlignment()->
				setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				
		$sheet->setCellValueByColumnAndRow(
										  2,
										  $row_my,
										  $area.' m2');
		// Применяем выравнивание
		$sheet->getStyleByColumnAndRow(2, $row_my)->getAlignment()->
				setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				
		$sheet->setCellValueByColumnAndRow(
										  3,
										  $row_my,
										  $fl);
		// Применяем выравнивание
		$sheet->getStyleByColumnAndRow(3, $row_my)->getAlignment()->
				setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				
		$sheet->setCellValueByColumnAndRow(
										  4,
										  $row_my,
										  $price);
		// Применяем выравнивание
		$sheet->getStyleByColumnAndRow(4, $row_my)->getAlignment()->
				setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				
		$sheet->setCellValueByColumnAndRow(
										  5,
										  $row_my,
										  $location.', '.$owner_show);
		// Применяем выравнивание
		$sheet->getStyleByColumnAndRow(5, $row_my)->getAlignment()->
				setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$sheet->getStyleByColumnAndRow(5, $row_my)->getAlignment()->setWrapText(true);		
		$sheet->setCellValueByColumnAndRow(
										  6,
										  $row_my,
										  '');
		// Применяем выравнивание
		$sheet->getStyleByColumnAndRow(6, $row_my)->getAlignment()->
				setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                            
                            unset($price);
							$row_my++;
                        }
	
header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
header ( "Cache-Control: no-cache, must-revalidate" );
header ( "Pragma: no-cache" );
header ( "Content-type: application/vnd.ms-excel" );
header ( "Content-Disposition: attachment; filename=недвижимости.xls" );
// Выводим содержимое файла
$objWriter = new PHPExcel_Writer_Excel5($xls);
$objWriter->save('php://output');
?>