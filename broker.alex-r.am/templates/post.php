<?php
require( TEMPLATE_PATH."/header.php" );
if(isset($_GET['subaction'])){
    switch($_GET['subaction']){
        case 'all':{
            require( TEMPLATE_PATH."/post/all.php" );
            break;
        }
        case 'add':{
            require( TEMPLATE_PATH."/post/add.php" );
            break;
        }
        case 'view':{
            require( TEMPLATE_PATH."/post/view.php" );
            break;
        }
        case 'map':{
            require( TEMPLATE_PATH."/post/map.php" );
            break;
        }
        case 'archive':{
            require( TEMPLATE_PATH."/post/archive.php" );
            break;
        }
        case 'search':{
            require( TEMPLATE_PATH."/post/search.php" );
            break;
        }
        case 'delete':{
            $id = (int)$_GET['id'];
            $post = new Post('ru',$id);
            $post->delete();
            echo '<script>document.location.href="http://user.alex-r.am/index.php?action=post&subaction=all"</script>';
            break;
        }
        case 'restore':{
            $id = (int)$_GET['id'];
            mwdb_query("UPDATE posts SET post_status='0' WHERE post_id={var}",array($id));
            echo '<script>document.location.href="http://user.alex-r.am/index.php?action=post&subaction=archive"</script>';
            break;
        }
        case 'find_client':{
            require( TEMPLATE_PATH."/post/find_client.php" );
            break;
        }
        case 'offer_client':{
            require( TEMPLATE_PATH."/post/offer_client.php" );
            break;
        }
        case 'sold':{
            require( TEMPLATE_PATH."/post/sold.php" );
            break;
        }
        case 'sales':{
            require( TEMPLATE_PATH."/post/sales.php" );
            break;
        }
        case 'restore_from_sold':{
            $id = (int)$_GET['id'];
            mwdb_query("UPDATE posts SET post_status='0' WHERE post_id={var}",array($id));
            mwdb_query("DELETE FROM post_meta WHERE post_id={var} AND meta_key='{var}'",array($id,'sold_price'));
            echo '<script>document.location.href="http://user.alex-r.am/index.php?action=post&subaction=sales"</script>';
            break;
        }
    }
}
require( TEMPLATE_PATH."/footer.php" );
?>