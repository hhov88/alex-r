<section class="content-header">
    <h1>Города</h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body table-responsive">
                    <style>
                        th{text-align: center}
                        .my_hide{display: none}
                    </style>
                    <div class="col-md-3" style="float: right;margin-bottom: 20px">
                        <select name="region_filter" class="form-control" onchange="document.location.href='http://user.alex-r.am/index.php?action=locations&subaction=city&region='+$(this).val()">
                        <?php
                        $result_regions = mwdb_select("SELECT region_id,region_ru FROM region WHERE 1",array());
                        foreach($result_regions as $row_region){
                            ?>
                            <option value="<?php echo $row_region->region_id; ?>" <?php if(isset($_GET['region']) && $_GET['region']==$row_region->region_id){echo 'selected';} ?>><?php echo $row_region->region_ru; ?></option>
                            <?php
                        }
                        ?>
                        </select>
                    </div>
                    <table class="table table-hover table-bordered" style="text-align: center">
                        <tr>
                            <th>ID</th>
                            <th>На армянском</th>
                            <th>На русском</th>
                            <th>На английском</th>
                            <th>Регион</th>
                            <th>Дествия</th>
                        </tr>
                        <?php
                        if(isset($_GET['region']) && (int)$_GET['region']>0){
                            $reg_filter = (int)$_GET['region'];
                            $qu = "region=".$reg_filter;
                        }
                        else{
                            $qu = 1;
                        }
                        $result_cities = mwdb_select("SELECT city_id FROM city WHERE {var} ORDER BY region,city_order",array($qu));
                        foreach($result_cities as $row_city){
                            $i = $row_city->city_id;
                            $city = new City($i);
                            $city->get();
                            ?>
                            <tr>
                                <td><?=$city->id; ?></td>
                                <td>
                                    <span class="my_show"><?=$city->hy; ?></span>
                                    <span class="my_hide" style="width: 100%;"><input type="text" name="hy" class="hy form-control" value="<?=$city->hy; ?>"></span>
                                </td>
                                <td>
                                    <span class="my_show"><?=$city->ru; ?></span>
                                    <span class="my_hide" style="width: 100%;"><input type="text" name="ru" class="ru form-control" value="<?=$city->ru; ?>"></span>
                                </td>
                                <td>
                                    <span class="my_show"><?=$city->en; ?></span>
                                    <span class="my_hide" style="width: 100%;"><input type="text" name="en" class="en form-control" value="<?=$city->en; ?>"></span>
                                </td>
                                <td>
                                    <?php
                                    $region = new Region($city->region);
                                    $region->get();
                                    echo $region->ru;
                                    ?>
                                </td>
                                <td>
                                    <div class="btn-group my_show">
                                        <a class="btn btn-default" onclick="edit_regions(this)"><i class="fa fa-edit"></i>Изменить</a>
                                        <!--<a onclick="delete_region(this)" data-id="<?=$city->id; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i> Удалить</a>-->
                                    </div>
                                    <div class="btn-group my_hide">
                                        <a data-id="<?=$city->id; ?>" class="btn btn-success" onclick="save_city(this,<?php echo $_GET['region'] ?>)"><i class="fa fa-check"></i>Сохранить</a>
                                    </div>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>