<section class="content-header">
    <h1>Районы</h1>
</section>
<?php
if(isset($_POST['backend_ad_user']) && $_POST['backend_ad_user']='Добавить'){
    $area_hy = (string)$_POST['area_hy'];
    $area_ru = (string)$_POST['area_ru'];
    $area_en = (string)$_POST['area_en'];
    $city_area = (int)$_POST['city_area'];
    $area_order = (int)$_POST['area_order'];
    $area = new Area(0,$area_hy,$area_ru,$area_en,$city_area);
    $area->insert($area_order);
    echo 'Район добавлен';
}
?>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Добавить Район</h3>
                    <div class="pull-right box-tools">
                        <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Скрыть" style="margin-right: 5px;"><i class="fa fa-minus"></i></button>
                    </div><!-- /. tools -->
                </div>
                <div class="box-body">
                    <form action="" method="post" enctype="multipart/form-data" autocomplete="off">
                        <table class="table table-hover table-bordered" style="text-align: center">
                            <tr>
                                <th>На армянском</th>
                                <th>На русском</th>
                                <th>На английском</th>
                            </tr>
                            <tr>
                                <td><input type="text" name="area_hy" class="form-control" placeholder="Название района"></td>
                                <td><input type="text" name="area_ru" class="form-control" placeholder="Название района"></td>
                                <td><input type="text" name="area_en" class="form-control" placeholder="Название района"></td>
                            </tr>
                        </table>
                        <select class="form-control" name="city_area" style="margin: 20px 0">
                            <option value="0">-- Выберите Город --</option>
                            <?php
                            $result_cities = mwdb_select("SELECT city_id,city_ru FROM city WHERE region={var} ORDER BY city_order",array(8));
                            foreach($result_cities as $city){
                                ?>
                                <option value="<?php echo $city->city_id ?>"><?php echo $city->city_ru ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <input type="number" class="form-control" name="area_order" placeholder="Порядок" style="margin-bottom: 20px">
                        <input type="submit" name="backend_ad_user" value="Добавить" class="backend_ad_user btn btn-primary">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body table-responsive">
                    <style>
                        th{text-align: center}
                        .my_hide{display: none}
                    </style>
                    <div class="col-md-12" style="margin-bottom: 20px">
                        <div class="col-md-3"></div>
                        <div class="col-md-3"></div>
                        <div class="col-md-3">
                            <select name="region_filter" class="form-control" onchange="document.location.href='http://user.alex-r.am/index.php?action=locations&subaction=area&region='+$(this).val()">
                                <?php
                                $result_regions = mwdb_select("SELECT region_id,region_ru FROM region WHERE 1",array());
                                foreach($result_regions as $row_region){
                                    ?>
                                    <option value="<?php echo $row_region->region_id; ?>" <?php if(isset($_GET['region']) && $_GET['region']==$row_region->region_id){echo 'selected';} ?>><?php echo $row_region->region_ru; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select name="city_filter" class="form-control" onchange="document.location.href='http://user.alex-r.am/index.php?action=locations&subaction=area&region='+$('select[name=region_filter]').val()+'&city='+$(this).val()">
                                <option value="">Все Города</option>
                                <?php
                                $in = '';
                                $region = (int)$_GET['region'];
                                $result_cities = mwdb_select("SELECT city_id,city_ru FROM city WHERE region={var}",array($region));
                                foreach($result_cities as $row_city){
                                    ?>
                                    <option value="<?php echo $row_city->city_id; ?>" <?php if(isset($_GET['city']) && $_GET['city']==$row_city->city_id){echo 'selected';} ?>><?php echo $row_city->city_ru; ?></option>
                                <?php
                                    $in .= $row_city->city_id.',';
                                }
                                $in = substr($in,0,-1);
                                ?>
                            </select>
                        </div>
                    </div>
                    <table class="table table-hover table-bordered" style="text-align: center">
                        <tr>
                            <th>ID</th>
                            <th>На армянском</th>
                            <th>На русском</th>
                            <th>На английском</th>
                            <th>Город</th>
                            <th>Порядок</th>
                            <th>Дествия</th>
                        </tr>
                        <?php
                        if(isset($_GET['city']) && (int)$_GET['city']>0){
                            $reg_filter = (int)$_GET['city'];
                            $qu = "city=".$reg_filter;
                        }
                        elseif(isset($_GET['region']) && (int)$_GET['region']>0){
                            $reg_filter = (int)$_GET['region'];
                            $qu = "city IN (".$in.")";
                        }
                        else{
                            $qu = 1;
                        }
                        $result_areas = mwdb_select("SELECT area_id,area_order FROM area WHERE {var} ORDER BY city,area_order",array($qu));
                        foreach($result_areas as $row_area){
                            $i = $row_area->area_id;
                            $area = new Area($i);
                            $area->get();
                            ?>
                            <tr>
                                <td><?=$area->id; ?></td>
                                <td>
                                    <span class="my_show"><?=$area->hy; ?></span>
                                    <span class="my_hide" style="width: 100%;"><input type="text" name="hy" class="hy form-control" value="<?=$area->hy; ?>"></span>
                                </td>
                                <td>
                                    <span class="my_show"><?=$area->ru; ?></span>
                                    <span class="my_hide" style="width: 100%;"><input type="text" name="ru" class="ru form-control" value="<?=$area->ru; ?>"></span>
                                </td>
                                <td>
                                    <span class="my_show"><?=$area->en; ?></span>
                                    <span class="my_hide" style="width: 100%;"><input type="text" name="en" class="en form-control" value="<?=$area->en; ?>"></span>
                                </td>
                                <td>
                                    <?php
                                    $city = new City($area->city);
                                    $city->get();
                                    echo $city->ru;
                                    ?>
                                </td>
                                <td><?php echo $row_area->area_order; ?></td>
                                <td>
                                    <div class="btn-group my_show">
                                        <a class="btn btn-default" onclick="edit_regions(this)"><i class="fa fa-edit"></i>Изменить</a>
                                        <a onclick="delete_area(this)" data-id="<?=$area->id; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i> Удалить</a>
                                    </div>
                                    <div class="btn-group my_hide">
                                        <a data-id="<?=$area->id; ?>" class="btn btn-success" onclick="save_area(this,<?php echo $_GET['region'] ?>)"><i class="fa fa-check"></i>Сохранить</a>
                                    </div>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>