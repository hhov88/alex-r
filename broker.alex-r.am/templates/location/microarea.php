<section class="content-header">
    <h1>Микрорайоны</h1>
</section>
<?php
if(isset($_POST['backend_ad_user']) && $_POST['backend_ad_user']='Добавить'){
    $area_hy = (string)$_POST['area_hy'];
    $area_ru = (string)$_POST['area_ru'];
    $area_en = (string)$_POST['area_en'];
    $area_microarea = (int)$_POST['area_microarea'];
    $area_order = (int)$_POST['area_order'];
    $area = new Microarea(0,$area_hy,$area_ru,$area_en,$area_microarea);
    $area->insert($area_order);
    echo 'Микрорайон добавлен';
}
?>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Добавить Микрорайон</h3>
                    <div class="pull-right box-tools">
                        <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Скрыть" style="margin-right: 5px;"><i class="fa fa-minus"></i></button>
                    </div><!-- /. tools -->
                </div>
                <div class="box-body">
                    <form action="" method="post" enctype="multipart/form-data" autocomplete="off">
                        <table class="table table-hover table-bordered" style="text-align: center">
                            <tr>
                                <th>На армянском</th>
                                <th>На русском</th>
                                <th>На английском</th>
                            </tr>
                            <tr>
                                <td><input type="text" name="area_hy" class="form-control" placeholder="Название Микрорайона"></td>
                                <td><input type="text" name="area_ru" class="form-control" placeholder="Название Микрорайона"></td>
                                <td><input type="text" name="area_en" class="form-control" placeholder="Название Микрорайона"></td>
                            </tr>
                        </table>
                        <select class="form-control" name="city_area" style="margin: 20px 0" onchange="get_area_microarea_data(this)">
                            <option value="0">-- Выберите Город --</option>
                            <?php
                            $result_cities = mwdb_select("SELECT city_id,city_ru FROM city WHERE region={var} ORDER BY city_order",array(8));
                            foreach($result_cities as $city){
                                ?>
                                <option value="<?php echo $city->city_id ?>"><?php echo $city->city_ru ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <select name="area_microarea" class="form-control area_microarea" style="margin-bottom: 20px">
                            <option value="0">-- Выберите Район --</option>
                        </select>
                        <input type="number" class="form-control" name="area_order" placeholder="Порядок" style="margin-bottom: 20px">
                        <input type="submit" name="backend_ad_user" value="Добавить" class="backend_ad_user btn btn-primary">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body table-responsive">
                    <style>
                        th{text-align: center}
                        .my_hide{display: none}
                    </style>
                    <div class="col-md-12" style="margin-bottom: 20px">
                        <div class="col-md-3"></div>
                        <div class="col-md-3">
                            <select name="region_filter" class="form-control" onchange="document.location.href='http://user.alex-r.am/index.php?action=locations&subaction=microarea&region='+$(this).val()">
                                <?php
                                $result_regions = mwdb_select("SELECT region_id,region_ru FROM region WHERE 1",array());
                                foreach($result_regions as $row_region){
                                    ?>
                                    <option value="<?php echo $row_region->region_id; ?>" <?php if(isset($_GET['region']) && $_GET['region']==$row_region->region_id){echo 'selected';} ?>><?php echo $row_region->region_ru; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select name="city_filter" class="form-control" onchange="document.location.href='http://user.alex-r.am/index.php?action=locations&subaction=microarea&region='+$('select[name=region_filter]').val()+'&city='+$(this).val()">
                                <option value="">Все Города</option>
                                <?php
                                $in = '';
                                $region = (int)$_GET['region'];
                                $result_cities = mwdb_select("SELECT city_id,city_ru FROM city WHERE region={var}",array($region));
                                foreach($result_cities as $row_city){
                                    ?>
                                    <option value="<?php echo $row_city->city_id; ?>" <?php if(isset($_GET['city']) && $_GET['city']==$row_city->city_id){echo 'selected';} ?>><?php echo $row_city->city_ru; ?></option>
                                    <?php
                                    $in .= $row_city->city_id.',';
                                }
                                $in = substr($in,0,-1);
                                ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select name="area_filter" class="form-control" onchange="document.location.href='http://user.alex-r.am/index.php?action=locations&subaction=microarea&region='+$('select[name=region_filter]').val()+'&city='+$('select[name=city_filter]').val()+'&area='+$(this).val()">
                                <option value="">Все Районы</option>
                                <?php
                                $in1 = '';
                                $city = (int)$_GET['city'];
                                $result_areas = mwdb_select("SELECT area_id,area_ru FROM area WHERE city={var}",array($city));
                                foreach($result_areas as $row_area){
                                    ?>
                                    <option value="<?php echo $row_area->area_id; ?>" <?php if(isset($_GET['area']) && $_GET['area']==$row_area->area_id){echo 'selected';} ?>><?php echo $row_area->area_ru; ?></option>
                                    <?php
                                    $in1 .= $row_area->area_id.',';
                                }
                                $in1 = substr($in,0,-1);
                                ?>
                            </select>
                        </div>
                    </div>
                    <table class="table table-hover table-bordered" style="text-align: center">
                        <tr>
                            <th>ID</th>
                            <th>На армянском</th>
                            <th>На русском</th>
                            <th>На английском</th>
                            <th>Район</th>
                            <th>Порядок</th>
                            <th>Дествия</th>
                        </tr>
                        <?php
                        if(isset($_GET['area']) && (int)$_GET['area']>0){
                            $reg_filter = (int)$_GET['area'];
                            $qu = "area=".$reg_filter;
                        }
                        elseif(isset($_GET['city']) && (int)$_GET['city']>0){
                            $reg_filter = (int)$_GET['city'];
                            $qu = "area IN (SELECT area_id FROM area WHERE city=$reg_filter)";
                        }
                        elseif(isset($_GET['region']) && (int)$_GET['region']>0){
                            $reg_filter = (int)$_GET['region'];
                            $qu = "area IN (SELECT area_id FROM area WHERE city IN (".$in1."))";
                        }
                        else{
                            $qu = 1;
                        }
                        $result_microareas = mwdb_select("SELECT area_id,area_order FROM microarea WHERE {var} ORDER BY area,area_order",array($qu));
                        foreach($result_microareas as $row_microarea){
                            $i = $row_microarea->area_id;
                            $microarea = new Microarea($i);
                            $microarea->get();
                            ?>
                            <tr>
                                <td><?=$microarea->id; ?></td>
                                <td>
                                    <span class="my_show"><?=$microarea->hy; ?></span>
                                    <span class="my_hide" style="width: 100%;"><input type="text" name="hy" class="hy form-control" value="<?=$microarea->hy; ?>"></span>
                                </td>
                                <td>
                                    <span class="my_show"><?=$microarea->ru; ?></span>
                                    <span class="my_hide" style="width: 100%;"><input type="text" name="ru" class="ru form-control" value="<?=$microarea->ru; ?>"></span>
                                </td>
                                <td>
                                    <span class="my_show"><?=$microarea->en; ?></span>
                                    <span class="my_hide" style="width: 100%;"><input type="text" name="en" class="en form-control" value="<?=$microarea->en; ?>"></span>
                                </td>
                                <td>
                                    <?php
                                    $area = new Area($microarea->area);
                                    $area->get();
                                    echo $area->ru;
                                    ?>
                                </td>
                                <td><?php echo $row_microarea->area_order; ?></td>
                                <td>
                                    <div class="btn-group my_show">
                                        <a class="btn btn-default" onclick="edit_regions(this)"><i class="fa fa-edit"></i>Изменить</a>
                                        <a onclick="delete_microarea(this)" data-id="<?=$microarea->id; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i> Удалить</a>
                                    </div>
                                    <div class="btn-group my_hide">
                                        <a data-id="<?=$microarea->id; ?>" class="btn btn-success" onclick="save_microarea(this,<?php echo $_GET['region'] ?>)"><i class="fa fa-check"></i>Сохранить</a>
                                    </div>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>