<section class="content-header">
    <h1>Регионы</h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body table-responsive">
                    <style>
                        th{text-align: center}
                        .my_hide{display: none}
                    </style>
                    <table class="table table-hover table-bordered" style="text-align: center">
                        <tr>
                            <th>ID</th>
                            <th>На армянском</th>
                            <th>На русском</th>
                            <th>На английском</th>
                            <th>Дествия</th>
                        </tr>
                        <?php
                        $result_regions = mwdb_select("SELECT region_id FROM region WHERE {var} ORDER BY region_order",array(1));
                        foreach($result_regions as $row_region){
                            $i = $row_region->region_id;
                            $region = new Region($i);
                            $region->get();
                            ?>
                        <tr>
                            <td><?=$region->id; ?></td>
                            <td>
                                <span class="my_show"><?=$region->hy; ?></span>
                                <span class="my_hide" style="width: 100%;"><input type="text" name="hy" class="hy form-control" value="<?=$region->hy; ?>"></span>
                            </td>
                            <td>
                                <span class="my_show"><?=$region->ru; ?></span>
                                <span class="my_hide" style="width: 100%;"><input type="text" name="ru" class="ru form-control" value="<?=$region->ru; ?>"></span>
                            </td>
                            <td>
                                <span class="my_show"><?=$region->en; ?></span>
                                <span class="my_hide" style="width: 100%;"><input type="text" name="en" class="en form-control" value="<?=$region->en; ?>"></span>
                            </td>
                            <td>
                                <div class="btn-group my_show">
                                        <a class="btn btn-default" onclick="edit_regions(this)"><i class="fa fa-edit"></i>Изменить</a>
                                        <!--<a onclick="delete_region(this)" data-id="<?=$region->id; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i> Удалить</a>-->
                                </div>
                                <div class="btn-group my_hide">
                                        <a data-id="<?=$region->id; ?>" class="btn btn-success" onclick="save_region(this)"><i class="fa fa-check"></i>Сохранить</a>
                                </div>
                            </td>
                        </tr>
                            <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>