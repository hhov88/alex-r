<section class="content-header">
    <h1>Проданные недвижимости</h1>
</section>
<style>th{text-align: center}</style>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body table-responsive">
                    <table class="table table-hover table-bordered" style="text-align:center">
                        <tr>
                            <th>ID</th>
                            <th>Название</th>
                            <?php if($user_level=='1'){ ?>
                                <th>Оператор</th>
                            <?php } ?>
                            <th>Адрес</th>
                            <th>Владелец</th>
                            <th>Изображение</th>
                            <th>Цена сделки</th>
                            <th>Дествия</th>
                        </tr>
                        <?php
                        global $user_level;
                        if($user_level=='1'){
                            $query_part = 'post_status=5';
                        }
                        else{
                            $u_id = $_SESSION['user_id'];
                            $query_part = "post_user=$u_id AND post_status=5";
                        }
                        if(isset($_GET['page'])){
                            $page = (int)$_GET['page'];
                            if($page<2){
                                $page = 1;
                            }
                        }
                        else{
                            $page = 1;
                        }
                        $offset = ($page-1)*20;
                        $result_posts = mwdb_select("SELECT post_id FROM posts WHERE $query_part ORDER BY post_id DESC LIMIT {var},20",array($offset));
                        foreach($result_posts as $row_post){
                            $post_class = new Post('ru',$row_post->post_id);
                            $post = $post_class->get();
                            $location = '';
                            if($post->post_region>0){
                                $location .= mwdb_get_var("SELECT region_ru FROM region WHERE region_id={var}",array($post->post_region)).', ';
                            }
                            if($post->post_city>0){
                                $location .= mwdb_get_var("SELECT city_ru FROM city WHERE city_id={var}",array($post->post_city)).', ';
                            }
                            if($post->post_area>0){
                                $location .= mwdb_get_var("SELECT area_ru FROM area WHERE area_id={var}",array($post->post_area)).', ';
                            }
                            if($post->post_microarea>0){
                                $location .= mwdb_get_var("SELECT area_ru FROM microarea WHERE area_id={var}",array($post->post_microarea)).', ';
                            }
                            if($post->post_street>0){
                                $location .= mwdb_get_var("SELECT street_ru FROM streets WHERE street_id={var}",array($post->post_street)).' ';
                            }
                            $location .= $post->post_address;
                            $owner = json_decode($post->post_owner);
                            $owner_show = 'Имя: '.$owner->owner_name.'<br>Телефон1: '.$owner->owner_phone1.'<br>Телефон2: '.$owner->owner_phone2.'<br>E-mail: '.$owner->owner_email;
                            $images = json_decode($post->post_images,true);
                            if(isset($images[0]['img']) && $images[0]['img']!=''){
                                $image_primary = 'http://user.alex-r.am/images/100/'.$images[0]['img'];
                            }
                            else{
                                $image_primary = 'http://user.alex-r.am/LOGO.png';
                            }
                            ?>
                            <tr>
                                <td><?php echo $post->post_id; ?></td>
                                <td><?php echo $post->post_title_ru; ?></td>
                                <?php if($user_level=='1'){ ?>
                                    <td><?php echo mwdb_get_var("SELECT user_name FROM users WHERE user_id={var}",array($post->post_user)); ?></td>
                                <?php } ?>
                                <td><?php echo $location; ?></td>
                                <td><?php echo $owner_show; ?></td>
                                <td><img src="<?php echo $image_primary; ?>" width="100"></td>
                                <td><?php echo mwdb_get_var("SELECT meta_value FROM post_meta WHERE post_id={var} AND meta_key='{var}'",array($post->post_id,'sold_price')); ?></td>
                                <td>
                                    <div class="btn-group">
                                        <a href="http://user.alex-r.am/index.php?action=post&subaction=view&id=<?php echo $post->post_id; ?>" class="btn btn-default">
                                            <i class="fa fa-eye"></i>Просмотр
                                        </a>
                                        <!--<a href="http://admin.arajark.am/admin/index.php?action=edit_estate_ads&id=56264f73fdcb369b488b4567" class="btn btn-default">
                                            <i class="fa fa-edit"></i>Редактировать
                                        </a>-->
                                        <a onclick="return confirm('Вы уверены?')" href="http://user.alex-r.am/index.php?action=post&subaction=restore_from_sold&id=<?php echo $post->post_id; ?>" class="btn btn-danger">
                                            <i class="fa fa-trash-o"></i> Восстоновить</a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                    <div class="col-md-12">
                        <?php
                        $count_posts = mwdb_get_var("SELECT COUNT(*) FROM posts WHERE {var}",array($query_part));
                        if($count_posts%20==0){
                            $page_count = (int)($count_posts/20);
                        }
                        else{
                            $page_c = (int)($count_posts/20);
                            $page_count = $page_c+1;
                        }
                        if($page_count>1){
                            ?>
                            <a href="http://user.alex-r.am/index.php?action=post&subaction=all" class="pager <?php if($page==1){echo 'current';} ?>">1</a>
                            <?php
                            for($i=2;$i<=$page_count;$i++){
                                ?>
                                <a href="http://user.alex-r.am/index.php?action=post&subaction=all&page=<?php echo $i; ?>" class="pager <?php if($page==$i){echo 'current';} ?>"><?php echo $i; ?></a>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>