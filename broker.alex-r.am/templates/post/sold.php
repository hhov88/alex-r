<?php
if(isset($_GET['id']) && (int)$_GET['id']>0){
    $id = (int)$_GET['id'];
}
else{
    echo '<script>document.location.href="http://user.alex-r.am/index.php?action=post&subaction=all"</script>';
}
if(isset($_POST['price_sold']) && isset($_POST['sold_id']) && (int)$_POST['sold_id']>0){
    $price = $_POST['price_sold'];
    $sold_id = (int)$_POST['sold_id'];
    mwdb_query("INSERT INTO post_meta(post_id,meta_key,meta_value) VALUES ('{var}','{var}','{var}')",array($sold_id,'sold_price',$price));
    mwdb_query("UPDATE posts SET post_status='5' WHERE post_id={var}",array($sold_id));
    echo '<script>document.location.href="http://user.alex-r.am/index.php?action=post&subaction=sales"</script>';
}
?>
<section class="content">
    <div class="row">
        <div class="modal show">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Цена сделки</h4>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post" class="sold_form">
                            <input type="text" name="price_sold" class="form-control">
                            <input type="hidden" name="sold_id" value="<?php echo $id; ?>">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onclick="$('.sold_form').submit();">Сохранить</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
    </div>
</section>