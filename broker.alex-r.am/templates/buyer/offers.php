<section class="content-header">
    <h1>Клиент N<?php echo $_GET['id'] ?></h1>
</section>
<?php
$id = (int)$_GET['id'];
$client_class = new Client($id);
$client_class->get();
global $user_level;
if(isset($_POST['sold_submit']) && $_POST['sold_submit']=='sold_submit'){
	$to_email = (string)$_POST['to_email'];
	$off_id1 = (int)$_POST['off_id'];
	$offers_class = new Offer($id,$off_id1);
	$offers_class->get_by_offer_id();
	//var_dump($offers_class);
	$url = 'http://alex-r.am/estate/'.$offers_class->post_id;
	$subject = 'Alex-R կայքը առաջարկում է դիտել այս հայտարարությունը';
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
	$headers .= 'From: Alex-r <info@alex-r.am>' . "\r\n";

	$message = '<html><body>';
	$message .= '<img src="http://alex-r.am/images/logo.png" alt="Alex-R" />';
	$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
	//$message .= "<tr style='background: #eee;'><td><strong>Name:</strong> </td><td>" . $name . "</td></tr>";
	$message .= "<tr><td><strong>Post:</strong> </td><td>" . $url . "</td></tr>";
	$message .= "</table>";
	$message .= "</body></html>";

	mail($to_email, $subject, $message, $headers);
	$offers_class->update_mail(1);
	echo '<script>document.location.href="http://broker.alex-r.am/index.php?action=buyer&subaction=offers&id='.$id.'"</script>';
}
?>
<style>{text-align: center}</style>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="overlay" style="display: none">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
                <div class="box-body table-responsive">
                    <div class="col-md-12" style="margin-bottom: 20px;">
                        <h3 class="col-md-2"><?php echo $client_class->client_name; ?></h3>
                        <div class="col-md-3">
                            <div class="col-md-2 no-padding" style="text-align: right;padding-top: 10px !important;"><i class="glyphicon glyphicon-earphone" style="font-size: 24px"></i></div>
                            <div class="col-md-10 no-padding" style="font-size: 16px">
                                <div class="col-md-12"><?php echo $client_class->client_phone1; ?></div>
                                <div class="col-md-12"><?php echo $client_class->client_phone2; ?></div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="col-md-2 no-padding" style="text-align: right;padding-top: 10px !important;"><i class="glyphicon glyphicon-envelope" style="font-size: 24px"></i></div>
                            <div class="col-md-10 no-padding" style="font-size: 16px;padding-top: 10px !important;">
                                <div class="col-md-12"><?php echo $client_class->client_email; ?></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="col-md-2 no-padding" style="text-align: right;padding-top: 10px !important;"><i class="glyphicon glyphicon-pushpin" style="font-size: 24px"></i></div>
                            <div class="col-md-10 no-padding" style="font-size: 16px;padding-top: 10px !important;">
                                <div class="col-md-12"><?php echo $client_class->client_settings; ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h4 style="color: #199AC0;">Предложения для покупателя</h4>
                        <div class="btn-group">
                            <a class="btn btn-default" onclick="print_list_offers('.print_area')">
                                <i class="fa fa-print"></i> Печать
                            </a>
							<a class="btn btn-success" href="http://broker.alex-r.am/index.php?action=excel&id=<?php echo $_GET['id'] ?>">
								<i class="fa fa-file-excel-o"></i> Экспорт
							</a>
							<a class="btn btn-primary" href="http://broker.alex-r.am/index.php?action=buyer&subaction=view&id=<?php echo $_GET['id'] ?>">
								<i class="fa fa-pencil"></i> Параметры поиска
							</a>
                        </div>
                        <style>
                            th{
                                text-align: center;
                            }
                        </style>
                        <div class="col-md-3" style="float: right;margin-bottom: 10px;">
                            <select class="form-control" name="filter_by_status" onchange="filter_offers(<?php echo $id ?>,this)">
                                <option value="">Все</option>
                                <option <?php if(isset($_GET['offer_status']) && (int)$_GET['offer_status']==0){echo 'selected';} ?> value="0">Не показан</option>
                                <option <?php if(isset($_GET['offer_status']) && (int)$_GET['offer_status']==1){echo 'selected';} ?> value="1">Показан</option>
                                <option <?php if(isset($_GET['offer_status']) && (int)$_GET['offer_status']==2){echo 'selected';} ?> value="2">Куплен</option>
                            </select>
                        </div>
                        <table class="table table-hover table-bordered" style="text-align:center">
                            <tr>
                                <th>ID</th>
                                <th>Комнаты</th>

                                <th>Площадь</th>
                                <th>Этаж</th>
                                <th>Цена</th>
                                <th>Адрес</th>
                                <th>Владелец</th>
								<th>Доп. инфо</th>
                                <th>Изображение</th>
                                <th>Статус</th>
                                <?php if($user_level=='1'){ ?>
                                    <th>Оператор</th>
                                <?php } ?>
                                <th>Заметка</th>
                                <th>Дествия</th>
                            </tr>
                            <?php
                            $offers_class = new Offer($id);
                            if(isset($_GET['offer_status'])){
                                $status = (int)$_GET['offer_status'];
                            }
                            else{
                                $status = '';
                            }
                            $offers = $offers_class->get($status);
                            foreach($offers as $offer){
                                $post_class = new Post('ru',$offer->post_id);
                                $post = $post_class->get();
                                $location = '';
                                if($post->post_region>0){
                                    $location .= '<small>'.mwdb_get_var("SELECT region_ru FROM region WHERE region_id={var}",array($post->post_region)).', </small>';
                                }
                                if($post->post_city>0){
                                    $location .= '<small>'.mwdb_get_var("SELECT city_ru FROM city WHERE city_id={var}",array($post->post_city)).', </small>';
                                }
                                if($post->post_area>0){
                                    $location .= '<small>'.mwdb_get_var("SELECT area_ru FROM area WHERE area_id={var}",array($post->post_area)).', </small>';
                                }
                                if($post->post_microarea>0){
                                    $location .= '<small>'.mwdb_get_var("SELECT area_ru FROM microarea WHERE area_id={var}",array($post->post_microarea)).', </small>';
                                }
                                if($post->post_street>0){
                                    $location .= '<br>'.mwdb_get_var("SELECT street_ru FROM streets WHERE street_id={var}",array($post->post_street)).' ';
                                }
                                $location .= $post->post_address;
                                $owner = json_decode($post->post_owner);
                                $owner_show = ''.$owner->owner_name.'<br> '.$owner->owner_phone1.'<br> '.$owner->owner_phone2.'<br> '.$owner->owner_email.($post->arajarkam!=''?'Arajark.am: '.$post->arajarkam:'');
                                $images = json_decode($post->post_images,true);
                                if(isset($images[0]['img']) && $images[0]['img']!=''){
                                    $image_primary = 'http://user.alex-r.am/images/100/'.$images[0]['img'];
                                }
                                else{
                                    $image_primary = 'http://user.alex-r.am/LOGO.png';
                                }
                                if($post->post_status=='1'){
                                    $style_row = 'style="background:rgba(255, 255, 0, 0.15)"';
                                }
                                elseif($post->post_status=='2'){
                                    $style_row = 'style="background:rgba(210, 236, 161, 0.45);"';
                                }
                                else{
                                    $style_row = '';
                                }
                                switch($offer->offer_status){
                                    case '0':{
                                        $status = '<span class="label label-warning">Не показан</span>';
                                        break;
                                    }
                                    case '1':{
                                        $status = '<span class="label label-primary">Показан</span>';
                                        break;
                                    }
                                    case '2':{
                                        $status = '<span class="label label-success">Куплен</span>';
                                        break;
                                    }
                                }
                                $settings = json_decode($post->post_meta);
                                $price = '';
                                if(isset($settings->sale) && $settings->sale=='1'){
                                    $price = $settings->price_number.' '.$settings->price_currency;
                                }
                                if(isset($settings->rent) && $settings->rent=='1'){
                                    $price .= '<br><small>Аренда: </small><br>';
                                    if(isset($settings->rent_cost1) && (double)$settings->rent_cost1>0){
                                        $price .= $settings->rent_cost1.' '.$settings->rent_currency1;
                                    }
                                    elseif(isset($settings->rent_cost2) && (double)$settings->rent_cost2>0){
                                        $price .= $settings->rent_cost2.' '.$settings->rent_currency2;
                                    }
                                    elseif(isset($settings->rent_cost3) && (double)$settings->rent_cost3>0){
                                        $price .= $settings->rent_cost3.' '.$settings->rent_currency3;
                                    }
                                }
                                $attributes = json_decode($post->settings);
                                if(isset($attributes->area)){
                                    $area = $attributes->area;
                                }
                                elseif($attributes->home_area){
                                    $area = $attributes->home_area;
                                }
                                else{
                                    $area = '';
                                }
                                $fl = '';
                                if(isset($attributes->floor) && $attributes->floor!=''){
                                    $fl .= $attributes->floor;
                                }
                                if(isset($attributes->floorer) && $attributes->floorer!=''){
                                    $fl .= '/'.$attributes->floorer;
                                }
                                if(isset($attributes->rooms) && $attributes->rooms!=''){
                                    $rooms = $attributes->rooms;
                                }
                                ?>
                                <tr <?php echo $style_row; ?>>
                                    <td><?php echo $post->post_id; ?></td>
                                    <td><?php echo $rooms; ?></td>

                                    <td><?php echo $area; ?> м2</td>
                                    <td><?php echo $fl; ?></td>
                                    <td><?php echo $price; ?></td>
                                    <td><?php echo $location; ?></td>
                                    <td><?php echo $owner_show; ?></td>
									<td style="width:8%"><?php echo nl2br($post->post_dop); ?></td>
                                    <td><img src="<?php echo $image_primary; ?>" width="100"></td>
                                    <td><?php echo $status; ?></td>
                                    <?php if($user_level=='1'){ ?>
                                        <td><?php echo mwdb_get_var("SELECT user_name FROM users WHERE user_id={var}",array($post->post_user)); ?></td>
                                    <?php } ?>
                                    <td style="width: 30%">
                                        <a onclick="edit_offer_text(<?php echo $offer->offer_id; ?>,this)" style="display: inline-block;float: right;cursor: pointer">
                                            <span class="glyphicon glyphicon-pencil" style="font-size: 20px"></span>
                                        </a>
                                        <div class="offer_text_block" style="float: left;">
                                            <?php echo $offer->offer_text; ?>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="http://alex-r.am/estate/<?php echo $post->post_id; ?>/?user_id=<?php echo $_SESSION['user_id']; ?>" target="_blank" class="btn btn-default">
                                                <i class="fa fa-eye" data-toggle="tooltip" title="" data-original-title="Просмотр"></i>
                                            </a>
                                            <?php if($offer->offer_status=='0'){ ?>
                                                <a href="http://broker.alex-r.am/index.php?action=buyer&subaction=change_status&status=1&id=<?php echo $offer->offer_id; ?>" class="btn btn-default">
                                                    <i class="fa fa-check-circle-o" data-toggle="tooltip" title="" data-original-title="Показан"></i>
                                                </a>
                                            <?php } else {
                                                ?>
                                                <a href="http://broker.alex-r.am/index.php?action=buyer&subaction=change_status&status=2&id=<?php echo $offer->offer_id; ?>" class="btn btn-default">
                                                    <i class="fa fa-check-circle" data-toggle="tooltip" title="" data-original-title="Куплен"></i>
                                                </a>
                                                <?php
                                            } ?>
                                            <a onclick="ajax_remove_fromoffer(this,'<?php echo $offer->offer_id; ?>');" class="btn btn-danger">
                                                <i class="fa fa-trash-o" data-toggle="tooltip" title="" data-original-title="Удалить"></i>
                                            </a>
											<a href="http://broker.alex-r.am/index.php?action=post&subaction=map&id=<?php echo $post->post_id; ?>" target="_blank" class="btn btn-default">
												<i class="fa fa-map-marker" data-toggle="tooltip" title="" data-original-title="Место"></i>
											</a>
											<a title="E-mail" class="btn <?php if($offer->offer_email==0){echo 'btn-success';} else{echo 'btn-default';} ?>" onclick="open_modal(this,'<?php echo $offer->offer_id; ?>')">
                                                <i class="fa fa-envelope" data-toggle="tooltip" title="" data-original-title="E-mail"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </table>
                        <div class="print_area" style="display: none">
                            <div style="display: inline-block;width: 100%">
                                <h3><?php echo $client_class->client_name; ?></h3>
                                <h5><?php echo $client_class->client_phone1;echo '<br>'; echo $client_class->client_phone2; ?></h5>
                            </div>
                            <table border="1" style="border-collapse: collapse;width: 100%;text-align: center;font-size: 12px">
                                <tr>
                                    <th>ID</th>
                                    <th>Кол-во комнат</th>
                                    <th>Площадь</th>
                                    <th>Этаж</th>
                                    <th>Цена</th>
                                    <th>Адрес,Тел.</th>
                                    <th style="width: 30%">Заметка</th>
                                </tr>
                                <?php
                                foreach($offers as $offer){
                                    $post_class = new Post('ru',$offer->post_id);
                                    $post = $post_class->get();
                                    $location = '';
                                    if($post->post_region>0){
                                        $location .= mwdb_get_var("SELECT region_ru FROM region WHERE region_id={var}",array($post->post_region)).', ';
                                    }
                                    if($post->post_city>0){
                                        $location .= mwdb_get_var("SELECT city_ru FROM city WHERE city_id={var}",array($post->post_city)).', ';
                                    }
                                    if($post->post_area>0){
                                        $location .= mwdb_get_var("SELECT area_ru FROM area WHERE area_id={var}",array($post->post_area)).', ';
                                    }
                                    if($post->post_microarea>0){
                                        $location .= mwdb_get_var("SELECT area_ru FROM microarea WHERE area_id={var}",array($post->post_microarea)).', ';
                                    }
                                    if($post->post_street>0){
                                        $location .= mwdb_get_var("SELECT street_ru FROM streets WHERE street_id={var}",array($post->post_street)).' ';
                                    }
                                    $location .= $post->post_address;
                                    $owner = json_decode($post->post_owner);
                                    $owner_show = $owner->owner_name.'<br>'.$owner->owner_phone1.'<br>'.$owner->owner_phone2;
                                    $attributes = json_decode($post->settings);
                                    if(isset($attributes->rooms)){
                                        $rooms = $attributes->rooms;
                                    }
                                    else{
                                        $rooms = '';
                                    }
                                    if(isset($attributes->area)){
                                        $area = $attributes->area;
                                    }
                                    elseif($attributes->home_area){
                                        $area = $attributes->home_area;
                                    }
                                    else{
                                        $area = '';
                                    }
                                    $fl = '';
                                    if(isset($attributes->floor) && $attributes->floor!=''){
                                        $fl .= $attributes->floor;
                                    }
                                    if(isset($attributes->floorer) && $attributes->floorer!=''){
                                        $fl .= '/'.$attributes->floorer;
                                    }
                                    $settings = json_decode($post->post_meta);
                                    if(isset($settings->sale) && $settings->sale=='1'){
                                        $price = $settings->price_number.' '.$settings->price_currency;
                                    }
                                    if(isset($settings->rent) && $settings->rent=='1'){
                                        $price .= '<br>Аренда: ';
                                        if(isset($settings->rent_cost1) && (double)$settings->rent_cost1>0){
                                            $price .= $settings->rent_cost1.' '.$settings->rent_currency1;
                                        }
                                        elseif(isset($settings->rent_cost2) && (double)$settings->rent_cost2>0){
                                            $price .= $settings->rent_cost2.' '.$settings->rent_currency2;
                                        }
                                        elseif(isset($settings->rent_cost3) && (double)$settings->rent_cost3>0){
                                            $price .= $settings->rent_cost3.' '.$settings->rent_currency3;
                                        }
                                    }
                                    ?>
                                    <tr>
                                        <td><?php echo $post->post_id; ?></td>
                                        <td><?php echo $rooms; ?></td>
                                        <td><?php echo $area; ?> м2</td>
                                        <td><?php echo $fl; ?></td>
                                        <td><?php echo $price; ?></td>
                                        <td><?php echo $location.'<br>'.$owner_show; ?></td>
                                        <td><?php echo $offer->offer_text; ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<!-- Modal -->
	<div id="soldModal" class="modal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" onclick="close_dialog()">&times;</button>
					<h4 class="modal-title">Отправить на E-mail</h4>
				</div>
				<form action="" method="post">
					<div class="modal-body">

						<div class="col-md-6">
							<label>E-mail</label>
							<input type="email" name="to_email" required class="form-control" value="<?php echo $client_class->client_email; ?>" />
						</div>
					</div>
					<div class="modal-footer col-md-12">
						<input type="hidden" name="off_id" class="off_id">
						<button type="submit" name="sold_submit" value="sold_submit" class="btn btn-success">Отправить</button>
					</div>
					<div style="clear: both;"></div>
				</form>
			</div>

		</div>
	</div>
</section>