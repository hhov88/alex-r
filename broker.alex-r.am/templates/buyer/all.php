<?php
if(isset($_POST['s']) && $_POST['s']=='search'){
	$_SESSION['post'] = $_POST;
}
else{
	if(isset($_GET['s_page']) && (int)$_GET['s_page']>0){}else{
		unset($_SESSION['post']);
	}
}
if(isset($_POST['is_submit']) && $_POST['is_submit']=='1'){
	$_SESSION['post1'] = $_POST;
}
else{
	if(isset($_GET['page']) && (int)$_GET['page']>0){
		$_POST = $_SESSION['post1'];
	}else{
		unset($_SESSION['post1']);
	}
}
?>
<section class="content-header">
    <h1>Все Клиенты (<span class="count_posts_all_in" id="myspan"></span>)</h1>
    <div class="serach_block">
        <form name="serach" class="search_form" action="" method="post">
            <input type="search" name="word" class="word" <?php if(isset($_POST['word'])){echo 'value="'.$_POST['word'].'"';} ?>>
            <select name="type" class="search_type">
                <option <?php if(isset($_POST['type']) && $_POST['type']=='id'){echo 'selected';} ?> value="id" selected>id</option>
                <option <?php if(isset($_POST['type']) && $_POST['type']=='phone'){echo 'selected';} ?> value="phone">phone/name</option>
            </select>
            <input type="submit" name="s" value="search">
        </form>
    </div>
    <div style="float:right">
        <a href="http://broker.alex-r.am/index.php?action=buyer&subaction=all">
            <span style="background: #fff;display: inline-block;width: 20px;height: 20px;margin-left: 40px"></span>Все
        </a>
        <a href="http://broker.alex-r.am/index.php?action=buyer&subaction=all&client_type=2">
            <span style="background: rgba(134, 199, 10, 0.45);display: inline-block;width: 20px;height: 20px;margin-left: 40px"></span>Первичный
        </a>
        <a href="http://broker.alex-r.am/index.php?action=buyer&subaction=all&client_type=1">
            <span style="background: rgb(222, 212, 139);display: inline-block;width: 20px;height: 20px;margin-left: 40px"></span>Вторичный
        </a>
    </div>
    <div class="col-md-12" style="background: #fff;padding:10px 20px;position: relative">
        <form action="" method="post">
            <div class="col-md-12" style="font-weight: bold;border-bottom: 1px solid #eee">Филтр</div>
            <div class="col-md-2">
                <?php
                $result_categories = mwdb_select("SELECT category_id,category_title_ru FROM category WHERE 1 ORDER BY category_order",array(1));
                foreach($result_categories as $row_category){
                    ?>
                    <div class="col-md-12" style="margin: 0">
                        <input type="radio" <?php if(isset($_POST['category']) && $_POST['category']==$row_category->category_id){echo 'checked';} ?> name="category" onclick="find_category_filter_settings(this)" value="<?php echo $row_category->category_id ?>"><span><?php echo $row_category->category_title_ru; ?></span>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="col-md-5 for_all_filter_settings">
                <?php
                if(isset($_POST['category'])){
                    switch($_POST['category']){
                        case '5':{
                            ?>
                            <div class="col-md-12">
                                <div class="col-md-2">Комнаты</div>
                                <div class="col-md-9">
                                    <input type="checkbox" name="rooms[]" <?php if(in_array('1',$_POST['rooms'])){echo 'checked';} ?> value="1" style="margin-left: 10px">1
                                    <input type="checkbox" name="rooms[]" <?php if(in_array('2',$_POST['rooms'])){echo 'checked';} ?> value="2" style="margin-left: 10px">2
                                    <input type="checkbox" name="rooms[]" <?php if(in_array('3',$_POST['rooms'])){echo 'checked';} ?> value="3" style="margin-left: 10px">3
                                    <input type="checkbox" name="rooms[]" <?php if(in_array('4',$_POST['rooms'])){echo 'checked';} ?> value="4" style="margin-left: 10px">4
                                    <input type="checkbox" name="rooms[]" <?php if(in_array('5',$_POST['rooms'])){echo 'checked';} ?> value="5" style="margin-left: 10px">5+
                                </div>
                            </div>
							<div class="col-md-6">
                            <div class="col-md-12">
                                <div class="col-md-2" style="padding-top: 4px;">Этаж</div>
                                <div class="col-md-9">
                                    <div class="col-md-4">
                                        <select name="floor_from" class="form-control" style="height: 20px">
                                            <option value="">От</option>
                                            <?php
                                            for($i=1;$i<21;$i++){
                                                ?>
                                                <option <?php if(isset($_POST['floor_from']) && $_POST['floor_from']==$i){echo 'selected';} ?> value="<?php echo $i ?>"><?php echo $i ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-1" style="text-align: center">_</div>
                                    <div class="col-md-4">
                                        <select name="floor_to" class="form-control" style="height: 20px">
                                            <option value="">От</option>
                                            <?php
                                            for($i=1;$i<21;$i++){
                                                ?>
                                                <option <?php if(isset($_POST['floor_to']) && $_POST['floor_to']==$i){echo 'selected';} ?> value="<?php echo $i ?>"><?php echo $i ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2" style="padding-top: 4px;">Площадь</div>
                                <div class="col-md-9">
                                    <div class="col-md-4">
                                        <input type="text" name="area_from" class="form-control" style="height: 20px" <?php if(isset($_POST['area_from'])){echo 'value="'.$_POST['area_from'].'"';} ?>>
                                    </div>
                                    <div class="col-md-1" style="text-align: center">_</div>
                                    <div class="col-md-4">
                                        <input type="text" name="area_to" class="form-control" style="height: 20px" <?php if(isset($_POST['area_to'])){echo 'value="'.$_POST['area_to'].'"';} ?>>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2" style="padding-top: 4px;">Цена</div>
                                <div class="col-md-9">
                                    <div class="col-md-4">
                                        <input type="text" name="price_from" class="form-control" style="height: 20px" placeholder="USD" <?php if(isset($_POST['price_from'])){echo 'value="'.$_POST['price_from'].'"';} ?>>
                                    </div>
                                    <div class="col-md-1" style="text-align: center">_</div>
                                    <div class="col-md-4">
                                        <input type="text" name="price_to" class="form-control" style="height: 20px" placeholder="USD" <?php if(isset($_POST['price_to'])){echo 'value="'.$_POST['price_to'].'"';} ?>>
                                    </div>
                                </div>
                            </div>
							</div>
							<div class="col-md-6">
								<div class="col-md-12"><input type="checkbox" name="building_type[]" value="building_type1" <?php if(isset($_POST['building_type']) && in_array('building_type1',$_POST['building_type'])){echo 'checked';} ?>> монолитный</div>
								<div class="col-md-12"><input type="checkbox" name="building_type[]" value="building_type2" <?php if(isset($_POST['building_type']) && in_array('building_type2',$_POST['building_type'])){echo 'checked';} ?>> каменный</div>
								<div class="col-md-12"><input type="checkbox" name="building_type[]" value="building_type3" <?php if(isset($_POST['building_type']) && in_array('building_type3',$_POST['building_type'])){echo 'checked';} ?>> крупнопанельный</div>
								<div class="col-md-12"><input type="checkbox" name="building_type[]" value="building_type4" <?php if(isset($_POST['building_type']) && in_array('building_type4',$_POST['building_type'])){echo 'checked';} ?>> каркаснопанельный</div>
							</div>
                            <?php
                            break;
                        }
                        case '6':{
                            ?>
                            <div class="col-md-12">
                                <div class="col-md-2">Комнаты</div>
                                <div class="col-md-9">
                                    <input type="checkbox" <?php if(in_array('1',$_POST['rooms'])){echo 'checked';} ?> name="rooms[]" value="1" style="margin-left: 10px">1
                                    <input type="checkbox" <?php if(in_array('2',$_POST['rooms'])){echo 'checked';} ?> name="rooms[]" value="2" style="margin-left: 10px">2
                                    <input type="checkbox" <?php if(in_array('3',$_POST['rooms'])){echo 'checked';} ?> name="rooms[]" value="3" style="margin-left: 10px">3
                                    <input type="checkbox" <?php if(in_array('4',$_POST['rooms'])){echo 'checked';} ?> name="rooms[]" value="4" style="margin-left: 10px">4
                                    <input type="checkbox" <?php if(in_array('5',$_POST['rooms'])){echo 'checked';} ?> name="rooms[]" value="5" style="margin-left: 10px">5+
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2" style="padding-top: 4px;">Этажность</div>
                                <div class="col-md-9">
                                    <div class="col-md-2">
                                        <select name="floor_from" class="form-control" style="height: 20px">
                                            <option value="">От</option>
                                            <?php
                                            for($i=1;$i<6;$i++){
                                                ?>
                                                <option <?php if(isset($_POST['floor_from']) && $_POST['floor_from']==$i){echo 'selected';} ?> value="<?php echo $i ?>"><?php echo $i ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-1" style="text-align: center">_</div>
                                    <div class="col-md-2">
                                        <select name="floor_to" class="form-control" style="height: 20px">
                                            <option value="">До</option>
                                            <?php
                                            for($i=1;$i<6;$i++){
                                                ?>
                                                <option <?php if(isset($_POST['floor_to']) && $_POST['floor_to']==$i){echo 'selected';} ?> value="<?php echo $i ?>"><?php echo $i ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2" style="padding-top: 4px;">Площадь</div>
                                <div class="col-md-9">
                                    <div class="col-md-2">
                                        <input type="text" name="area_from" class="form-control" style="height: 20px" <?php if(isset($_POST['area_from'])){echo 'value="'.$_POST['area_from'].'"';} ?>>
                                    </div>
                                    <div class="col-md-1" style="text-align: center">_</div>
                                    <div class="col-md-2">
                                        <input type="text" name="area_to" class="form-control" style="height: 20px" <?php if(isset($_POST['area_to'])){echo 'value="'.$_POST['area_to'].'"';} ?>>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2" style="padding-top: 4px;">Цена</div>
                                <div class="col-md-9">
                                    <div class="col-md-2">
                                        <input type="text" name="price_from" class="form-control" style="height: 20px" placeholder="USD" <?php if(isset($_POST['price_from'])){echo 'value="'.$_POST['price_from'].'"';} ?>>
                                    </div>
                                    <div class="col-md-1" style="text-align: center">_</div>
                                    <div class="col-md-2">
                                        <input type="text" name="price_to" class="form-control" style="height: 20px" placeholder="USD" <?php if(isset($_POST['price_to'])){echo 'value="'.$_POST['price_to'].'"';} ?>>
                                    </div>
                                </div>
                            </div>
                            <?php
                            break;
                        }
                        case '7':{
                            ?>
                            <div class="col-md-12">
                                <div class="col-md-2" style="padding-top: 4px;">Тип</div>
                                <div class="col-md-9">
                                    <div class="col-md-4">
                                        <select name="type" class="form-control" style="height: 20px">
                                            <option value="">-- Выбрать --</option>
                                            <option <?php if(isset($_POST['type']) && $_POST['type']=='cm_type2'){echo 'selected';} ?> value="cm_type2">гостиница</option>
                                            <option <?php if(isset($_POST['type']) && $_POST['type']=='cm_type1'){echo 'selected';} ?> value="cm_type1">оффис</option>
                                            <option <?php if(isset($_POST['type']) && $_POST['type']=='cm_type3'){echo 'selected';} ?> value="cm_type3">развлекательный</option>
                                            <option <?php if(isset($_POST['type']) && $_POST['type']=='cm_type4'){echo 'selected';} ?> value="cm_type4">магазин</option>
                                            <option <?php if(isset($_POST['type']) && $_POST['type']=='cm_type5'){echo 'selected';} ?> value="cm_type5">парикмахерская</option>
                                            <option <?php if(isset($_POST['type']) && $_POST['type']=='cm_type6'){echo 'selected';} ?> value="cm_type6">автотехобслуживание</option>
                                            <option <?php if(isset($_POST['type']) && $_POST['type']=='cm_type7'){echo 'selected';} ?> value="cm_type7">склад</option>
                                            <option <?php if(isset($_POST['type']) && $_POST['type']=='cm_type8'){echo 'selected';} ?> value="cm_type8">сельскохозяйственный</option>
                                            <option <?php if(isset($_POST['type']) && $_POST['type']=='cm_type9'){echo 'selected';} ?> value="cm_type9">бизнес центр</option>
                                            <option <?php if(isset($_POST['type']) && $_POST['type']=='cm_type10'){echo 'selected';} ?> value="cm_type10">производственный</option>
                                            <option <?php if(isset($_POST['type']) && $_POST['type']=='cm_type11'){echo 'selected';} ?> value="cm_type11">учебный</option>
                                            <option <?php if(isset($_POST['type']) && $_POST['type']=='cm_type12'){echo 'selected';} ?> value="cm_type12">медицинские заведения</option>
                                            <option <?php if(isset($_POST['type']) && $_POST['type']=='cm_type13'){echo 'selected';} ?> value="cm_type13">универсальный</option>
                                            <option <?php if(isset($_POST['type']) && $_POST['type']=='cm_type14'){echo 'selected';} ?> value="cm_type14">другое</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2" style="padding-top: 4px;">Площадь</div>
                                <div class="col-md-9">
                                    <div class="col-md-2">
                                        <input type="text" name="area_from" class="form-control" style="height: 20px" <?php if(isset($_POST['area_from'])){echo 'value="'.$_POST['area_from'].'"';} ?>>
                                    </div>
                                    <div class="col-md-1" style="text-align: center">_</div>
                                    <div class="col-md-2">
                                        <input type="text" name="area_to" class="form-control" style="height: 20px" <?php if(isset($_POST['area_to'])){echo 'value="'.$_POST['area_to'].'"';} ?>>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2" style="padding-top: 4px;">Цена</div>
                                <div class="col-md-9">
                                    <div class="col-md-2">
                                        <input type="text" name="price_from" class="form-control" style="height: 20px" placeholder="USD" <?php if(isset($_POST['price_from'])){echo 'value="'.$_POST['price_from'].'"';} ?>>
                                    </div>
                                    <div class="col-md-1" style="text-align: center">_</div>
                                    <div class="col-md-2">
                                        <input type="text" name="price_to" class="form-control" style="height: 20px" placeholder="USD" <?php if(isset($_POST['price_to'])){echo 'value="'.$_POST['price_to'].'"';} ?>>
                                    </div>
                                </div>
                            </div>
                            <?php
                            break;
                        }
                        case '8':{
                            ?>
                            <div class="col-md-12">
                                <div class="col-md-2" style="padding-top: 4px;">Площадь</div>
                                <div class="col-md-9">
                                    <div class="col-md-2">
                                        <input type="text" name="area_from" class="form-control" style="height: 20px" <?php if(isset($_POST['area_from'])){echo 'value="'.$_POST['area_from'].'"';} ?>>
                                    </div>
                                    <div class="col-md-1" style="text-align: center">_</div>
                                    <div class="col-md-2">
                                        <input type="text" name="area_to" class="form-control" style="height: 20px" <?php if(isset($_POST['area_to'])){echo 'value="'.$_POST['area_to'].'"';} ?>>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2" style="padding-top: 4px;">Цена</div>
                                <div class="col-md-9">
                                    <div class="col-md-2">
                                        <input type="text" name="price_from" class="form-control" style="height: 20px" placeholder="USD" <?php if(isset($_POST['price_from'])){echo 'value="'.$_POST['price_from'].'"';} ?>>
                                    </div>
                                    <div class="col-md-1" style="text-align: center">_</div>
                                    <div class="col-md-2">
                                        <input type="text" name="price_to" class="form-control" style="height: 20px" placeholder="USD" <?php if(isset($_POST['price_to'])){echo 'value="'.$_POST['price_to'].'"';} ?>>
                                    </div>
                                </div>
                            </div>
                            <?php
                            break;
                        }
                        case '9':{
                            ?>
                            <div class="col-md-12">
                                <div class="col-md-2" style="padding-top: 4px;">Площадь</div>
                                <div class="col-md-9">
                                    <div class="col-md-2">
                                        <input type="text" name="area_from" class="form-control" style="height: 20px" <?php if(isset($_POST['area_from'])){echo 'value="'.$_POST['area_from'].'"';} ?>>
                                    </div>
                                    <div class="col-md-1" style="text-align: center">_</div>
                                    <div class="col-md-2">
                                        <input type="text" name="area_to" class="form-control" style="height: 20px" <?php if(isset($_POST['area_to'])){echo 'value="'.$_POST['area_to'].'"';} ?>>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2" style="padding-top: 4px;">Цена</div>
                                <div class="col-md-9">
                                    <div class="col-md-2">
                                        <input type="text" name="price_from" class="form-control" style="height: 20px" placeholder="USD" <?php if(isset($_POST['price_from'])){echo 'value="'.$_POST['price_from'].'"';} ?>>
                                    </div>
                                    <div class="col-md-1" style="text-align: center">_</div>
                                    <div class="col-md-2">
                                        <input type="text" name="price_to" class="form-control" style="height: 20px" placeholder="USD" <?php if(isset($_POST['price_to'])){echo 'value="'.$_POST['price_to'].'"';} ?>>
                                    </div>
                                </div>
                            </div>
                            <?php
                            break;
                        }
                    }
                }
                ?>
            </div>
            <div class="col-md-5">
                <div class="col-md-12">
                    <div class="col-md-5">
                        <select name="f_region" class="form-control f_region" style="height: 20px" onchange="get_f_city(this)">
                            <?php
                            $result_region = mwdb_select("SELECT region_id,region_ru FROM region WHERE 1 ORDER BY region_order",array());
                            foreach($result_region as $row_region){
                                ?>
                                <option <?php if(isset($_POST['f_region']) && $_POST['f_region']==$row_region->region_id){echo 'selected';} ?> value="<?php echo $row_region->region_id; ?>"><?php echo $row_region->region_ru; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-5">
                        <select name="f_city" class="form-control f_city" style="height: 20px" onchange="get_f_area(this)">
                            <option value="0">-- Город/община --</option>
                            <?php
                            $result_cities = mwdb_select("SELECT city_id,city_ru FROM city WHERE region={var} ORDER BY city_order",array(8));
                            foreach ($result_cities as $result_city) {
                                ?>
                                <option <?php if(isset($_POST['f_city']) && $_POST['f_city']==$result_city->city_id){echo 'selected';} ?> value="<?php echo $result_city->city_id; ?>"><?php echo $result_city->city_ru; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-5">
                        <select name="f_area" class="form-control f_area" style="height: 20px" onchange="get_f_microarea(this)">
                            <option value="0">-- Район --</option>
                            <?php
                            if(isset($_POST['f_city']) && $_POST['f_city']>0){
                                $result_areas = mwdb_select("SELECT area_id,area_ru FROM area WHERE city={var} ORDER BY area_order",array($_POST['f_city']));
                                foreach($result_areas as $row_area){
                                    ?>
                                    <option <?php if(isset($_POST['f_area']) && $_POST['f_area']==$row_area->area_id){echo 'selected';} ?> value="<?php echo $row_area->area_id; ?>"><?php echo $row_area->area_ru; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-5">
                        <select name="f_microarea" class="form-control f_microarea" style="height: 20px">
                            <option value="0">-- Микрорайон --</option>
                            <?php
                            if(isset($_POST['f_area']) && $_POST['f_area']>0){
                                $result_areas = mwdb_select("SELECT area_id,area_ru FROM microarea WHERE area={var} ORDER BY area_order",array($_POST['f_area']));
                                foreach($result_areas as $row_area){
                                    ?>
                                    <option <?php if(isset($_POST['f_microarea']) && $_POST['f_microarea']==$row_area->area_id){echo 'selected';} ?> value="<?php echo $row_area->area_id; ?>"><?php echo $row_area->area_ru; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <!--<div class="col-md-5">
                    <input type="text" name="f_street_input" class="form-control f_street_input" oninput="get_f_streets(this);" style="height: 20px" placeholder="Улица" value="<?php if(isset($_POST['f_street_input'])){echo $_POST['f_street_input'];} ?>">
                    <div class="f_street_res" style="position: absolute;z-index: 100;background: #fff;padding: 0px 10px;border: 1px solid #ccc;width: 100%;border-top: none;max-height: 200px;overflow: auto;"></div>
                    <input type="hidden" name="f_street" value="<?php if(isset($_POST['f_street'])){echo $_POST['f_street'];} ?>" class="f_street">
                </div>-->
				<div class="col-md-12">
                    <div class="col-md-3"><input type="radio" name="ad_name" <?php if(!isset($_POST['ad_name']) || ($_POST['ad_name']!='1' && $_POST['ad_name']!='2')){echo 'checked';} ?> value="">Все</div>
                    <div class="col-md-3"><input type="radio" name="ad_name" <?php if(isset($_POST['ad_name']) && $_POST['ad_name']=='1'){echo 'checked';} ?> value="1">Продажа</div>
                    <div class="col-md-3"><input type="radio" name="ad_name" <?php if(isset($_POST['ad_name']) && $_POST['ad_name']=='2'){echo 'checked';} ?> value="2">Аренда</div>
                </div>
            </div>
            <input type="hidden" name="is_submit" value="1">
            <div style="position: absolute;width: 95px;bottom: 10px;right: 10px">
                <button type="submit" class="btn btn-block btn-success btn-social" style="padding: 5px;font-size: 13px;">
                    <i class="fa fa-search" style="font-size: 13px !important;line-height: 28px !important;"></i> Найти
                </button>
            </div>
        </form>
    </div>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body table-responsive">
                    <style>
                        th{text-align: center}
                    </style>
                    <table class="table table-hover table-bordered" style="text-align: center">
                        <tr>
                            <th>ID</th>
                            <th>Инфо клиента</th>
                            <th>Район</th>
                            <th>Категория</th>
                            <th>Цена</th>
                            <th>Площадь</th>
                            <th>Комнаты</th>
                            <th>Этаж</th>
                            <th>Дата регистрации</th>
                            <th>Статус</th>
                            <th>Дествия</th>
                        </tr>
                        <?php
                        if(isset($_GET['page'])){
                            $page = (int)$_GET['page'];
                            if($page<2){
                                $page = 1;
                            }
                        }
                        else{
							if(isset($_GET['s_page'])){
								$page = (int)$_GET['s_page'];
								if($page<2){
									$page = 1;
								}
							}
							else{
								$page = 1;
							}
                        }
                        $offset = ($page-1)*20;
                        global $user_level;
                        if(isset($_GET['client_type'])){
                            $client_type = (int)$_GET['client_type'];
                            $qu_part = ' AND client_type='.$client_type.' ';
                        }
                        else{
                            $qu_part = '';
                        }
                        if(isset($_POST['s']) && $_POST['s']=='search'){
                            $word = (string)$_POST['word'];
                            $type = (string)$_POST['type'];
                            if($type=='id' && $word!=''){
                                $qu_part .= " AND client_id LIKE '%$word%' ";
                            }
                            elseif($type=='phone' && $word!=''){
                                $qu_part .= " AND (client_name LIKE '%$word%' OR client_phone1 LIKE '%$word%' OR client_phone2 LIKE '%$word%') ";
                            }
                        }
						elseif(isset($_SESSION['post']['word']) && isset($_SESSION['post']['type'])){
							$word = (string)$_SESSION['post']['word'];
                            $type = (string)$_SESSION['post']['type'];
                            if($type=='id' && $word!=''){
                                $qu_part .= " AND client_id LIKE '%$word%' ";
                            }
                            elseif($type=='phone' && $word!=''){
                                $qu_part .= " AND (client_name LIKE '%$word%' OR client_phone1 LIKE '%$word%' OR client_phone2 LIKE '%$word%') ";
                            }
						}
                        if(isset($_POST['is_submit']) && $_POST['is_submit']=='1'){
                            $query_part = '1';
                            if(isset($_POST['f_region']) && $_POST['f_region']>0){
                                $query_part .= " AND region=". $_POST['f_region']." ";
                            }
                            if(isset($_POST['f_city']) && $_POST['f_city']>0){
                                $query_part .= " AND city LIKE '%". $_POST['f_city']."%' ";
                            }
                            if(isset($_POST['f_area']) && $_POST['f_area']>0){
                                $query_part .= " AND area LIKE '%". $_POST['f_area']."%' ";
                            }
                            if(isset($_POST['f_microarea']) && $_POST['f_microarea']>0){
                                $query_part .= " AND microarea LIKE '%". $_POST['f_microarea']."%' ";
                            }
                            if(isset($_POST['price_from']) && $_POST['price_from']>0){
                                $query_part .= " AND (price_from>=".$_POST['price_from']." AND price_from!=0)";
                            }
                            if(isset($_POST['price_to']) && $_POST['price_to']>0){
                                $query_part .= " AND (price_to<=".$_POST['price_to']." AND price_to!=0) ";
                            }
							if(isset($_POST['ad_name']) && $_POST['ad_name']>0){
                                if($_POST['ad_name']==1){
									$query_part .= " AND type_search='sale' ";
								}
								if($_POST['ad_name']==2){
									$query_part .= " AND type_search='rent' ";
								}
                            }
                            if(isset($_POST['category'])){
                                $query_part .= " AND category=".$_POST['category']." ";
                                /*$q = '1=1 AND ';
                                switch($_POST['category']){
                                    case '5':{
                                        if(isset($_POST['rooms'])){
                                            $q = '(';
                                            foreach($_POST['rooms'] as $room){
                                                $q .= " rooms LIKE '%$rooms%' OR ";
                                            }
                                            $q = ') ';
                                        }
                                        if(isset($_POST['floor_from'])){

                                        }
                                        break;
                                    }
                                }*/
								switch($_POST['category']){
									case '5':{
										$q = '1=1 AND ';
										if(isset($_POST['rooms'])){
                                            $q .= '(';
                                            foreach($_POST['rooms'] as $room){
                                                $q .= " rooms LIKE '%$room%' OR ";
                                            }
											$q = substr($q,0,-3);
                                            $q .= ') AND ';
                                        }
										if(isset($_POST['floor_from']) && $_POST['floor_from']>0){
											$floor_from = $_POST['floor_from'];
											$q .= " floor>='".$floor_from."' AND ";
										}
										if(isset($_POST['floor_to']) && $_POST['floor_to']>0){
											$floor_to = (int)$_POST['floor_to'];
											$q .= ' (';
											for($fl_to=1;$fl_to<=$floor_to;$fl_to++){
												$q .= " floor LIKE '%-$fl_to' OR ";
											}
											$q = substr($q,0,-3);
											$q .= ') AND ';
										}
										if(isset($_POST['area_from']) && $_POST['area_from']>0){
											$area_from = $_POST['area_from'];
											$q .= " area>='".$area_from."' AND ";
										}
										if(isset($_POST['area_to']) && $_POST['area_to']>0){
											$area_to = (int)$_POST['area_to'];
											$q .= ' ( area LIKE "%-" OR ';
											for($fl_to=1;$fl_to<=$area_to;$fl_to++){
												$q .= " area LIKE '%-$fl_to' OR ";
											}
											$q = substr($q,0,-3);
											$q .= ') AND ';
										}
										if(isset($_POST['building_type'])){
                                            $q .= '(';
                                            foreach($_POST['building_type'] as $building_type){
                                                $q .= " building_type LIKE '%$building_type%' OR ";
                                            }
											$q = substr($q,0,-3);
                                            $q .= ') AND ';
                                        }
										$q = substr($q,0,-4);
										$result_search_ids = mwdb_select("SELECT search_id FROM client_apartments WHERE $q");
										$in_m = '0,';
										foreach($result_search_ids as $row_search_id){
											$in_m .= $row_search_id->search_id.',';
										}
										$in_m = substr($in_m,0,-1);
										$query_part .= " AND search_id IN (".$in_m.") ";
										break;
									}
									case '6':{
										$q = '1=1 AND ';
										if(isset($_POST['rooms'])){
                                            $q .= '(';
                                            foreach($_POST['rooms'] as $room){
                                                $q .= " rooms LIKE '%$room%' OR ";
                                            }
											$q = substr($q,0,-3);
                                            $q .= ') AND ';
                                        }
										if(isset($_POST['floor_from']) && $_POST['floor_from']>0){
											$floor_from = $_POST['floor_from'];
											$q .= " floorer>='".$floor_from."' AND ";
										}
										if(isset($_POST['floor_to']) && $_POST['floor_to']>0){
											$floor_to = (int)$_POST['floor_to'];
											$q .= ' (';
											for($fl_to=1;$fl_to<=$floor_to;$fl_to++){
												$q .= " floorer LIKE '%-$fl_to' OR ";
											}
											$q = substr($q,0,-3);
											$q .= ') AND ';
										}
										if(isset($_POST['area_from']) && $_POST['area_from']>0){
											$area_from = $_POST['area_from'];
											$q .= " home_area>='".$area_from."' AND ";
										}
										if(isset($_POST['area_to']) && $_POST['area_to']>0){
											$area_to = (int)$_POST['area_to'];
											$q .= ' ( home_area LIKE "%-" OR ';
											for($fl_to=1;$fl_to<=$area_to;$fl_to++){
												$q .= " home_area LIKE '%-$fl_to' OR ";
											}
											$q = substr($q,0,-3);
											$q .= ') AND ';
										}
										$q = substr($q,0,-4);
										$result_search_ids = mwdb_select("SELECT search_id FROM client_houses WHERE $q");
										$in_m = '0,';
										foreach($result_search_ids as $row_search_id){
											$in_m .= $row_search_id->search_id.',';
										}
										$in_m = substr($in_m,0,-1);
										$query_part .= " AND search_id IN (".$in_m.") ";
										break;
									}
									case '7':{
										$q = '1=1 AND ';
										if(isset($_POST['type'])){
											$type = $_POST['type'];
                                            $q .= " type LIKE '%$type%' AND ";
                                        }
										if(isset($_POST['area_from']) && $_POST['area_from']>0){
											$area_from = $_POST['area_from'];
											$q .= " area>='".$area_from."' AND ";
										}
										if(isset($_POST['area_to']) && $_POST['area_to']>0){
											$area_to = (int)$_POST['area_to'];
											$q .= ' ( area LIKE "%-" OR ';
											for($fl_to=1;$fl_to<=$area_to;$fl_to++){
												$q .= " area LIKE '%-$fl_to' OR ";
											}
											$q = substr($q,0,-3);
											$q .= ') AND ';
										}
										$q = substr($q,0,-4);
										$result_search_ids = mwdb_select("SELECT search_id FROM client_commercial WHERE $q");
										$in_m = '0,';
										foreach($result_search_ids as $row_search_id){
											$in_m .= $row_search_id->search_id.',';
										}
										$in_m = substr($in_m,0,-1);
										$query_part .= " AND search_id IN (".$in_m.") ";
										break;
									}
									case '8':{
										$q = '1=1 AND ';
										if(isset($_POST['area_from']) && $_POST['area_from']>0){
											$area_from = $_POST['area_from'];
											$q .= " area>='".$area_from."' AND ";
										}
										if(isset($_POST['area_to']) && $_POST['area_to']>0){
											$area_to = (int)$_POST['area_to'];
											$q .= ' ( area LIKE "%-" OR ';
											for($fl_to=1;$fl_to<=$area_to;$fl_to++){
												$q .= " area LIKE '%-$fl_to' OR ";
											}
											$q = substr($q,0,-3);
											$q .= ') AND ';
										}
										$q = substr($q,0,-4);
										$result_search_ids = mwdb_select("SELECT search_id FROM client_land WHERE $q");
										$in_m = '0,';
										foreach($result_search_ids as $row_search_id){
											$in_m .= $row_search_id->search_id.',';
										}
										$in_m = substr($in_m,0,-1);
										$query_part .= " AND search_id IN (".$in_m.") ";
										break;
									}
									case '9':{
										$q = '1=1 AND ';
										if(isset($_POST['area_from']) && $_POST['area_from']>0){
											$area_from = $_POST['area_from'];
											$q .= " area>='".$area_from."' AND ";
										}
										if(isset($_POST['area_to']) && $_POST['area_to']>0){
											$area_to = (int)$_POST['area_to'];
											$q .= ' ( area LIKE "%-" OR ';
											for($fl_to=1;$fl_to<=$area_to;$fl_to++){
												$q .= " area LIKE '%-$fl_to' OR ";
											}
											$q = substr($q,0,-3);
											$q .= ') AND ';
										}
										$q = substr($q,0,-4);
										$result_search_ids = mwdb_select("SELECT search_id FROM client_garages WHERE $q");
										$in_m = '0,';
										foreach($result_search_ids as $row_search_id){
											$in_m .= $row_search_id->search_id.',';
										}
										$in_m = substr($in_m,0,-1);
										$query_part .= " AND search_id IN (".$in_m.") ";
										break;
									}
								}
                            }
                            $result_from_search = mwdb_select("SELECT client_id FROM client_search WHERE $query_part");
                            $in_part = '';
                            foreach($result_from_search as $row_from_search){
                                $in_part .= $row_from_search->client_id.',';
                            }
                            $in_part = substr($in_part,0,-1);
                            $qu_part .= " AND client_id IN ($in_part) ";
                        }
                        if($user_level=='1'){
                            //echo "SELECT * FROM post_client WHERE client_status!={var} $qu_part ORDER BY client_time DESC LIMIT {var},20";
                            $result_clients = mwdb_select("SELECT * FROM post_client WHERE client_status!={var} $qu_part ORDER BY client_time DESC LIMIT {var},20",array(6,$offset));
                        }
                        else{
                            $result_clients = mwdb_select("SELECT * FROM post_client WHERE client_status!={var} AND broker={var} $qu_part ORDER BY client_time DESC LIMIT {var},20",array(6,$_SESSION['user_id'],$offset));
                        }
                        foreach($result_clients as $row_client){
                            switch($row_client->client_status){
                                case '0':{
                                    $status = '<span class="label label-warning">Новый</span>';
                                    break;
                                }
                                case '1':{
                                    $status = '<span class="label label-primary">В процессе</span>';
                                    break;
                                }
                                case '2':{
                                    $status = '<span class="label label-success">Завершен</span>';
                                    break;
                                }
                            }
                            if($row_client->client_type=='1'){
                                $style_tr = 'style="background:rgb(222, 212, 139)"';
                            }
                            elseif($row_client->client_type=='2'){
                                $style_tr = 'style="background:rgba(134, 199, 10, 0.45);"';
                            }
                            else{
                                $style_tr = '';
                            }
                            $result_search_params = mwdb_get_row("SELECT search_id,type_search,price_from,price_to,category FROM client_search WHERE client_id={var}",array($row_client->client_id));
                            $category_name = mwdb_get_var("SELECT category_title_ru FROM category WHERE category_id={var}",array($result_search_params->category));
                            switch($result_search_params->type_search){
                                case 'sale':{
                                    $category_name .= ' <br>(Продажа)';
                                    break;
                                }
                                case 'rent':{
                                    $category_name .= ' <br>(Аренда)';
                                    break;
                                }
                            }
                            if($result_search_params->price_from>0){
                                $price_from = $result_search_params->price_from;
                            }
                            else{
                                $price_from = '';
                            }
                            if($result_search_params->price_to>0){
                                $price_to = $result_search_params->price_to;
                            }
                            else{
                                $price_to = '';
                            }
                            switch($result_search_params->category){
                                case '5':{
                                    $petq_attr = mwdb_get_row("SELECT area,rooms,floor,floorer FROM client_apartments WHERE search_id={var}",array($result_search_params->search_id));
                                    $area = $petq_attr->area;
                                    $rooms = $petq_attr->rooms;
                                    $floor = $petq_attr->floor.'/'.$petq_attr->floorer;
                                    break;
                                }
                                case '6':{
                                    $petq_attr = mwdb_get_row("SELECT home_area,rooms,floorer FROM client_houses WHERE search_id={var}",array($result_search_params->search_id));
                                    $area = $petq_attr->home_area;
                                    $rooms = $petq_attr->rooms;
                                    $floor = $petq_attr->floorer;
                                    break;
                                }
                                case '7':{
                                    $petq_attr = mwdb_get_row("SELECT area,rooms,floor FROM client_commercial WHERE search_id={var}",array($result_search_params->search_id));
                                    $area = $petq_attr->area;
                                    $rooms = $petq_attr->rooms;
                                    $floor = $petq_attr->floor;
                                    break;
                                }
                                case '8':{
                                    $petq_attr = mwdb_get_row("SELECT area FROM client_land WHERE search_id={var}",array($result_search_params->search_id));
                                    $area = $petq_attr->area;
                                    $rooms = '';
                                    $floor = '';
                                    break;
                                }
                                case '9':{
                                    $petq_attr = mwdb_get_row("SELECT area FROM client_garages WHERE search_id={var}",array($result_search_params->search_id));
                                    $area = $petq_attr->area;
                                    $rooms = '';
                                    $floor = '';
                                    break;
                                }
                            }
                            ?>
                            <tr <?php echo $style_tr; ?>>
                                <td><?php echo $row_client->client_id; ?></td>
                                <td><?php echo $row_client->client_name; ?><br><?php echo $row_client->client_phone1;if(isset($row_client->client_phone2) && $row_client->client_phone2!=''){echo ', '.$row_client->client_phone2;} ?><br><?php echo $row_client->client_email; ?></td>
                                <td style="text-align: left"><?php echo client_search_region($row_client->client_id); ?></td>
								<td><?php echo $category_name; ?></td>
                                <td><?php echo number_format($price_from,0,'',' ').' - '.number_format($price_to,0,'',' '); ?></td>
                                <td><?php echo $area; ?></td>
                                <td><?php echo $rooms; ?></td>
                                <td><?php echo $floor; ?></td>
                                <td><?php echo $row_client->client_time; ?></td>
                                <td><?php echo $status; ?></td>
                                <td>
                                    <div class="btn-group">
                                        <a target="_blank" href="http://broker.alex-r.am/index.php?action=buyer&subaction=view&id=<?php echo $row_client->client_id; ?>" class="btn btn-default">
                                            <i class="fa fa-eye" data-toggle="tooltip" title="" data-original-title="Просмотр"></i>
                                        </a>
                                        <?php
                                        $offers_class = new Offer($row_client->client_id);
                                        $offers = $offers_class->get();
                                        if($offers){
                                            ?>
                                            <a target="_blank" style="background: #FFFF65" href="http://broker.alex-r.am/index.php?action=buyer&subaction=offers&id=<?php echo $row_client->client_id; ?>" class="btn btn-default">
                                                <i class="fa fa-list" data-toggle="tooltip" title="" data-original-title="Список предложений"></i>
                                            </a>
											<?php }
                                        ?>
                                        <a onclick="delete_buyer_ajax(this,<?php echo $row_client->client_id; ?>)" class="btn btn-danger">
                                            <i class="fa fa-trash-o" data-toggle="tooltip" title="" data-original-title="Удалить"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
					<?php if(isset($_SESSION['post'])){
					?>
                    <div class="col-md-12">
                        <?php
                        $count_posts = mwdb_get_var("SELECT COUNT(*) FROM post_client WHERE client_status!={var}  AND broker={var} $qu_part",array(6,$_SESSION['user_id']));
                        if($count_posts%20==0){
                            $page_count = (int)($count_posts/20);
                        }
                        else{
                            $page_c = (int)($count_posts/20);
                            $page_count = $page_c+1;
                        }
                        if($page_count>1){
                            ?>
                            <a href="http://broker.alex-r.am/index.php?action=buyer&subaction=all&s_page=1" class="pager <?php if($page==1){echo 'current';} ?>">1</a>
                            <?php
                            for($i=2;$i<=$page_count;$i++){
                                ?>
                                <a href="http://broker.alex-r.am/index.php?action=buyer&subaction=all&s_page=<?php echo $i; ?>" class="pager <?php if($page==$i){echo 'current';} ?>"><?php echo $i; ?></a>
                                <?php
                            }
                        }
                        ?>
                    </div>
					<?php } else{
						?>
						<div class="col-md-12">
                        <?php
                        $count_posts = mwdb_get_var("SELECT COUNT(*) FROM post_client WHERE client_status!={var}  AND broker={var} $qu_part",array(6,$_SESSION['user_id']));
                        if($count_posts%20==0){
                            $page_count = (int)($count_posts/20);
                        }
                        else{
                            $page_c = (int)($count_posts/20);
                            $page_count = $page_c+1;
                        }
                        if($page_count>1){
                            ?>
                            <a href="http://broker.alex-r.am/index.php?action=buyer&subaction=all&page=1" class="pager <?php if($page==1){echo 'current';} ?>">1</a>
                            <?php
                            for($i=2;$i<=$page_count;$i++){
                                ?>
                                <a href="http://broker.alex-r.am/index.php?action=buyer&subaction=all&page=<?php echo $i; ?>" class="pager <?php if($page==$i){echo 'current';} ?>"><?php echo $i; ?></a>
                                <?php
                            }
                        }
                        ?>
                    </div>
						<?php
					} ?>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    document.getElementById("myspan").innerHTML="<?php echo $count_posts; ?>";
</script>