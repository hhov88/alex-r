<style>
    .city .col-md-12{
        margin: 0 !important;
    }
    .area .col-md-12{
        margin: 0 !important;
    }
    .for_microarea .col-md-12{
        padding-left: 10px;
    }
</style>
<section class="content-header">
    <h1 style="margin: 33px 4px 4px 0px">Клиент N<?php echo $_GET['id'] ?></h1>
</section>
<?php
$id = (int)$_GET['id'];
if($_SESSION['user_id']==mwdb_get_var("SELECT broker FROM post_client WHERE client_id={var}",array($id))){
mwdb_query("UPDATE post_client SET client_status='1' WHERE client_id={var}",array($id));
if(isset($_POST['add_client']) && $_POST['add_client']=='Сохранить'){
    if($_POST['owner_name']!='') {
        if($_POST['broker']!='0') {
            $client = new Client($id, $_POST['owner_name'], $_POST['owner_phone1'], $_POST['owner_phone2'], $_POST['owner_email'], $_POST['owner_skype'], $_POST['owner_details'], $_SESSION['user_id'], $_POST['owner_type'],1,$_SESSION['user_id']);
            $client->update();
            $city = '';
            if(isset($_POST['city_val'])){
                foreach($_POST['city_val'] as $city_val){
                    $city .= $city_val.',';
                }
                $city = substr($city,0,-1);
            }
            $area = '';
            if(isset($_POST['area_val'])){
                foreach($_POST['area_val'] as $area_val){
                    $area .= $area_val.',';
                }
                $area = substr($area,0,-1);
            }
            $microarea = '';
            if(isset($_POST['micro_val'])){
                foreach($_POST['micro_val'] as $micro_val){
                    $microarea .= $micro_val.',';
                }
                $microarea = substr($microarea,0,-1);
            }
            mwdb_query("UPDATE client_search SET type_search='{var}',price_from='{var}',price_to='{var}',region='{var}',city='{var}',area='{var}',microarea='{var}',category='{var}' WHERE client_id={var}",
                array($_POST['s_type'],$_POST['price_from'],$_POST['price_to'],$_POST['region'],$city,$area,$microarea,$_POST['category'],$id));
            $id_search = mwdb_get_var("SELECT search_id FROM client_search WHERE client_id={var}",array($id));
            switch($_POST['category']){
                case '5':{
                    if($_POST['area_from']=='' && $_POST['area_to']==''){
                        $area = '';
                    }
                    else {
                        $area = $_POST['area_from'] . '-' . $_POST['area_to'];
                    }
                    $rooms = '';
                    if(isset($_POST['rooms'])){
                        foreach($_POST['rooms'] as $room){
                            $rooms .= $room.',';
                        }
                        $rooms = substr($rooms,0,-1);
                    }
                    if($_POST['floorer_from']=='0' && $_POST['floorer_to']=='0'){
                        $floorer = '';
                    }
                    else {
                        $floorer = $_POST['floorer_from'] . '-' . $_POST['floorer_to'];
                    }
                    if($_POST['floor_from']=='0' && $_POST['floor_to']=='0'){
                        $floor = '';
                    }
                    else {
                        $floor = $_POST['floor_from'] . '-' . $_POST['floor_to'];
                    }
                    $building_project= '';
                    if(isset($_POST['building_project'])){
                        foreach($_POST['building_project'] as $building_pr){
                            $building_project .= $building_pr.',';
                        }
                        $building_project = substr($building_project,0,-1);
                    }
                    $building_type= '';
                    if(isset($_POST['building_type'])){
                        foreach($_POST['building_type'] as $building_ty){
                            $building_type .= $building_ty.',';
                        }
                        $building_type = substr($building_type,0,-1);
                    }
                    $building_position= '';
                    if(isset($_POST['building_position'])){
                        foreach($_POST['building_position'] as $building_po){
                            $building_position .= $building_po.',';
                        }
                        $building_position = substr($building_position,0,-1);
                    }
                    $housetop= '';
                    if(isset($_POST['housetop'])){
                        foreach($_POST['housetop'] as $houseto){
                            $housetop .= $houseto.',';
                        }
                        $housetop = substr($housetop,0,-1);
                    }
                    $floor_name= '';
                    if(isset($_POST['floor_name'])){
                        foreach($_POST['floor_name'] as $floor_n){
                            $floor_name .= $floor_n.',';
                        }
                        $floor_name = substr($floor_name,0,-1);
                    }
                    $repair= '';
                    if(isset($_POST['repair'])){
                        foreach($_POST['repair'] as $repai){
                            $repair .= $repai.',';
                        }
                        $repair = substr($repair,0,-1);
                    }
                    if(isset($_POST['water'])){
                        $water = $_POST['water'];
                    }
                    else{
                        $water = '0';
                    }
                    if(isset($_POST['gas'])){
                        $gas = $_POST['gas'];
                    }
                    else{
                        $gas = '0';
                    }
                    $balcony= '';
                    if(isset($_POST['balcony'])){
                        foreach($_POST['balcony'] as $balcon){
                            $balcony .= $balcon.',';
                        }
                        $balcony = substr($balcony,0,-1);
                    }
                    $garage= '';
                    if(isset($_POST['garage'])){
                        foreach($_POST['garage'] as $garag){
                            $garage .= $garag.',';
                        }
                        $garage = substr($garage,0,-1);
                    }
                    if(isset($_POST['basement'])){
                        $basement = $_POST['basement'];
                    }
                    else{
                        $basement = '0';
                    }
                    if(mwdb_get_var("SELECT setting_id FROM client_apartments WHERE search_id={var}",array($id_search))) {
                        mwdb_query("UPDATE client_apartments SET area='{var}',rooms='{var}',floorer='{var}',floor='{var}',floor_name='{var}',repair='{var}',balcony='{var}',garage='{var}',basement='{var}',water='{var}',gas='{var}',building_project='{var}',building_position='{var}',building_type='{var}',housetop='{var}' WHERE search_id={var}",
                            array($area, $rooms, $floorer, $floor, $floor_name, $repair, $balcony, $garage, $basement, $water, $gas, $building_project, $building_position, $building_type, $housetop, $id_search));
                    }
                    else{
                        mwdb_query("INSERT INTO client_apartments(search_id,area,rooms,floorer,floor,floor_name,repair,balcony,garage,basement,water,gas,building_project,building_position,building_type,housetop) VALUES
                        ('{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}')",
                            array($id_search,$area,$rooms,$floorer,$floor,$floor_name,$repair,$balcony,$garage,$basement,$water,$gas,$building_project,$building_position,$building_type,$housetop));
                    }
                    break;
                }
                case '6':{
                    if($_POST['total_area_from']=='' && $_POST['total_area_to']==''){
                        $total_area = '';
                    }
                    else {
                        $total_area = $_POST['total_area_from'] . '-' . $_POST['total_area_to'];
                    }
                    if($_POST['home_area_from']=='' && $_POST['home_area_to']==''){
                        $home_area = '';
                    }
                    else {
                        $home_area = $_POST['home_area_from'] . '-' . $_POST['home_area_to'];
                    }
                    $rooms = '';
                    if(isset($_POST['rooms'])){
                        foreach($_POST['rooms'] as $room){
                            $rooms .= $room.',';
                        }
                        $rooms = substr($rooms,0,-1);
                    }
                    if($_POST['floorer_from']=='0' && $_POST['floorer_to']=='0'){
                        $floorer = '';
                    }
                    else {
                        $floorer = $_POST['floorer_from'] . '-' . $_POST['floorer_to'];
                    }
                    $building_type= '';
                    if(isset($_POST['building_type'])){
                        foreach($_POST['building_type'] as $building_ty){
                            $building_type .= $building_ty.',';
                        }
                        $building_type = substr($building_type,0,-1);
                    }
                    $building_position= '';
                    if(isset($_POST['building_position'])){
                        foreach($_POST['building_position'] as $building_po){
                            $building_position .= $building_po.',';
                        }
                        $building_position = substr($building_position,0,-1);
                    }
                    $repair= '';
                    if(isset($_POST['repair'])){
                        foreach($_POST['repair'] as $repai){
                            $repair .= $repai.',';
                        }
                        $repair = substr($repair,0,-1);
                    }
                    $water = '';
                    if(isset($_POST['water'])){
                        $water = $_POST['water'];
                    }
                    $gas = '';
                    if(isset($_POST['gas'])){
                        $gas = $_POST['gas'];
                    }
                    $heating_system = '';
                    if(isset($_POST['heating_system'])){
                        $heating_system = $_POST['heating_system'];
                    }
                    $balcony= '';
                    if(isset($_POST['balcony'])){
                        foreach($_POST['balcony'] as $balcon){
                            $balcony .= $balcon.',';
                        }
                        $balcony = substr($balcony,0,-1);
                    }
                    $garage= '';
                    if(isset($_POST['garage'])){
                        foreach($_POST['garage'] as $garag){
                            $garage .= $garag.',';
                        }
                        $garage = substr($garage,0,-1);
                    }
                    $basement = '';
                    if(isset($_POST['basement'])){
                        $basement = $_POST['basement'];
                    }
                    if(mwdb_get_var("SELECT setting_id FROM client_houses WHERE search_id={var}",array($id_search))) {
                        mwdb_query("UPDATE client_houses SET total_area='{var}',home_area='{var}',rooms='{var}',floorer='{var}',floor='{var}',repair='{var}',balcony='{var}',garage='{var}',basement='{var}',water='{var}',gas='{var}',heating_system='{var}',building_type='{var}',building_position='{var}' WHERE search_id={var}",
                            array($total_area, $home_area, $rooms, $floorer, '', $repair, $balcony, $garage, $basement, $water, $gas, $heating_system, $building_type, $building_position, $id_search));
                    }
                    else{
                        mwdb_query("INSERT INTO client_houses(search_id,total_area,home_area,rooms,floorer,floor,repair,balcony,garage,basement,water,gas,heating_system,building_type,building_position) VALUES
                        ('{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}')",
                            array($id_search,$total_area,$home_area,$rooms,$floorer,'',$repair,$balcony,$garage,$basement,$water,$gas,$heating_system,$building_type,$building_position));
                    }
                    break;
                }
                case '7':{
                    $typer= '';
                    if(isset($_POST['typer'])){
                        foreach($_POST['typer'] as $type){
                            $typer .= $type.',';
                        }
                        $typer = substr($typer,0,-1);
                    }
                    if($_POST['area_from']=='' && $_POST['area_to']==''){
                        $area = '';
                    }
                    else {
                        $area = $_POST['area_from'] . '-' . $_POST['area_to'];
                    }
                    $rooms = '';
                    if(isset($_POST['rooms'])){
                        foreach($_POST['rooms'] as $room){
                            $rooms .= $room.',';
                        }
                        $rooms = substr($rooms,0,-1);
                    }
                    if($_POST['floor_from']=='0' && $_POST['floor_to']=='0'){
                        $floor = '';
                    }
                    else {
                        $floor = $_POST['floor_from'] . '-' . $_POST['floor_to'];
                    }
                    $floor_name= '';
                    if(isset($_POST['floor_name'])){
                        foreach($_POST['floor_name'] as $floor_n){
                            $floor_name .= $floor_n.',';
                        }
                        $floor_name = substr($floor_name,0,-1);
                    }
                    $building_type= '';
                    if(isset($_POST['building_type'])){
                        foreach($_POST['building_type'] as $building_ty){
                            $building_type .= $building_ty.',';
                        }
                        $building_type = substr($building_type,0,-1);
                    }
                    $building_position= '';
                    if(isset($_POST['building_position'])){
                        foreach($_POST['building_position'] as $building_po){
                            $building_position .= $building_po.',';
                        }
                        $building_position = substr($building_position,0,-1);
                    }
                    $showcases = '';
                    if(isset($_POST['showcases'])){
                        $showcases = $_POST['showcases'];
                    }
                    $entrance = '';
                    if(isset($_POST['entrance'])){
                        $entrance = $_POST['entrance'];
                    }
                    $repair = '';
                    if(isset($_POST['repair'])){
                        $repair = $_POST['repair'];
                    }
                    if(mwdb_get_var("SELECT setting_id FROM client_commercial WHERE search_id={var}",array($id_search))) {
                        mwdb_query("UPDATE client_commercial SET type='{var}',area='{var}',rooms='{var}',floor='{var}',floorer='{var}',floor_name='{var}',showcases='{var}',entrance='{var}',repair='{var}',building_type='{var}',building_position='{var}' WHERE search_id={var}",
                            array($typer, $area, $rooms, $floor, '', $floor_name, $showcases, $entrance, $repair, $building_type, $building_position, $id_search));
                    }
                    else{
                        mwdb_query("INSERT INTO client_commercial(search_id,type,area,rooms,floor,floorer,floor_name,showcases,entrance,repair,building_type,building_position) VALUES
                        ('{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}')",
                            array($id_search,$typer,$area,$rooms,$floor,'',$floor_name,$showcases,$entrance,$repair,$building_type,$building_position));
                    }
                    break;
                }
                case '8':{
                    if($_POST['area_from']=='' && $_POST['area_to']==''){
                        $area = '';
                    }
                    else {
                        $area = $_POST['area_from'] . '-' . $_POST['area_to'];
                    }
                    if($_POST['buildings_from']=='' && $_POST['buildings_to']==''){
                        $buildings = '';
                    }
                    else {
                        $buildings = $_POST['buildings_from'] . '-' . $_POST['buildings_to'];
                    }
                    $water_drink = '';
                    if(isset($_POST['water_drink'])){
                        $water_drink = $_POST['water_drink'];
                    }
                    $water_irrigation = '';
                    if(isset($_POST['water_irrigation'])){
                        $water_irrigation = $_POST['water_irrigation'];
                    }
                    $gas = '';
                    if(isset($_POST['gas'])){
                        $gas = $_POST['gas'];
                    }
                    $electricity = '';
                    if(isset($_POST['electricity'])){
                        $electricity = $_POST['electricity'];
                    }
                    $high_voltage_pillars = '';
                    if(isset($_POST['high_voltage_pillars'])){
                        $high_voltage_pillars = $_POST['high_voltage_pillars'];
                    }
                    $sewage = '';
                    if(isset($_POST['sewage'])){
                        $sewage = $_POST['sewage'];
                    }
                    $fence = '';
                    if(isset($_POST['fence'])){
                        $fence = $_POST['fence'];
                    }
                    $fruit_trees = '';
                    if(isset($_POST['fruit_trees'])){
                        $fruit_trees = $_POST['fruit_trees'];
                    }
                    if(mwdb_get_var("SELECT setting_id FROM client_land WHERE search_id={var}",array($id_search))) {
                        mwdb_query("UPDATE client_land SET area='{var}',buildings='{var}',water_drink='{var}',water_irrigation='{var}',gas='{var}',electricity='{var}',high_voltage_pillars='{var}',sewage='{var}',fence='{var}',fruit_trees='{var}' WHERE search_id={var}",
                            array($area, $buildings, $water_drink, $water_irrigation, $gas, $electricity, $high_voltage_pillars, $sewage, $fence, $fruit_trees, $id_search));
                    }
                    else{
                        mwdb_query("INSERT INTO client_land(search_id,area,buildings,water_drink,water_irrigation,gas,electricity,high_voltage_pillars,sewage,fence,fruit_trees) VALUES
                        ('{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}')",
                            array($id_search,$area,$buildings,$water_drink,$water_irrigation,$gas,$electricity,$high_voltage_pillars,$sewage,$fence,$fruit_trees));
                    }
                    break;
                }
                case '9':{
                    if($_POST['area_from']=='' && $_POST['area_to']==''){
                        $area = '';
                    }
                    else {
                        $area = $_POST['area_from'] . '-' . $_POST['area_to'];
                    }
                    $building_type = '';
                    if(isset($_POST['building_type'])){
                        $building_type = $_POST['building_type'];
                    }
                    if(mwdb_get_var("SELECT setting_id FROM client_garages WHERE search_id={var}",array($id_search))) {
                        mwdb_query("UPDATE client_garages SET area='{var}',building_type='{var}' WHERE search_id={var}", array($area, $building_type, $id_search));
                    }
                    else{
                        mwdb_query("INSERT INTO client_garages(search_id,area,building_type) VALUES ('{var}','{var}','{var}')",array($id_search,$area,$building_type));
                    }
                    break;
                }
            }
            //echo '<script>document.location.href="http://user.alex-r.am/index.php?action=buyer&subaction=view&id=' . $post_client . '"</script>';
        }
        else{
            echo 'Выберите Риэлтора!';
        }
    }
    else{
        echo 'Заполните Имя клиента!';
    }
}
$client_class = new Client($id);
$client_class->get();
?>
<style>{text-align: center}</style>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="overlay" style="display: none">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
                <div class="box-body table-responsive">
                    <div class="col-md-12 search_params">
                        <form action="" method="post">
                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <!--<select class="form-control" name="broker">
                                        <?php
                                        $result_brokers = mwdb_select("SELECT user_id,user_name FROM users WHERE user_type={var}",array(3));
                                        foreach($result_brokers as $row_broker){
                                            ?>
                                            <option <?php if($row_broker->user_id==$client_class->broker){echo 'selected';} ?> value="<?php echo $row_broker->user_id; ?>"><?php echo $row_broker->user_name; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>-->
                                </div>
                                <div class="col-md-2" style="float: right">
                                    <select name="owner_type" class="form-control">
                                        <option value="0">Обычный клиент</option>
                                        <option <?php if($client_class->client_type=='2'){echo 'selected';} ?> value="2">Первичный клиент</option>
                                        <option <?php if($client_class->client_type=='1'){echo 'selected';} ?> value="1">Вторичный клиент</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-5">
                                    <div class="col-md-6">
                                        <input type="text" name="owner_name" class="owner_name form-control" placeholder="Имя клиента" value="<?php echo $client_class->client_name; ?>"><br>
                                        <input type="text" name="owner_phone1" class="owner_phone1 form-control" placeholder="Телефон1" value="<?php echo $client_class->client_phone1; ?>"><br>
                                        <input type="text" name="owner_phone2" class="owner_phone2 form-control" placeholder="Телефон2" value="<?php echo $client_class->client_phone2; ?>"><br>
                                        <input type="email" name="owner_email" class="owner_email form-control" placeholder="E-mail" value="<?php echo $client_class->client_email; ?>"><br>
                                        <input type="skype" name="owner_skype" class="owner_skype form-control" placeholder="Skype" value="<?php echo $client_class->client_skype; ?>">
                                    </div>
                                    <div class="col-md-6">
                                        <textarea name="owner_details" class="owner_details form-control" placeholder="Другие детали клиента" style="height: 212px;resize: none"><?php echo $client_class->client_settings; ?></textarea>
                                    </div>
                                </div>
                                <?php
                                $result_search = mwdb_get_row("SELECT * FROM client_search WHERE client_id={var}",array($client_class->client_id));
                                ?>
                                <div class="col-md-7">
                                    <div class="col-md-12">
                                        <div class="col-md-3" style="text-align: center">
                                            <input type="radio" name="s_type" value="sale" <?php if($result_search->type_search=='sale'){echo 'checked';} ?>><label for="s_type">Продажа</label>
                                        </div>
                                        <div class="col-md-3" style="text-align: center">
                                            <input type="radio" name="s_type" value="rent" <?php if($result_search->type_search=='rent'){echo 'checked';} ?>><label for="s_type">Аренда</label>
                                        </div>
                                        <div class="col-md-3" style="text-align: center">
                                            <input type="radio" name="s_type" value="day" <?php if($result_search->type_search=='day'){echo 'checked';} ?>><label for="s_type">Посуточнo</label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-2" style="text-align: center">Цена</div>
                                        <div class="col-md-2">
                                            <input type="text" name="price_from" placeholder="От" class="form-control" style="height: 20px" value="<?php if($result_search->price_from!='0'){echo $result_search->price_from;} ?>">
                                        </div>
                                        <div class="col-md-1" style="text-align: center;">_</div>
                                        <div class="col-md-2">
                                            <input type="text" name="price_to" placeholder="До" class="form-control" style="height: 20px" value="<?php if($result_search->price_to!='0'){echo $result_search->price_to;} ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-2">
                                            <select name="region" class="region form-control" onchange="region_change(this);$('.area').html('');">
                                                <option value="0">-- Регион --</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4 city" style="height: 150px;overflow: auto;border: 1px solid #D2D6DE;padding: 0 3px">
                                        </div>
                                        <div class="col-md-5 area" style="height: 150px;overflow: auto;border: 1px solid #D2D6DE;padding: 0 3px">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" style="border-top: 1px solid #3C8DBC;">
                                <div class="col-md-2">
                                    <select name="category" class="form-control" onchange="get_client_search_attributes(this)" style="background: #C4EBFF;">
                                        <option value="0">-- Категория --</option>
                                        <?php
                                        $cats = mwdb_select("SELECT category_id,category_title_ru FROM category WHERE {var} ORDER BY category_order",array(1));
                                        foreach($cats as $cat){
                                            ?>
                                            <option value="<?php echo $cat->category_id; ?>" <?php if($cat->category_id==$result_search->category){echo 'selected';} ?>><?php echo $cat->category_title_ru; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-12 search_attributes" style="background: #F9FAFC;padding: 23px 25px;">
                                    <?php
                                    switch($result_search->category){
                                        case '5':{
                                            $row_search_attr = mwdb_get_row("SELECT * FROM client_apartments WHERE search_id={var}",array($result_search->search_id));
                                            ?>
                                            <div class="col-md-12">
                                                <div class="col-md-3">
                                                    <?php
                                                    $array_area = explode('-',$row_search_attr->area);
                                                    ?>
                                                    <div class="col-md-12">Площадь</div>
                                                    <div class="col-md-11">
                                                        <div class="col-md-4"><input type="text" name="area_from" class="form-control" placeholder="От" value="<?php if(isset($array_area[0])){echo $array_area[0];} ?>"></div>
                                                        <div class="col-md-4"><input type="text" name="area_to" class="form-control" placeholder="До" value="<?php if(isset($array_area[1])){echo $array_area[1];} ?>"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <?php
                                                    $array_rooms = explode(',',$row_search_attr->rooms);
                                                    ?>
                                                    <div class="col-md-12">Комнаты</div>
                                                    <div class="col-md-11">
                                                        <div class="col-md-12">
                                                            <?php
                                                            for($i=1;$i<11;$i++){
																if($i==6){echo '<div class="col-md-2" style="height: 22px;"></div>';}
                                                                ?>
                                                                <div class="col-md-2"><input type="checkbox" name="rooms[]" <?php if(in_array($i,$array_rooms)){echo 'checked';} ?> value="<?php echo $i; ?>"><?php echo $i;if($i==10){echo '+';} ?></div>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <?php
                                                    $array_floorer = explode('-',$row_search_attr->floorer);
                                                    ?>
                                                    <div class="col-md-3">Этажность</div>
                                                    <div class="col-md-11">
                                                        <div class="col-md-4">
                                                            <select name="floorer_from" class="form-control">
                                                                <option value="0">От</option>
                                                                <?php
                                                                for($i=1;$i<21;$i++){
                                                                    ?>
                                                                    <option <?php if(isset($array_floorer[0]) && $array_floorer[0]==$i){echo 'selected';} ?> value="<?php echo $i ?>"><?php echo $i; ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select name="floorer_to" class="form-control">
                                                                <option value="0">До</option>
                                                                <?php
                                                                for($i=1;$i<21;$i++){
                                                                    ?>
                                                                    <option <?php if(isset($array_floorer[1]) && $array_floorer[1]==$i){echo 'selected';} ?> value="<?php echo $i ?>"><?php echo $i; ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <?php
                                                    $array_floor = explode('-',$row_search_attr->floor);
                                                    ?>
                                                    <div class="col-md-12">Этаж</div>
                                                    <div class="col-md-11">
                                                        <div class="col-md-4">
                                                            <select name="floor_from" class="form-control">
                                                                <option value="0">От</option>
                                                                <?php
                                                                for($i=1;$i<21;$i++){
                                                                    ?>
                                                                    <option <?php if(isset($array_floor[0]) && $array_floor[0]==$i){echo 'selected';} ?> value="<?php echo $i ?>"><?php echo $i; ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select name="floor_to" class="form-control">
                                                                <option value="0">До</option>
                                                                <?php
                                                                for($i=1;$i<21;$i++){
                                                                    ?>
                                                                    <option <?php if(isset($array_floor[1]) && $array_floor[1]==$i){echo 'selected';} ?> value="<?php echo $i ?>"><?php echo $i; ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="col-md-6">
                                                    <?php
                                                    $array_building_project = explode(',',$row_search_attr->building_project);
                                                    ?>
                                                    <div class="col-md-12">Проект здания</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 10px;">
                                                        <div class="col-md-4">
                                                            <div class="col-md-12"><input type="checkbox" <?php if(in_array('building_project14',$array_building_project)){echo 'checked';} ?> name="building_project[]" value="building_project14">Новостройка</div>
                                                            <div class="col-md-12"><input type="checkbox" <?php if(in_array('building_project1',$array_building_project)){echo 'checked';} ?> name="building_project[]" value="building_project1">Сталинский</div>
                                                            <div class="col-md-12"><input type="checkbox" <?php if(in_array('building_project2',$array_building_project)){echo 'checked';} ?> name="building_project[]" value="building_project2">Хрущевский</div>
                                                            <div class="col-md-12"><input type="checkbox" <?php if(in_array('building_project3',$array_building_project)){echo 'checked';} ?> name="building_project[]" value="building_project3">Пост хрущевский</div>
                                                            <div class="col-md-12"><input type="checkbox" <?php if(in_array('building_project4',$array_building_project)){echo 'checked';} ?> name="building_project[]" value="building_project4">Каменный спецпроект</div>
                                                            <div class="col-md-12"><input type="checkbox" <?php if(in_array('building_project6',$array_building_project)){echo 'checked';} ?> name="building_project[]" value="building_project6">Бадалян</div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="col-md-12"><input type="checkbox" <?php if(in_array('building_project7',$array_building_project)){echo 'checked';} ?> name="building_project[]" value="building_project7">Московский ДСК</div>
                                                            <div class="col-md-12"><input type="checkbox" <?php if(in_array('building_project8',$array_building_project)){echo 'checked';} ?> name="building_project[]" value="building_project8">Ереванский ДСК</div>
                                                            <div class="col-md-12"><input type="checkbox" <?php if(in_array('building_project9',$array_building_project)){echo 'checked';} ?> name="building_project[]" value="building_project9">Грузинский</div>
                                                            <div class="col-md-12"><input type="checkbox" <?php if(in_array('building_project10',$array_building_project)){echo 'checked';} ?> name="building_project[]" value="building_project10">Ленточный</div>
                                                            <div class="col-md-12"><input type="checkbox" <?php if(in_array('building_project11',$array_building_project)){echo 'checked';} ?> name="building_project[]" value="building_project11">Монолитый сейсмоустойчивый</div>
                                                            <div class="col-md-12"><input type="checkbox" <?php if(in_array('building_project12',$array_building_project)){echo 'checked';} ?> name="building_project[]" value="building_project12">Трехъярусный</div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="col-md-12"><input type="checkbox" <?php if(in_array('building_project13',$array_building_project)){echo 'checked';} ?> name="building_project[]" value="building_project13">Четырехъярусный</div>
                                                            <div class="col-md-12"><input type="checkbox" <?php if(in_array('building_project16',$array_building_project)){echo 'checked';} ?> name="building_project[]" value="building_project16">Чешский проект</div>
                                                            <div class="col-md-12"><input type="checkbox" <?php if(in_array('building_project15',$array_building_project)){echo 'checked';} ?> name="building_project[]" value="building_project15">Спецпроект</div>
                                                            <div class="col-md-12"><input type="checkbox" <?php if(in_array('building_project17',$array_building_project)){echo 'checked';} ?> name="building_project[]" value="building_project17">Югославский</div>
                                                            <div class="col-md-12"><input type="checkbox" <?php if(in_array('building_project18',$array_building_project)){echo 'checked';} ?> name="building_project[]" value="building_project18">Финский</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <?php
                                                    $array_building_type = explode(',',$row_search_attr->building_type);
                                                    ?>
                                                    <div class="col-md-12">Тип здания</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('building_type1',$array_building_type)){echo 'checked';} ?> name="building_type[]" value="building_type1">монолитный
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('building_type2',$array_building_type)){echo 'checked';} ?> name="building_type[]" value="building_type2">каменный
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('building_type3',$array_building_type)){echo 'checked';} ?> name="building_type[]" value="building_type3">крупнопанельный
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('building_type4',$array_building_type)){echo 'checked';} ?> name="building_type[]" value="building_type4">каркаснопанельный
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <?php
                                                    $array_building_position = explode(',',$row_search_attr->building_position);
                                                    ?>
                                                    <div class="col-md-12">Расположение здания</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <div class="col-md-12">
                                                                <input type="checkbox" <?php if(in_array('building_position1',$array_building_position)){echo 'checked';} ?> name="building_position[]" value="building_position1">Первая линия
                                                            </div>
                                                            <div class="col-md-12">
                                                                <input type="checkbox" <?php if(in_array('building_position2',$array_building_position)){echo 'checked';} ?> name="building_position[]" value="building_position2">Вторая линия
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <?php
                                                    $array_housetop = explode(',',$row_search_attr->housetop);
                                                    ?>
                                                    <div class="col-md-12">Тип покрытия</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <div class="col-md-12">
                                                                <input type="checkbox" <?php if(in_array('housetop1',$array_housetop)){echo 'checked';} ?> name="housetop[]" value="housetop1">Деревянный
                                                            </div>
                                                            <div class="col-md-12">
                                                                <input type="checkbox" <?php if(in_array('housetop2',$array_housetop)){echo 'checked';} ?> name="housetop[]" value="housetop2">Панельный
                                                            </div>
                                                            <div class="col-md-12">
                                                                <input type="checkbox" <?php if(in_array('housetop3',$array_housetop)){echo 'checked';} ?> name="housetop[]" value="housetop3">Монолитный бетон
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="col-md-3">
                                                    <?php
                                                    $array_floor_name = explode(',',$row_search_attr->floor_name);
                                                    ?>
                                                    <div class="col-md-12">Название этажа</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('semibasement',$array_floor_name)){echo 'checked';} ?> name="floor_name[]" value="semibasement">полуподвал
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('basement',$array_floor_name)){echo 'checked';} ?> name="floor_name[]" value="basement">подвал
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('first',$array_floor_name)){echo 'checked';} ?> name="floor_name[]" value="first">первый
                                                        </div>
														<div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('high_first',$array_floor_name)){echo 'checked';} ?> name="floor_name[]" value="high_first">белэтаж
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('average',$array_floor_name)){echo 'checked';} ?> name="floor_name[]" value="average">средний
                                                        </div>
														<div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('high_',$array_floor_name)){echo 'checked';} ?> name="floor_name[]" value="high_">высокий
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('last',$array_floor_name)){echo 'checked';} ?> name="floor_name[]" value="last">последний
                                                        </div>
														<div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('duplex',$array_floor_name)){echo 'checked';} ?> name="floor_name[]" value="duplex">дуплекс
                                                        </div>
														<div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('mansard',$array_floor_name)){echo 'checked';} ?> name="floor_name[]" value="mansard">мансард
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <?php
                                                    $array_repair = explode(',',$row_search_attr->repair);
                                                    ?>
                                                    <div class="col-md-12">Отделка</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('repair1',$array_repair)){echo 'checked';} ?> name="repair[]" value="repair1">Нулевое состояние
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('repair2',$array_repair)){echo 'checked';} ?> name="repair[]" value="repair2">Гос. состояние
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('repair3',$array_repair)){echo 'checked';} ?> name="repair[]" value="repair3">Капитальный ремонт
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('repair4',$array_repair)){echo 'checked';} ?> name="repair[]" value="repair4">Стильный ремонт
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('repair5',$array_repair)){echo 'checked';} ?> name="repair[]" value="repair5">Среднее состояние
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('repair6',$array_repair)){echo 'checked';} ?> name="repair[]" value="repair6">Косметический ремонт
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('repair7',$array_repair)){echo 'checked';} ?> name="repair[]" value="repair7">В процессе ремонта
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('repair8',$array_repair)){echo 'checked';} ?> name="repair[]" value="repair8">Частичный ремонт
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="col-md-12">Вода</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->water=='ap_water1'){echo 'checked';} ?> name="water" value="ap_water1">Постоянная
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->water=='ap_water2'){echo 'checked';} ?>  name="water" value="ap_water2">Временная
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->water=='0'){echo 'checked';} ?> name="water" value="0">Не важно
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="col-md-12">Газ</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->gas=='ap_gas1'){echo 'checked';} ?> name="gas" value="ap_gas1">В здании
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->gas=='ap_gas2'){echo 'checked';} ?> name="gas" value="ap_gas2">В квартире
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->gas=='0'){echo 'checked';} ?> name="gas" value="0" checked>Не важно
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="col-md-3">
                                                    <?php
                                                    $array_balcony = explode(',',$row_search_attr->balcony);
                                                    ?>
                                                    <div class="col-md-12">Балкон</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('balcony1',$array_balcony)){echo 'checked';} ?> name="balcony[]" value="balcony1">Висячий
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('balcony2',$array_balcony)){echo 'checked';} ?> name="balcony[]" value="balcony2">Француский
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('balcony3',$array_balcony)){echo 'checked';} ?> name="balcony[]" value="balcony3">Открытая Лоджия
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('balcony4',$array_balcony)){echo 'checked';} ?> name="balcony[]" value="balcony4">Закрытая Лоджия
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <?php
                                                    $array_garage = explode(',',$row_search_attr->garage);
                                                    ?>
                                                    <div class="col-md-12">Гараж</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('ap_garage1',$array_garage)){echo 'checked';} ?> name="garage[]" value="ap_garage1">Каменный
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('ap_garage2',$array_garage)){echo 'checked';} ?> name="garage[]" value="ap_garage2" >Железный
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="col-md-12">Подвал</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->basement=='1'){echo 'checked';} ?> name="basement" value="1">Да
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->basement==''){echo 'checked';} ?> name="basement" value="">Нет
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->basement=='0'){echo 'checked';} ?> name="basement" value="0" checked>Не важно
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            break;
                                        }
                                        case '6':{
                                            $row_search_attr = mwdb_get_row("SELECT * FROM client_houses WHERE search_id={var}",array($result_search->search_id));
                                            ?>
                                            <div class="col-md-12">
                                                <div class="col-md-3" >
                                                    <?php
                                                    $array_total_area = explode('-',$row_search_attr->total_area);
                                                    ?>
                                                    <div class="col-md-12">Общая площадь</div>
                                                    <div class="col-md-11">
                                                        <div class="col-md-4"><input type="text" name="total_area_from" class="form-control" placeholder="От" value="<?php if(isset($array_total_area[0])){echo $array_total_area[0];} ?>"></div>
                                                        <div class="col-md-4"><input type="text" name="total_area_to" class="form-control" placeholder="До" value="<?php if(isset($array_total_area[1])){echo $array_total_area[1];} ?>"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <?php
                                                    $array_home_area = explode('-',$row_search_attr->home_area);
                                                    ?>
                                                    <div class="col-md-12">Площадь дома</div>
                                                    <div class="col-md-11">
                                                        <div class="col-md-4"><input type="text" name="home_area_from" class="form-control" placeholder="От" value="<?php if(isset($array_home_area[0])){echo $array_home_area[0];} ?>"></div>
                                                        <div class="col-md-4"><input type="text" name="home_area_to" class="form-control" placeholder="До" value="<?php if(isset($array_home_area[1])){echo $array_home_area[1];} ?>"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <?php
                                                    $array_rooms = explode(',',$row_search_attr->rooms);
                                                    ?>
                                                    <div class="col-md-12">Комнаты</div>
                                                    <div class="col-md-11">
                                                        <div class="col-md-12">
                                                            <?php
                                                            for($i=1;$i<11;$i++){
																if($i==6){echo '<div class="col-md-2" style="height: 22px;"></div>';}
                                                                ?>
                                                                <div class="col-md-2"><input type="checkbox" <?php if(in_array($i,$array_rooms)){echo 'checked';} ?> name="rooms[]" value="<?php echo $i; ?>"><?php echo $i;if($i==10){echo '+';} ?></div>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <?php
                                                    $array_floorer = explode('-',$row_search_attr->floorer);
                                                    ?>
                                                    <div class="col-md-12">Этажность</div>
                                                    <div class="col-md-11">
                                                        <div class="col-md-4">
                                                            <select name="floorer_from" class="form-control">
                                                                <option value="0">От</option>
                                                                <?php
                                                                for($i=1;$i<21;$i++){
                                                                    ?>
                                                                    <option <?php if($i==$array_floorer[0]){echo 'selected';} ?> value="<?php echo $i ?>"><?php echo $i; ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select name="floorer_to" class="form-control">
                                                                <option value="0">До</option>
                                                                <?php
                                                                for($i=1;$i<21;$i++){
                                                                    ?>
                                                                    <option <?php if($i==$array_floorer[1]){echo 'selected';} ?> value="<?php echo $i ?>"><?php echo $i; ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="col-md-2">
                                                    <?php
                                                    $array_building_type = explode(',',$row_search_attr->building_type);
                                                    ?>
                                                    <div class="col-md-12">Тип здания</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('building_type1',$array_building_type)){echo 'checked';} ?> name="building_type[]" value="building_type1">монолитный
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('building_type2',$array_building_type)){echo 'checked';} ?> name="building_type[]" value="building_type2">каменный
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('building_type3',$array_building_type)){echo 'checked';} ?> name="building_type[]" value="building_type3">крупнопанельный
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('building_type4',$array_building_type)){echo 'checked';} ?> name="building_type[]" value="building_type4">каркаснопанельный
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <?php
                                                    $array_building_position = explode(',',$row_search_attr->building_position);
                                                    ?>
                                                    <div class="col-md-12">Расположение здания</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <div class="col-md-12">
                                                                <input type="checkbox" <?php if(in_array('building_position1',$array_building_position)){echo 'checked';} ?> name="building_position[]" value="building_position1">Первая линия
                                                            </div>
                                                            <div class="col-md-12">
                                                                <input type="checkbox" <?php if(in_array('building_position2',$array_building_position)){echo 'checked';} ?> name="building_position[]" value="building_position2">Вторая линия
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <?php
                                                    $array_repair = explode(',',$row_search_attr->repair);
                                                    ?>
                                                    <div class="col-md-12">Отделка</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('repair1',$array_repair)){echo 'checked';} ?> name="repair[]" value="repair1">Нулевое состояние
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('repair2',$array_repair)){echo 'checked';} ?> name="repair[]" value="repair2">Гос. состояние
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('repair3',$array_repair)){echo 'checked';} ?> name="repair[]" value="repair3">Капитальный ремонт
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('repair4',$array_repair)){echo 'checked';} ?> name="repair[]" value="repair4">Стильный ремонт
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('repair5',$array_repair)){echo 'checked';} ?> name="repair[]" value="repair5">Среднее состояние
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('repair6',$array_repair)){echo 'checked';} ?> name="repair[]" value="repair6">Косметический ремонт
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('repair7',$array_repair)){echo 'checked';} ?> name="repair[]" value="repair7">В процессе ремонта
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('repair8',$array_repair)){echo 'checked';} ?> name="repair[]" value="repair8">Частичный ремонт
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="col-md-12">Вода</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->water=='hm_water1'){echo 'checked';} ?> name="water" value="hm_water1">Постоянная
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->water=='hm_water2'){echo 'checked';} ?> name="water" value="hm_water2">Временная
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->water=='0'){echo 'checked';} ?> name="water" value="0">Не важно
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="col-md-12">Газ</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="radio" name="gas" <?php if($row_search_attr->gas=='hm_gas1'){echo 'checked';} ?> value="hm_gas1">Есть
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" name="gas" <?php if($row_search_attr->gas=='hm_gas2'){echo 'checked';} ?> value="hm_gas2">Возможность
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" name="gas" <?php if($row_search_attr->gas=='hm_gas3'){echo 'checked';} ?> value="hm_gas3">Нет
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" name="gas" <?php if($row_search_attr->gas=='0'){echo 'checked';} ?> value="0">Не важно
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="col-md-12">Система отопления</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->heating_system=='hm_heating_system1'){echo 'checked';} ?> name="heating_system" value="hm_heating_system1">Есть
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->heating_system=='hm_heating_system2'){echo 'checked';} ?> name="heating_system" value="hm_heating_system2">Нет
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->heating_system=='0'){echo 'checked';} ?> name="heating_system" value="0">Не важно
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="col-md-2">
                                                    <?php
                                                    $array_balcony = explode(',',$row_search_attr->balcony);
                                                    ?>
                                                    <div class="col-md-12">Балкон</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('balcony1',$array_balcony)){echo 'checked';} ?> name="balcony[]" value="balcony1">Висячий
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('balcony2',$array_balcony)){echo 'checked';} ?> name="balcony[]" value="balcony2">Француский
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('balcony3',$array_balcony)){echo 'checked';} ?> name="balcony[]" value="balcony3">Открытая Лоджия
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('balcony4',$array_balcony)){echo 'checked';} ?> name="balcony[]" value="balcony4">Закрытая Лоджия
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <?php
                                                    $array_garage = explode(',',$row_search_attr->garage);
                                                    ?>
                                                    <div class="col-md-12">Гараж</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('garage1',$array_garage)){echo 'checked';} ?> name="garage[]" value="garage1">Соседние
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('garage2',$array_garage)){echo 'checked';} ?> name="garage[]" value="garage2">Под зданием
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('garage3',$array_garage)){echo 'checked';} ?> name="garage[]" value="garage3">Отдельный
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="col-md-12">Подвал</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->basement=='1'){echo 'checked';} ?> name="basement" value="1">Да
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->basement==''){echo 'checked';} ?> name="basement" value="">Нет
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->basement=='0'){echo 'checked';} ?> name="basement" value="0">Не важно
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            break;
                                        }
                                        case '7':{
                                            $row_search_attr = mwdb_get_row("SELECT * FROM client_commercial WHERE search_id={var}",array($result_search->search_id));
                                            ?>
                                            <div class="col-md-12">
                                                <div class="col-md-7">
                                                    <?php
                                                    $array_type = explode(',',$row_search_attr->type);
                                                    ?>
                                                    <div class="col-md-12">Тип</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-4">
                                                            <div class="col-md-12">
                                                                <input type="checkbox" <?php if(in_array('cm_type2',$array_type)){echo 'checked';} ?> name="typer[]" value="cm_type2">гостиница
                                                            </div>
                                                            <div class="col-md-12">
                                                                <input type="checkbox" <?php if(in_array('cm_type1',$array_type)){echo 'checked';} ?> name="typer[]" value="cm_type1">оффис
                                                            </div>
                                                            <div class="col-md-12">
                                                                <input type="checkbox" <?php if(in_array('cm_type3',$array_type)){echo 'checked';} ?> name="typer[]" value="cm_type3">развлекательный
                                                            </div>
                                                            <div class="col-md-12">
                                                                <input type="checkbox" <?php if(in_array('cm_type4',$array_type)){echo 'checked';} ?> name="typer[]" value="cm_type4">магазин
                                                            </div>
                                                            <div class="col-md-12">
                                                                <input type="checkbox" <?php if(in_array('cm_type5',$array_type)){echo 'checked';} ?> name="typer[]" value="cm_type5">парикмахерская
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="col-md-12">
                                                                <input type="checkbox" <?php if(in_array('cm_type6',$array_type)){echo 'checked';} ?> name="typer[]" value="cm_type6">автотехобслуживание
                                                            </div>
                                                            <div class="col-md-12">
                                                                <input type="checkbox" <?php if(in_array('cm_type7',$array_type)){echo 'checked';} ?> name="typer[]" value="cm_type7">склад
                                                            </div>
                                                            <div class="col-md-12">
                                                                <input type="checkbox" <?php if(in_array('cm_type8',$array_type)){echo 'checked';} ?> name="typer[]" value="cm_type8">сельскохозяйственный
                                                            </div>
                                                            <div class="col-md-12">
                                                                <input type="checkbox" <?php if(in_array('cm_type9',$array_type)){echo 'checked';} ?> name="typer[]" value="cm_type9">бизнес центр
                                                            </div>
                                                            <div class="col-md-12">
                                                                <input type="checkbox" <?php if(in_array('cm_type10',$array_type)){echo 'checked';} ?> name="typer[]" value="cm_type10">производственный
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="col-md-12">
                                                                <input type="checkbox" <?php if(in_array('cm_type11',$array_type)){echo 'checked';} ?> name="typer[]" value="cm_type11">учебный
                                                            </div>
                                                            <div class="col-md-12">
                                                                <input type="checkbox" <?php if(in_array('cm_type12',$array_type)){echo 'checked';} ?> name="typer[]" value="cm_type12">медицинские заведения
                                                            </div>
                                                            <div class="col-md-12">
                                                                <input type="checkbox" <?php if(in_array('cm_type13',$array_type)){echo 'checked';} ?> name="typer[]" value="cm_type13">универсальный
                                                            </div>
                                                            <div class="col-md-12">
                                                                <input type="checkbox" <?php if(in_array('cm_type14',$array_type)){echo 'checked';} ?> name="typer[]" value="cm_type14">другое
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <?php
                                                    $array_area = explode('-',$row_search_attr->area);
                                                    ?>
                                                    <div class="col-md-12">Площадь</div>
                                                    <div class="col-md-11">
                                                        <div class="col-md-12">
                                                            <div class="col-md-4"><input type="text" name="area_from" class="form-control" placeholder="От" value="<?php if(isset($array_area[0])){echo $array_area[0];} ?>"></div>
                                                            <div class="col-md-4"><input type="text" name="area_to" class="form-control" placeholder="До" value="<?php if(isset($array_area[1])){echo $array_area[1];} ?>"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12" style="margin-top: 10px">Этаж</div>
                                                    <div class="col-md-11">
                                                        <?php
                                                        $array_floor_new = array();
                                                        $array_floor = explode('-',$row_search_attr->floor);
                                                        if(count($array_floor)>2){
                                                            $array_floor_new[0] = '-'.$array_floor[1];
                                                            $array_floor_new[1] = $array_floor[2];
                                                        }
                                                        else{
                                                            $array_floor_new[0] = $array_floor[0];
                                                            $array_floor_new[1] = $array_floor[1];
                                                        }
                                                        ?>
                                                        <div class="col-md-12">
                                                            <div class="col-md-4">
                                                                <select name="floor_from" class="form-control" style="padding: 0">
                                                                    <option value="0">От</option>
                                                                    <?php
                                                                    for($i=-3;$i<6;$i++){
                                                                        ?>
                                                                        <option <?php if($i==$array_floor_new[0]){echo 'selected';} ?> value="<?php echo $i ?>"><?php echo $i; ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <select name="floor_to" class="form-control" style="padding: 0">
                                                                    <option value="0">До</option>
                                                                    <?php
                                                                    for($i=-3;$i<6;$i++){
                                                                        ?>
                                                                        <option <?php if($i==$array_floor_new[1]){echo 'selected';} ?> value="<?php echo $i ?>"><?php echo $i; ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="col-md-12">Комнаты</div>
                                                    <div class="col-md-11">
                                                        <?php
                                                        $array_rooms = explode(',',$row_search_attr->rooms);
                                                        ?>
                                                        <div class="col-md-12">
                                                            <?php
                                                            for($i=1;$i<11;$i++){
																if($i==6){echo '<div class="col-md-2" style="height:22px"></div>';}
                                                                ?>
                                                                <div class="col-md-2"><input type="checkbox" <?php if(in_array($i,$array_rooms)){echo 'checked';} ?> name="rooms[]" value="<?php echo $i; ?>"><?php echo $i;if($i==10){echo '+';} ?></div>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12" style="margin-top: 10px">Название этажа</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <?php
                                                        $array_floor_name = explode(',',$row_search_attr->floor_name);
                                                        ?>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('cm_floor_name1',$array_floor_name)){echo 'checked';} ?> name="floor_name[]" value="cm_floor_name1">Подвал
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('cm_floor_name2',$array_floor_name)){echo 'checked';} ?> name="floor_name[]" value="cm_floor_name2">Полуподвал
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('cm_floor_name3',$array_floor_name)){echo 'checked';} ?> name="floor_name[]" value="cm_floor_name3">Цокольный этаж
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('cm_floor_name4',$array_floor_name)){echo 'checked';} ?> name="floor_name[]" value="cm_floor_name4">Первый этаж
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="col-md-2">
                                                    <?php
                                                    $array_building_type = explode(',',$row_search_attr->building_type);
                                                    ?>
                                                    <div class="col-md-12">Тип здания</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('building_type1',$array_building_type)){echo 'checked';} ?> name="building_type[]" value="building_type1">монолитный
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('building_type2',$array_building_type)){echo 'checked';} ?> name="building_type[]" value="building_type2">каменный
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('building_type3',$array_building_type)){echo 'checked';} ?> name="building_type[]" value="building_type3">крупнопанельный
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('building_type4',$array_building_type)){echo 'checked';} ?> name="building_type[]" value="building_type4">каркаснопанельный
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <?php
                                                    $array_building_position = explode(',',$row_search_attr->building_position);
                                                    ?>
                                                    <div class="col-md-12">Расположение здания</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('building_position1',$array_building_position)){echo 'checked';} ?> name="building_position[]" value="building_position1">Первая линия
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('building_position2',$array_building_position)){echo 'checked';} ?> name="building_position[]" value="building_position2">Вторая линия
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('building_position3',$array_building_position)){echo 'checked';} ?> name="building_position[]" value="building_position3">Третья линия
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('building_position4',$array_building_position)){echo 'checked';} ?> name="building_position[]" value="building_position4">Четвертая линия
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="checkbox" <?php if(in_array('building_position5',$array_building_position)){echo 'checked';} ?> name="building_position[]" value="building_position5">Другое
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="col-md-12">Наличие витрин</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->showcases=='cm_showcases1'){echo 'checked';} ?> name="showcases" value="cm_showcases1">Есть
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->showcases=='cm_showcases2'){echo 'checked';} ?> name="showcases" value="cm_showcases2">Нет
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->showcases=='0'){echo 'checked';} ?> name="showcases" value="0">Не важно
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="col-md-12">Вход</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->entrance=='cm_entrance1'){echo 'checked';} ?> name="entrance" value="cm_entrance1">С улицы
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->entrance=='cm_entrance2'){echo 'checked';} ?> name="entrance" value="cm_entrance2">Со двора
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->entrance=='cm_entrance3'){echo 'checked';} ?> name="entrance" value="cm_entrance3">Главный вход
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->entrance=='0'){echo 'checked';} ?> name="entrance" value="0">Не важно
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="col-md-12">Состояние</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="radio" name="repair" <?php if($row_search_attr->repair=='cm_repair1'){echo 'checked';} ?> value="cm_repair1">Отремонтированный
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" name="repair" <?php if($row_search_attr->repair=='cm_repair2'){echo 'checked';} ?> value="cm_repair2">Не отремонтировано
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" name="repair" <?php if($row_search_attr->repair=='0'){echo 'checked';} ?> value="0">Не важно
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            break;
                                        }
                                        case '8':{
                                            $row_search_attr = mwdb_get_row("SELECT * FROM client_land WHERE search_id={var}",array($result_search->search_id));
                                            ?>
                                            <div class="col-md-12">
                                                <div class="col-md-4">
                                                    <?php
                                                    $array_area = explode('-',$row_search_attr->area);
                                                    ?>
                                                    <div class="col-md-12">Площадь</div>
                                                    <div class="col-md-11">
                                                        <div class="col-md-5"><input type="text" name="area_from" class="form-control" placeholder="От" value="<?php if(isset($array_area[0])){echo $array_area[0];} ?>"></div>
                                                        <div class="col-md-5"><input type="text" name="area_to" class="form-control" placeholder="До" value="<?php if(isset($array_area[1])){echo $array_area[1];} ?>"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <?php
                                                    $array_buildings = explode('-',$row_search_attr->buildings);
                                                    ?>
                                                    <div class="col-md-12">Наличие постройки(Площадь)</div>
                                                    <div class="col-md-11">
                                                        <div class="col-md-4"><input type="text" name="buildings_from" class="form-control" placeholder="От" value="<?php if(isset($array_buildings[0])){echo $array_buildings[0];} ?>"></div>
                                                        <div class="col-md-4"><input type="text" name="buildings_to" class="form-control" placeholder="До" value="<?php if(isset($array_buildings[1])){echo $array_buildings[1];} ?>"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="col-md-12">Вода (питьевая)</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->water_drink=='water_drink1'){echo 'checked';} ?> name="water_drink" value="water_drink1">Есть
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->water_drink=='water_drink2'){echo 'checked';} ?> name="water_drink" value="water_drink2">Нет
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->water_drink=='water_drink3'){echo 'checked';} ?> name="water_drink" value="water_drink3">Возможность
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->water_drink=='0'){echo 'checked';} ?> name="water_drink" value="0">Не важно
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="col-md-12">Вода (ирригационная)</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->water_irrigation=='water_irrigation1'){echo 'checked';} ?> name="water_irrigation" value="water_irrigation1">Есть
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->water_irrigation=='water_irrigation2'){echo 'checked';} ?> name="water_irrigation" value="water_irrigation2">Нет
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->water_irrigation=='water_irrigation3'){echo 'checked';} ?> name="water_irrigation" value="water_irrigation3">Возможность
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->water_irrigation=='0'){echo 'checked';} ?> name="water_irrigation" value="0">Не важно
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="col-md-2">
                                                    <div class="col-md-12">Газ</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->gas=='ld_gas1'){echo 'checked';} ?> name="gas" value="ld_gas1">Есть
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->gas=='ld_gas2'){echo 'checked';} ?> name="gas" value="ld_gas2">Нет
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->gas=='ld_gas3'){echo 'checked';} ?> name="gas" value="ld_gas3">Возможность
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" <?php if($row_search_attr->gas=='0'){echo 'checked';} ?> name="gas" value="0">Не важно
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="col-md-12">Ток</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="radio" name="electricity" <?php if($row_search_attr->electricity=='ld_electricity1'){echo 'checked';} ?> value="ld_electricity1">Есть
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" name="electricity" <?php if($row_search_attr->electricity=='ld_electricity2'){echo 'checked';} ?> value="ld_electricity2">Нет
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" name="electricity" <?php if($row_search_attr->electricity=='ld_electricity3'){echo 'checked';} ?> value="ld_electricity3">Возможность
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" name="electricity" <?php if($row_search_attr->electricity=='0'){echo 'checked';} ?> value="0">Не важно
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="col-md-12">Высоковольтные столбы</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="radio" name="high_voltage_pillars" <?php if($row_search_attr->high_voltage_pillars=='high_voltage_pillars1'){echo 'checked';} ?> value="high_voltage_pillars1">Есть
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" name="high_voltage_pillars" <?php if($row_search_attr->high_voltage_pillars=='high_voltage_pillars2'){echo 'checked';} ?> value="high_voltage_pillars2">Нет
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" name="high_voltage_pillars" <?php if($row_search_attr->high_voltage_pillars=='0'){echo 'checked';} ?> value="0">Не важно
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="col-md-12">Канализация</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="radio" name="sewage" <?php if($row_search_attr->sewage=='sewage1'){echo 'checked';} ?> value="sewage1">Есть
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" name="sewage" <?php if($row_search_attr->sewage=='sewage2'){echo 'checked';} ?> value="sewage2">Нет
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" name="sewage" <?php if($row_search_attr->sewage=='sewage3'){echo 'checked';} ?> value="sewage3">Возможность
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" name="sewage" <?php if($row_search_attr->sewage=='0'){echo 'checked';} ?> value="0">Не важно
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="col-md-12">Ограждение</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="radio" name="fence" <?php if($row_search_attr->fence=='ld_fence1'){echo 'checked';} ?> value="ld_fence1">Камень
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" name="fence" <?php if($row_search_attr->fence=='ld_fence2'){echo 'checked';} ?> value="ld_fence2">Проволочные сетки
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" name="fence" <?php if($row_search_attr->fence=='ld_fence3'){echo 'checked';} ?> value="ld_fence3">Нет
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" name="fence" <?php if($row_search_attr->fence=='0'){echo 'checked';} ?> value="0">Не важно
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="col-md-12">Плодовые деревья</div>
                                                    <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                        <div class="col-md-12">
                                                            <input type="radio" name="fruit_trees" value="fruit_trees1" <?php if($row_search_attr->fruit_trees=='fruit_trees1'){echo 'checked';} ?>>Есть
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" name="fruit_trees" value="fruit_trees2" <?php if($row_search_attr->fruit_trees=='fruit_trees2'){echo 'checked';} ?>>Нет
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="radio" name="fruit_trees" value="0" <?php if($row_search_attr->fruit_trees=='0'){echo 'checked';} ?>>Не важно
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            break;
                                        }
                                        case '9':{
                                            $row_search_attr = mwdb_get_row("SELECT * FROM client_garages WHERE search_id={var}",array($result_search->search_id));
                                            ?>
                                            <div class="col-md-3">
                                                <?php
                                                $array_area = explode('-',$row_search_attr->area);
                                                ?>
                                                <div class="col-md-12">Площадь</div>
                                                <div class="col-md-11">
                                                    <div class="col-md-4"><input type="text" name="area_from" class="form-control" placeholder="От" value="<?php if(isset($array_area[0])){echo $array_area[0];} ?>"></div>
                                                    <div class="col-md-4"><input type="text" name="area_to" class="form-control" placeholder="До" value="<?php if(isset($array_area[1])){echo $array_area[1];} ?>"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="col-md-12">Тип здания</div>
                                                <div class="col-md-11" style="background: #fff;border: 1px solid #D2D6DE;padding: 0 5px;">
                                                    <div class="col-md-12">
                                                        <input type="radio" <?php if($row_search_attr->building_type=='gg_building1'){echo 'checked';} ?> name="building_type" value="gg_building1">Камень
                                                    </div>
                                                    <div class="col-md-12">
                                                        <input type="radio" <?php if($row_search_attr->building_type=='gg_building2'){echo 'checked';} ?> name="building_type" value="gg_building2">Металл
                                                    </div>
                                                    <div class="col-md-12">
                                                        <input type="radio" <?php if($row_search_attr->building_type=='0'){echo 'checked';} ?> name="building_type" value="0">Не важно
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            break;
                                        }
                                        default:{
                                            break;
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-12" style="text-align: center;margin-top: 20px">
                                <input type="submit" name="add_client" value="Сохранить" class="btn btn-primary">
                            </div>
                        </form>
                    </div>
                    <div class="col-md-12">
                        <h5 style="color: #199AC0;">Результат поиска</h5>
                        <div class="col-md-12 search_results" data-user="<?php echo $id; ?>"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="bg-yellow color-palette block_offers" style="width: 500px;position: fixed;top: 55px;right: 0;padding:1px 10px;height: 63px;">
    <?php
        $offers_class = new Offer($id);
        $offers = $offers_class->get();
        if($offers){
            $i=0;
            foreach($offers as $offer){
                $i++;
            }
            ?>
            <div>
                <div>Количество выбранных недвижимостей для даннего пользователя: <strong style="font-size: 20px"><?php echo $i; ?></strong></div>
                <a href="http://broker.alex-r.am/index.php?action=buyer&subaction=offers&id=<?php echo $id; ?>" class="btn btn-block btn-primary" style="float: right;padding: 3px 12px;width: 30%">Список выбранных</a>
            </div>
            <?php
        }
        else{
            ?>
            <div>Пока нет предложения для покупателя!</div>
            <?php
        }
    ?>
</div>
<script>
    var page = 'add_post1';
    edit_region=<?php echo $result_search->region; ?>;
    <?php
    $city = '';
    $city_array = explode(',',$result_search->city);
    foreach($city_array as $cit){
        $city .= '"'.$cit.'",';
    }
    $city = substr($city,0,-1);
    ?>
    edit_city=[<?php echo $city; ?>];

    <?php
    $area = '';
    $area_array = explode(',',$result_search->area);
    foreach($area_array as $are){
        $area .= '"'.$are.'",';
    }
    $area = substr($area,0,-1);
    ?>
    edit_area=[<?php echo $area; ?>];

    edit_microarea='<?php echo $result_search->microarea; ?>'
</script>
<?php } ?>