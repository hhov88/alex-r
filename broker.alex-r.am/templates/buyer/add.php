<style>
    .city .col-md-12{
        margin: 0 !important;
    }
    .area .col-md-12{
        margin: 0 !important;
    }
    .for_microarea .col-md-12{
        padding-left: 10px;
    }
</style>
<?php
if(isset($_POST['add_client']) && $_POST['add_client']=='Добавить'){
    if($_POST['owner_name']!='') {
        if($_POST['broker']!='0') {
            $client = new Client(0, $_POST['owner_name'], $_POST['owner_phone1'], $_POST['owner_phone2'], $_POST['owner_email'], $_POST['owner_skype'], $_POST['owner_details'], $_SESSION['user_id'], $_POST['owner_type'],0,$_SESSION['user_id']);
            $post_client = $client->insert();
            $city = '';
            if(isset($_POST['city_val'])){
                foreach($_POST['city_val'] as $city_val){
                    $city .= $city_val.',';
                }
                $city = substr($city,0,-1);
            }
            $area = '';
            if(isset($_POST['area_val'])){
                foreach($_POST['area_val'] as $area_val){
                    $area .= $area_val.',';
                }
                $area = substr($area,0,-1);
            }
            $microarea = '';
            if(isset($_POST['micro_val'])){
                foreach($_POST['micro_val'] as $micro_val){
                    $microarea .= $micro_val.',';
                }
                $microarea = substr($microarea,0,-1);
            }
            mwdb_query("INSERT INTO client_search (client_id,type_search,price_from,price_to,region,city,area,microarea,category) VALUES ('{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}')",
                array($post_client,$_POST['s_type'],$_POST['price_from'],$_POST['price_to'],$_POST['region'],$city,$area,$microarea,$_POST['category']));
            $id_search = mwdb_get_var("SELECT search_id FROM client_search WHERE 1 ORDER BY search_id DESC");
            switch($_POST['category']){
                case '5':{
                    if($_POST['area_from']=='' && $_POST['area_to']==''){
                        $area = '';
                    }
                    else {
                        $area = $_POST['area_from'] . '-' . $_POST['area_to'];
                    }
                    $rooms = '';
                    if(isset($_POST['rooms'])){
                        foreach($_POST['rooms'] as $room){
                            $rooms .= $room.',';
                        }
                        $rooms = substr($rooms,0,-1);
                    }
                    if($_POST['floorer_from']=='0' && $_POST['floorer_to']=='0'){
                        $floorer = '';
                    }
                    else {
                        $floorer = $_POST['floorer_from'] . '-' . $_POST['floorer_to'];
                    }
                    if($_POST['floor_from']=='0' && $_POST['floor_to']=='0'){
                        $floor = '';
                    }
                    else {
                        $floor = $_POST['floor_from'] . '-' . $_POST['floor_to'];
                    }
                    $building_project= '';
                    if(isset($_POST['building_project'])){
                        foreach($_POST['building_project'] as $building_pr){
                            $building_project .= $building_pr.',';
                        }
                        $building_project = substr($building_project,0,-1);
                    }
                    $building_type= '';
                    if(isset($_POST['building_type'])){
                        foreach($_POST['building_type'] as $building_ty){
                            $building_type .= $building_ty.',';
                        }
                        $building_type = substr($building_type,0,-1);
                    }
                    $building_position= '';
                    if(isset($_POST['building_position'])){
                        foreach($_POST['building_position'] as $building_po){
                            $building_position .= $building_po.',';
                        }
                        $building_position = substr($building_position,0,-1);
                    }
                    $housetop= '';
                    if(isset($_POST['housetop'])){
                        foreach($_POST['housetop'] as $houseto){
                            $housetop .= $houseto.',';
                        }
                        $housetop = substr($housetop,0,-1);
                    }
                    $floor_name= '';
                    if(isset($_POST['floor_name'])){
                        foreach($_POST['floor_name'] as $floor_n){
                            $floor_name .= $floor_n.',';
                        }
                        $floor_name = substr($floor_name,0,-1);
                    }
                    $repair= '';
                    if(isset($_POST['repair'])){
                        foreach($_POST['repair'] as $repai){
                            $repair .= $repai.',';
                        }
                        $repair = substr($repair,0,-1);
                    }
                    $water = '';
                    if(isset($_POST['water'])){
                        $water = $_POST['water'];
                    }
                    $gas = '';
                    if(isset($_POST['gas'])){
                        $gas = $_POST['gas'];
                    }
                    $balcony= '';
                    if(isset($_POST['balcony'])){
                        foreach($_POST['balcony'] as $balcon){
                            $balcony .= $balcon.',';
                        }
                        $balcony = substr($balcony,0,-1);
                    }
                    $garage= '';
                    if(isset($_POST['garage'])){
                        foreach($_POST['garage'] as $garag){
                            $garage .= $garag.',';
                        }
                        $garage = substr($garage,0,-1);
                    }
                    $basement = '';
                    if(isset($_POST['basement'])){
                        $basement = $_POST['basement'];
                    }
                    mwdb_query("INSERT INTO client_apartments(search_id,area,rooms,floorer,floor,floor_name,repair,balcony,garage,basement,water,gas,building_project,building_position,building_type,housetop) VALUES
                    ('{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}')",
                        array($id_search,$area,$rooms,$floorer,$floor,$floor_name,$repair,$balcony,$garage,$basement,$water,$gas,$building_project,$building_position,$building_type,$housetop));
                    break;
                }
                case '6':{
                    if($_POST['total_area_from']=='' && $_POST['total_area_to']==''){
                        $total_area = '';
                    }
                    else {
                        $total_area = $_POST['total_area_from'] . '-' . $_POST['total_area_to'];
                    }
                    if($_POST['home_area_from']=='' && $_POST['home_area_to']==''){
                        $home_area = '';
                    }
                    else {
                        $home_area = $_POST['home_area_from'] . '-' . $_POST['home_area_to'];
                    }
                    $rooms = '';
                    if(isset($_POST['rooms'])){
                        foreach($_POST['rooms'] as $room){
                            $rooms .= $room.',';
                        }
                        $rooms = substr($rooms,0,-1);
                    }
                    if($_POST['floorer_from']=='0' && $_POST['floorer_to']=='0'){
                        $floorer = '';
                    }
                    else {
                        $floorer = $_POST['floorer_from'] . '-' . $_POST['floorer_to'];
                    }
                    $building_type= '';
                    if(isset($_POST['building_type'])){
                        foreach($_POST['building_type'] as $building_ty){
                            $building_type .= $building_ty.',';
                        }
                        $building_type = substr($building_type,0,-1);
                    }
                    $building_position= '';
                    if(isset($_POST['building_position'])){
                        foreach($_POST['building_position'] as $building_po){
                            $building_position .= $building_po.',';
                        }
                        $building_position = substr($building_position,0,-1);
                    }
                    $repair= '';
                    if(isset($_POST['repair'])){
                        foreach($_POST['repair'] as $repai){
                            $repair .= $repai.',';
                        }
                        $repair = substr($repair,0,-1);
                    }
                    $water = '';
                    if(isset($_POST['water'])){
                        $water = $_POST['water'];
                    }
                    $gas = '';
                    if(isset($_POST['gas'])){
                        $gas = $_POST['gas'];
                    }
                    $heating_system = '';
                    if(isset($_POST['heating_system'])){
                        $heating_system = $_POST['heating_system'];
                    }
                    $balcony= '';
                    if(isset($_POST['balcony'])){
                        foreach($_POST['balcony'] as $balcon){
                            $balcony .= $balcon.',';
                        }
                        $balcony = substr($balcony,0,-1);
                    }
                    $garage= '';
                    if(isset($_POST['garage'])){
                        foreach($_POST['garage'] as $garag){
                            $garage .= $garag.',';
                        }
                        $garage = substr($garage,0,-1);
                    }
                    $basement = '';
                    if(isset($_POST['basement'])){
                        $basement = $_POST['basement'];
                    }
                    mwdb_query("INSERT INTO client_houses(search_id,total_area,home_area,rooms,floorer,floor,repair,balcony,garage,basement,water,gas,heating_system,building_type,building_position) VALUES
                    ('{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}')",
                        array($id_search,$total_area,$home_area,$rooms,$floorer,'',$repair,$balcony,$garage,$basement,$water,$gas,$heating_system,$building_type,$building_position));
                    break;
                }
                case '7':{
                    $typer= '';
                    if(isset($_POST['typer'])){
                        foreach($_POST['typer'] as $type){
                            $typer .= $type.',';
                        }
                        $typer = substr($typer,0,-1);
                    }
                    if($_POST['area_from']=='' && $_POST['area_to']==''){
                        $area = '';
                    }
                    else {
                        $area = $_POST['area_from'] . '-' . $_POST['area_to'];
                    }
                    $rooms = '';
                    if(isset($_POST['rooms'])){
                        foreach($_POST['rooms'] as $room){
                            $rooms .= $room.',';
                        }
                        $rooms = substr($rooms,0,-1);
                    }
                    if($_POST['floor_from']=='0' && $_POST['floor_to']=='0'){
                        $floor = '';
                    }
                    else {
                        $floor = $_POST['floor_from'] . '-' . $_POST['floor_to'];
                    }
                    $floor_name= '';
                    if(isset($_POST['floor_name'])){
                        foreach($_POST['floor_name'] as $floor_n){
                            $floor_name .= $floor_n.',';
                        }
                        $floor_name = substr($floor_name,0,-1);
                    }
                    $building_type= '';
                    if(isset($_POST['building_type'])){
                        foreach($_POST['building_type'] as $building_ty){
                            $building_type .= $building_ty.',';
                        }
                        $building_type = substr($building_type,0,-1);
                    }
                    $building_position= '';
                    if(isset($_POST['building_position'])){
                        foreach($_POST['building_position'] as $building_po){
                            $building_position .= $building_po.',';
                        }
                        $building_position = substr($building_position,0,-1);
                    }
                    $showcases = '';
                    if(isset($_POST['showcases'])){
                        $showcases = $_POST['showcases'];
                    }
                    $entrance = '';
                    if(isset($_POST['entrance'])){
                        $entrance = $_POST['entrance'];
                    }
                    $repair = '';
                    if(isset($_POST['repair'])){
                        $repair = $_POST['repair'];
                    }
                    mwdb_query("INSERT INTO client_commercial(search_id,type,area,rooms,floor,floorer,floor_name,showcases,entrance,repair,building_type,building_position) VALUES
                    ('{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}')",
                        array($id_search,$typer,$area,$rooms,$floor,'',$floor_name,$showcases,$entrance,$repair,$building_type,$building_position));
                    break;
                }
                case '8':{
                    if($_POST['area_from']=='' && $_POST['area_to']==''){
                        $area = '';
                    }
                    else {
                        $area = $_POST['area_from'] . '-' . $_POST['area_to'];
                    }
                    if($_POST['buildings_from']=='' && $_POST['buildings_to']==''){
                        $buildings = '';
                    }
                    else {
                        $buildings = $_POST['buildings_from'] . '-' . $_POST['buildings_to'];
                    }
                    $water_drink = '';
                    if(isset($_POST['water_drink'])){
                        $water_drink = $_POST['water_drink'];
                    }
                    $water_irrigation = '';
                    if(isset($_POST['water_irrigation'])){
                        $water_irrigation = $_POST['water_irrigation'];
                    }
                    $gas = '';
                    if(isset($_POST['gas'])){
                        $gas = $_POST['gas'];
                    }
                    $electricity = '';
                    if(isset($_POST['electricity'])){
                        $electricity = $_POST['electricity'];
                    }
                    $high_voltage_pillars = '';
                    if(isset($_POST['high_voltage_pillars'])){
                        $high_voltage_pillars = $_POST['high_voltage_pillars'];
                    }
                    $sewage = '';
                    if(isset($_POST['sewage'])){
                        $sewage = $_POST['sewage'];
                    }
                    $fence = '';
                    if(isset($_POST['fence'])){
                        $fence = $_POST['fence'];
                    }
                    $fruit_trees = '';
                    if(isset($_POST['fruit_trees'])){
                        $fruit_trees = $_POST['fruit_trees'];
                    }
                    mwdb_query("INSERT INTO client_land(search_id,area,buildings,water_drink,water_irrigation,gas,electricity,high_voltage_pillars,sewage,fence,fruit_trees) VALUES
                    ('{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}')",
                        array($id_search,$area,$buildings,$water_drink,$water_irrigation,$gas,$electricity,$high_voltage_pillars,$sewage,$fence,$fruit_trees));
                    break;
                }
                case '9':{
                    if($_POST['area_from']=='' && $_POST['area_to']==''){
                        $area = '';
                    }
                    else {
                        $area = $_POST['area_from'] . '-' . $_POST['area_to'];
                    }
                    $building_type = '';
                    if(isset($_POST['building_type'])){
                        $building_type = $_POST['building_type'];
                    }
                    mwdb_query("INSERT INTO client_garages(search_id,area,building_type) VALUES ('{var}','{var}','{var}')",array($id_search,$area,$building_type));
                    break;
                }
            }
            echo '<script>document.location.href="http://broker.alex-r.am/index.php?action=buyer&subaction=view&id=' . $post_client . '"</script>';
        }
        else{
            echo 'Выберите Риэлтора!';
        }
    }
    else{
        echo 'Заполните Имя клиента!';
    }
}
?>
<section class="content-header">
    <h1>Добавить Клиента</h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body table-responsive">
                    <form action="" method="post">
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <!--<select class="form-control" name="broker">
                                    <option value="0">-- Риэлтор --</option>
                                    <?php
                                    $result_brokers = mwdb_select("SELECT user_id,user_name FROM users WHERE user_type={var}",array(3));
                                    foreach($result_brokers as $row_broker){
                                        ?>
                                        <option value="<?php echo $row_broker->user_id; ?>"><?php echo $row_broker->user_name; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>-->
                            </div>
                            <div class="col-md-2" style="float: right">
                                <select name="owner_type" class="form-control">
                                    <option value="0">Обычный клиент</option>
                                    <option value="2">Первичный клиент</option>
                                    <option value="1">Вторичный клиент</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-5">
                                <div class="col-md-6">
                                    <input type="text" name="owner_name" class="owner_name form-control" placeholder="Имя клиента"><br>
                                    <input type="text" name="owner_phone1" class="owner_phone1 form-control" placeholder="Телефон1"><br>
                                    <input type="text" name="owner_phone2" class="owner_phone2 form-control" placeholder="Телефон2"><br>
                                    <input type="email" name="owner_email" class="owner_email form-control" placeholder="E-mail"><br>
                                    <input type="skype" name="owner_skype" class="owner_skype form-control" placeholder="Skype">
                                </div>
                                <div class="col-md-6">
                                    <textarea name="owner_details" class="owner_details form-control" placeholder="Другие детали клиента" style="height: 212px;resize: none"></textarea>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="col-md-12">
                                    <div class="col-md-3" style="text-align: center">
                                        <input type="radio" name="s_type" value="sale" checked><label for="s_type">Продажа</label>
                                    </div>
                                    <div class="col-md-3" style="text-align: center">
                                        <input type="radio" name="s_type" value="rent"><label for="s_type">Аренда</label>
                                    </div>
                                    <div class="col-md-3" style="text-align: center">
                                        <input type="radio" name="s_type" value="day"><label for="s_type">Посуточнo</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-2" style="text-align: center">Цена</div>
                                    <div class="col-md-2">
                                        <input type="text" name="price_from" placeholder="От" class="form-control" style="height: 20px">
                                    </div>
                                    <div class="col-md-1" style="text-align: center;">_</div>
                                    <div class="col-md-2">
                                        <input type="text" name="price_to" placeholder="До" class="form-control" style="height: 20px">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-2">
                                        <select name="region" class="region form-control" onchange="region_change(this);$('.area').html('');">
                                            <option value="0">-- Регион --</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 city" style="height: 150px;overflow: auto;border: 1px solid #D2D6DE;padding: 0 3px">
                                    </div>
                                    <div class="col-md-5 area" style="height: 150px;overflow: auto;border: 1px solid #D2D6DE;padding: 0 3px">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="border-top: 1px solid #3C8DBC;">
                            <div class="col-md-2">
                                <select name="category" class="form-control" onchange="get_client_search_attributes(this)" style="background: #C4EBFF;">
                                    <option value="0">-- Категория --</option>
                                    <?php
                                    $cats = mwdb_select("SELECT category_id,category_title_ru FROM category WHERE {var} ORDER BY category_order",array(1));
                                    foreach($cats as $cat){
                                        ?>
                                        <option value="<?php echo $cat->category_id; ?>"><?php echo $cat->category_title_ru; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-12 search_attributes" style="background: #F9FAFC;padding: 23px 25px;"></div>
                        </div>
                        <div class="col-md-12" style="text-align: center;margin-top: 20px">
                            <input type="submit" name="add_client" value="Добавить" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    var page = 'add_post1';edit_region=8;
</script>