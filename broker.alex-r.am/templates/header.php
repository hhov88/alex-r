<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>alex-r.am | <?php echo $globval['title']; ?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!--<link rel="stylesheet" href="<?php //echo base_url(); ?>admin/css/style.css" type="text/css" media="screen" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="<?php //echo base_url(); ?>admin/js/nicEdit.js"></script>
    <!--<script type="text/javascript">
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas({fullPanel : true}) });
    </script>-->
    <link href="<?php echo base_url(); ?>css/dropzone.css" rel="stylesheet" type="text/css">
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo base_url(); ?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="<?php echo base_url(); ?>plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url(); ?>dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?php echo base_url(); ?>dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>plugins/iCheck/flat/blue.css" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="<?php echo base_url(); ?>plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="<?php echo base_url(); ?>plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- Date Picker -->
    <link href="<?php echo base_url(); ?>plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="<?php echo base_url(); ?>plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="<?php echo base_url(); ?>css/blueimp-gallery.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.fileupload.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.fileupload-ui.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/sweetalert.css">
    <!-- CSS adjustments for browsers with JavaScript disabled -->
    <noscript><link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.fileupload-noscript.css"></noscript>
    <noscript><link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.fileupload-ui-noscript.css"></noscript>
    <!-- image upload -->
    <link href="<?php echo base_url(); ?>css/new_style.css"  rel="stylesheet" type="text/css" />
</head>
<body class="skin-black-light sidebar-mini <?php if(isset($_SESSION['sidebar']) && $_SESSION['sidebar']=='0'){ ?>sidebar-collapse <?php } ?>">
<div class="box" style="position: fixed;height: 100%;z-index: 10000;display: none" id="ajax_load_my">
<div class="overlay">
    <i class="fa fa-refresh fa-spin"></i>
</div>
</div>
<div class="wrapper">


    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="<?php echo base_url(); ?>index.php" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><img src="<?php echo base_url(); ?>LOGO.png" width="40" /></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><img src="<?php echo base_url(); ?>LOGO.png" width="40" /></span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" onclick="set_sideabr_type()" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->
                    <!--<li class="dropdown messages-menu">
                        <!-- Menu toggle button
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-envelope-o"></i>
                            <span class="label label-success">4</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have 4 messages</li>
                            <li>
                                <!-- inner menu: contains the messages
                                <ul class="menu">
                                    <li><!-- start message
                                        <a href="#">
                                            <div class="pull-left">
                                                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
                                            </div>
                                            <h4>
                                                Support Team
                                                <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                            </h4>
                                            <p>Why not buy a new awesome theme?</p>
                                        </a>
                                    </li><!-- end message
                                    <li>
                                        <a href="#">
                                            <div class="pull-left">
                                                <img src="dist/img/user3-128x128.jpg" class="img-circle" alt="user image"/>
                                            </div>
                                            <h4>
                                                AdminLTE Design Team
                                                <small><i class="fa fa-clock-o"></i> 2 hours</small>
                                            </h4>
                                            <p>Why not buy a new awesome theme?</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <div class="pull-left">
                                                <img src="dist/img/user4-128x128.jpg" class="img-circle" alt="user image"/>
                                            </div>
                                            <h4>
                                                Developers
                                                <small><i class="fa fa-clock-o"></i> Today</small>
                                            </h4>
                                            <p>Why not buy a new awesome theme?</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <div class="pull-left">
                                                <img src="dist/img/user3-128x128.jpg" class="img-circle" alt="user image"/>
                                            </div>
                                            <h4>
                                                Sales Department
                                                <small><i class="fa fa-clock-o"></i> Yesterday</small>
                                            </h4>
                                            <p>Why not buy a new awesome theme?</p>
                                        </a>
                                    </li>

                                </ul>
                            </li>
                            <li class="footer"><a href="#">See All Messages</a></li>
                        </ul>
                    </li><!-- /.messages-menu -->

                    <!-- Notifications Menu
                    <li class="dropdown notifications-menu">
                        <!-- Menu toggle button
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="label label-warning">5</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have 5 notifications</li>
                            <li>
                                <!-- Inner Menu: contains the notifications
                                <ul class="menu">
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the page and may cause design problems
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-users text-red"></i> 5 new members joined
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#">
                                            <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-user text-red"></i> You changed your username
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer"><a href="#">View all</a></li>
                        </ul>
                    </li>
                    <!-- Tasks Menu
                    <li class="dropdown tasks-menu">
                        <!-- Menu Toggle Button
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-flag-o"></i>
                            <span class="label label-danger">9</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have 9 tasks</li>
                            <li>
                                <!-- Inner menu: contains the tasks
                                <ul class="menu">
                                    <li><!-- Task item
                                        <a href="#">
                                            <h3>
                                                Design some buttons
                                                <small class="pull-right">20%</small>
                                            </h3>
                                            <div class="progress xs">
                                                <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                    <span class="sr-only">20% Complete</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li><!-- end task item -->
                                    <li><!-- Task item
                                        <a href="#">
                                            <h3>
                                                Create a nice theme
                                                <small class="pull-right">40%</small>
                                            </h3>
                                            <div class="progress xs">
                                                <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                    <span class="sr-only">40% Complete</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li><!-- end task item -->
                                    <li><!-- Task item
                                        <a href="#">
                                            <h3>
                                                Some task I need to do
                                                <small class="pull-right">60%</small>
                                            </h3>
                                            <div class="progress xs">
                                                <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                    <span class="sr-only">60% Complete</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li><!-- end task item -->
                                    <li><!-- Task item
                                        <a href="#">
                                            <h3>
                                                Make beautiful transitions
                                                <small class="pull-right">80%</small>
                                            </h3>
                                            <div class="progress xs">
                                                <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                    <span class="sr-only">80% Complete</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li><!-- end task item
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="#">View all tasks</a>
                            </li>
                        </ul>
                    </li>
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="<?php echo base_url(); ?>index.php?action=logout" class="dropdown-toggle" >
                            <!-- The user image in the navbar-->
                            <img src="<?php echo base_url(); ?>LOGO.png" class="user-image" alt="User Image"/>
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">Выйти</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="<?php echo base_url(); ?>LOGO.png" class="img-circle" alt="User Image" />
                                <p>
                                    Administrator of Alex-r.am
                                    <small>Create at 07.10.2015</small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <!--<li class="user-body">
                                <div class="col-xs-4 text-center">
                                    <a href="#">Followers</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Sales</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Friends</a>
                                </div>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-right">
                                    <a href="<?php echo base_url(); ?>index.php?action=logout" class="btn btn-default btn-flat">Выйти</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>





    <!--<div class="menu_list">
        <?php
        //if(isset( $_SESSION['username'] )){
            ?>
            <ul class="menu">
                <li><a href="<?php //echo base_url(); ?>admin/index.php">Главная</a></li>
                <li><a href="<?php //echo base_url(); ?>admin/index.php?action=operators">Операторы</a></li>
                <li><a href="<?php //echo base_url(); ?>admin/index.php?action=users">Пользователи</a></li>
                <li><a href="<?php //echo base_url(); ?>admin/index.php?action=location">Страна, регион, город</a></li>
            </ul>
        <?php
        //}
        ?>
    </div>-->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <!--<div class="user-panel">
                <div class="pull-left image">
                    <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>Alexander Pierce</p>

                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>-->
            <ul class="sidebar-menu">
                <?php
                global $user_level;
                if($user_level!='3'){
                if($user_level=='1'){
                ?>
                <li <?php if(!isset($_GET['action']) || $_GET['action']==''){echo 'class="active treeview"';} ?>>
                    <a href="<?php echo base_url(); ?>index.php">
                        <i class="fa fa-home"></i> <span>Главная</span>
                    </a>
                </li>
                <li <?php if(isset($_GET['action']) && $_GET['action']=='operators'){echo 'class="active treeview"';} ?>>
                    <a href="<?php echo base_url(); ?>index.php?action=operators">
                        <i class="fa fa-user-secret"></i> <span>Операторы</span>
                    </a>
                </li>
                    <li <?php if(isset($_GET['action']) && $_GET['action']=='brokers'){echo 'class="active treeview"';} ?>>
                        <a href="<?php echo base_url(); ?>index.php?action=brokers">
                            <i class="fa fa-user-secret"></i> <span>Брокеры</span>
                        </a>
                    </li>
                <li <?php if(isset($_GET['action']) && $_GET['action']=='category'){echo 'class="active treeview"';} ?>>
                    <a href="<?php echo base_url(); ?>index.php?action=category">
                        <i class="fa fa-qrcode"></i> <span>Категории</span>
                    </a>
                </li>
                <?php } ?>
                <li class="treeview <?php if(isset($_GET['action']) && $_GET['action']=='post'){echo 'active';} ?>">
                    <a href="#">
                        <i class="fa fa-building"></i>
                        <span>Недвижимость</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url(); ?>index.php?action=post&subaction=all"><i class="fa fa-circle-o"></i> Все</a></li>
                        <li><a href="<?php echo base_url(); ?>index.php?action=post&subaction=add"><i class="fa fa-circle-o"></i> Добавить</a></li>
                        <li><a href="<?php echo base_url(); ?>index.php?action=post&subaction=map"><i class="fa fa-map-marker"></i> На карте</a></li>
                        <li><a href="<?php echo base_url(); ?>index.php?action=post&subaction=sales"><i class="fa fa-circle-o"></i> Проданные</a></li>
                        <li><a href="<?php echo base_url(); ?>index.php?action=post&subaction=archive"><i class="fa fa-circle-o"></i> Архив</a></li>
                    </ul>
                </li>
                <li class="treeview <?php if(isset($_GET['action']) && $_GET['action']=='buyer'){echo 'active';} ?>">
                    <a href="#">
                        <i class="fa  fa-user-plus"></i>
                        <span>Покупатели</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url(); ?>index.php?action=buyer&subaction=all"><i class="fa fa-circle-o"></i> Все</a></li>
                        <li><a href="<?php echo base_url(); ?>index.php?action=buyer&subaction=add"><i class="fa fa-circle-o"></i> Добавить</a></li>
                        <li><a href="<?php echo base_url(); ?>index.php?action=buyer&subaction=archive"><i class="fa fa-circle-o"></i> Архив</a></li>
                    </ul>
                </li>
                <?php
                if($user_level=='1'){
                ?>
                <li <?php if(isset($_GET['action']) && $_GET['action']=='translations'){echo 'class="active treeview"';} ?>>
                    <a href="<?php echo base_url(); ?>index.php?action=translations">
                        <i class="fa fa-flag"></i> <span>Переводы</span>
                    </a>
                </li>
                    <li <?php if(isset($_GET['action']) && $_GET['action']=='page'){echo 'class="active treeview"';} ?>>
                        <a href="<?php echo base_url(); ?>index.php?action=page">
                            <i class="fa fa-external-link"></i> <span>Страницы</span>
                        </a>
                    </li>
                    <li <?php if(isset($_GET['action']) && $_GET['action']=='news'){echo 'class="active treeview"';} ?>>
                        <a href="<?php echo base_url(); ?>index.php?action=news">
                            <i class="fa fa-newspaper-o"></i> <span>Новости</span>
                        </a>
                    </li>
                    <li <?php if(isset($_GET['action']) && $_GET['action']=='team'){echo 'class="active treeview"';} ?>>
                        <a href="<?php echo base_url(); ?>index.php?action=team">
                            <i class="fa fa-meh-o"></i> <span>Наша команда</span>
                        </a>
                    </li>
                    <li class="treeview <?php if(isset($_GET['action']) && $_GET['action']=='locations'){echo 'active';} ?>">
                        <a href="#">
                            <i class="fa  fa-map-marker"></i>
                            <span>Места</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url(); ?>index.php?action=locations&subaction=region"><i class="fa fa-circle-o"></i> Регионы</a></li>
                            <li><a href="<?php echo base_url(); ?>index.php?action=locations&subaction=city&region=8"><i class="fa fa-circle-o"></i> Города</a></li>
                            <li><a href="<?php echo base_url(); ?>index.php?action=locations&subaction=area&region=8"><i class="fa fa-circle-o"></i> Районы</a></li>
                            <li><a href="<?php echo base_url(); ?>index.php?action=locations&subaction=microarea&region=8"><i class="fa fa-circle-o"></i> Микрорайоны</a></li>
                            <li><a href="<?php echo base_url(); ?>index.php?action=locations&subaction=street&region=8"><i class="fa fa-circle-o"></i> Улицы</a></li>
                        </ul>
                    </li>
                    <li <?php if(isset($_GET['action']) && $_GET['action']=='branch'){echo 'class="active treeview"';} ?>>
                        <a href="<?php echo base_url(); ?>index.php?action=branch">
                            <i class="fa fa-sitemap"></i> <span>Наши офисы</span>
                        </a>
                    </li>
                <?php } } else{
                    ?>
                    <li class="treeview <?php if(isset($_GET['action']) && $_GET['action']=='post'){echo 'active';} ?>">
                        <a href="#">
                            <i class="fa fa-building"></i>
                            <span>Недвижимость</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url(); ?>index.php?action=post&subaction=all"><i class="fa fa-circle-o"></i> Все</a></li>
                            <li><a href="<?php echo base_url(); ?>index.php?action=post&subaction=map"><i class="fa fa-map-marker"></i> На карте</a></li>
                        </ul>
                    </li>
                    <li class="treeview <?php if(isset($_GET['action']) && $_GET['action']=='buyer'){echo 'active';} ?>">
                        <a href="#">
                            <i class="fa  fa-user-plus"></i>
                            <span>Покупатели</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url(); ?>index.php?action=buyer&subaction=all"><i class="fa fa-circle-o"></i> Все</a></li>
                            <li><a href="<?php echo base_url(); ?>index.php?action=buyer&subaction=add"><i class="fa fa-circle-o"></i> Добавить</a></li>
                        </ul>
                    </li>
                    <?php
                } ?>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
    <div class="content-wrapper">
        <section class="content">

