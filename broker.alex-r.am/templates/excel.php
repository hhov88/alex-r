<?php
$id = (int)$_GET['id'];
$client_class = new Client($id);
$client_class->get();
$offers_class = new Offer($id);
if(isset($_GET['offer_status'])){
	$status = (int)$_GET['offer_status'];
}
else{
	$status = '';
}
$offers = $offers_class->get($status);
require_once($_SERVER['DOCUMENT_ROOT'].'/PHPExcel/PHPExcel.php');
// Подключаем класс для вывода данных в формате excel
require_once($_SERVER['DOCUMENT_ROOT'].'/PHPExcel/PHPExcel/Writer/Excel5.php');

// Создаем объект класса PHPExcel
$xls = new PHPExcel();
// Устанавливаем индекс активного листа
$xls->setActiveSheetIndex(0);
// Получаем активный лист
$sheet = $xls->getActiveSheet();
// Подписываем лист
$sheet->setTitle('Клиент N'.$_GET['id']);
$sheet->getColumnDimension('A')->setWidth(10);
$sheet->getColumnDimension('B')->setWidth(10);
$sheet->getColumnDimension('C')->setWidth(14);
$sheet->getColumnDimension('D')->setWidth(10);
$sheet->getColumnDimension('E')->setWidth(14);
$sheet->getColumnDimension('F')->setWidth(40);
$sheet->getColumnDimension('G')->setWidth(100);
// Вставляем текст в ячейку A1
$sheet->setCellValue("A1", $client_class->client_name.'  '.$client_class->client_phone1.($client_class->client_phone2!=''?', '.$client_class->client_phone2:''));
$sheet->getStyle('A1')->getFill()->setFillType(
	PHPExcel_Style_Fill::FILL_SOLID);
$sheet->getStyle('A1')->getFill()->getStartColor()->setRGB('EEEEEE');

// Объединяем ячейки
$sheet->mergeCells('A1:G1');

// Выравнивание текста
$sheet->getStyle('A1')->getAlignment()->setHorizontal(
	PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
$sheet->setCellValue("A2", "ID");
$sheet->getStyle('A2')->getFill()->setFillType(
	PHPExcel_Style_Fill::FILL_SOLID);
$sheet->getStyle('A2')->getFill()->getStartColor()->setRGB('CCCCCC');
$sheet->getStyle('A2')->getAlignment()->setHorizontal(
	PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$sheet->setCellValue("B2", "Кол-во комнат");
$sheet->getStyle('B2')->getFill()->setFillType(
	PHPExcel_Style_Fill::FILL_SOLID);
$sheet->getStyle('B2')->getFill()->getStartColor()->setRGB('CCCCCC');
$sheet->getStyle('B2')->getAlignment()->setHorizontal(
	PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
$sheet->setCellValue("C2", "Площадь");
$sheet->getStyle('C2')->getFill()->setFillType(
	PHPExcel_Style_Fill::FILL_SOLID);
$sheet->getStyle('C2')->getFill()->getStartColor()->setRGB('CCCCCC');
$sheet->getStyle('C2')->getAlignment()->setHorizontal(
	PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
$sheet->setCellValue("D2", "Этаж");
$sheet->getStyle('D2')->getFill()->setFillType(
	PHPExcel_Style_Fill::FILL_SOLID);
$sheet->getStyle('D2')->getFill()->getStartColor()->setRGB('CCCCCC');
$sheet->getStyle('D2')->getAlignment()->setHorizontal(
	PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
$sheet->setCellValue("E2", "Цена");
$sheet->getStyle('E2')->getFill()->setFillType(
	PHPExcel_Style_Fill::FILL_SOLID);
$sheet->getStyle('E2')->getFill()->getStartColor()->setRGB('CCCCCC');
$sheet->getStyle('E2')->getAlignment()->setHorizontal(
	PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
$sheet->setCellValue("F2", "Адрес,Тел.");
$sheet->getStyle('F2')->getFill()->setFillType(
	PHPExcel_Style_Fill::FILL_SOLID);
$sheet->getStyle('F2')->getFill()->getStartColor()->setRGB('CCCCCC');
$sheet->getStyle('F2')->getAlignment()->setHorizontal(
	PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
$sheet->setCellValue("G2", "Заметка");
$sheet->getStyle('G2')->getFill()->setFillType(
	PHPExcel_Style_Fill::FILL_SOLID);
$sheet->getStyle('G2')->getFill()->getStartColor()->setRGB('CCCCCC');
$sheet->getStyle('G2')->getAlignment()->setHorizontal(
	PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
/*<th>ID</th>
<th>Кол-во комнат</th>
<th>Площадь</th>
<th>Этаж</th>
<th>Цена</th>
<th>Адрес,Тел.</th>
<th style="width: 30%">Заметка</th>*/
	$row_my = 3;
	 foreach($offers as $offer){
		$post_class = new Post('ru',$offer->post_id);
		$post = $post_class->get();
		$location = '';
		if($post->post_region>0){
			$location .= mwdb_get_var("SELECT region_ru FROM region WHERE region_id={var}",array($post->post_region)).', ';
		}
		if($post->post_city>0){
			$location .= mwdb_get_var("SELECT city_ru FROM city WHERE city_id={var}",array($post->post_city)).', ';
		}
		if($post->post_area>0){
			$location .= mwdb_get_var("SELECT area_ru FROM area WHERE area_id={var}",array($post->post_area)).', ';
		}
		if($post->post_microarea>0){
			$location .= mwdb_get_var("SELECT area_ru FROM microarea WHERE area_id={var}",array($post->post_microarea)).', ';
		}
		if($post->post_street>0){
			$location .= mwdb_get_var("SELECT street_ru FROM streets WHERE street_id={var}",array($post->post_street)).' ';
		}
		$location .= $post->post_address;
		$owner = json_decode($post->post_owner);
		$owner_show = $owner->owner_name.' '.$owner->owner_phone1.' '.$owner->owner_phone2;
		$attributes = json_decode($post->settings);
		if(isset($attributes->rooms)){
			$rooms = $attributes->rooms;
		}
		else{
			$rooms = '';
		}
		if(isset($attributes->area)){
			$area = $attributes->area;
		}
		elseif($attributes->home_area){
			$area = $attributes->home_area;
		}
		else{
			$area = '';
		}
		$fl = '';
		if(isset($attributes->floor) && $attributes->floor!=''){
			$fl .= $attributes->floor;
		}
		if(isset($attributes->floorer) && $attributes->floorer!=''){
			$fl .= '/'.$attributes->floorer;
		}
		$settings = json_decode($post->post_meta);
		if(isset($settings->sale) && $settings->sale=='1'){
			$price = $settings->price_number.' '.$settings->price_currency;
		}
		if(isset($settings->rent) && $settings->rent=='1'){
			$price .= '<br>Аренда: ';
			if(isset($settings->rent_cost1) && (double)$settings->rent_cost1>0){
				$price .= $settings->rent_cost1.' '.$settings->rent_currency1;
			}
			elseif(isset($settings->rent_cost2) && (double)$settings->rent_cost2>0){
				$price .= $settings->rent_cost2.' '.$settings->rent_currency2;
			}
			elseif(isset($settings->rent_cost3) && (double)$settings->rent_cost3>0){
				$price .= $settings->rent_cost3.' '.$settings->rent_currency3;
			}
		}
		$sheet->setCellValueByColumnAndRow(
										  0,
										  $row_my,
										  $post->post_id);
		// Применяем выравнивание
		$sheet->getStyleByColumnAndRow(0, $row_my)->getAlignment()->
				setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		$sheet->setCellValueByColumnAndRow(
										  1,
										  $row_my,
										  $rooms);
		// Применяем выравнивание
		$sheet->getStyleByColumnAndRow(1, $row_my)->getAlignment()->
				setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				
		$sheet->setCellValueByColumnAndRow(
										  2,
										  $row_my,
										  $area.' m2');
		// Применяем выравнивание
		$sheet->getStyleByColumnAndRow(2, $row_my)->getAlignment()->
				setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				
		$sheet->setCellValueByColumnAndRow(
										  3,
										  $row_my,
										  $fl);
		// Применяем выравнивание
		$sheet->getStyleByColumnAndRow(3, $row_my)->getAlignment()->
				setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				
		$sheet->setCellValueByColumnAndRow(
										  4,
										  $row_my,
										  $price);
		// Применяем выравнивание
		$sheet->getStyleByColumnAndRow(4, $row_my)->getAlignment()->
				setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				
		$sheet->setCellValueByColumnAndRow(
										  5,
										  $row_my,
										  $location.', '.$owner_show);
		// Применяем выравнивание
		$sheet->getStyleByColumnAndRow(5, $row_my)->getAlignment()->
				setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$sheet->getStyleByColumnAndRow(5, $row_my)->getAlignment()->setWrapText(true);		
		$sheet->setCellValueByColumnAndRow(
										  6,
										  $row_my,
										  $offer->offer_text);
		// Применяем выравнивание
		$sheet->getStyleByColumnAndRow(6, $row_my)->getAlignment()->
				setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$row_my++;
	}
header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
header ( "Cache-Control: no-cache, must-revalidate" );
header ( "Pragma: no-cache" );
header ( "Content-type: application/vnd.ms-excel" );
header ( "Content-Disposition: attachment; filename=Клиент N".$_GET['id'].".xls" );
// Выводим содержимое файла
$objWriter = new PHPExcel_Writer_Excel5($xls);
$objWriter->save('php://output');
?>