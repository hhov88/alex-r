<?php
function mwdb_select($zapros,$array_args=array()){
    global $mwdb;
    $return = array();
    foreach($array_args as $arg){
        $arg = $mwdb->real_escape_string($arg);
        $zapros = preg_replace('/{var}/',$arg,$zapros,1);
    }
    $result_zapros = $mwdb->query($zapros);
    while($row_zapros = mysqli_fetch_object($result_zapros)){
        array_push($return,$row_zapros);
    }
    return $return;
}
function mwdb_get_row($zapros,$array_args=array()){
    global $mwdb;
    foreach($array_args as $arg){
        $arg = $mwdb->real_escape_string($arg);
        $zapros = preg_replace('/{var}/',$arg,$zapros,1);
    }
    $result_zapros = $mwdb->query($zapros." LIMIT 1");
    $row_zapros = mysqli_fetch_object($result_zapros);
    return $row_zapros;
}
function mwdb_get_var($zapros,$array_args=array()){
    global $mwdb;
    foreach($array_args as $arg){
        $arg = $mwdb->real_escape_string($arg);
        $zapros = preg_replace('/{var}/',$arg,$zapros,1);
    }
    $result_zapros = $mwdb->query($zapros." LIMIT 1");
    if(mysqli_num_rows($result_zapros)>0){
        $row_zapros = mysqli_fetch_array($result_zapros);
        return $row_zapros[0];
    }
    else{
        return false;
    }
}
function mwdb_query($zapros,$array_args=array()){
    global $mwdb;
    foreach($array_args as $arg){
        $arg = $mwdb->real_escape_string($arg);
        $zapros = preg_replace('/{var}/',$arg,$zapros,1);
    }
    $result_zapros = $mwdb->query($zapros);
}
function mwdb_id(){
    global $mwdb;
    return $mwdb->insert_id;
}
?>