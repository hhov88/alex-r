<?php
if (!empty($_FILES)) {
    $uploadpath = $_SERVER['DOCUMENT_ROOT']."/images/ads/";      // directory to store the uploaded files
    $max_size = 4000;          // maximum file size, in KiloBytes         // maximum allowed height, in pixels
    $allowtype = array('gif', 'jpg', 'jpeg', 'png');        // allowed extensions

    if(isset($_FILES['file']) && strlen($_FILES['file']['name']) > 1) {
        $img_title = time().'-'.basename( $_FILES['file']['name']);
        $uploadpath1 = $uploadpath .  $img_title;       // gets the file name
        $uploadpath2 = $_SERVER['DOCUMENT_ROOT']."/images/700/" .  $img_title;
        $uploadpath3 = $_SERVER['DOCUMENT_ROOT']."/images/100/" .  $img_title;
        $uploadpath4 = $_SERVER['DOCUMENT_ROOT']."/images/250/" .  $img_title;
        $sepext = explode('.', strtolower($_FILES['file']['name']));
        $type = end($sepext);       // gets extension
        list($width, $height) = getimagesize($_FILES['file']['tmp_name']);     // gets image width and height
        $err = '';         // to store the errors

        // Checks if the file has allowed type, size, width and height (for images)
        if(!in_array($type, $allowtype)) $err .= 'The file: <b>'. $_FILES['file']['name']. '</b> not has the allowed extension type.';
        if($_FILES['file']['size'] > $max_size*1000) $err .= 'Maximum file size must be: '. $max_size. ' KB.';

        // If no errors, upload the image, else, output the errors
        if($err == '') {
            if(move_uploaded_file($_FILES['file']['tmp_name'], $uploadpath1)) {
                echo $img_title;
                require_once('php_image_magician.php');
                $magicianObj = new imageLib($uploadpath1);

                // *** Resize to best fit then crop
                $magicianObj -> resizeImage(800, 600, 2);

                // *** Save resized image as a PNG
                $magicianObj -> saveImage($uploadpath2);
                $magicianObj1 = new imageLib($uploadpath2);
                if($magicianObj1->getOriginalWidth()>$magicianObj1->getOriginalHeight()){
                    $magicianObj1 -> addWatermark('alex-r-watermarkerH533.png', 'tl', 0);
                }
                else{
                    $magicianObj1 -> addWatermark('alex-r-watermarkerH1200.png', 'tl', 0);
                }
                $magicianObj1 -> saveImage($uploadpath2);
                $magicianObj2 = new imageLib($uploadpath1);

                // *** Resize to best fit then crop
                $magicianObj2 -> resizeImage(100, 20, 2);

                // *** Save resized image as a PNG
                $magicianObj2 -> saveImage($uploadpath3);

                $magicianObj3 = new imageLib($uploadpath1);

                // *** Resize to best fit then crop
                $magicianObj3 -> resizeImage(250, 160, 2);

                // *** Save resized image as a PNG
                $magicianObj3 -> saveImage($uploadpath4);

            }
            else echo '<div class="error"> <b>Unable to upload the file.</b></div>';
        }
        else echo "<div class='error'>".$err."</div>";
    }
}
?>