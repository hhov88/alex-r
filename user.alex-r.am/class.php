<?php
class Category{
    public $id;
    public $title;
    public $content;
    public $parent=0;
    public $slug;
    public $order;
    public $keywords;
    public $title_ru;
    public $content_ru;
    public $keywords_ru;
    public $title_en;
    public $content_en;
    public $keywords_en;
    function __construct($id=0,$title='',$content='',$slug='',$order=0,$keywords='',$title_ru='',$content_ru='',$keywords_ru='',$title_en='',$content_en='',$keywords_en='') {
        $this->id = $id;
        $this->title = $title;
        $this->content = $content;
        $this->slug = $slug;
        $this->order = $order;
        $this->keywords = $keywords;
        $this->title_ru = $title_ru;
        $this->content_ru = $content_ru;
        $this->keywords_ru = $keywords_ru;
        $this->title_en = $title_en;
        $this->content_en = $content_en;
        $this->keywords_en = $keywords_en;
    }
    public function insert(){
        if(mwdb_get_var("SELECT category_id FROM category WHERE category_slug='{var}'",array($this->slug))){
            return false;
        }
        else{
            if($this->title!=''){
                mwdb_query("INSERT INTO category (category_title,category_parent,category_slug,category_content,category_order,category_keywords,category_title_ru,category_content_ru,category_keywords_ru,category_title_en,category_content_en,category_keywords_en)
                VALUES ('{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}')",
                    array($this->title,$this->parent,$this->slug,$this->content,$this->order,$this->keywords,$this->title_ru,$this->content_ru,$this->keywords_ru,$this->title_en,$this->content_en,$this->keywords_en));
                return true;
            }
            else{
                return false;
            }
        }
    }
    public function update(){
        mwdb_query("UPDATE category SET category_title='{var}',category_slug='{var}',category_content='{var}',category_order='{var}',category_keywords='{var}',category_title_ru='{var}',category_content_ru='{var}',category_keywords_ru='{var}',category_title_ru='{var}',category_content_ru='{var}',category_keywords_ru='{var}' WHERE category_id={var}",
            array($this->title,$this->slug,$this->content,$this->order,$this->keywords,$this->title_ru,$this->content_ru,$this->keywords_ru,$this->title_en,$this->content_en,$this->keywords_en,$this->id));
    }
    public function delete(){
        mwdb_query("DELETE FROM category WHERE category_id={var}",array($this->id));
    }
    public function get(){
        return mwdb_get_row("SELECT * FROM category WHERE category_id={var}",array($this->id));
    }
}
class Post{
    public $language;
    public $post_id;
    public $post_code;
    public $post_title;
    public $post_content;
    public $post_keyword;
    public $post_title_ru;
    public $post_content_ru;
    public $post_keyword_ru;
    public $post_title_en;
    public $post_content_en;
    public $post_keyword_en;
    public $post_category;
    public $post_region;
    public $post_city;
    public $post_area;
    public $post_microarea;
    public $post_street;
    public $post_address;
    public $post_user;
    public $post_owner;
    public $settings;
    public $post_images;
    public $post_meta;
    public $post_map;
    public $post_status;
    public $post_dop;
	
    function __construct($language='hy',$post_id=0,$post_code='',$post_title='',$post_content='',$post_keyword='',$post_title_ru='',$post_content_ru='',$post_keyword_ru='',$post_title_en='',$post_content_en='',$post_keyword_en='',$post_category=0,$post_region=0,$post_city=0,$post_area=0,$post_microarea=0,$post_street=0,$post_address='',$post_user=0,$post_owner='{}',$settings='{}',$post_images='{}',$post_meta='{}',$post_map='',$post_status=0,$post_dop=''){
        $this->language = $language;
        $this->post_id = $post_id;
        $this->post_code = $post_code;
        $this->post_title = $post_title;
        $this->post_content = $post_content;
        $this->post_keyword = $post_keyword;
        $this->post_title_ru = $post_title_ru;
        $this->post_content_ru = $post_content_ru;
        $this->post_keyword_ru = $post_keyword_ru;
        $this->post_title_en = $post_title_en;
        $this->post_content_en = $post_content_en;
        $this->post_keyword_en = $post_keyword_en;
        $this->post_category = $post_category;
        $this->post_region = $post_region;
        $this->post_city = $post_city;
        $this->post_area = $post_area;
        $this->post_microarea = $post_microarea;
        $this->post_street = $post_street;
        $this->post_address = $post_address;
        $this->post_user = $post_user;
        $this->post_owner = $post_owner;
        $this->settings = $settings;
        $this->post_images = $post_images;
        $this->post_meta = $post_meta;
        $this->post_map = $post_map;
        $this->post_status = $post_status;
        $this->post_dop = $post_dop;
    }
    public function insert(){
        mwdb_query("INSERT INTO posts (post_code,post_title,post_content,post_keywords,post_title_ru,post_content_ru,post_keywords_ru,post_title_en,post_content_en,post_keywords_en,post_category,post_region,post_city,post_area,post_microarea,post_street,post_address,post_user,post_owner,post_map,post_status,post_dop_settings)
        VALUES ('{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}')",
            array(
                $this->post_code,
                $this->post_title,
                $this->post_content,
                $this->post_keyword,
                $this->post_title_ru,
                $this->post_content_ru,
                $this->post_keyword_ru,
                $this->post_title_en,
                $this->post_content_en,
                $this->post_keyword_en,
                $this->post_category,
                $this->post_region,
                $this->post_city,
                $this->post_area,
                $this->post_microarea,
                $this->post_street,
                $this->post_address,
                $this->post_user,
                $this->post_owner,
                $this->post_map,
                $this->post_status,
                $this->post_dop
            ));
        $this->post_id = mwdb_get_var("SELECT post_id FROM posts WHERE {var} ORDER BY post_id DESC",array(1));
        mwdb_query("UPDATE posts SET post_code='{var}' WHERE post_id={var}",array($this->post_code.$this->post_id,$this->post_id));
        $settings = new PostSettings($this->post_id,$this->settings);
        $settings->insert();
        $images = new PostImages($this->post_id,$this->post_images);
        $images->insert();
        $meta = new PostMeta($this->post_id,$this->post_meta);
        $meta->insert();
        return $this->post_id;
    }
    public function get(){
        $row_post = mwdb_get_row("SELECT * FROM posts WHERE post_id={var}",array($this->post_id));
        $this->post_code = $row_post->post_code;
        $this->post_title = $row_post->post_title;
        $this->post_content = $row_post->post_content;
        $this->post_keyword = $row_post->post_keywords;
        $this->post_title_ru = $row_post->post_title_ru;
        $this->post_content_ru = $row_post->post_content_ru;
        $this->post_keyword_ru = $row_post->post_keywords_ru;
        $this->post_title_en = $row_post->post_title_en;
        $this->post_content_en = $row_post->post_content_en;
        $this->post_keyword_en = $row_post->post_keywords_en;
        $this->post_category = $row_post->post_category;
        $this->post_region = $row_post->post_region;
        $this->post_city = $row_post->post_city;
        $this->post_area = $row_post->post_area;
        $this->post_microarea = $row_post->post_microarea;
        $this->post_street = $row_post->post_street;
        $this->post_address = $row_post->post_address;
        $this->post_user = $row_post->post_user;
        $post_owner = new PostOwner($row_post->post_owner);
        $this->post_owner = $post_owner->get();
        $settings = new PostSettings($this->post_id);
        $this->settings = $settings->get();
        $post_images = new PostImages($this->post_id);
        $this->post_images = $post_images->get();
        $post_meta = new PostMeta($this->post_id);
        $this->post_meta = $post_meta->get();
        $this->post_map = $row_post->post_map;
        $this->post_status = $row_post->post_status;
        $this->post_dop = $row_post->post_dop_settings;
		$this->arajarkam = $row_post->post_arajarkam;
		$this->published = $row_post->post_publish;
        return $this;
    }
    public function delete(){
        mwdb_query("UPDATE posts SET post_status='6' WHERE post_id={var}",array($this->post_id));
    }
    public function update(){
        mwdb_query("UPDATE posts SET post_code='{var}',post_title='{var}',post_content='{var}',post_keywords='{var}',post_title_ru='{var}',post_content_ru='{var}',post_keywords_ru='{var}',post_title_en='{var}',post_content_en='{var}',post_keywords_en='{var}',post_category='{var}',post_region='{var}',post_city='{var}',post_area='{var}',post_microarea='{var}',post_street='{var}',post_address='{var}',post_owner='{var}',post_map='{var}',post_status='{var}',post_dop_settings='{var}' WHERE post_id={var}",
            array(
                $this->post_code,
                $this->post_title,
                $this->post_content,
                $this->post_keyword,
                $this->post_title_ru,
                $this->post_content_ru,
                $this->post_keyword_ru,
                $this->post_title_en,
                $this->post_content_en,
                $this->post_keyword_en,
                $this->post_category,
                $this->post_region,
                $this->post_city,
                $this->post_area,
                $this->post_microarea,
                $this->post_street,
                $this->post_address,
                $this->post_owner,
                $this->post_map,
                $this->post_status,
                $this->post_dop,
                $this->post_id
            ));
        $settings = new PostSettings($this->post_id,$this->settings);
        $settings->delete();
        $settings->insert();
        $images = new PostImages($this->post_id,$this->post_images);
        $images->delete();
        $images->insert();
        $meta = new PostMeta($this->post_id,$this->post_meta);
        $meta->delete();
        $meta->insert();
    }
}
class PostMeta{
    public $meta;
    public $post_id;
    function __construct($post_id=0,$meta='{}'){
        $this->post_id = $post_id;
        $this->meta = $meta;
    }
    function insert(){
        $met = json_decode($this->meta);
        foreach($met as $key=>$val){
            mwdb_query("INSERT INTO post_meta (post_id,meta_key,meta_value) VALUES ('{var}','{var}','{var}')",array($this->post_id,$key,$val));
        }
    }
    function get(){
        $result_meta = mwdb_select("SELECT meta_key,meta_value FROM post_meta WHERE post_id={var}",array($this->post_id));
        $array_meta = array();
        foreach($result_meta as $row_meta){
            $array_meta[$row_meta->meta_key] = $row_meta->meta_value;
        }
        return json_encode($array_meta);
    }
    function delete(){
        mwdb_query("DELETE FROM post_meta WHERE post_id={var}",array($this->post_id));
    }
}
class PostImages{
    public $images;
    public $post_id;
    function __construct($post_id=0,$images='{}'){
        $this->post_id = $post_id;
        $this->images = $images;
    }
    function insert(){
        $imgs = json_decode($this->images,true);
        foreach($imgs as $img){
            mwdb_query("INSERT INTO post_images (post_id,image_name,image_order,image_hide) VALUES ('{var}','{var}','{var}','{var}')",array($this->post_id,$img['img'],$img['order'],$img['hide']));
        }
    }
    function get(){
        $result_images = mwdb_select("SELECT image_name,image_order,image_hide FROM post_images WHERE post_id={var} ORDER BY image_order",array($this->post_id));
        $array_img = array();
        $a = 0;
        foreach($result_images as $row_images){
            $array_img[$a] = array("img"=>$row_images->image_name,"order"=>$row_images->image_order,"hide"=>$row_images->image_hide);
            $a++;
        }
        return json_encode($array_img);
    }
    function delete(){
        $images = json_decode($this->get(),true);
        /*foreach($images as $order=>$img){
            if(file_exists($_SERVER['DOCUMENT_ROOT'].'/images/ads/'.$img['img'])){
                unlink($_SERVER['DOCUMENT_ROOT'].'/images/ads/'.$img['img']);
            }
            if(file_exists($_SERVER['DOCUMENT_ROOT'].'/images/100/'.$img['img'])){
                unlink($_SERVER['DOCUMENT_ROOT'].'/images/100/'.$img['img']);
            }
            if(file_exists($_SERVER['DOCUMENT_ROOT'].'/images/700/'.$img['img'])){
                unlink($_SERVER['DOCUMENT_ROOT'].'/images/700/'.$img['img']);
            }
        }*/
        mwdb_query("DELETE FROM post_images WHERE post_id={var}",array($this->post_id));
    }
}
class PostSettings{
    public $post_id;
    public $post_settings;
    function __construct($post_id=0,$post_settings='{}'){
        $this->post_id = $post_id;
        $this->post_settings = $post_settings;
    }
    function insert(){
        $settings = json_decode($this->post_settings,true);
        $cat_slug = mwdb_get_var("SELECT category.category_slug FROM category,posts WHERE category.category_id=posts.post_category AND posts.post_id={var}",array($this->post_id));
        $table = 'settings_'.$cat_slug;
        $if_table = mwdb_get_var("SELECT count(*) FROM information_schema.TABLES WHERE (TABLE_SCHEMA = '{var}') AND (TABLE_NAME = '{var}')",array('infoalexr_master',$table));
        if($if_table>0){
            $zap_part = '';
            foreach($settings as $key=>$val){
                $zap_part .= $key."='".$val."',";
            }
            $zap_part = substr($zap_part,0,-1);
            mwdb_query("INSERT INTO $table (post_id) VALUES ('{var}')",array($this->post_id));
            mwdb_query("UPDATE $table SET $zap_part WHERE post_id={var}",array($this->post_id));
        }
    }
    function get(){
        $cat_slug = mwdb_get_var("SELECT category.category_slug FROM category,posts WHERE category.category_id=posts.post_category AND posts.post_id={var}",array($this->post_id));
        $table = 'settings_'.$cat_slug;
        $if_table = mwdb_get_var("SELECT count(*) FROM information_schema.TABLES WHERE (TABLE_SCHEMA = '{var}') AND (TABLE_NAME = '{var}')",array('infoalexr_master',$table));
        if($if_table>0){
            $array_sett = array();
            $row_settings = mwdb_get_row("SELECT * FROM $table WHERE post_id={var}",array($this->post_id));
            if($row_settings){
            foreach($row_settings as $key=>$val){
                if($key != 'setting_id' && $key != 'post_id'){
                    $array_sett[$key] = $val;
                }
            }
            $this->post_settings = json_encode($array_sett);
            }
            else{
                $this->post_settings = '{}';
            }
        }
        else{
            $this->post_settings = '{}';
        }
        return $this->post_settings;
    }
    function delete(){
        $cat_slug = mwdb_get_var("SELECT category.category_slug FROM category,posts WHERE category.category_id=posts.post_category AND posts.post_id={var}",array($this->post_id));
        $table = 'settings_'.$cat_slug;
        $if_table = mwdb_get_var("SELECT count(*) FROM information_schema.TABLES WHERE (TABLE_SCHEMA = '{var}') AND (TABLE_NAME = '{var}')",array('infoalexr_master',$table));
        if($if_table>0){
            mwdb_query("DELETE FROM $table WHERE post_id={var}",array($this->post_id));
        }
    }
}
class PostOwner{
    public $owner_id;
    public $owner_name;
    public $owner_phone1;
    public $owner_phone2;
    public $owner_phone3;
    public $owner_email;
    public $owner_settings;
    public $owner_skype;
    function __construct($owner_id=0,$owner_name='',$owner_phone1='',$owner_phone2='',$owner_phone3='',$owner_email='',$owner_settings='',$owner_skype=''){
        $this->owner_id = $owner_id;
        $this->owner_name = $owner_name;
        $this->owner_phone1 = $owner_phone1;
        $this->owner_phone2 = $owner_phone2;
        $this->owner_phone3 = $owner_phone3;
        $this->owner_email = $owner_email;
        $this->owner_settings = $owner_settings;
        $this->owner_skype = $owner_skype;
    }
    function insert(){
        $id_exists = mwdb_get_var("SELECT owner_id FROM post_owner WHERE owner_phone1='{var}'",array($this->owner_phone1));
        if($id_exists){
            return $id_exists;
        }
        else{
            mwdb_query("INSERT INTO post_owner (owner_name,owner_phone1,owner_phone2,owner_phone3,owner_email,owner_settings,owner_skype) VALUES ('{var}','{var}','{var}','{var}','{var}','{var}','{var}')",array($this->owner_name,$this->owner_phone1,$this->owner_phone2,$this->owner_phone3,$this->owner_email,$this->owner_settings,$this->owner_skype));
            return mwdb_get_var("SELECT owner_id FROM post_owner WHERE {var} ORDER BY owner_id DESC",array(1));
        }
    }
	function insert2(){
		mwdb_query("INSERT INTO post_owner (owner_name,owner_phone1,owner_phone2,owner_phone3,owner_email,owner_settings,owner_skype) VALUES ('{var}','{var}','{var}','{var}','{var}','{var}','{var}')",array($this->owner_name,$this->owner_phone1,$this->owner_phone2,$this->owner_phone3,$this->owner_email,$this->owner_settings,$this->owner_skype));
		return mwdb_id();
    }
    function update(){
        mwdb_query("UPDATE post_owner SET owner_name='{var}',owner_phone1='{var}',owner_phone2='{var}',owner_phone3='{var}',owner_email='{var}',owner_settings='{var}',owner_skype='{var}' WHERE owner_id={var}",array($this->owner_name,$this->owner_phone1,$this->owner_phone2,$this->owner_phone3,$this->owner_email,$this->owner_settings,$this->owner_skype,$this->owner_id));
    }
    function get(){
        $owner = mwdb_get_row("SELECT * FROM post_owner WHERE owner_id={var}",array($this->owner_id));
        return json_encode($owner);
    }
    function delete(){
        mwdb_query("DELETE FROM post_owner WHERE owner_id={var}",array($this->owner_id));
    }
}
class Client{
    public $client_id;
    public $client_name;
    public $client_phone1;
    public $client_phone2;
    public $client_email;
    public $client_skype;
    public $client_settings;
    public $user_id;
    public $client_status;
    public $client_time;
    public $client_type;
    public $broker;
    function __construct($client_id=0,$client_name='',$client_phone1='',$client_phone2='',$client_email='',$client_skype='',$client_settings='',$user_id=1,$client_type=0,$status=0,$broker=0){
        $this->client_id = $client_id;
        $this->client_name = $client_name;
        $this->client_phone1 = $client_phone1;
        $this->client_phone2 = $client_phone2;
        $this->client_email = $client_email;
        $this->client_skype = $client_skype;
        $this->client_settings = $client_settings;
        $this->user_id = $user_id;
        $this->client_status = $status;
        $this->client_type = $client_type;
        $this->broker = $broker;
    }
    function insert(){
       /* $id_exists = mwdb_get_var("SELECT client_id FROM post_client WHERE (client_phone1='{var}' OR client_email='{var}') AND user_id={var} AND ",array($this->client_phone1,$this->client_email,$this->user_id));
        if($id_exists){
            $this->client_id = $id_exists;
            return $id_exists;
        }
        else{*/
            mwdb_query("INSERT INTO post_client (client_name,client_phone1,client_phone2,client_email,client_skype,client_settings,user_id,client_type,broker) VALUES ('{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}')",array($this->client_name,$this->client_phone1,$this->client_phone2,$this->client_email,$this->client_skype,$this->client_settings,$this->user_id,$this->client_type,$this->broker));
            $this->client_id = mwdb_get_var("SELECT client_id FROM post_client WHERE {var} ORDER BY client_id DESC",array(1));
            return $this->client_id;
        //}
    }
    function update(){
        mwdb_query("UPDATE post_client SET client_name='{var}',client_phone1='{var}',client_phone2='{var}',client_email='{var}',client_skype='{var}',client_settings='{var}',client_status='{var}',client_type='{var}',broker='{var}' WHERE client_id={var}",array($this->client_name,$this->client_phone1,$this->client_phone2,$this->client_email,$this->client_skype,$this->client_settings,$this->client_status,$this->client_type,$this->broker,$this->client_id));
    }
    function get(){
        $client = mwdb_get_row("SELECT * FROM post_client WHERE client_id={var}",array($this->client_id));
        $this->client_id = $client->client_id;
        $this->client_name = $client->client_name;
        $this->client_phone1 = $client->client_phone1;
        $this->client_phone2 = $client->client_phone2;
        $this->client_email = $client->client_email;
        $this->client_skype = $client->client_skype;
        $this->client_settings = $client->client_settings;
        $this->user_id = $client->user_id;
        $this->client_status = $client->client_status;
        $this->client_time = $client->client_time;
        $this->client_type = $client->client_type;
        $this->broker = $client->broker;
    }
    function delete(){
        mwdb_query("UPDATE post_client SET client_status='6' WHERE client_id={var}",array($this->client_id));
    }
}
class Translations{
    public $translation_id;
    public $translation_code;
    public $translation_hy;
    public $translation_ru;
    public $translation_en;
    function __construct($translation_id=0,$translation_code='',$translation_hy='',$translation_ru='',$translation_en=''){
        $this->translation_id = $translation_id;
        $this->translation_code = $translation_code;
        $this->translation_hy = $translation_hy;
        $this->translation_ru = $translation_ru;
        $this->translation_en = $translation_en;
    }
    function check_exists($code,$update=false){
        if($update){
            $qu = " AND translation_id!=".$this->translation_id." ";
        }
        else{
            $qu = " ";
        }
        if(mwdb_get_var("SELECT translation_id FROM translations WHERE translation_code='{var}' $qu",array($code))){
            return false;
        }
        else{
            return true;
        }
    }
    function insert(){
        if($this->check_exists($this->translation_code)){
            mwdb_query("INSERT INTO  translations (translation_code,translation_hy,translation_ru,translation_en) VALUES ('{var}','{var}','{var}','{var}')",
                array($this->translation_code,$this->translation_hy,$this->translation_ru,$this->translation_en));
            $this->translation_id = mwdb_get_var("SELECT translation_id FROM translations WHERE 1 ORDER BY translation_id DESC",array());
            return $this->translation_id;
        }
        else{
            return false;
        }
    }
    function update(){
        if($this->check_exists($this->translation_code,true)){
            mwdb_query("UPDATE translations SET translation_code='{var}',translation_hy='{var}',translation_ru='{var}',translation_en='{var}' WHERE translation_id={var}",
                array($this->translation_code,$this->translation_hy,$this->translation_ru,$this->translation_en,$this->translation_id));
            return true;
        }
        else{
            return false;
        }
    }
    function delete(){
        mwdb_query("DELETE FROM translations WHERE translation_id={var}",array($this->translation_id));
    }
    function get_lang_translations($language){
        $lang_translation = 'translation_'.$language;
        $result_translations = mwdb_select("SELECT $lang_translation,translation_code FROM translations WHERE 1",array());
        $array_translation = array();
        foreach($result_translations as $translation){
            $array_translation[$translation->translation_code] = $translation->$lang_translation;
        }
        return $array_translation;
    }
    function get_translations(){
        return mwdb_select("SELECT * FROM translations WHERE 1",array());
    }
    function get_translation_by_id(){
        return mwdb_get_row("SELECT * FROM translations WHERE translation_id={var}",array($this->translation_id));
    }
}
class Offer{
    public $offer_id;
    public $client_id;
    public $post_id;
    public $offer_status;
    public $offer_text;
    function __construct($client_id=0,$offer_id=0,$post_id=0,$offer_status=0,$offer_text=''){
        $this->offer_id = $offer_id;
        $this->client_id = $client_id;
        $this->post_id = $post_id;
        $this->offer_status = $offer_status;
        $this->offer_text = $offer_text;
    }
    function get($status=''){
        if($status!==''){
            $status = (int)$status;
            $query_part = "offer_status=".$status." AND";
        }
        else{
            $query_part = " ";
        }
        $result_offer = mwdb_select("SELECT * FROM client_offers WHERE $query_part client_id={var}",array($this->client_id));
        return $result_offer;
    }
    function insert(){
        if(mwdb_get_var("SELECT offer_id FROM client_offers WHERE client_id={var} AND post_id={var}",array($this->client_id,$this->post_id))){}else{
            mwdb_query("INSERT INTO client_offers (client_id,post_id,offer_status) VALUES ('{var}','{var}','{var}')",array($this->client_id,$this->post_id,0));
        }
    }
    function delete(){
        mwdb_query("DELETE FROM client_offers WHERE offer_id={var}",array($this->offer_id));
    }
    function get_by_offer_id(){
        $result_offer = mwdb_get_row("SELECT * FROM client_offers WHERE offer_id={var}",array($this->offer_id));
        $this->client_id = $result_offer->client_id;
        $this->post_id = $result_offer->post_id;
        $this->offer_status = $result_offer->offer_status;
        $this->offer_text = $result_offer->offer_text;
    }
    function update_text($text){
        $this->offer_text = $text;
        mwdb_query("UPDATE client_offers SET offer_text='{var}' WHERE offer_id={var}",array($this->offer_text,$this->offer_id));
    }
    function update_status($status){
        $this->offer_status = $status;
        mwdb_query("UPDATE client_offers SET offer_status='{var}' WHERE offer_id={var}",array($this->offer_status,$this->offer_id));
    }
}
class Page{
    public $id;
    public $title;
    public $content;
    public $slug;
    public $keywords;
    public $title_ru;
    public $content_ru;
    public $keywords_ru;
    public $title_en;
    public $content_en;
    public $keywords_en;
    function __construct($id=0,$title='',$content='',$slug='',$keywords='',$title_ru='',$content_ru='',$keywords_ru='',$title_en='',$content_en='',$keywords_en='') {
        $this->id = $id;
        $this->title = $title;
        $this->content = $content;
        $this->slug = $slug;
        $this->keywords = $keywords;
        $this->title_ru = $title_ru;
        $this->content_ru = $content_ru;
        $this->keywords_ru = $keywords_ru;
        $this->title_en = $title_en;
        $this->content_en = $content_en;
        $this->keywords_en = $keywords_en;
    }
    public function insert(){
        if(mwdb_get_var("SELECT page_id FROM page WHERE page_slug='{var}'",array($this->slug))){
            return false;
        }
        else{
            if($this->title!=''){
                mwdb_query("INSERT INTO page (page_title,page_content,page_keywords,page_tutle_ru,page_content_ru,page_keywords_ru,page_tutle_en,page_content_en,page_keywords_en,page_slug)
                VALUES ('{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}')",
                    array($this->title,$this->content,$this->keywords,$this->title_ru,$this->content_ru,$this->keywords_ru,$this->title_en,$this->content_en,$this->keywords_en,$this->slug));
                return true;
            }
            else{
                return false;
            }
        }
    }
    public function update(){
        mwdb_query("UPDATE page SET page_title='{var}',page_content='{var}',page_keywords='{var}',page_tutle_ru='{var}',page_content_ru='{var}',page_keywords_ru='{var}',page_tutle_en='{var}',page_content_en='{var}',page_keywords_en='{var}',page_slug='{var}' WHERE page_id={var}",
            array($this->title,$this->content,$this->keywords,$this->title_ru,$this->content_ru,$this->keywords_ru,$this->title_en,$this->content_en,$this->keywords_en,$this->slug,$this->id));
    }
    public function delete(){
        mwdb_query("DELETE FROM page WHERE page_id={var}",array($this->id));
    }
    public function get(){
        return mwdb_get_row("SELECT * FROM page WHERE page_id={var}",array($this->id));
    }
}
class News{
    public $id;
    public $title;
    public $content;
    public $slug;
    public $keywords;
    public $title_ru;
    public $content_ru;
    public $keywords_ru;
    public $title_en;
    public $content_en;
    public $keywords_en;
    public $img;
    function __construct($id=0,$title='',$content='',$slug='',$keywords='',$title_ru='',$content_ru='',$keywords_ru='',$title_en='',$content_en='',$keywords_en='',$img='') {
        $this->id = $id;
        $this->title = $title;
        $this->content = $content;
        $this->slug = $slug;
        $this->keywords = $keywords;
        $this->title_ru = $title_ru;
        $this->content_ru = $content_ru;
        $this->keywords_ru = $keywords_ru;
        $this->title_en = $title_en;
        $this->content_en = $content_en;
        $this->keywords_en = $keywords_en;
        $this->img = $img;
    }
    public function insert(){
        if(mwdb_get_var("SELECT news_id FROM news WHERE news_slug='{var}'",array($this->slug))){
            return false;
        }
        else{
            if($this->title!=''){
                mwdb_query("INSERT INTO news (news_title,news_content,news_keywords,news_title_ru,news_content_ru,news_keywords_ru,news_title_en,news_content_en,news_keywords_en,news_img,news_slug)
                VALUES ('{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}')",
                    array($this->title,$this->content,$this->keywords,$this->title_ru,$this->content_ru,$this->keywords_ru,$this->title_en,$this->content_en,$this->keywords_en,$this->img,$this->slug));
                return true;
            }
            else{
                return false;
            }
        }
    }
    public function update(){
        mwdb_query("UPDATE news SET news_title='{var}',news_content='{var}',news_keywords='{var}',news_title_ru='{var}',news_content_ru='{var}',news_keywords_ru='{var}',news_title_en='{var}',news_content_en='{var}',news_keywords_en='{var}',news_slug='{var}' WHERE news_id={var}",
            array($this->title,$this->content,$this->keywords,$this->title_ru,$this->content_ru,$this->keywords_ru,$this->title_en,$this->content_en,$this->keywords_en,$this->slug,$this->id));
    }
    public function update_img($new_img){
        mwdb_query("UPDATE news SET news_img='{var}' WHERE news_id={var}",array($new_img,$this->id));
    }
    public function delete(){
        mwdb_query("DELETE FROM news WHERE news_id={var}",array($this->id));
    }
    public function get(){
        return mwdb_get_row("SELECT * FROM news WHERE news_id={var}",array($this->id));
    }
}
class Person{
    public $id;
    public $title;
    public $content;
    public $title_ru;
    public $content_ru;
    public $title_en;
    public $content_en;
    public $img;
    function __construct($id=0,$title='',$content='',$title_ru='',$content_ru='',$title_en='',$content_en='',$img='') {
        $this->id = $id;
        $this->title = $title;
        $this->content = $content;
        $this->title_ru = $title_ru;
        $this->content_ru = $content_ru;
        $this->title_en = $title_en;
        $this->content_en = $content_en;
        $this->img = $img;
    }
    public function insert(){
        if($this->title!=''){
            mwdb_query("INSERT INTO team (person_name,person_text,person_name_ru,person_text_ru,person_name_en,person_text_en,person_img)
            VALUES ('{var}','{var}','{var}','{var}','{var}','{var}','{var}')",
                array($this->title,$this->content,$this->title_ru,$this->content_ru,$this->title_en,$this->content_en,$this->img));
            return true;
        }
        else{
            return false;
        }
    }
    public function update(){
        mwdb_query("UPDATE team SET person_name='{var}',person_text='{var}',person_name_ru='{var}',person_text_ru='{var}',person_name_en='{var}',person_text_en='{var}' WHERE person_id={var}",
            array($this->title,$this->content,$this->title_ru,$this->content_ru,$this->title_en,$this->content_en,$this->id));
    }
    public function update_img($new_img){
        mwdb_query("UPDATE team SET person_img='{var}' WHERE person_id={var}",array($new_img,$this->id));
    }
    public function delete(){
        mwdb_query("DELETE FROM team WHERE person_id={var}",array($this->id));
    }
    public function get(){
        return mwdb_get_row("SELECT * FROM team WHERE person_id={var}",array($this->id));
    }
}
class Location{
    public $hy;
    public $ru;
    public $en;
    public $id;
    function __construct($id=0,$hy='',$ru='',$en=''){
        $this->hy = $hy;
        $this->ru = $ru;
        $this->en = $en;
        $this->id = $id;
    }
}
class Region extends Location{
    function __construct($id=0,$hy='',$ru='',$en=''){
        $this->hy = $hy;
        $this->ru = $ru;
        $this->en = $en;
        $this->id = $id;
    }
    function get(){
        $row = mwdb_get_row("SELECT region_name,region_ru,region_en FROM region WHERE region_id={var}",array($this->id));
        $this->hy = $row->region_name;
        $this->ru = $row->region_ru;
        $this->en = $row->region_en;
    }
    function update(){
        mwdb_query("UPDATE region SET region_name='{var}',region_ru='{var}',region_en='{var}' WHERE region_id={var}",array($this->hy,$this->ru,$this->en,$this->id));
    }
    function insert($order){
        mwdb_query("INSERT INTO region(region_name,region_ru,region_en,region_order) VALUES ('{var}','{var}','{var}','{var}')",array($this->hy,$this->ru,$this->en,$order));
    }
}
class City extends Location{
    public $region;
    function __construct($id=0,$hy='',$ru='',$en='',$region=0){
        $this->hy = $hy;
        $this->ru = $ru;
        $this->en = $en;
        $this->id = $id;
        $this->region = $region;
    }
    function get(){
        $row = mwdb_get_row("SELECT region,city_name,city_ru,city_en FROM city WHERE city_id={var}",array($this->id));
        $this->hy = $row->city_name;
        $this->ru = $row->city_ru;
        $this->en = $row->city_en;
        $this->region = $row->region;
    }
    function update(){
        mwdb_query("UPDATE city SET city_name='{var}',city_ru='{var}',city_en='{var}' WHERE city_id={var}",array($this->hy,$this->ru,$this->en,$this->id));
    }
    function insert($order){
        mwdb_query("INSERT INTO city(city_name,city_ru,city_en,region,city_order) VALUES ('{var}','{var}','{var}','{var}','{var}')",array($this->hy,$this->ru,$this->en,$this->region,$order));
    }
}
class Area extends Location{
    public $city;
    function __construct($id=0,$hy='',$ru='',$en='',$city=0){
        $this->hy = $hy;
        $this->ru = $ru;
        $this->en = $en;
        $this->id = $id;
        $this->city = $city;
    }
    function get(){
        $row = mwdb_get_row("SELECT city,area_name,area_ru,area_en FROM area WHERE area_id={var}",array($this->id));
        if($row){
            $this->hy = $row->area_name;
            $this->ru = $row->area_ru;
            $this->en = $row->area_en;
            $this->city = $row->city;
        }
    }
    function update(){
        mwdb_query("UPDATE area SET area_name='{var}',area_ru='{var}',area_en='{var}' WHERE area_id={var}",array($this->hy,$this->ru,$this->en,$this->id));
    }
    function insert($order){
        mwdb_query("INSERT INTO area(area_name,area_ru,area_en,city,area_order) VALUES ('{var}','{var}','{var}','{var}','{var}')",array($this->hy,$this->ru,$this->en,$this->city,$order));
    }
}
class Microarea extends Location{
    public $area;
    function __construct($id=0,$hy='',$ru='',$en='',$area=0){
        $this->hy = $hy;
        $this->ru = $ru;
        $this->en = $en;
        $this->id = $id;
        $this->area = $area;
    }
    function get(){
        $row = mwdb_get_row("SELECT area,area_name,area_ru,area_en FROM microarea WHERE area_id={var}",array($this->id));
        if($row){
        $this->hy = $row->area_name;
        $this->ru = $row->area_ru;
        $this->en = $row->area_en;
        $this->area = $row->area;
        }
    }
    function update(){
        mwdb_query("UPDATE microarea SET area_name='{var}',area_ru='{var}',area_en='{var}' WHERE area_id={var}",array($this->hy,$this->ru,$this->en,$this->id));
    }
    function insert($order){
        mwdb_query("INSERT INTO microarea(area_name,area_ru,area_en,area,area_order) VALUES ('{var}','{var}','{var}','{var}','{var}')",array($this->hy,$this->ru,$this->en,$this->area,$order));
    }
}
class Street extends Location{
    public $region;
    public $city;
    public $area;
    public $microarea;
    function __construct($id=0,$hy='',$ru='',$en='',$region=0,$city=0,$area=0,$microarea=0){
        $this->hy = $hy;
        $this->ru = $ru;
        $this->en = $en;
        $this->id = $id;
        $this->region = $region;
        $this->city = $city;
        $this->area = $area;
        $this->microarea = $microarea;
    }
    function get(){
        $row = mwdb_get_row("SELECT * FROM streets WHERE street_id={var}",array($this->id));
        if($row){
        $this->hy = $row->street_hy;
        $this->ru = $row->street_ru;
        $this->en = $row->street_en;
        $this->region = $row->region;
        $this->city = $row->city;
        $this->area = $row->area;
        $this->microarea = $row->microarea;
        }
    }
    function update(){
        mwdb_query("UPDATE streets SET street_hy='{var}',street_ru='{var}',street_en='{var}', region='{var}',city='{var}',area='{var}',microarea='{var}' WHERE street_id={var}",array($this->hy,$this->ru,$this->en,$this->region,$this->city,$this->area,$this->microarea,$this->id));
    }
    function delete(){
        mwdb_query("DELETE FROM streets WHERE street_id={var}",array($this->id));
    }
    function insert(){
        mwdb_query("INSERT INTO streets(street_hy,street_ru,street_en,country,region,city,area,microarea) VALUES ('{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}')",array($this->hy,$this->ru,$this->en,5,$this->region,$this->city,$this->area,$this->microarea));
    }
}
class Branch{
    public $id;
    public $name_hy;
    public $name_ru;
    public $name_en;
    public $address_hy;
    public $address_ru;
    public $address_en;
    public $map;
    public $phone1;
    public $phone2;
    public $email;
    public $skype;
    function __construct($id=0,$hy='',$ru='',$en='',$address_hy='',$address_ru='',$address_en='',$map='',$phone1='{}',$phone2='{}',$email='',$skype=''){
        $this->id = $id;
        $this->name_hy = $hy;
        $this->name_ru = $ru;
        $this->name_en = $en;
        $this->address_hy = $address_hy;
        $this->address_ru = $address_ru;
        $this->address_en = $address_en;
        $this->map = $map;
        $this->phone1 = $phone1;
        $this->phone2 = $phone2;
        $this->email = $email;
        $this->skype = $skype;
    }
    function insert(){
        mwdb_query("INSERT INTO branch (branch_name_hy,branch_name_ru,branch_name_en,branch_address_hy,branch_address_ru,branch_address_en,branch_map,branch_phone1,branch_phone2,branch_email,branch_skype) VALUES ('{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}','{var}')",
            array($this->name_hy,$this->name_ru,$this->name_en,$this->address_hy,$this->address_ru,$this->address_en,$this->map,$this->phone1,$this->phone2,$this->email,$this->skype));
    }
}
?>