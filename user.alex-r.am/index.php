<?php
require( "config.php" );
session_start();
$action = isset( $_GET['action'] ) ? $_GET['action'] : "";
$username = isset( $_SESSION['username'] ) ? $_SESSION['username'] : "";
if($username!=''){
    $now = date("Y-m-d H:i:s");
	$ip = $_SERVER['REMOTE_ADDR'];
    mwdb_query("UPDATE users SET user_last_login='{var}',user_login_ip='{var}' WHERE user_name='{var}'",array($now,$ip,$username));
}
$user_level = isset( $_SESSION['userlevel'] ) ? $_SESSION['userlevel'] : "";
$globval = array();
if ($action != "login" && $action != "logout" && !$username) {
    login();
    exit;
}
switch ( $action ) {
    case 'login':
        login();
        break;
    case 'logout':
        logout();
        break;
    case 'operators':
        operators();
        break;
    case 'brokers':
        brokers();
        break;
    case 'category':
        category();
        break;
    case 'post':{
        post();
        break;
    }
    case 'buyer':{
        buyer();
        break;
    }
    case 'translations':{
        translations();
        break;
    }
    case 'page':{
        page();
        break;
    }
    case 'news':{
        news();
        break;
    }
    case 'team':{
        team();
        break;
    }
    case 'locations':{
        locations();
        break;
    }
    case 'branch':{
        branch();
        break;
    }
    case 'edit_branch':{
        edit_branch();
        break;
    }
	case 'excel':{
		excel();
        break;
	}
    default:
        admin_index();
        break;
}

function login(){
    global $user_level;
    $globval['title'] = 'Login';
    if(isset($_COOKIE['PHPSESSID'])){
        $login_sess = $_COOKIE['PHPSESSID'];
    }
    if ( isset( $_POST['login'] ) ) {
	//echo "SELECT user_id,user_name,user_password,user_type FROM users WHERE user_name='{var}' AND user_password='{var}' AND user_type!=3 AND user_status=1";
        $admin = mwdb_get_row("SELECT user_id,user_name,user_password,user_type FROM users WHERE user_name='{var}' AND user_password='{var}' AND user_type!=3 AND user_status=1",array($_POST['username'], md5($_POST['password'])));
        if($admin){
            $_SESSION['user_id'] = $admin->user_id;
            define( "ADMIN_USERNAME", $admin->user_name );
            define( "ADMIN_PASSWORD", $admin->user_password );
            $_SESSION['userlevel'] = $admin->user_type;
            if ( $_POST['username'] == ADMIN_USERNAME && md5($_POST['password']) == ADMIN_PASSWORD ) {
                $_SESSION['username'] = ADMIN_USERNAME;
                header( "Location: index.php?action=loged&ssid=".$login_sess );
            } else {
                $results['errorMessage'] = "Incorrect username or password. Please try again.";
                require(  TEMPLATE_PATH."/loginForm.php" );
            }
        }
        else{
            require(TEMPLATE_PATH."/loginForm.php" );
        }

    } else {
        require(TEMPLATE_PATH."/loginForm.php" );
    }
}
function logout(){
    $id_ssses = $_COOKIE['PHPSESSID'];
    global $mwadb;
    unset( $_SESSION['username'] );
    session_regenerate_id();
    if (isset($_COOKIE['PHPSESSID'])){
        setcookie("PHPSESSID","",time()-7200);
    }
    session_unset();
    session_destroy();
    header( "Location: index.php" );
}
function admin_index(){
    global $mwadb;
    $globval['title'] = 'Home';
    require( TEMPLATE_PATH."/home.php" );
}
function operators(){
    global $mwadb;
    $globval['title'] = 'Operators';
    require( TEMPLATE_PATH."/operators.php" );
}
function brokers(){
    global $mwadb;
    $globval['title'] = 'Brokers';
    require( TEMPLATE_PATH."/brokers.php" );
}
function category(){
    global $mwadb;
    $globval['title'] = 'Category';
    require( TEMPLATE_PATH."/category.php" );
}
function post(){
    global $mwadb;
    $globval['title'] = 'Post';
    require( TEMPLATE_PATH."/post.php" );
}
function buyer(){
    global $mwadb;
    global $user_level;
    $globval['title'] = 'Buyers';
    require( TEMPLATE_PATH."/buyer.php" );
}
function translations(){
    global $mwadb;
    $globval['title'] = 'Translations';
    require( TEMPLATE_PATH."/translations.php" );
}
function page(){
    global $mwadb;
    $globval['title'] = 'Pages';
    require( TEMPLATE_PATH."/page.php" );
}
function news(){
    global $mwadb;
    $globval['title'] = 'News';
    require( TEMPLATE_PATH."/news.php" );
}
function team(){
    global $mwadb;
    $globval['title'] = 'Team';
    require( TEMPLATE_PATH."/team.php" );
}
function locations(){
    global $mwadb;
    $globval['title'] = 'Locations';
    require( TEMPLATE_PATH."/location.php" );
}
function branch(){
    global $mwadb;
    $globval['title'] = 'Branch';
    require( TEMPLATE_PATH."/branch.php" );
}
function edit_branch(){
    global $mwadb;
    $globval['title'] = 'Edit Branch';
    require( TEMPLATE_PATH."/edit_branch.php" );
}
function excel(){
	global $mwadb;
    $globval['title'] = 'Excel';
    require( TEMPLATE_PATH."/excel.php" );
}
?>