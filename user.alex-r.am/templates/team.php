<?php
require( TEMPLATE_PATH."/header.php" );
?>
<section class="content-header">
    <h1>Наша команда</h1>
</section>
<section class="content">
<?php
if(isset($_POST['backend_ad_user']) && $_POST['backend_ad_user']=='Добавить'){
    $category_title = (string)$_POST['category_title'];
    $category_content = (string)$_POST['category_content'];
    $category_title_ru = (string)$_POST['category_title_ru'];
    $category_content_ru = (string)$_POST['category_content_ru'];
    $category_title_en = (string)$_POST['category_title_en'];
    $category_content_en = (string)$_POST['category_content_en'];
    $uploadpath = $_SERVER['DOCUMENT_ROOT'] ."/images/team/";
    $max_size = 2000;
    $allowtype = array('gif', 'jpg', 'jpeg', 'png');
    if(isset($_FILES['image_r']) && strlen($_FILES['image_r']['name']) > 1) {
        $img_title_r = time().'-'.basename( $_FILES['image_r']['name']);
        $uploadpath = $uploadpath .  $img_title_r;
        $sepext = explode('.', strtolower($_FILES['image_r']['name']));
        $type = end($sepext);
        list($width, $height) = getimagesize($_FILES['image_r']['tmp_name']);
        $err = '';
        if(!in_array($type, $allowtype)) $err .= 'The file: <b>'. $_FILES['image_r']['name']. '</b> not has the allowed extension type.';
        if($_FILES['image_r']['size'] > $max_size*1000) $err .= 'Maximum file size must be: '. $max_size. ' KB.';
        if($err == '') {
            if(move_uploaded_file($_FILES['image_r']['tmp_name'], $uploadpath)) {
            }
            else echo '<div class="error"> <b>Unable to upload the file.</b></div>';
        }
        else echo "<div class='error'>".$err."</div>";
    }
    else{
        $img_title_r = '';
    }
    $ins = new Person(0,$category_title,$category_content,$category_title_ru,$category_content_ru,$category_title_en,$category_content_en,$img_title_r);
    $ins->insert();
}
if(isset($_POST['backend_ad_user']) && $_POST['backend_ad_user']=='Сохранить'){
    $id = (int)$_POST['category_ed_id'];
    $category_title = (string)$_POST['category_title'];
    $category_content = (string)$_POST['category_content'];
    $category_title_ru = (string)$_POST['category_title_ru'];
    $category_content_ru = (string)$_POST['category_content_ru'];
    $category_title_en = (string)$_POST['category_title_en'];
    $category_content_en = (string)$_POST['category_content_en'];
    $update = new Person($id,$category_title,$category_content,$category_title_ru,$category_content_ru,$category_title_en,$category_content_en);
    $uploadpath = $_SERVER['DOCUMENT_ROOT'] ."/images/team/";
    $max_size = 2000;
    $allowtype = array('gif', 'jpg', 'jpeg', 'png');
    if(isset($_FILES['image_r']) && strlen($_FILES['image_r']['name']) > 1) {
        $img_title_r = time().'-'.basename( $_FILES['image_r']['name']);
        $uploadpath = $uploadpath .  $img_title_r;
        $sepext = explode('.', strtolower($_FILES['image_r']['name']));
        $type = end($sepext);
        list($width, $height) = getimagesize($_FILES['image_r']['tmp_name']);
        $err = '';
        if(!in_array($type, $allowtype)) $err .= 'The file: <b>'. $_FILES['image_r']['name']. '</b> not has the allowed extension type.';
        if($_FILES['image_r']['size'] > $max_size*1000) $err .= 'Maximum file size must be: '. $max_size. ' KB.';
        if($err == '') {
            if(move_uploaded_file($_FILES['image_r']['tmp_name'], $uploadpath)) {
                $update->update_img($img_title_r);
            }
            else echo '<div class="error"> <b>Unable to upload the file.</b></div>';
        }
        else echo "<div class='error'>".$err."</div>";
    }
    else{
        $img_title_r = '';
    }
    $update->update();
}
if(isset($_GET['subaction']) && $_GET['subaction']=='delete'){
    $id = (int)$_GET['id'];
    $edit_obj = new Person($id);
    $edit = $edit_obj->delete();
}
if(isset($_GET['subaction']) && $_GET['subaction']=='edit'){
    $id = (int)$_GET['id'];
    $edit_obj = new Person($id);
    $edit = $edit_obj->get();
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Редактировать</h3>
                    <div class="pull-right box-tools">
                        <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Скрыть" style="margin-right: 5px;"><i class="fa fa-minus"></i></button>
                    </div><!-- /. tools -->
                </div>
                <div class="box-body">
                    <form action="" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="category_ed_id" value="<?php echo $edit->person_id; ?>">
                        <table class="table table-hover table-bordered" style="text-align:center">
                            <tr>
                                <th>На армянском</th>
                                <th>На русском</th>
                                <th>На английском</th>
                            </tr>
                            <tr>
                                <td><input type="text" name="category_title" placeholder="Имя" class="form-control" value="<?php echo $edit->person_name ?>"></td>
                                <td><input type="text" name="category_title_ru" placeholder="Имя" class="form-control" value="<?php echo $edit->person_name_ru ?>"></td>
                                <td><input type="text" name="category_title_en" placeholder="Имя" class="form-control" value="<?php echo $edit->person_name_en ?>"></td>
                            </tr>
                            <tr>
                                <td><textarea name="category_content" placeholder="Текст" class="form-control"><?php echo $edit->person_text ?></textarea></td>
                                <td><textarea name="category_content_ru" placeholder="Текст" class="form-control"><?php echo $edit->person_text_ru ?></textarea></td>
                                <td><textarea name="category_content_en" placeholder="Текст" class="form-control"><?php echo $edit->person_text_en ?></textarea></td>
                            </tr>
                        </table>
                        <div class="col-md-12" style="margin-bottom: 10px">
                            <label style="margin-right: 30px">Image:</label><input type="file" name="image_r" class="image" onchange="vibr(this)" style="display: inline-block" />
                            <img id="uploadPreview" src="<?php if($edit->person_img!=''){echo 'https://user.alex-r.am/images/team/'.$edit->person_img;} ?>" style="width: 100px;background: #fff;" /></br>
                        </div>
                        <input type="submit" name="backend_ad_user" value="Сохранить" class="backend_ad_user btn btn-primary">
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php
}
else{
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Добавить</h3>
                    <div class="pull-right box-tools">
                        <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Скрыть" style="margin-right: 5px;"><i class="fa fa-minus"></i></button>
                    </div><!-- /. tools -->
                </div>
                <div class="box-body">
                    <form action="" method="post" enctype="multipart/form-data">
                        <table class="table table-hover table-bordered" style="text-align:center">
                            <tr>
                                <th>На армянском</th>
                                <th>На русском</th>
                                <th>На английском</th>
                            </tr>
                            <tr>
                                <td><input type="text" name="category_title" placeholder="Имя" class="form-control"></td>
                                <td><input type="text" name="category_title_ru" placeholder="Имя" class="form-control"></td>
                                <td><input type="text" name="category_title_en" placeholder="Имя" class="form-control"></td>
                            </tr>
                            <tr>
                                <td><textarea name="category_content" placeholder="Текст" class="form-control"></textarea></td>
                                <td><textarea name="category_content_ru" placeholder="Текст" class="form-control"></textarea></td>
                                <td><textarea name="category_content_en" placeholder="Текст" class="form-control"></textarea></td>
                            </tr>
                        </table>
                        <div class="col-md-12" style="margin-bottom: 10px">
                            <label style="margin-right: 30px">Image:</label><input type="file" name="image_r" class="image" onchange="vibr(this)" style="display: inline-block" />
                            <img id="uploadPreview" src="" style="width: 100px;background: #fff;" /></br>
                        </div>
                        <input type="submit" name="backend_ad_user" value="Добавить" class="backend_ad_user btn btn-primary">
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<style>th{text-align: center !important;}</style>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Все</h3>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover table-bordered" style="text-align:center">
                    <tr>
                        <th>Имя на армянском</th>
                        <th>Имя на русском</th>
                        <th>Имя на английском</th>
                        <th>Картинка</th>
                        <th>действие</th>
                    </tr>
                    <?php
                    if(isset($_GET['page']) && $_GET['page']>1){
                        $page = (int)$_GET['page'];
                    }
                    else{
                        $page = 1;
                    }
                    $offset = ($page-1)*20;
                    $users = mwdb_select("SELECT * FROM team WHERE {var}",array(1));
                    if($users){
                        foreach($users as $user){
                            ?>
                            <tr>
                                <td><?php echo $user->person_name; ?></td>
                                <td><?php echo $user->person_name_ru; ?></td>
                                <td><?php echo $user->person_name_en; ?></td>
                                <td><img src="<?php if($user->person_img!=''){echo 'https://user.alex-r.am/images/team/'.$user->person_img;} ?>" style="height: 80px;background: #fff;" /></td>
                                <td>
                                    <div class="btn-group">
                                        <a href="https://user.alex-r.am/index.php?action=team&subaction=edit&id=<?php echo $user->person_id; ?>" class="btn btn-default"><i class="fa fa-edit"></i>Изменить</a>
                                        <a onclick="return confirm('Вы уверены?')" href="https://user.alex-r.am/index.php?action=team&subaction=delete&id=<?php echo $user->person_id; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i> Удалить</a>
                                    </div>
                                </td>
                            </tr>
                        <?php
                        }
                    }
                    ?>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>
</section>
<?php
require( TEMPLATE_PATH."/footer.php" );
?>
<script type="text/javascript">
    function readURL(input,img) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                img.attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    function vibr(elm){
        var img = jQuery(elm).parent().find("#uploadPreview");
        readURL(elm,img);
    }
</script>