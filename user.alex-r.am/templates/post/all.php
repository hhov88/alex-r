<?php
if(isset($_POST['is_submit']) && $_POST['is_submit']=='1'){
	$_SESSION['post'] = $_POST;
}
else{
	if(isset($_GET['s_page']) && (int)$_GET['s_page']>0){}else{
		unset($_SESSION['post']);
	}
}
?>
<section class="content-header">
    <h1>Все недвижимости (<span class="count_posts_all_in" id="myspan"></span>)</h1>
    <div class="serach_block">
        <form name="serach" class="search_form" action="" method="get">
            <input type="search" name="word" class="word">
            <input type="hidden" name="action" value="post">
            <input type="hidden" name="subaction" value="search">
            <select name="type" class="search_type">
                <option value="id" selected>id</option>
                <option value="phone">phone/name</option>
				<option value="arajark">Arajark.am ID</option>
            </select>
            <input type="submit" name="s" value="search">
        </form>
    </div>
    <div style="float:right">
        <a href="https://user.alex-r.am/index.php?action=post&subaction=all">
            <span style="background: #fff;display: inline-block;width: 20px;height: 20px"></span>Все
        </a>
        <a href="https://user.alex-r.am/index.php?action=post&subaction=all&status=2">
            <span style="background: rgba(134, 199, 10, 0.45);display: inline-block;width: 20px;height: 20px;margin-left: 40px"></span>TOP
        </a>
        <a href="https://user.alex-r.am/index.php?action=post&subaction=all&&status=1">
            <span style="background: rgb(222, 212, 139);display: inline-block;width: 20px;height: 20px;margin-left: 40px"></span>Первичный
        </a>
		 <a href="https://user.alex-r.am/index.php?action=post&subaction=all&&published=0">
            <span style="background: rgba(255, 0, 0, 0.28);display: inline-block;width: 20px;height: 20px;margin-left: 40px"></span>Неопубликованные
        </a>
		<a href="https://user.alex-r.am/index.php?action=post&subaction=all&&published=1">
            <span style="background: #fff;display: inline-block;width: 20px;height: 20px;margin-left: 40px"></span>Опубликованные
        </a>
    </div>
    <div class="col-md-12" style="background: #fff;padding:10px 20px;position: relative">
        <form action="" method="post">
            <div class="col-md-12" style="font-weight: bold;border-bottom: 1px solid #eee">Филтр</div>
            <div class="col-md-2">
                <?php
                $result_categories = mwdb_select("SELECT category_id,category_title_ru FROM category WHERE 1 ORDER BY category_order",array(1));
                foreach($result_categories as $row_category){
                    ?>
                    <div class="col-md-12" style="margin: 0">
                        <input type="radio" <?php if(isset($_SESSION['post']['category']) && $_SESSION['post']['category']==$row_category->category_id){echo 'checked';} ?> name="category" onclick="find_category_filter_settings(this)" value="<?php echo $row_category->category_id ?>"><span><?php echo $row_category->category_title_ru; ?></span>
                    </div> 
                    <?php
                }
                ?>
            </div>
            <div class="col-md-5 for_all_filter_settings">
                <?php
                if(isset($_SESSION['post']['category'])){
                    switch($_SESSION['post']['category']){
                        case '5':{
                            ?>
                            <div class="col-md-12">
                                <div class="col-md-2">Комнаты</div>
                                <div class="col-md-9">
                                    <input type="checkbox" name="rooms[]" <?php if(in_array('1',$_SESSION['post']['rooms'])){echo 'checked';} ?> value="1">1
                                    <input type="checkbox" name="rooms[]" <?php if(in_array('2',$_SESSION['post']['rooms'])){echo 'checked';} ?> value="2" style="margin-left: 9px">2
                                    <input type="checkbox" name="rooms[]" <?php if(in_array('3',$_SESSION['post']['rooms'])){echo 'checked';} ?> value="3" style="margin-left: 9px">3
                                    <input type="checkbox" name="rooms[]" <?php if(in_array('4',$_SESSION['post']['rooms'])){echo 'checked';} ?> value="4" style="margin-left: 9px">4
                                    <input type="checkbox" name="rooms[]" <?php if(in_array('5',$_SESSION['post']['rooms'])){echo 'checked';} ?> value="5" style="margin-left: 9px">5
									<input type="checkbox" name="rooms[]" <?php if(in_array('6',$_SESSION['post']['rooms'])){echo 'checked';} ?> value="6" style="margin-left: 9px">6
									<input type="checkbox" name="rooms[]" <?php if(in_array('7',$_SESSION['post']['rooms'])){echo 'checked';} ?> value="7" style="margin-left: 9px">7
									<input type="checkbox" name="rooms[]" <?php if(in_array('8',$_SESSION['post']['rooms'])){echo 'checked';} ?> value="8" style="margin-left: 9px">8
									<input type="checkbox" name="rooms[]" <?php if(in_array('9',$_SESSION['post']['rooms'])){echo 'checked';} ?> value="9" style="margin-left: 9px">9
									<input type="checkbox" name="rooms[]" <?php if(in_array('10',$_SESSION['post']['rooms'])){echo 'checked';} ?> value="10" style="margin-left: 9px">10+
                                </div>
                            </div>
							<div class="col-md-6">
                            <div class="col-md-12">
                                <div class="col-md-2" style="padding-top: 4px;">Этаж</div>
                                <div class="col-md-9">
                                    <div class="col-md-4">
                                        <select name="floor_from" class="form-control" style="height: 20px">
                                            <option value="">От</option>
                                            <?php
                                            for($i=1;$i<21;$i++){
                                                ?>
                                                <option <?php if(isset($_SESSION['post']['floor_from']) && $_SESSION['post']['floor_from']==$i){echo 'selected';} ?> value="<?php echo $i ?>"><?php echo $i ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-1" style="text-align: center">_</div>
                                    <div class="col-md-4">
                                        <select name="floor_to" class="form-control" style="height: 20px">
                                            <option value="">До</option>
                                            <?php
                                            for($i=1;$i<21;$i++){
                                                ?>
                                                <option <?php if(isset($_SESSION['post']['floor_to']) && $_SESSION['post']['floor_to']==$i){echo 'selected';} ?> value="<?php echo $i ?>"><?php echo $i ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2" style="padding-top: 4px;">Площадь</div>
                                <div class="col-md-9">
                                    <div class="col-md-4">
                                        <input type="text" name="area_from" class="form-control" style="height: 20px" <?php if(isset($_SESSION['post']['area_from'])){echo 'value="'.$_SESSION['post']['area_from'].'"';} ?>>
                                    </div>
                                    <div class="col-md-1" style="text-align: center">_</div>
                                    <div class="col-md-4">
                                        <input type="text" name="area_to" class="form-control" style="height: 20px" <?php if(isset($_SESSION['post']['area_to'])){echo 'value="'.$_SESSION['post']['area_to'].'"';} ?>>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2" style="padding-top: 4px;">Цена</div>
                                <div class="col-md-9">
                                    <div class="col-md-4">
                                        <input type="text" name="price_from" class="form-control" style="height: 20px" placeholder="USD" <?php if(isset($_SESSION['post']['price_from'])){echo 'value="'.$_SESSION['post']['price_from'].'"';} ?>>
                                    </div>
                                    <div class="col-md-1" style="text-align: center">_</div>
                                    <div class="col-md-4">
                                        <input type="text" name="price_to" class="form-control" style="height: 20px" placeholder="USD" <?php if(isset($_SESSION['post']['price_to'])){echo 'value="'.$_SESSION['post']['price_to'].'"';} ?>>
                                    </div>
                                </div>
                            </div>
							</div>
							<div class="col-md-6">
								<div class="col-md-12"><input type="checkbox" name="building_type[]" value="building_type1" <?php if(isset($_SESSION['post']['building_type']) && in_array('building_type1',$_SESSION['post']['building_type'])){echo 'checked';} ?>> монолитный</div>
								<div class="col-md-12"><input type="checkbox" name="building_type[]" value="building_type2" <?php if(isset($_SESSION['post']['building_type']) && in_array('building_type2',$_SESSION['post']['building_type'])){echo 'checked';} ?>> каменный</div>
								<div class="col-md-12"><input type="checkbox" name="building_type[]" value="building_type3" <?php if(isset($_SESSION['post']['building_type']) && in_array('building_type3',$_SESSION['post']['building_type'])){echo 'checked';} ?>> крупнопанельный</div>
								<div class="col-md-12"><input type="checkbox" name="building_type[]" value="building_type4" <?php if(isset($_SESSION['post']['building_type']) && in_array('building_type4',$_SESSION['post']['building_type'])){echo 'checked';} ?>> каркаснопанельный</div>
							</div>
                            <?php
                            break;
                        }
                        case '6':{
                            ?>
                            <div class="col-md-12">
                                <div class="col-md-2">Комнаты</div>
                                <div class="col-md-9">
                                    <input type="checkbox" <?php if(in_array('1',$_SESSION['post']['rooms'])){echo 'checked';} ?> name="rooms[]" value="1">1
                                    <input type="checkbox" <?php if(in_array('2',$_SESSION['post']['rooms'])){echo 'checked';} ?> name="rooms[]" value="2" style="margin-left: 9px">2
                                    <input type="checkbox" <?php if(in_array('3',$_SESSION['post']['rooms'])){echo 'checked';} ?> name="rooms[]" value="3" style="margin-left: 9px">3
                                    <input type="checkbox" <?php if(in_array('4',$_SESSION['post']['rooms'])){echo 'checked';} ?> name="rooms[]" value="4" style="margin-left: 9px">4
                                    <input type="checkbox" name="rooms[]" <?php if(in_array('5',$_SESSION['post']['rooms'])){echo 'checked';} ?> value="5" style="margin-left: 9px">5
									<input type="checkbox" name="rooms[]" <?php if(in_array('6',$_SESSION['post']['rooms'])){echo 'checked';} ?> value="6" style="margin-left: 9px">6
									<input type="checkbox" name="rooms[]" <?php if(in_array('7',$_SESSION['post']['rooms'])){echo 'checked';} ?> value="7" style="margin-left: 9px">7
									<input type="checkbox" name="rooms[]" <?php if(in_array('8',$_SESSION['post']['rooms'])){echo 'checked';} ?> value="8" style="margin-left: 9px">8
									<input type="checkbox" name="rooms[]" <?php if(in_array('9',$_SESSION['post']['rooms'])){echo 'checked';} ?> value="9" style="margin-left: 9px">9
									<input type="checkbox" name="rooms[]" <?php if(in_array('10',$_SESSION['post']['rooms'])){echo 'checked';} ?> value="10" style="margin-left: 9px">10+
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2" style="padding-top: 4px;">Этажность</div>
                                <div class="col-md-9">
                                    <div class="col-md-2">
                                        <select name="floor_from" class="form-control" style="height: 20px">
                                            <option value="">От</option>
                                            <?php
                                            for($i=1;$i<6;$i++){
                                                ?>
                                                <option <?php if(isset($_SESSION['post']['floor_from']) && $_SESSION['post']['floor_from']==$i){echo 'selected';} ?> value="<?php echo $i ?>"><?php echo $i ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-1" style="text-align: center">_</div>
                                    <div class="col-md-2">
                                        <select name="floor_to" class="form-control" style="height: 20px">
                                            <option value="">До</option>
                                            <?php
                                            for($i=1;$i<6;$i++){
                                                ?>
                                                <option <?php if(isset($_SESSION['post']['floor_to']) && $_SESSION['post']['floor_to']==$i){echo 'selected';} ?> value="<?php echo $i ?>"><?php echo $i ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2" style="padding-top: 4px;">Площадь</div>
                                <div class="col-md-9">
                                    <div class="col-md-2">
                                        <input type="text" name="area_from" class="form-control" style="height: 20px" <?php if(isset($_SESSION['post']['area_from'])){echo 'value="'.$_SESSION['post']['area_from'].'"';} ?>>
                                    </div>
                                    <div class="col-md-1" style="text-align: center">_</div>
                                    <div class="col-md-2">
                                        <input type="text" name="area_to" class="form-control" style="height: 20px" <?php if(isset($_SESSION['post']['area_to'])){echo 'value="'.$_SESSION['post']['area_to'].'"';} ?>>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2" style="padding-top: 4px;">Цена</div>
                                <div class="col-md-9">
                                    <div class="col-md-2">
                                        <input type="text" name="price_from" class="form-control" style="height: 20px" placeholder="USD" <?php if(isset($_SESSION['post']['price_from'])){echo 'value="'.$_SESSION['post']['price_from'].'"';} ?>>
                                    </div>
                                    <div class="col-md-1" style="text-align: center">_</div>
                                    <div class="col-md-2">
                                        <input type="text" name="price_to" class="form-control" style="height: 20px" placeholder="USD" <?php if(isset($_SESSION['post']['price_to'])){echo 'value="'.$_SESSION['post']['price_to'].'"';} ?>>
                                    </div>
                                </div>
                            </div>
                            <?php
                            break;
                        }
                        case '7':{
                            ?>
                            <div class="col-md-12">
                                <div class="col-md-2" style="padding-top: 4px;">Тип</div>
                                <div class="col-md-9">
                                    <div class="col-md-4">
                                        <select name="type" class="form-control" style="height: 20px">
                                            <option value="">-- Выбрать --</option>
                                            <option <?php if(isset($_SESSION['post']['type']) && $_SESSION['post']['type']=='cm_type2'){echo 'selected';} ?> value="cm_type2">гостиница</option>
                                            <option <?php if(isset($_SESSION['post']['type']) && $_SESSION['post']['type']=='cm_type1'){echo 'selected';} ?> value="cm_type1">оффис</option>
                                            <option <?php if(isset($_SESSION['post']['type']) && $_SESSION['post']['type']=='cm_type3'){echo 'selected';} ?> value="cm_type3">развлекательный</option>
                                            <option <?php if(isset($_SESSION['post']['type']) && $_SESSION['post']['type']=='cm_type4'){echo 'selected';} ?> value="cm_type4">магазин</option>
                                            <option <?php if(isset($_SESSION['post']['type']) && $_SESSION['post']['type']=='cm_type5'){echo 'selected';} ?> value="cm_type5">парикмахерская</option>
                                            <option <?php if(isset($_SESSION['post']['type']) && $_SESSION['post']['type']=='cm_type6'){echo 'selected';} ?> value="cm_type6">автотехобслуживание</option>
                                            <option <?php if(isset($_SESSION['post']['type']) && $_SESSION['post']['type']=='cm_type7'){echo 'selected';} ?> value="cm_type7">склад</option>
                                            <option <?php if(isset($_SESSION['post']['type']) && $_SESSION['post']['type']=='cm_type8'){echo 'selected';} ?> value="cm_type8">сельскохозяйственный</option>
                                            <option <?php if(isset($_SESSION['post']['type']) && $_SESSION['post']['type']=='cm_type9'){echo 'selected';} ?> value="cm_type9">бизнес центр</option>
                                            <option <?php if(isset($_SESSION['post']['type']) && $_SESSION['post']['type']=='cm_type10'){echo 'selected';} ?> value="cm_type10">производственный</option>
                                            <option <?php if(isset($_SESSION['post']['type']) && $_SESSION['post']['type']=='cm_type11'){echo 'selected';} ?> value="cm_type11">учебный</option>
                                            <option <?php if(isset($_SESSION['post']['type']) && $_SESSION['post']['type']=='cm_type12'){echo 'selected';} ?> value="cm_type12">медицинские заведения</option>
                                            <option <?php if(isset($_SESSION['post']['type']) && $_SESSION['post']['type']=='cm_type13'){echo 'selected';} ?> value="cm_type13">универсальный</option>
                                            <option <?php if(isset($_SESSION['post']['type']) && $_SESSION['post']['type']=='cm_type14'){echo 'selected';} ?> value="cm_type14">другое</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2" style="padding-top: 4px;">Площадь</div>
                                <div class="col-md-9">
                                    <div class="col-md-2">
                                        <input type="text" name="area_from" class="form-control" style="height: 20px" <?php if(isset($_SESSION['post']['area_from'])){echo 'value="'.$_SESSION['post']['area_from'].'"';} ?>>
                                    </div>
                                    <div class="col-md-1" style="text-align: center">_</div>
                                    <div class="col-md-2">
                                        <input type="text" name="area_to" class="form-control" style="height: 20px" <?php if(isset($_SESSION['post']['area_to'])){echo 'value="'.$_SESSION['post']['area_to'].'"';} ?>>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2" style="padding-top: 4px;">Цена</div>
                                <div class="col-md-9">
                                    <div class="col-md-2">
                                        <input type="text" name="price_from" class="form-control" style="height: 20px" placeholder="USD" <?php if(isset($_SESSION['post']['price_from'])){echo 'value="'.$_SESSION['post']['price_from'].'"';} ?>>
                                    </div>
                                    <div class="col-md-1" style="text-align: center">_</div>
                                    <div class="col-md-2">
                                        <input type="text" name="price_to" class="form-control" style="height: 20px" placeholder="USD" <?php if(isset($_SESSION['post']['price_to'])){echo 'value="'.$_SESSION['post']['price_to'].'"';} ?>>
                                    </div>
                                </div>
                            </div>
                            <?php
                            break;
                        }
                        case '8':{
                            ?>
                            <div class="col-md-12">
                                <div class="col-md-2" style="padding-top: 4px;">Площадь</div>
                                <div class="col-md-9">
                                    <div class="col-md-2">
                                        <input type="text" name="area_from" class="form-control" style="height: 20px" <?php if(isset($_SESSION['post']['area_from'])){echo 'value="'.$_SESSION['post']['area_from'].'"';} ?>>
                                    </div>
                                    <div class="col-md-1" style="text-align: center">_</div>
                                    <div class="col-md-2">
                                        <input type="text" name="area_to" class="form-control" style="height: 20px" <?php if(isset($_SESSION['post']['area_to'])){echo 'value="'.$_SESSION['post']['area_to'].'"';} ?>>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2" style="padding-top: 4px;">Цена</div>
                                <div class="col-md-9">
                                    <div class="col-md-2">
                                        <input type="text" name="price_from" class="form-control" style="height: 20px" placeholder="USD" <?php if(isset($_SESSION['post']['price_from'])){echo 'value="'.$_SESSION['post']['price_from'].'"';} ?>>
                                    </div>
                                    <div class="col-md-1" style="text-align: center">_</div>
                                    <div class="col-md-2">
                                        <input type="text" name="price_to" class="form-control" style="height: 20px" placeholder="USD" <?php if(isset($_SESSION['post']['price_to'])){echo 'value="'.$_SESSION['post']['price_to'].'"';} ?>>
                                    </div>
                                </div>
                            </div>
                            <?php
                            break;
                        }
                        case '9':{
                            ?>
                            <div class="col-md-12">
                                <div class="col-md-2" style="padding-top: 4px;">Площадь</div>
                                <div class="col-md-9">
                                    <div class="col-md-2">
                                        <input type="text" name="area_from" class="form-control" style="height: 20px" <?php if(isset($_SESSION['post']['area_from'])){echo 'value="'.$_SESSION['post']['area_from'].'"';} ?>>
                                    </div>
                                    <div class="col-md-1" style="text-align: center">_</div>
                                    <div class="col-md-2">
                                        <input type="text" name="area_to" class="form-control" style="height: 20px" <?php if(isset($_SESSION['post']['area_to'])){echo 'value="'.$_SESSION['post']['area_to'].'"';} ?>>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2" style="padding-top: 4px;">Цена</div>
                                <div class="col-md-9">
                                    <div class="col-md-2">
                                        <input type="text" name="price_from" class="form-control" style="height: 20px" placeholder="USD" <?php if(isset($_SESSION['post']['price_from'])){echo 'value="'.$_SESSION['post']['price_from'].'"';} ?>>
                                    </div>
                                    <div class="col-md-1" style="text-align: center">_</div>
                                    <div class="col-md-2">
                                        <input type="text" name="price_to" class="form-control" style="height: 20px" placeholder="USD" <?php if(isset($_SESSION['post']['price_to'])){echo 'value="'.$_SESSION['post']['price_to'].'"';} ?>>
                                    </div>
                                </div>
                            </div>
                            <?php
                            break;
                        }
                    }
                }
                ?>
            </div>
            <div class="col-md-5">
                <div class="col-md-12">
                    <div class="col-md-5">
                        <select name="f_region" class="form-control f_region" style="height: 20px" onchange="get_f_city(this)">
                            <?php
                            $result_region = mwdb_select("SELECT region_id,region_ru FROM region WHERE 1 ORDER BY region_order",array());
                            foreach($result_region as $row_region){
                                ?>
                                <option <?php if(isset($_SESSION['post']['f_region']) && $_SESSION['post']['f_region']==$row_region->region_id){echo 'selected';} ?> value="<?php echo $row_region->region_id; ?>"><?php echo $row_region->region_ru; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-5">
                        <select name="f_city" class="form-control f_city" style="height: 20px" onchange="get_f_area(this)">
                            <option value="0">-- Город/община --</option>
                            <?php
                            $result_cities = mwdb_select("SELECT city_id,city_ru FROM city WHERE region={var} ORDER BY city_order",array(8));
                            foreach ($result_cities as $result_city) {
                                ?>
                                <option <?php if(isset($_SESSION['post']['f_city']) && $_SESSION['post']['f_city']==$result_city->city_id){echo 'selected';} ?> value="<?php echo $result_city->city_id; ?>"><?php echo $result_city->city_ru; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-5">
                        <select name="f_area" class="form-control f_area" style="height: 20px" onchange="get_f_microarea(this)">
                            <option value="0">-- Район --</option>
                            <?php
                            if(isset($_SESSION['post']['f_city']) && $_SESSION['post']['f_city']>0){
                                $result_areas = mwdb_select("SELECT area_id,area_ru FROM area WHERE city={var} ORDER BY area_order",array($_SESSION['post']['f_city']));
                                foreach($result_areas as $row_area){
                                    ?>
                                    <option <?php if(isset($_SESSION['post']['f_area']) && $_SESSION['post']['f_area']==$row_area->area_id){echo 'selected';} ?> value="<?php echo $row_area->area_id; ?>"><?php echo $row_area->area_ru; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-5">
                        <select name="f_microarea" class="form-control f_microarea" style="height: 20px">
                            <option value="0">-- Микрорайон --</option>
                            <?php
                            if(isset($_SESSION['post']['f_area']) && $_SESSION['post']['f_area']>0){
                                $result_areas = mwdb_select("SELECT area_id,area_ru FROM microarea WHERE area={var} ORDER BY area_order",array($_SESSION['post']['f_area']));
                                foreach($result_areas as $row_area){
                                    ?>
                                    <option <?php if(isset($_SESSION['post']['f_microarea']) && $_SESSION['post']['f_microarea']==$row_area->area_id){echo 'selected';} ?> value="<?php echo $row_area->area_id; ?>"><?php echo $row_area->area_ru; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-5">
                    <input type="text" name="f_street_input" class="form-control f_street_input" oninput="get_f_streets(this);" style="height: 20px" placeholder="Улица" value="<?php if(isset($_SESSION['post']['f_street_input'])){echo $_SESSION['post']['f_street_input'];} ?>">
                    <div class="f_street_res" style="position: absolute;z-index: 100;background: #fff;padding: 0px 10px;border: 1px solid #ccc;width: 100%;border-top: none;max-height: 200px;overflow: auto;"></div>
                    <input type="hidden" name="f_street" value="<?php if(isset($_SESSION['post']['f_street'])){echo $_SESSION['post']['f_street'];} ?>" class="f_street">
                </div>
				<div class="col-md-5">
                    <input type="text" name="f_addrss_input" class="form-control f_addrss_input" style="height: 20px" placeholder="Адрес" value="<?php if(isset($_SESSION['post']['f_addrss_input'])){echo $_SESSION['post']['f_addrss_input'];} ?>">
                </div>
                <div class="col-md-12">
                    <div class="col-md-3"><input type="radio" name="ad_name" <?php if(!isset($_SESSION['post']['ad_name']) || ($_SESSION['post']['ad_name']!='1' && $_SESSION['post']['ad_name']!='2')){echo 'checked';} ?> value="">Все</div>
                    <div class="col-md-3"><input type="radio" name="ad_name" <?php if(isset($_SESSION['post']['ad_name']) && $_SESSION['post']['ad_name']=='1'){echo 'checked';} ?> value="1">Продажа</div>
                    <div class="col-md-3"><input type="radio" name="ad_name" <?php if(isset($_SESSION['post']['ad_name']) && $_SESSION['post']['ad_name']=='2'){echo 'checked';} ?> value="2">Аренда</div>
                </div>
            </div>
            <input type="hidden" name="is_submit" value="1">
            <div style="position: absolute;width: 95px;bottom: 10px;right: 85px">
                <button type="submit" class="btn btn-block btn-success btn-social" style="padding: 5px;font-size: 13px;">
                    <i class="fa fa-search" style="font-size: 13px !important;line-height: 28px !important;"></i> Найти
                </button>
            </div>
			<a href="https://user.alex-r.am/index.php?action=post&subaction=all" class="btn btn-danger" style="position: absolute;bottom: 10px;right: 15px;padding: 5.5px 10px;">Մաքրել</a>
        </form>
    </div>
</section>
<style>th{text-align: center}</style>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body table-responsive">
                    <table class="table table-hover table-bordered" style="text-align:center">
                        <tr>
                            <th>ID</th>
                            <th>Комнаты</th>
                            <th>Площадь</th>
                            <th>Этаж</th>
                            <th>Цена</th>
                            <th>Адрес</th>
                            <th>Владелец</th>
							<th style="width:10%">Доп. инфо</th>
							<th style="width:7%">Иные детали</th>
                            <th>Изображение</th>
                            <th>Дествия</th>
                            <?php if($user_level=='1'){ ?>
                                <th>Оператор</th>
                            <?php } ?>
                        </tr>
                        <?php
                        global $user_level;
                        if($user_level=='1'){
                            $query_part = 'post_status NOT IN (6,5)';
                        }
                        else{
                            $u_id = $_SESSION['user_id'];
                            $query_part = "post_user=$u_id AND post_status NOT IN (6,5)";
                        }
                        if(isset($_GET['status']) && $_GET['status']=='1'){
                            $query_part .= " AND post_status=1 ";
                        }
                        if(isset($_GET['status']) && $_GET['status']=='2'){
                            $query_part .= " AND post_status=2 ";
                        }
						if(isset($_GET['published']) && $_GET['published']=='0'){
                            $query_part .= " AND post_publish=0 ";
                        }
						if(isset($_GET['published']) && $_GET['published']=='1'){
                            $query_part .= " AND post_publish=1 ";
                        }
                        if(isset($_SESSION['post']) ){
                            if(isset($_SESSION['post']['category'])){
                                $category = (int)$_SESSION['post']['category'];
                                $query_part .= " AND post_category=$category ";
                                switch($category){
                                    case '5':{
                                        $part = '';
                                        if(isset($_SESSION['post']['rooms'])){
                                            $room_f = '';
                                            foreach($_SESSION['post']['rooms'] as $room){
                                                $room_f .= $room.',';
                                            }
                                            $room_f = substr($room_f,0,-1);
                                            if(in_array('10',$_SESSION['post']['rooms'])){
                                                $part .= " (rooms IN ($room_f) OR rooms>10) AND ";
                                            }
                                            else {
                                                $part .= " rooms IN ($room_f) AND ";
                                            }
                                        }
                                        if(isset($_SESSION['post']['floor_from']) && $_SESSION['post']['floor_from']!=''){
                                            $floor_from = $_SESSION['post']['floor_from'];
                                            $part .= " floor>=$floor_from AND ";
                                        }
                                        if(isset($_SESSION['post']['floor_to']) && $_SESSION['post']['floor_to']!=''){
                                            $floor_to = $_SESSION['post']['floor_to'];
                                            $part .= " floor<=$floor_to AND ";
                                        }
                                        if(isset($_SESSION['post']['area_from']) && $_SESSION['post']['area_from']!=''){
                                            $area_from = $_SESSION['post']['area_from'];
                                            $part .= " area>=$area_from AND ";
                                        }
                                        if(isset($_SESSION['post']['area_to']) && $_SESSION['post']['area_to']!=''){
                                            $area_to = $_SESSION['post']['area_to'];
                                            $part .= " area<=$area_to AND ";
                                        }
										if(isset($_SESSION['post']['building_type'])){
											$part .= " building_type IN (";
											foreach($_SESSION['post']['building_type'] as $one_building_type){
												$part .= " '$one_building_type',";
											}
											$part = substr($part,0,-1);
											$part .= ') AND ';
										}
                                        if($part==''){
                                            $part = '1';
                                        }
                                        else{
                                            $part = substr($part,0,-4);
                                        }
										
                                        $result_from_rooms = mwdb_select("SELECT post_id FROM settings_apartments WHERE $part");
                                        $atr_part = '';
                                        foreach($result_from_rooms as $row_from_rooms){
                                            $atr_part .= $row_from_rooms->post_id.',';
                                        }
                                        $atr_part = substr($atr_part,0,-1);
                                        $query_part .= " AND post_id IN ($atr_part) ";
                                        break;
                                    }
                                    case '6':{
                                        $part = '';
                                        if(isset($_SESSION['post']['rooms'])){
                                            $room_f = '';
                                            foreach($_SESSION['post']['rooms'] as $room){
                                                $room_f .= $room.',';
                                            }
                                            $room_f = substr($room_f,0,-1);
                                            if(in_array('10',$_SESSION['post']['rooms'])){
                                                $part .= " (rooms IN ($room_f) OR rooms>10) AND ";
                                            }
                                            else {
                                                $part .= " rooms IN ($room_f) AND ";
                                            }
                                        }
                                        if(isset($_SESSION['post']['floor_from']) && $_SESSION['post']['floor_from']!=''){
                                            $floor_from = $_SESSION['post']['floor_from'];
                                            $part .= " floorer>=$floor_from AND ";
                                        }
                                        if(isset($_SESSION['post']['floor_to']) && $_SESSION['post']['floor_to']!=''){
                                            $floor_to = $_SESSION['post']['floor_to'];
                                            $part .= " floorer<=$floor_to AND ";
                                        }
                                        if(isset($_SESSION['post']['area_from']) && $_SESSION['post']['area_from']!=''){
                                            $area_from = $_SESSION['post']['area_from'];
                                            $part .= " home_area>=$area_from AND ";
                                        }
                                        if(isset($_SESSION['post']['area_to']) && $_SESSION['post']['area_to']!=''){
                                            $area_to = $_SESSION['post']['area_to'];
                                            $part .= " home_area<=$area_to AND ";
                                        }
                                        if($part==''){
                                            $part = '1';
                                        }
                                        else{
                                            $part = substr($part,0,-4);
                                        }
                                        $result_from_rooms = mwdb_select("SELECT post_id FROM settings_houses WHERE $part");
                                        $atr_part = '';
                                        foreach($result_from_rooms as $row_from_rooms){
                                            $atr_part .= $row_from_rooms->post_id.',';
                                        }
                                        $atr_part = substr($atr_part,0,-1);
                                        $query_part .= " AND post_id IN ($atr_part) ";
                                        break;
                                    }
                                    case '7':{
                                        $part = '';
                                        if(isset($_SESSION['post']['type']) && $_SESSION['post']['type']!=''){
                                            $part .= " type='".$_SESSION['post']['type']."' AND ";
                                        }
                                        if(isset($_SESSION['post']['area_from']) && $_SESSION['post']['area_from']!=''){
                                            $area_from = $_SESSION['post']['area_from'];
                                            $part .= " area>=$area_from AND ";
                                        }
                                        if(isset($_SESSION['post']['area_to']) && $_SESSION['post']['area_to']!=''){
                                            $area_to = $_SESSION['post']['area_to'];
                                            $part .= " area<=$area_to AND ";
                                        }
                                        if($part==''){
                                            $part = '1';
                                        }
                                        else{
                                            $part = substr($part,0,-4);
                                        }
                                        $result_from_rooms = mwdb_select("SELECT post_id FROM settings_commercial WHERE $part");
                                        $atr_part = '';
                                        foreach($result_from_rooms as $row_from_rooms){
                                            $atr_part .= $row_from_rooms->post_id.',';
                                        }
                                        $atr_part = substr($atr_part,0,-1);
                                        $query_part .= " AND post_id IN ($atr_part) ";
                                        break;
                                    }
                                    case '8':{
                                        $part = '';
                                        if(isset($_SESSION['post']['area_from']) && $_SESSION['post']['area_from']!=''){
                                            $area_from = $_SESSION['post']['area_from'];
                                            $part .= " area>=$area_from AND ";
                                        }
                                        if(isset($_SESSION['post']['area_to']) && $_SESSION['post']['area_to']!=''){
                                            $area_to = $_SESSION['post']['area_to'];
                                            $part .= " area<=$area_to AND ";
                                        }
                                        if($part==''){
                                            $part = '1';
                                        }
                                        else{
                                            $part = substr($part,0,-4);
                                        }
                                        $result_from_rooms = mwdb_select("SELECT post_id FROM settings_land WHERE $part");
                                        $atr_part = '';
                                        foreach($result_from_rooms as $row_from_rooms){
                                            $atr_part .= $row_from_rooms->post_id.',';
                                        }
                                        $atr_part = substr($atr_part,0,-1);
                                        $query_part .= " AND post_id IN ($atr_part) ";
                                        break;
                                    }
                                    case '9':{
                                        $part = '';
                                        if(isset($_SESSION['post']['area_from']) && $_SESSION['post']['area_from']!=''){
                                            $area_from = $_SESSION['post']['area_from'];
                                            $part .= " area>=$area_from AND ";
                                        }
                                        if(isset($_SESSION['post']['area_to']) && $_SESSION['post']['area_to']!=''){
                                            $area_to = $_SESSION['post']['area_to'];
                                            $part .= " area<=$area_to AND ";
                                        }
                                        if($part==''){
                                            $part = '1';
                                        }
                                        else{
                                            $part = substr($part,0,-4);
                                        }
                                        $result_from_rooms = mwdb_select("SELECT post_id FROM settings_garages WHERE $part");
                                        $atr_part = '';
                                        foreach($result_from_rooms as $row_from_rooms){
                                            $atr_part .= $row_from_rooms->post_id.',';
                                        }
                                        $atr_part = substr($atr_part,0,-1);
                                        $query_part .= " AND post_id IN ($atr_part) ";
                                        break;
                                    }
                                }
                            }
                            if(isset($_SESSION['post']['ad_name']) && $_SESSION['post']['ad_name']>0){
                                if($_SESSION['post']['ad_name']=='1') {
                                    $result_types_o = mwdb_select("SELECT post_id FROM post_meta WHERE meta_key='sale' AND meta_value='1'");
                                    $poqr_qu = '';
                                    foreach($result_types_o as $row_types_o){
                                        $poqr_qu .= $row_types_o->post_id.',';
                                    }
                                    $poqr_qu = substr($poqr_qu,0,-1);
                                    $query_part .= " AND post_id IN ($poqr_qu) ";
                                }
                                elseif($_SESSION['post']['ad_name']=='2'){
                                    $result_types_o = mwdb_select("SELECT post_id FROM post_meta WHERE meta_key='rent' AND meta_value='1'");
                                    $poqr_qu = '';
                                    foreach($result_types_o as $row_types_o){
                                        $poqr_qu .= $row_types_o->post_id.',';
                                    }
                                    $poqr_qu = substr($poqr_qu,0,-1);
                                    $query_part .= " AND post_id IN ($poqr_qu) ";
                                }
                            }
                            if(isset($_SESSION['post']['price_from']) && $_SESSION['post']['price_from']!=''){
                                $price_from = $_SESSION['post']['price_from'];
                                if(isset($poqr_qu)){
                                    $ass = " AND post_id IN ($poqr_qu) ";
                                }
                                else{
                                    $ass = '';
                                }
                                $result_price_filter = mwdb_select("SELECT post_id FROM post_meta WHERE $poqr_qu (meta_key='price_number' OR meta_key='rent_cost1' OR meta_key='rent_cost2' OR meta_key='rent_cost3') AND meta_value>=$price_from $ass");
                                $p_part = '';
                                foreach($result_price_filter as $row_pri_filt){
                                    $p_part .= $row_pri_filt->post_id.',';
                                }
                                $p_part = substr($p_part,0,-1);
                                if(!isset($_SESSION['post']['price_to']) || $_SESSION['post']['price_to']==''){
                                    $query_part .= " AND post_id IN ($p_part) ";
                                }
                            }
                            if(isset($_SESSION['post']['price_to']) && $_SESSION['post']['price_to']!=''){
                                $price_to = $_SESSION['post']['price_to'];
                                if(isset($p_part)){
                                    $ass = " AND post_id IN ($p_part) ";
                                }
                                else{
                                    $ass = '';
                                }
                                $result_price_filter = mwdb_select("SELECT post_id FROM post_meta WHERE $poqr_qu (meta_key='price_number' OR meta_key='rent_cost1' OR meta_key='rent_cost2' OR meta_key='rent_cost3') AND meta_value<=$price_to $ass");
                                $p_part = '';
                                foreach($result_price_filter as $row_pri_filt){
                                    $p_part .= $row_pri_filt->post_id.',';
                                }
                                $p_part = substr($p_part,0,-1);
                                $query_part .= " AND post_id IN ($p_part) ";
                            }
                            if(isset($_SESSION['post']['f_region']) && $_SESSION['post']['f_region']>0){
                                $query_part .= " AND post_region=". $_SESSION['post']['f_region']." ";
                            }
                            if(isset($_SESSION['post']['f_city']) && $_SESSION['post']['f_city']>0){
                                $query_part .= " AND post_city=". $_SESSION['post']['f_city']." ";
                            }
                            if(isset($_SESSION['post']['f_area']) && $_SESSION['post']['f_area']>0){
                                $query_part .= " AND post_area=". $_SESSION['post']['f_area']." ";
                            }
                            if(isset($_SESSION['post']['f_microarea']) && $_SESSION['post']['f_microarea']>0){
                                $query_part .= " AND post_microarea=". $_SESSION['post']['f_microarea']." ";
                            }
                            if(isset($_SESSION['post']['f_street']) && $_SESSION['post']['f_street']!=''){
                                $query_part .= " AND post_street IN (". $_SESSION['post']['f_street'].") ";
                            }
							if(isset($_SESSION['post']['f_addrss_input']) && $_SESSION['post']['f_addrss_input']!=''){
                                $query_part .= " AND post_address LIKE '".$_SESSION['post']['f_addrss_input']."%' ";
                            }
                            if (isset($_GET['s_page'])) {
                                $page = (int)$_GET['s_page'];
                                if ($page < 2) {
                                    $page = 1;
                                }
                            } else {
                                $page = 1;
                            }
                            $offset = ($page - 1) * 20;
                            $lim = " LIMIT $offset,20 ";
                        }
                        else {
                            if (isset($_GET['page'])) {
                                $page = (int)$_GET['page'];
                                if ($page < 2) {
                                    $page = 1;
                                }
                            } else {
                                $page = 1;
                            }
                            $offset = ($page - 1) * 20;
                            $lim = " LIMIT $offset,20 ";
                        }
                        $result_posts = mwdb_select("SELECT post_id FROM posts WHERE $query_part ORDER BY post_id DESC $lim",array());
                        foreach($result_posts as $row_post){
                            $post_class = new Post('ru',$row_post->post_id);
                            $post = $post_class->get();
                            $location = '';
                            /*if($post->post_region>0){
                                $location .= mwdb_get_var("SELECT region_ru FROM region WHERE region_id={var}",array($post->post_region)).', ';
                            }*/
                            if($post->post_city>0){
                                $location .= mwdb_get_var("SELECT city_ru FROM city WHERE city_id={var}",array($post->post_city)).', ';
                            }
                            if($post->post_area>0){
                                $location .= mwdb_get_var("SELECT area_ru FROM area WHERE area_id={var}",array($post->post_area)).', ';
                            }
                            if($post->post_microarea>0){
                                $location .= mwdb_get_var("SELECT area_ru FROM microarea WHERE area_id={var}",array($post->post_microarea)).', ';
                            }
                            if($post->post_street>0){
                                $location .= mwdb_get_var("SELECT street_ru FROM streets WHERE street_id={var}",array($post->post_street)).' ';
                            }
                            $location .= $post->post_address;
                            $owner = json_decode($post->post_owner);
                            $owner_show = ''.$owner->owner_name.'<br> '.$owner->owner_phone1.'<br> '.($owner->owner_phone2!=''?$owner->owner_phone2.'<br> ':'').($owner->owner_phone3!=''?$owner->owner_phone3.'<br> ':'').($owner->owner_email!=''?$owner->owner_email.'<br>':'').($post->arajarkam!=''?'Arajark.am: '.$post->arajarkam:'');
                            $images = json_decode($post->post_images,true);
                            if(isset($images[0]['img']) && $images[0]['img']!=''){
                                $image_primary = 'https://user.alex-r.am/images/100/'.$images[0]['img'];
                            }
                            else{
                                $image_primary = 'https://user.alex-r.am/LOGO.png';
                            }
                            if($post->post_status=='1'){
                                $style_row = 'style="background:rgb(222, 212, 139)"';
                            }
                            elseif($post->post_status=='2'){
                                $style_row = 'style="background:rgb(190, 222, 139);"';
                            }
                            else{
                                $style_row = '';
                            }
							if($post->published=='0'){
								$style_row = 'style="background: rgba(255, 0, 0, 0.28);"';
							}
                            $settings = json_decode($post->post_meta);
                            if(isset($settings->sale) && $settings->sale=='1'){
                                $price = $settings->price_number.' '.$settings->price_currency;
                            }
                            if(isset($settings->rent) && $settings->rent=='1'){
                                $price .= '<br>Аренда: ';
                                if(isset($settings->rent_cost1) && (double)$settings->rent_cost1>0){
                                    $price .= $settings->rent_cost1.' '.$settings->rent_currency1;
                                }
                                elseif(isset($settings->rent_cost2) && (double)$settings->rent_cost2>0){
                                    $price .= $settings->rent_cost2.' '.$settings->rent_currency2;
                                }
                                elseif(isset($settings->rent_cost3) && (double)$settings->rent_cost3>0){
                                    $price .= $settings->rent_cost3.' '.$settings->rent_currency3;
                                }
                            }
                            $attributes = json_decode($post->settings);
                            if(isset($attributes->area)){
                                $area = $attributes->area;
                            }
                            elseif($attributes->home_area){
                                $area = $attributes->home_area;
                            }
                            else{
                                $area = '';
                            }
                            $fl = '';
                            if(isset($attributes->floor) && $attributes->floor!=''){
                                $fl .= $attributes->floor;
                            }
                            if(isset($attributes->floorer) && $attributes->floorer!=''){
                                $fl .= '/'.$attributes->floorer;
                            }
                            if(isset($attributes->rooms) && $attributes->rooms!=''){
                                $rooms = $attributes->rooms;
                            }
                            ?>
                            <tr <?php echo $style_row; ?>>
                                <td><?php echo $post->post_id; ?></td>
                                <td><?php echo $rooms; ?></td>
                                <td><?php echo $area; ?> м2</td>
                                <td><?php echo $fl; ?></td>
                                <td><?php echo $price; ?></td>
                                <td><?php echo $location; ?></td>
                                <td><?php echo $owner_show; ?></td>
								<td><?php echo nl2br($post->post_dop); ?></td>
								<td><?php echo nl2br($owner->owner_settings); ?></td>
                                <td><img src="<?php echo $image_primary; ?>" width="100"></td>
                                <td>
                                    <div class="btn-group">
                                        <a href="https://alex-r.am/estate/<?php echo $post->post_id; ?>/?user_id=<?php echo $_SESSION['user_id']; ?>" class="btn btn-default" target="_blank" style="background: #e6c924">
                                            <i class="fa fa-eye" data-toggle="tooltip" title="" data-original-title="Просмотр"></i>
                                        </a>
                                        <a href="https://user.alex-r.am/index.php?action=post&subaction=view&id=<?php echo $post->post_id; ?>" class="btn btn-default" style="background: #59e614">
                                            <i class="fa fa-edit" data-toggle="tooltip" title="" data-original-title="Редактировать"></i>
                                        </a>
                                        <a href="https://user.alex-r.am/index.php?action=post&subaction=find_client&id=<?php echo $post->post_id; ?>" class="btn btn-default">
                                            <i class="fa fa-search" data-toggle="tooltip" title="" data-original-title="Найти покупателя"></i>
                                        </a>
                                        <a href="https://user.alex-r.am/index.php?action=post&subaction=offer_client&id=<?php echo $post->post_id; ?>" class="btn btn-default">
                                            <i class="fa fa-reply" data-toggle="tooltip" title="" data-original-title="Предложить покупателю"></i>
                                        </a>
                                        <a href="https://user.alex-r.am/index.php?action=post&subaction=sold&id=<?php echo $post->post_id; ?>" class="btn btn-default">
                                            <i class="fa fa-usd" data-toggle="tooltip" title="" data-original-title="Продано"></i>
                                        </a>
                                        <a onclick="return confirm('Вы уверены?')" href="https://user.alex-r.am/index.php?action=post&subaction=delete&id=<?php echo $post->post_id; ?>" class="btn btn-danger">
                                            <i class="fa fa-trash-o" data-toggle="tooltip" title="" data-original-title="Удалить"></i> </a>
										<?php
										if($post->published=='0'){
											?>
											<a onclick="open_br_post_comment(this,<?php echo $post->post_id; ?>)" class="btn btn-primary">
												<i class="fa fa-comment-o" data-toggle="tooltip" title="" data-original-title="Комментировать"></i> <?=mwdb_get_var("SELECT COUNT(*) FROM broker_note_post WHERE post_id={var}",array($post->post_id))?>
											</a>
											<?php
										}
										?>
                                    </div>
                                </td>
                                <?php if($user_level=='1'){ ?>
                                    <td><?php echo mwdb_get_var("SELECT user_name FROM users WHERE user_id={var}",array($post->post_user)); ?></td>
                                <?php } ?>
                            </tr>
                            <?php
                            unset($price);
                        }
                        ?>
                    </table>
                    <?php if(isset($_SESSION['post']) ){ ?>
					<div class="col-md-12">
                        <?php
                        $count_posts = mwdb_get_var("SELECT COUNT(*) FROM posts WHERE {var}",array($query_part));
                        if($count_posts%20==0){
                            $page_count = (int)($count_posts/20);
                        }
                        else{
                            $page_c = (int)($count_posts/20);
                            $page_count = $page_c+1;
                        }
                        if($page_count>1){
                            ?>
                            <a href="https://user.alex-r.am/index.php?action=post&subaction=all&s_page=1" class="pager <?php if($page==1){echo 'current';} ?>">1</a>
                            <?php
                            for($i=2;$i<=$page_count;$i++){
                                ?>
                                <a href="https://user.alex-r.am/index.php?action=post&subaction=all&s_page=<?php echo $i; ?>" class="pager <?php if($page==$i){echo 'current';} ?>"><?php echo $i; ?></a>
                                <?php
                            }
                        }
                        ?>
                    </div>
					<?php
					} else{ ?>
                    <div class="col-md-12">
                        <?php
                        $count_posts = mwdb_get_var("SELECT COUNT(*) FROM posts WHERE {var}",array($query_part));
                        if($count_posts%20==0){
                            $page_count = (int)($count_posts/20);
                        }
                        else{
                            $page_c = (int)($count_posts/20);
                            $page_count = $page_c+1;
                        }
                        if($page_count>1){
                            ?>
                            <a href="https://user.alex-r.am/index.php?action=post&subaction=all<?=(isset($_GET['published'])?'&published='.$_GET['published']:'')?>" class="pager <?php if($page==1){echo 'current';} ?>">1</a>
                            <?php
                            for($i=2;$i<=$page_count;$i++){
                                ?>
                                <a href="https://user.alex-r.am/index.php?action=post&subaction=all&page=<?php echo $i; ?><?=(isset($_GET['published'])?'&published='.$_GET['published']:'')?>" class="pager <?php if($page==$i){echo 'current';} ?>"><?php echo $i; ?></a>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
<div class="example-modal hide">
            <div class="modal" style="display:block">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$('.example-modal').addClass('hide')"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Добавить комментарий</h4>
                  </div>
                  <div class="modal-body">
                    
                  </div>
                  <div class="modal-footer">
                    
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div>
</section>
<?php
//echo $count_posts;
?>
<script>
    document.getElementById("myspan").innerHTML="<?php echo $count_posts; ?>";
</script>