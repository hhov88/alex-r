<section class="content-header">
    <h1>Найти покупателя</h1>
</section>
<style>th{text-align: center}</style>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body table-responsive">
                    <style>
                        th{text-align: center}
                    </style>
                    <table class="table table-hover table-bordered" style="text-align: center">
                        <tr>
                            <th>ID</th>
                            <th>Инфо клиента</th>
                            <th>Район</th>
                            <th>Категория</th>
                            <th>Цена</th>
                            <th>Площадь</th>
                            <th>Комнаты</th>
                            <th>Этаж</th>
                            <th>Дата регистрации</th>
                            <th>Статус</th>
                            <th>Дествия</th>
                        </tr>
                    <?php
                    $post_id = (int)$_GET['id'];
                    $post_class = new Post('hy',$post_id);
                    $post = $post_class->get();
                    //print_r($post);echo '<br>';
                    $options = json_decode($post->post_meta);
                    $attributes = json_decode($post->settings);
                    //print_r($attributes);
                    $category = $post->post_category;
                    $region = $post->post_region;
                    $city = $post->post_city;
                    $area = $post->post_area;
                    $microarea = $post->post_microarea;
                    if($options->sale=='1'){
                        $type_s = 'sale';
                        $price = $post->price_number;
                    }
                    else{
                        if($options->rent=='1'){
                            if($options->rent_type3=='1') {
                                $type_s = 'day';
                                $price = $post->rent_cost3;
                            }
                            else{
                                $type_s = 'rent';
                                if(isset($price->rent_cost1)) {
                                    $price = $post->rent_cost1;
                                }
                                else{
                                    $price = $post->rent_cost2;
                                }
                            }
                        }
                    }
                    $query = " region=".$region." AND city LIKE '%".$city."%' AND category=".$category." ";
                    if($area!='0'){
                        $query .= " AND area LIKE '%".$area."%' ";
                    }
                    if($microarea!='0'){
                        $query .= " AND microarea LIKE '%".$microarea."%' ";
                    }
                    $result_clients = mwdb_select("SELECT search_id FROM client_search WHERE $query",array());
                    $array_search_id = array();
                    foreach($result_clients as $row_client) {
                        switch ($category) {
                            case '5': {
                                $row_search = mwdb_get_row("SELECT * FROM client_apartments WHERE search_id={var}",array($row_client->search_id));
                                $a_test = true;
                                if($row_search->area!='') {
                                    $array_area = explode('-', $row_search->area);
                                    $min_area = (double)$array_area[0];
                                    $max_area = (double)$array_area[1];
                                    if($attributes->area<$min_area){
                                        $a_test = false;
                                    }
                                    if($attributes->area>$max_area){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->rooms!='') {
                                    $array_room = explode(',', $row_search->rooms);
                                    if(!in_array($attributes->rooms,$array_room)){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->floorer!='') {
                                    $array_floorer = explode('-', $row_search->floorer);
                                    $min_floorer = (int)$array_floorer[0];
                                    $max_floorer = (int)$array_floorer[1];
                                    if($attributes->floorer<$min_floorer){
                                        $a_test = false;
                                    }
                                    if($attributes->floorer>$max_floorer){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->floor!='') {
                                    $array_floor = explode('-', $row_search->floor);
                                    $min_floor = (int)$array_floor[0];
                                    $max_floor = (int)$array_floor[1];
                                    if($attributes->floor<$min_floor){
                                        $a_test = false;
                                    }
                                    if($attributes->floor>$max_floor){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->floor_name!=''){
                                    $array_floor_name = explode(',', $row_search->floor_name);
                                    if(!in_array($attributes->floor_name,$array_floor_name)){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->repair!=''){
                                    $array_repair = explode(',', $row_search->repair);
                                    $repair_js = json_decode($attributes->repair,true);
                                    if(!in_array($repair_js['repair'],$array_repair)){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->balcony!=''){
                                    $array_balcony = explode(',', $row_search->balcony);
                                    $balcony_js = json_decode($attributes->balcony,true);
                                    if(!in_array($balcony_js['balcony'],$array_balcony)){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->garage!=''){
                                    $array_garage = explode(',', $row_search->garage);
                                    if(!in_array($attributes->garage,$array_garage)){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->basement!='0'){
                                    if($row_search->basement!=$attributes->basement){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->water!='0'){
                                    if($row_search->water!=$attributes->water){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->gas!='0'){
                                    if($row_search->gas!=$attributes->gas){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->building_project!=''){
                                    $array_building_project = explode(',', $row_search->building_project);
                                    if(!in_array($attributes->building_project,$array_building_project)){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->building_position!=''){
                                    $array_building_position = explode(',', $row_search->building_position);
                                    if(!in_array($attributes->building_position,$array_building_position)){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->building_type!=''){
                                    $array_building_type = explode(',', $row_search->building_type);
                                    if(!in_array($attributes->building_type,$array_building_type)){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->housetop!=''){
                                    $array_housetop = explode(',', $row_search->housetop);
                                    if(!in_array($attributes->housetop,$array_housetop)){
                                        $a_test = false;
                                    }
                                }
                                if($a_test){
                                    array_push($array_search_id,$row_client->search_id);
                                }
                                break;
                            }
                            case '6':{
                                $row_search = mwdb_get_row("SELECT * FROM client_houses WHERE search_id={var}",array($row_client->search_id));
                                $a_test = true;
                                if($row_search->total_area!='') {
                                    $array_total_area = explode('-', $row_search->total_area);
                                    $min_total_area = (double)$array_total_area[0];
                                    $max_total_area = (double)$array_total_area[1];
                                    if($attributes->total_area<$min_total_area){
                                        $a_test = false;
                                    }
                                    if($attributes->total_area>$max_total_area){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->home_area!='') {
                                    $array_home_area = explode('-', $row_search->home_area);
                                    $min_home_area = (double)$array_home_area[0];
                                    $max_home_area = (double)$array_home_area[1];
                                    if($attributes->home_area<$min_home_area){
                                        $a_test = false;
                                    }
                                    if($attributes->home_area>$max_home_area){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->rooms!='') {
                                    $array_room = explode(',', $row_search->rooms);
                                    if(!in_array($attributes->rooms,$array_room)){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->floorer!='') {
                                    $array_floorer = explode('-', $row_search->floorer);
                                    $min_floorer = (int)$array_floorer[0];
                                    $max_floorer = (int)$array_floorer[1];
                                    if($attributes->floorer<$min_floorer){
                                        $a_test = false;
                                    }
                                    if($attributes->floorer>$max_floorer){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->repair!=''){
                                    $array_repair = explode(',', $row_search->repair);
                                    $repair_js = json_decode($attributes->repair,true);
                                    if(!in_array($repair_js['repair'],$array_repair)){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->balcony!=''){
                                    $array_balcony = explode(',', $row_search->balcony);
                                    $balcony_js = json_decode($attributes->balcony,true);
                                    if(!in_array($balcony_js['balcony'],$array_balcony)){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->garage!=''){
                                    $array_garage = explode(',', $row_search->garage);
                                    if(!in_array($attributes->garage,$array_garage)){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->basement!='0'){
                                    if($row_search->basement!=$attributes->basement){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->water!='0'){
                                    if($row_search->water!=$attributes->water){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->gas!='0'){
                                    if($row_search->gas!=$attributes->gas){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->heating_system!='0'){
                                    if($row_search->heating_system!=$attributes->heating_system){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->building_position!=''){
                                    $array_building_position = explode(',', $row_search->building_position);
                                    if(!in_array($attributes->building_position,$array_building_position)){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->building_type!=''){
                                    $array_building_type = explode(',', $row_search->building_type);
                                    if(!in_array($attributes->building_type,$array_building_type)){
                                        $a_test = false;
                                    }
                                }
                                if($a_test){
                                    array_push($array_search_id,$row_client->search_id);
                                }
                                break;
                            }
                            case '7':{
                                $row_search = mwdb_get_row("SELECT * FROM client_commercial WHERE search_id={var}",array($row_client->search_id));
                                $a_test = true;
                                if($row_search->type!=''){
                                    $array_type = explode(',', $row_search->type);
                                    if(!in_array($attributes->type,$array_type)){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->area!='') {
                                    $array_area = explode('-', $row_search->area);
                                    $min_area = (double)$array_area[0];
                                    $max_area = (double)$array_area[1];
                                    if($attributes->area<$min_area){
                                        $a_test = false;
                                    }
                                    if($attributes->area>$max_area){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->rooms!='') {
                                    $array_room = explode(',', $row_search->rooms);
                                    if(!in_array($attributes->rooms,$array_room)){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->floorer!='') {
                                    $array_floorer = explode('-', $row_search->floorer);
                                    $min_floorer = (int)$array_floorer[0];
                                    $max_floorer = (int)$array_floorer[1];
                                    if($attributes->floorer<$min_floorer){
                                        $a_test = false;
                                    }
                                    if($attributes->floorer>$max_floorer){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->floor!='') {
                                    $array_floor = explode('-', $row_search->floor);
                                    $min_floor = (int)$array_floor[0];
                                    $max_floor = (int)$array_floor[1];
                                    if($attributes->floor<$min_floor){
                                        $a_test = false;
                                    }
                                    if($attributes->floor>$max_floor){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->floor_name!=''){
                                    $array_floor_name = explode(',', $row_search->floor_name);
                                    if(!in_array($attributes->floor_name,$array_floor_name)){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->showcases!='0'){
                                    if($row_search->showcases!=$attributes->showcases){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->entrance!='0'){
                                    if($row_search->entrance!=$attributes->entrance){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->repair!='0'){
                                    if($row_search->repair!=$attributes->repair){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->building_position!=''){
                                    $array_building_position = explode(',', $row_search->building_position);
                                    if(!in_array($attributes->building_position,$array_building_position)){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->building_type!=''){
                                    $array_building_type = explode(',', $row_search->building_type);
                                    if(!in_array($attributes->building_type,$array_building_type)){
                                        $a_test = false;
                                    }
                                }
                                if($a_test){
                                    array_push($array_search_id,$row_client->search_id);
                                }
                                break;
                            }
                            case '8':{
                                $row_search = mwdb_get_row("SELECT * FROM client_land WHERE search_id={var}",array($row_client->search_id));
                                $a_test = true;
                                if($row_search->area!='') {
                                    $array_area = explode('-', $row_search->area);
                                    $min_area = (double)$array_area[0];
                                    $max_area = (double)$array_area[1];
                                    if($attributes->area<$min_area){
                                        $a_test = false;
                                    }
                                    if($attributes->area>$max_area){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->buildings!='') {
                                    $array_buildings = explode('-', $row_search->buildings);
                                    $min_buildings = (double)$array_buildings[0];
                                    $max_buildings = (double)$array_buildings[1];
                                    if($attributes->buildings<$min_buildings){
                                        $a_test = false;
                                    }
                                    if($attributes->buildings>$max_buildings){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->water_drink!='0'){
                                    if($row_search->water_drink!=$attributes->water_drink){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->water_irrigation!='0'){
                                    if($row_search->water_irrigation!=$attributes->water_irrigation){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->gas!='0'){
                                    if($row_search->gas!=$attributes->gas){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->electricity!='0'){
                                    if($row_search->electricity!=$attributes->electricity){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->high_voltage_pillars!='0'){
                                    if($row_search->high_voltage_pillars!=$attributes->high_voltage_pillars){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->sewage!='0'){
                                    if($row_search->sewage!=$attributes->sewage){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->fence!='0'){
                                    if($row_search->fence!=$attributes->fence){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->fruit_trees!='0'){
                                    if($row_search->fruit_trees!=$attributes->fruit_trees){
                                        $a_test = false;
                                    }
                                }
                                if($a_test){
                                    array_push($array_search_id,$row_client->search_id);
                                }
                                break;
                            }
                            case '9':{
                                $row_search = mwdb_get_row("SELECT * FROM client_garages WHERE search_id={var}",array($row_client->search_id));
                                $a_test = true;
                                if($row_search->area!='') {
                                    $array_area = explode('-', $row_search->area);
                                    $min_area = (double)$array_area[0];
                                    $max_area = (double)$array_area[1];
                                    if($attributes->area<$min_area){
                                        $a_test = false;
                                    }
                                    if($attributes->area>$max_area){
                                        $a_test = false;
                                    }
                                }
                                if($row_search->building_type!='0'){
                                    if($row_search->building_type!=$attributes->building_type){
                                        $a_test = false;
                                    }
                                }
                                if($a_test){
                                    array_push($array_search_id,$row_client->search_id);
                                }
                                break;
                            }
                        }
                    }
                    foreach($array_search_id as $search_i){
                        $row_client = mwdb_get_row("SELECT post_client.*,client_search.* FROM post_client,client_search WHERE post_client.client_id=client_search.client_id AND client_search.search_id={var}",array($search_i));
                        switch($row_client->client_status){
                            case '0':{
                                $status = '<span class="label label-warning">Новый</span>';
                                break;
                            }
                            case '1':{
                                $status = '<span class="label label-primary">В процессе</span>';
                                break;
                            }
                            case '2':{
                                $status = '<span class="label label-success">Завершен</span>';
                                break;
                            }
                        }
                        if($row_client->client_type=='1'){
                            $style_tr = 'style="background:rgb(222, 212, 139)"';
                        }
                        elseif($row_client->client_type=='2'){
                            $style_tr = 'style="background:rgba(134, 199, 10, 0.45);"';
                        }
                        else{
                            $style_tr = '';
                        }
                        $category_name = mwdb_get_var("SELECT category_title_ru FROM category WHERE category_id={var}",array($row_client->category));
                        switch($row_client->type_search){
                            case 'sale':{
                                $category_name .= ' <br>(Продажа)';
                                break;
                            }
                            case 'rent':{
                                $category_name .= ' <br>(Аренда)';
                                break;
                            }
                        }
                        if($row_client->price_from>0){
                            $price_from = $row_client->price_from;
                        }
                        else{
                            $price_from = '';
                        }
                        if($row_client->price_to>0){
                            $price_to = $row_client->price_to;
                        }
                        else{
                            $price_to = '';
                        }
                        switch($row_client->category){
                            case '5':{
                                $petq_attr = mwdb_get_row("SELECT area,rooms,floor,floorer FROM client_apartments WHERE search_id={var}",array($row_client->search_id));
                                $area = $petq_attr->area;
                                $rooms = $petq_attr->rooms;
                                $floor = $petq_attr->floor.'/'.$petq_attr->floorer;
                                break;
                            }
                            case '6':{
                                $petq_attr = mwdb_get_row("SELECT home_area,rooms,floorer FROM client_houses WHERE search_id={var}",array($row_client->search_id));
                                $area = $petq_attr->home_area;
                                $rooms = $petq_attr->rooms;
                                $floor = $petq_attr->floorer;
                                break;
                            }
                            case '7':{
                                $petq_attr = mwdb_get_row("SELECT area,rooms,floor FROM client_commercial WHERE search_id={var}",array($row_client->search_id));
                                $area = $petq_attr->area;
                                $rooms = $petq_attr->rooms;
                                $floor = $petq_attr->floor;
                                break;
                            }
                            case '8':{
                                $petq_attr = mwdb_get_row("SELECT area FROM client_land WHERE search_id={var}",array($row_client->search_id));
                                $area = $petq_attr->area;
                                $rooms = '';
                                $floor = '';
                                break;
                            }
                            case '9':{
                                $petq_attr = mwdb_get_row("SELECT area FROM client_garages WHERE search_id={var}",array($row_client->search_id));
                                $area = $petq_attr->area;
                                $rooms = '';
                                $floor = '';
                                break;
                            }
                        }
                        ?>
                        <tr <?php echo $style_tr; ?>>
                            <td><?php echo $row_client->client_id; ?></td>
                            <td><?php echo $row_client->client_name; ?><br><?php echo $row_client->client_phone1;if(isset($row_client->client_phone2) && $row_client->client_phone2!=''){echo ', '.$row_client->client_phone2;} ?><br><?php echo $row_client->client_email; ?></td>
                            <td style="text-align: left"><?php echo client_search_region($row_client->client_id); ?></td>
                            <td><?php echo $category_name; ?></td>
                            <td><?php echo number_format($price_from,0,'',' ').' - '.number_format($price_to,0,'',' '); ?></td>
                            <td><?php echo $area; ?></td>
                            <td><?php echo $rooms; ?></td>
                            <td><?php echo $floor; ?></td>
                            <td><?php echo $row_client->client_time; ?></td>
                            <td><?php echo $status; ?></td>
                            <td>
                                <div class="btn-group">
                                    <?php
                                    $offers_class = new Offer($row_client->client_id);
                                    $offers = $offers_class->get();
                                    $array_offered_posts = array();
                                    if($offers){
                                        foreach($offers as $offer){
                                            array_push($array_offered_posts,$offer->post_id);
                                        }
                                    }
                                    ?>
                                    <a onclick="offer_to_user(<?php echo $post_id; ?>,<?php echo $row_client->client_id; ?>,this)" class="btn <?php if(in_array($post_id,$array_offered_posts)){echo 'btn-warning';} else{echo 'btn-success';} ?>">
                                        <i class="fa <?php if(in_array($post_id,$array_offered_posts)){echo 'fa-check-square-o';} else{echo 'fa-check';} ?>"></i>
                                        <?php if(in_array($post_id,$array_offered_posts)){echo 'Выбран';} else{echo 'Предложить';} ?>
                                    </a>
                                    <a href="http://user.alex-r.am/index.php?action=buyer&subaction=offers&id=<?php echo $row_client->client_id; ?>" class="btn btn-default">
                                        <i class="fa fa-list"></i> Список предложений
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>