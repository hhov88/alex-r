<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZ2D02mcYa-GES5qXKTrVOHIabQirrDi4&libraries=places"></script>
<section class="content-header">
    <h1>Недвижимости на карте</h1>
</section>
<section class="content">
    <style>
        #map{
            height: 650px;
            width: 100%;
        }
    </style>
    <div id="map" class="md-col-12"></div>
    <input type="hidden" name="map_address" class="map_address" value="">
<?php
global $user_level;
if($user_level=='1'){
    $query_part = 'posts.post_status!=6';
}
else{
    $u_id = $_SESSION['user_id'];
    $query_part = "posts.post_user=$u_id AND posts.post_status!=6 AND posts.post_status!=5";
}
$result_posts = mwdb_select("SELECT posts.post_map,posts.post_id,posts.post_title,posts.post_address,posts.post_code FROM posts WHERE $query_part ORDER BY post_id",array());
?>
    <script type="text/javascript">
        function initialize() {
            var mapOptions = {
                center: { lat: 40.177134, lng: 44.515690},
                zoom: 13
            };
            var map = new google.maps.Map(document.getElementById('map'),
                mapOptions);
            var markers = [];
            var infowindow = [];
            <?php
            $i = 0;
            foreach($result_posts as $row_post){
                $map_pos = $row_post->post_map;
                if($map_pos!=''){
                    $array_map_pos = explode(',',$map_pos);
                    $lat = $array_map_pos[0];
                    $lng = $array_map_pos[1];
                }
                else{
                    $lat = 40.177134;
                    $lng = 44.515690;
                }
                ?>
                infowindow[<?php echo $i; ?>] = new google.maps.InfoWindow({
                    content: '<?php echo '<a target="_blank" href="http://alex-r.am/estate/'.$row_post->post_id.'/?user_id='.$_SESSION['user_id'].'">'.$row_post->post_title.'<br>'.$row_post->post_code.'</a>'; ?>'
                });
                markers[<?php echo $i; ?>] = new google.maps.Marker({
                    map: map,
                    position: {
                        lat:<?php echo $lat ?>,
                        lng:<?php echo $lng ?>
                    }
                });
                markers[<?php echo $i; ?>].addListener('click', function() {
                    infowindow.forEach(function(info, i, infowindow){
                        info.close();
                    })
                    infowindow[<?php echo $i; ?>].open(map, markers[<?php echo $i; ?>]);
                });
                <?php
                $i++;
            }
            ?>
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</section>
<script>
    var page = 'map';
</script>