<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZ2D02mcYa-GES5qXKTrVOHIabQirrDi4&libraries=places"></script>
<link rel="stylesheet" href="<?php echo base_url() ?>css/chosen.css">
<style>
.a_hide:hover{cursor: pointer;height: 455px !important;}
.a_hide1:hover{cursor: pointer;height: 95px !important;}
.a_hide2:hover{cursor: pointer;height: 87px !important;}
.a_hide3:hover{cursor: pointer;height: 40px !important;}
.a_hide4:hover{cursor: pointer;height: 165px !important;}
.h_hide:hover{cursor: pointer}
.com_etaj:hover{height: 255px !important;}
</style>
<?php
if(isset($_POST['add_estate']) && $_POST['add_estate']=='Сохранить'){
    $owner_id = (int)$_POST['owner_id'];
    //$owner = new PostOwner($owner_id,$_POST['owner_name'],$_POST['owner_phone1'],$_POST['owner_phone2'],$_POST['owner_phone3'],$_POST['owner_email'],$_POST['owner_details'],$_POST['owner_skype']);
    //$post_owner = $owner->delete();
    
	$owner = new PostOwner(0,$_POST['owner_name'],$_POST['owner_phone1'],$_POST['owner_phone2'],$_POST['owner_phone3'],$_POST['owner_email'],$_POST['owner_details'],$_POST['owner_skype']);
    $owner_id = $owner->insert2();
	
	switch($_POST['category']){
        case '5':{
            if(isset($_POST['ap_repair'])){
                $rep_type = $_POST['ap_repair'];
                $dop_index = $rep_type.'_dop';
                if(isset($_POST[$dop_index])){
                    $repair_dop = $_POST[$dop_index];
                }
                else{
                    $repair_dop = '';
                }
                $repair = json_encode(array("repair"=>$rep_type,"repair_dop"=>$repair_dop));
            }
            else{
                $repair = '';
            }
            if(isset($_POST['ap_balcony'])){
                $array_balc = array();
                foreach($_POST['ap_balcony'] as $balcony_type) {
                    $dop_index = $balcony_type . '_dop';
                    if (isset($_POST[$dop_index])) {
                        $balcony_dop = $_POST[$dop_index];
                    } else {
                        $balcony_dop = '';
                    }
                    array_push($array_balc,array("balcony" => $balcony_type, "balcony_count" => $balcony_dop));
                }
                $balcony = json_encode($array_balc);
            }
            else{
                $balcony = '';
            }
            $sett_array = array(
                "area"=>isset($_POST['ap_area'])?$_POST['ap_area']:'',
                "rooms"=>isset($_POST['ap_rooms'])?$_POST['ap_rooms']:'',
                "floorer"=>isset($_POST['ap_floorer'])?$_POST['ap_floorer']:'',
                "floor"=>isset($_POST['ap_floor'])?$_POST['ap_floor']:'',
                "floor_name"=>isset($_POST['ap_floor_name'])?$_POST['ap_floor_name']:'',
                "repair"=>$repair,
                "balcony"=>$balcony,
                "garage"=>isset($_POST['ap_garage'])?$_POST['ap_garage']:'',
                "basement"=>isset($_POST['ap_basement'])?$_POST['ap_basement']:'',
                "water"=>isset($_POST['ap_water'])?$_POST['ap_water']:'',
                "gas"=>isset($_POST['ap_gas'])?$_POST['ap_gas']:'',
                "building_project"=>isset($_POST['ap_building_project'])?$_POST['ap_building_project']:'',
                "building_position"=>isset($_POST['ap_building_position'])?$_POST['ap_building_position']:'',
                "building_type"=>isset($_POST['ap_building_type'])?$_POST['ap_building_type']:'',
                "housetop"=>isset($_POST['ap_housetop'])?$_POST['ap_housetop']:''
            );
            $settings = json_encode($sett_array);
            break;
        }
        case '6':{
            if(isset($_POST['hm_repair'])){
                $rep_type = $_POST['hm_repair'];
                $dop_index = $rep_type.'_dop';
                if(isset($_POST[$dop_index])){
                    $repair_dop = $_POST[$dop_index];
                }
                else{
                    $repair_dop = '';
                }
                $repair = json_encode(array("repair"=>$rep_type,"repair_dop"=>$repair_dop));
            }
            else{
                $repair = '';
            }
            if(isset($_POST['hm_balcony'])){
                $balcony_type = $_POST['hm_balcony'];
                $dop_index = $balcony_type.'_dop';
                if(isset($_POST[$dop_index])){
                    $balcony_dop = $_POST[$dop_index];
                }
                else{
                    $balcony_dop = '';
                }
                $balcony = json_encode(array("balcony"=>$balcony_type,"balcony_count"=>$balcony_dop));
            }
            else{
                $balcony = '';
            }
            if(isset($_POST['hm_garage'])){
                $garage_type = $_POST['hm_garage'];
                $dop_index1 = $garage_type.'_dop1';
                $dop_index2 = $garage_type.'_dop2';
                if(isset($_POST[$dop_index1])){
                    $garage_count = $_POST[$dop_index1];
                }
                else{
                    $garage_count = 1;
                }
                if(isset($_POST[$dop_index2])){
                    $garage_area = $_POST[$dop_index2];
                }
                else{
                    $garage_area = '';
                }
                $garage = json_encode(array("garage_type"=>$garage_type,"garage_area"=>$garage_area));
            }
            else{
                $garage = '';
            }
            if(isset($_POST['hm_basement'])){
                $basement_status = $_POST['hm_basement'];
                $dop_index = $basement_status.'_dop';
                $basement = json_encode(array("basement"=>$basement_status,"basement_area"=>$_POST[$dop_index]));
            }
            else{
                $basement = '';
            }
            $sett_array = array(
                "total_area"=>isset($_POST['hm_total_area'])?$_POST['hm_total_area']:'',
                "home_area"=>isset($_POST['hm_home_area'])?$_POST['hm_home_area']:'',
                "rooms"=>isset($_POST['hm_rooms'])?$_POST['hm_rooms']:'',
                "floorer"=>isset($_POST['hm_floorer'])?$_POST['hm_floorer']:'',
                "floor"=>isset($_POST['hm_floor'])?$_POST['hm_floor']:'',
                "repair"=>$repair,
                "balcony"=>$balcony,
                "garage"=>$garage,
                "basement"=>$basement,
                "water"=>isset($_POST['hm_water'])?$_POST['hm_water']:'',
                "gas"=>isset($_POST['hm_gas'])?$_POST['hm_gas']:'',
                "heating_system"=>isset($_POST['hm_heating_system'])?$_POST['hm_heating_system']:'',
                "building_type"=>isset($_POST['hm_building_type'])?$_POST['hm_building_type']:'',
                "building_position"=>isset($_POST['hm_building_position'])?$_POST['hm_building_position']:''
            );
            $settings = json_encode($sett_array);
            break;
        }
        case '7':{
            $sett_array = array(
                "type"=>isset($_POST['cm_type'])?$_POST['cm_type']:'',
                "area"=>isset($_POST['cm_area'])?$_POST['cm_area']:'',
                "rooms"=>isset($_POST['cm_rooms'])?$_POST['cm_rooms']:'',
                "floor"=>isset($_POST['cm_floor'])?$_POST['cm_floor']:'',
                "floorer"=>isset($_POST['cm_floorer'])?$_POST['cm_floorer']:'',
                "floor_name"=>isset($_POST['cm_floor_name'])?$_POST['cm_floor_name']:'',
                "showcases"=>isset($_POST['cm_showcases'])?$_POST['cm_showcases']:'',
                "entrance"=>isset($_POST['cm_entrance'])?$_POST['cm_entrance']:'',
                "repair"=>isset($_POST['cm_repair'])?$_POST['cm_repair']:'',
                "building_position"=>isset($_POST['ap_building_position'])?$_POST['ap_building_position']:'',
                "building_type"=>isset($_POST['ap_building_type'])?$_POST['ap_building_type']:''
            );
            $settings = json_encode($sett_array);
            break;
        }
        case '8':{
            if(isset($_POST['ld_high_voltage_pillars'])){
                $high_voltage_pillars_exists = $_POST['ld_high_voltage_pillars'];
                $high_voltage_pillars_dop = $_POST[$high_voltage_pillars_exists.'_dop'];
                $high_voltage_pillars = json_encode(array("high_voltage_pillars_exists"=>$high_voltage_pillars_exists,"high_voltage_pillars_dop"=>$high_voltage_pillars_dop));
            }
            else{
                $high_voltage_pillars = '';
            }
            if(isset($_POST['ld_sewage'])){
                $sewage_status = $_POST['ld_sewage'];
                if(isset($_POST[$sewage_status.'_dop'])){
                    $sewage_dop = $_POST[$sewage_status.'_dop'];
                }
                else{
                    $sewage_dop = '';
                }
                $sewage = json_encode(array("sewage_status"=>$sewage_status,"sewage_dop"=>$sewage_dop));
            }
            else{
                $sewage = '';
            }
            $sett_array = array(
                "area"=>isset($_POST['ld_area'])?$_POST['ld_area']:'',
                "buildings"=>isset($_POST['ld_buildings'])?$_POST['ld_buildings']:'',
                "water_drink"=>isset($_POST['ld_water_drink'])?$_POST['ld_water_drink']:'',
                "water_irrigation"=>isset($_POST['ld_water_irrigation'])?$_POST['ld_water_irrigation']:'',
                "gas"=>isset($_POST['ld_gas'])?$_POST['ld_gas']:'',
                "electricity"=>isset($_POST['ld_electricity'])?$_POST['ld_electricity']:'',
                "high_voltage_pillars"=>$high_voltage_pillars,
                "sewage"=>$sewage,
                "fence"=>isset($_POST['ld_fence'])?$_POST['ld_fence']:'',
                "fruit_trees"=>isset($_POST['fruit_trees'])?$_POST['fruit_trees']:''
            );
            $settings = json_encode($sett_array);
            break;
        }
        case '9':{
            $sett_array = array(
                "area"=>isset($_POST['gg_area'])?$_POST['gg_area']:'',
                "building_type"=>isset($_POST['gg_building_type'])?$_POST['gg_building_type']:''
            );
            $settings = json_encode($sett_array);
            break;
        }
        case '10':{
            $settings = '{}';
            break;
        }
    }
    $images_array = array();
    if(isset($_POST['image_u'])){
        $hi = 0;
        foreach($_POST['image_u'] as $key=>$val){
            if(isset($_POST['image_hide'][$key]) && $_POST['image_hide'][$key]=='1'){
                $h = '1';
            }
            else{
                $h = '0';
            }
            $images_array[$hi] = array("img"=>$val,"order"=>$_POST['image_order'][$key],"hide"=>$h);
            $hi++;
        }
    }
    $images = json_encode($images_array);
    $meta_array = array();
    if(isset($_POST['sale']) && $_POST['sale']=='1'){
        $meta_array['sale'] = 1;
        $meta_array['price_type'] = $_POST['price_type'];
        if($meta_array['price_type']=='1'){
            $meta_array['price_number'] = (double)$_POST['price_number'];
            $meta_array['price_currency'] = $_POST['price_currency'];
        }
        if(isset($_POST['obmen'])){
            $meta_array['obmen'] = $_POST['obmen'];
        }
        else{
            $meta_array['obmen'] = 0;
        }
    }
    else{
        $meta_array['sale'] = 0;
    }
    if(isset($_POST['rent']) && $_POST['rent']=='1'){
        $meta_array['rent'] = 1;
        if(isset($_POST['rent_type1']) && $_POST['rent_type1']=='1'){
            $meta_array['rent_type1'] = 1;
            $meta_array['rent_cost1'] = $_POST['rent_cost1'];
            $meta_array['rent_currency1'] = $_POST['rent_currency1'];
        }
        else{
            $meta_array['rent_type1'] = 0;
        }
        if(isset($_POST['rent_type2']) && $_POST['rent_type2']=='1'){
            $meta_array['rent_type2'] = 1;
            $meta_array['rent_cost2'] = $_POST['rent_cost2'];
            $meta_array['rent_currency2'] = $_POST['rent_currency2'];
        }
        else{
            $meta_array['rent_type2'] = 0;
        }
        if(isset($_POST['rent_type3']) && $_POST['rent_type3']=='1'){
            $meta_array['rent_type3'] = 1;
            $meta_array['rent_cost3'] = $_POST['rent_cost3'];
            $meta_array['rent_currency3'] = $_POST['rent_currency3'];
        }
        else{
            $meta_array['rent_type3'] = 0;
        }
    }
    else{
        $meta_array['rent'] = 0;
    }
    $meta = json_encode($meta_array);
    if($_POST['app_number']!=''){
        $app_number = ' кв. '.$_POST['app_number'];
    }
    else{
        $app_number = '';
    }
	if(isset($_POST['pst_user_id_c']) && (int)$_POST['pst_user_id_c']>0){
		$pst_user_id_c = (int)$_POST['pst_user_id_c'];
	}
	else{
		$pst_user_id_c = $_SESSION['user_id'];
	}
    $ad = new Post('hy',$_GET['id'],$_POST['code'],$_POST['title_hy'],$_POST['content_hy'],$_POST['keywords_hy'],$_POST['title_ru'],$_POST['content_ru'],$_POST['keywords_ru'],$_POST['title_en'],$_POST['content_en'],$_POST['keywords_en'],$_POST['category'],$_POST['region'],$_POST['city'],$_POST['area'],$_POST['microarea'],$_POST['street'],$_POST['address'].$app_number,$pst_user_id_c,$owner_id,$settings,$images,$meta,$_POST['map_address'],$_POST['status_ad'],$_POST['post_details']);
    $ad->update();
	mwdb_query("UPDATE posts SET post_arajarkam='{var}',post_user='{var}' WHERE post_id={var}",array($_POST['arajark'],$pst_user_id_c,$_GET['id']));
	if(isset($_POST['published']) && (int)$_POST['published']==1){
		$published = 1;
	}
	else{
		$published = 0;
	}
	mwdb_query("UPDATE posts SET post_publish={var} WHERE post_id={var}",array($published,$_GET['id']));
}
?>
<?php
$id = (int)$_GET['id'];
$post_class = new Post('hy',$id);
$post = $post_class->get();
$post_meta = json_decode($post->post_meta);
?>
<section class="content-header">
    <h1>Недвижимость</h1>
</section>
<style>
    .dz_home_ch {
        position: absolute;
        z-index: 100000;
        top: 0px;
        right: 0px;
        padding: 5px;
        background-color: rgba(255, 255, 255, 0.65098);
    }
    .dz_home_ch input {
        width: 76px;
        height: 20px;
        float: left;
    }
</style>
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box">
<div class="box-body table-responsive">
<form action="" method="post" enctype="multipart/form-data">
<input type="hidden" name="id" value="<?php echo $post->post_id; ?>">
<div class="col-md-12" style="margin-bottom: 0px">
    <label style="float: left; margin-top: 5px;">Код:</label>
    <div class="col-md-1"><input type="text" name="code" class="form-control" value="<?php echo $post->post_code; ?>"></div>
    <div style="float: right">
        <div style="float: left;margin: 7px;">Статус:</div>
        <div style="float: left">
            <select name="status_ad" class="form-control">
                <option value="0" <?php if($post->post_status=='0'){echo 'selected';} ?>>Обычный</option>
                <option value="1" <?php if($post->post_status=='1'){echo 'selected';} ?>>Первичный</option>
                <option value="2" <?php if($post->post_status=='2'){echo 'selected';} ?>>TOP</option>
            </select>
        </div>
    </div>
</div>
<div class="col-md-5" style="margin-bottom: 10px">
        <div class="col-md-12"><input style="width: 17px;height: 17px" type="checkbox" name="sale" class="sale" value="1" <?php if($post_meta->sale=='1'){echo 'checked';} ?>> Продажа</div>
        <div class="col-md-8 sale_options" <?php if($post_meta->sale!='1'){echo 'style="display: none"';} ?>>
            <div class="col-md-12">
                <div class="col-md-12" style="margin-bottom: 9px;">
                    <div class="col-md-1"><input type="radio" name="price_type" value="1" <?php if($post_meta->price_type=='1'){echo 'checked';} ?> style="width: 17px;height: 17px"></div>
                    <div class="col-md-7"><input type="text" name="price_number" class="form-control" value="<?php if(isset($post_meta->price_number)){echo $post_meta->price_number;} ?>"></div>
                    <div class="col-md-3">
                        <select class="form-control" name="price_currency">
                            <option value="USD">USD</option>
                            <option value="AMD" <?php if(isset($post_meta->price_currency) && $post_meta->price_currency=='AMD'){echo 'selected';} ?>>AMD</option>
                            <option value="EUR" <?php if(isset($post_meta->price_currency) && $post_meta->price_currency=='EUR'){echo 'selected';} ?>>EUR</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-12">
                    <div class="col-md-1 no-padding"><input type="radio" name="price_type" value="2" style="width: 17px;height: 17px" <?php if($post_meta->price_type=='2'){echo 'checked';} ?>></div>
                    <div class="col-md-11" style="margin-top: 10px;">Договорная</div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-1 no-padding"><input type="checkbox" name="obmen" value="1" style="width: 17px;height: 17px" <?php if(isset($post_meta->obmen) && $post_meta->obmen=='1'){echo 'checked';} ?>></div>
                    <div class="col-md-11" style="margin-top: 10px;">Обмен</div>
                </div>
            </div>
        </div>
    </div>
<div class="col-md-7" style="margin-bottom: 10px">
        <div class="col-md-2"><input style="width: 17px;height: 17px" type="checkbox" name="rent" class="rent" value="1" <?php if($post_meta->rent=='1'){echo 'checked';} ?>> Аренда</div>
        <div class="col-md-10 rent_options" <?php if($post_meta->rent!='1'){echo 'style="display: none"';} ?>>
            <div class="col-md-12" style="margin-bottom: 10px">
                <div class="col-md-1"><input type="checkbox" name="rent_type1" value="1" style="width: 17px;height: 17px" <?php if(isset($post_meta->rent_type1) && $post_meta->rent_type1=='1'){echo 'checked';} ?>></div>
                <div class="col-md-2 no-padding">длительный</div>
                <div class="col-md-3">
                    <select name="rent_time1" class="rent_time1 form-control">
                        <option value="6m">6 месяцев</option>
                        <option value="7m">7 месяцев</option>
                        <option value="8m">8 месяцев</option>
                        <option value="9m">9 месяцев</option>
                        <option value="10m">10 месяцев</option>
                        <option value="11m">11 месяцев</option>
                        <option value="12m">12 месяцев</option>
                        <option value="13m">12+ месяцев</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <input type="number" name="rent_cost1" class="form-control" value="<?php if(isset($post_meta->rent_cost1)){echo $post_meta->rent_cost1;} ?>" placeholder="Цена">
                </div>
                <div class="col-md-2">
                    <select class="form-control" name="rent_currency1">
                        <option value="USD">USD</option>
                        <option value="AMD" <?php if(isset($post_meta->rent_currency1) && $post_meta->rent_currency1=='AMD'){echo 'selected';} ?>>AMD</option>
                        <option value="EUR" <?php if(isset($post_meta->rent_currency1) && $post_meta->rent_currency1=='EUR'){echo 'selected';} ?>>EUR</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12" style="margin-bottom: 10px">
                <div class="col-md-1"><input type="checkbox" name="rent_type2" value="1" style="width: 17px;height: 17px" <?php if(isset($post_meta->rent_type2) && $post_meta->rent_type2=='1'){echo 'checked';} ?>></div>
                <div class="col-md-2 no-padding">кратковременный</div>
                <div class="col-md-3">
                    <select name="rent_time2" class="rent_time2 form-control">
                        <option value="1m">1 месяц</option>
                        <option value="2m">2 месяца</option>
                        <option value="3m">3 месяца</option>
                        <option value="4m">4 месяца</option>
                        <option value="5m">5 месяцев</option>
                        <option value="6m">6 месяцев</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <input type="number" name="rent_cost2" class="form-control" value="<?php if(isset($post_meta->rent_cost2)){echo $post_meta->rent_cost2;} ?>" placeholder="Цена">
                </div>
                <div class="col-md-2">
                    <select class="form-control" name="rent_currency2">
                        <option value="USD">USD</option>
                        <option value="AMD" <?php if(isset($post_meta->rent_currency2) && $post_meta->rent_currency2=='AMD'){echo 'selected';} ?>>AMD</option>
                        <option value="EUR" <?php if(isset($post_meta->rent_currency2) && $post_meta->rent_currency2=='EUR'){echo 'selected';} ?>>EUR</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12" style="margin-bottom: 10px">
                <div class="col-md-1"><input type="checkbox" name="rent_type3" value="1" style="width: 17px;height: 17px" <?php if(isset($post_meta->rent_type3) && $post_meta->rent_type3=='1'){echo 'checked';} ?>></div>
                <div class="col-md-2 no-padding">посуточнo</div>
                <div class="col-md-3">
                    <select name="rent_time3" class="rent_time3 form-control">
                        <option value="1d">1 день</option>
                        <option value="2d">2 дня</option>
                        <option value="3d">3 дня</option>
                        <option value="4d">4 дня</option>
                        <option value="5d">5 дней</option>
                        <option value="6d">6 дней</option>
                        <option value="7d">7 дней</option>
                        <option value="8d">8 дней</option>
                        <option value="9d">9 дней</option>
                        <option value="10d">10 дней</option>
                        <option value="11d">11 дней</option>
                        <option value="12d">12 дней</option>
                        <option value="13d">13 дней</option>
                        <option value="14d">14 дней</option>
                        <option value="15d">15 дней</option>
                        <option value="16d">16 дней</option>
                        <option value="17d">17 дней</option>
                        <option value="18d">18 дней</option>
                        <option value="19d">19 дней</option>
                        <option value="20d">20 дней</option>
                        <option value="21d">21 дней</option>
                        <option value="22d">22 дней</option>
                        <option value="23d">23 дней</option>
                        <option value="24d">24 дней</option>
                        <option value="25d">25 дней</option>
                        <option value="26d">26 дней</option>
                        <option value="27d">27 дней</option>
                        <option value="28d">28 дней</option>
                        <option value="29d">29 дней</option>
                        <option value="30d">30 дней</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <input type="number" name="rent_cost3" class="form-control" value="<?php if(isset($post_meta->rent_cost3)){echo $post_meta->rent_cost3;} ?>" placeholder="Цена">
                </div>
                <div class="col-md-2">
                    <select class="form-control" name="rent_currency3">
                        <option value="USD">USD</option>
                        <option value="AMD" <?php if(isset($post_meta->rent_currency3) && $post_meta->rent_currency3=='AMD'){echo 'selected';} ?>>AMD</option>
                        <option value="EUR" <?php if(isset($post_meta->rent_currency3) && $post_meta->rent_currency3=='EUR'){echo 'selected';} ?>>EUR</option>
                    </select>
                </div>
            </div>
        </div>
</div>
    <div class="col-md-12">
        <div class="col-md-4">
<select name="category" class="category form-control" onchange="get_category_attributes(this);" style="background: #C4EBFF;">
    <?php
    $cats = mwdb_select("SELECT category_id,category_title_ru FROM category WHERE {var} ORDER BY category_order",array(1));
    foreach($cats as $cat){
        ?>
        <option value="<?php echo $cat->category_id; ?>" <?php if($post->post_category==$cat->category_id){echo 'selected';} ?>><?php echo $cat->category_title_ru; ?></option>
    <?php
    }
    ?>
</select>
    <div class="col-md-12">
        <select name="region" class="region form-control" onchange="region_change(this)">
            <option value="0">-- Регион --</option>
        </select>
    </div>
    <div class="col-md-12">
        <select name="city" class="city form-control" onchange="city_change(this)">
            <option value="0">-- Город --</option>
        </select>
    </div>
    <div class="col-md-12">
        <select name="area" class="area form-control" onchange="area_change(this)">
            <option value="0">-- Район --</option>
        </select>
    </div>
    <div class="col-md-12">
        <select name="microarea" class="microarea form-control" onchange="microarea_change(this)">
            <option value="0">-- Микрорайон --</option>
        </select>
    </div>
    <div class="col-md-12">
        <select name="street" class="street_selecton street form-control" data-placeholder="-- Улица --" tabindex="2">
        </select>
    </div>
    <?php
    $address = $post->post_address;
    $array_address = explode(' кв. ',$address);
    if(count($array_address)>0){
        $a = $array_address[0];
        $b = $array_address[1];
    }
    else{
        $a = $address;
        $b = '';
    }
    ?>
    <div class="col-md-6">
        <input type="text" name="address" class="address form-control" placeholder="Адрес" value="<?php echo $a; ?>" autocomplete="off">
    </div>
    <div class="col-md-6">
        <input type="text" name="app_number" class="app_number form-control" placeholder="Квартира" value="<?php echo $b; ?>" autocomplete="off">
    </div>
            <div style="display: inline-block;width: 100%;border-top: 3px solid #F00;border-bottom: 3px solid #FF0000;">
                <?php
                $owner = json_decode($post->post_owner);
                ?>
                <div class="col-md-6">
                    <input type="hidden" name="owner_id" value="<?php echo $owner->owner_id; ?>">
                    <input type="text" name="owner_name" class="owner_name form-control" placeholder="Имя Владельца" value="<?php echo $owner->owner_name; ?>" style="    margin-bottom: 3px;">
                    <input type="text" name="owner_phone1" class="owner_phone1 form-control" placeholder="Телефон1 " value="<?php echo $owner->owner_phone1; ?>" style="    margin-bottom: 3px;">
                    <input type="text" name="owner_phone2" class="owner_phone2 form-control" placeholder="Телефон2 " value="<?php echo $owner->owner_phone2; ?>" style="    margin-bottom: 3px;">
                    <input type="text" name="arajark" class="owner_phone3 form-control" placeholder="Arajark.am " value="<?php echo $post->arajarkam; ?>" style="    margin-bottom: 3px;">
                    <input type="email" name="owner_email" class="owner_email form-control" placeholder="E-mail Владельца" value="<?php echo $owner->owner_email; ?>" style="    margin-bottom: 3px;">
                    <input type="skype" name="owner_skype" class="owner_skype form-control" placeholder="Skype " value="<?php echo $owner->owner_skype; ?>">
                </div>
                <div class="col-md-6">
                    <textarea name="owner_details" class="owner_details form-control" placeholder="Иные детали" style="height: 100px;"><?php echo $owner->owner_settings; ?></textarea>
                </div>
                <div class="col-md-6">
                    <textarea name="post_details" class="post_details form-control" placeholder="Дополнительно" style="height: 100px;    background: #FFFED2;"><?php echo $post->post_dop; ?></textarea>
                </div>
            </div>
    </div>
    <div class="col-md-8">
<style>
    #map{
        height: 425px;
        width: 100%;
    }
</style>
<div id="map" class="md-col-12"></div>
<?php
$map_pos = $post->post_map;
if($map_pos!=''){
    $array_map_pos = explode(',',$map_pos);
    $lat = $array_map_pos[0];
    $lng = $array_map_pos[1];
}
else{
    $lat = 40.177134;
    $lng = 44.515690;
}
?>
<input type="hidden" name="map_address" class="map_address" value="<?php echo $map_pos; ?>">
<script type="text/javascript">
    function initialize() {
        var mapOptions = {
            center: { lat: <?php echo $lat ?>, lng: <?php echo $lng ?>},
            zoom: 15
        };
        var map = new google.maps.Map(document.getElementById('map'),
            mapOptions);
        var marker = new google.maps.Marker({
            map: map,
            draggable: true,
            position: mapOptions.center
        });
        google.maps.event.addListener(marker, 'dragend', function() {
            var position = marker.position;
            var lat = position.lat();
            var lng = position.lng();
            $(".map_address").val(lat+','+lng);
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
        </div>
    </div>
<div class="add_images">
    <div class="add_images_center">
        <div id="mydropzone" class="dropzone">
            <?php
            $images = json_decode($post->post_images,true);
            foreach($images as $img){
                ?>
                <div class="dz-preview dz-processing dz-image-preview dz-success dz-complete" onclick="select_order_input(this)">
                    <div class="dz-filename-after">
                        <input type="hidden" name="image_u[]" value="<?php echo $img['img']; ?>">
                    </div>
                    <div class="dz_home_ch">
                        <input type="text" name="image_order[]" value="<?php echo $img['order']; ?>">Order</div>
                    <div class="dz_hide_ch"><input type="checkbox" name="image_hide_a" onclick="do_image_hide_action(this)" <?php if($img['hide']=='1'){echo 'checked';} ?> value="1"><input type="hidden" name="image_hide[]" value="<?php if($img['hide']=='1'){echo '1';} else{echo '0';} ?>"> скрыть</div>
                    <div class="dz-image">
                        <img data-dz-thumbnail="" alt="<?php echo $img['img']; ?>" src="https://user.alex-r.am/images/100/<?php echo $img['img']; ?>" width="120" height="120">
                    </div>
                    <div class="dz-details"></div>
                    <div class="dz-progress"></div>
                    <div class="dz-error-message"></div>
                    <div class="dz-success-mark"></svg>
                    </div>  <div class="dz-error-mark"></div>
                    <a class="dz-remove" onclick="delete_image(this,'<?php echo $img['img']; ?>','<?php echo $post->post_id; ?>')" data-dz-remove="">Remove file</a></div>
                <?php
            }
            ?>
        </div>
    </div>
</div>
<div class="category_attributes" style="margin-top: 20px">
    <?php
    $attributes = json_decode($post->settings,true);
    switch($post->post_category){
        case '5':{
            ?>
            <div class="col-md-6" style="background: #F9FAFC;padding: 23px 25px;">
                <div class="col-md-12" style="height: 190px">
                    <div class="col-md-3">
                        <div class="col-md-8">Площадь</div>
                        <div class="col-md-8">
                            <input type="text" name="ap_area" class="apartment_attr ap_area form-control" value="<?php if(isset($attributes['area'])){echo $attributes['area'];} ?>">
                        </div>
                        <div class="col-md-4">m2</div>
                    </div>
                    <div class="col-md-3">
                        <div class="col-md-8">Комнаты</div>
                        <div class="col-md-11">
                            <select name="ap_rooms" class="apartment_attr ap_rooms form-control">
                                <?php
                                for($i=1;$i<10;$i++){
                                    ?>
                                    <option value="<?php echo $i ?>" <?php if(isset($attributes['rooms']) && $attributes['rooms']==$i){echo 'selected';} ?>><?php echo $i ?></option>
                                <?php
                                }
                                ?>
                                <option value="10">10+</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="col-md-8">Этажность</div>
                        <div class="col-md-11">
                            <select name="ap_floorer" class="apartment_attr ap_floorer form-control">
                                <?php
                                for($i=1;$i<=25;$i++){
                                    ?>
                                    <option value="<?php echo $i ?>" <?php if(isset($attributes['floorer']) && $attributes['floorer']==$i){echo 'selected';} ?>><?php echo $i ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="col-md-8">Этаж</div>
                        <div class="col-md-11 a_hide" style="height: 160px;overflow: hidden;position: absolute;z-index: 1001;line-height: 10px;margin-top: 24px;background: #fff url(https://user.alex-r.am/images/select.png) no-repeat 96% 10px;border: 1px solid #D2D6DE;">
                            <?php
                            for($i=1;$i<=25;$i++){
                                ?>
                                <div style="display: inline-block;width: 80%;padding: 0 10%;">
                                    <input type="checkbox" onclick="change_position_in_list(this)" name="ap_floor" <?php if(isset($attributes['floor']) && $attributes['floor']==$i){echo 'checked';} ?> class="apartment_attr ap_floor" value="<?php echo $i ?>" style="float: left"><span style="float: left;margin-top: 6px;margin-left: 6px;"><?php echo $i ?></span>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-5">
                        <div class="col-md-8">Проект</div>
                        <div class="col-md-11">
                            <select name="ap_building_project" class="apartment_attr ap_building_project form-control">
                                <option value="building_project1" <?php if(isset($attributes['building_project']) && $attributes['building_project']=='building_project1'){echo 'selected';} ?>>Сталинский проект</option>
                                <option value="building_project2" <?php if(isset($attributes['building_project']) && $attributes['building_project']=='building_project2'){echo 'selected';} ?>>Хрущевский</option>
                                <option value="building_project3" <?php if(isset($attributes['building_project']) && $attributes['building_project']=='building_project3'){echo 'selected';} ?>>Пост хрущевский</option>
                                <option value="building_project4" <?php if(isset($attributes['building_project']) && $attributes['building_project']=='building_project4'){echo 'selected';} ?>>Каменный спецпроект</option>
                                <!--<option value="building_project5" <?php if(isset($attributes['building_project']) && $attributes['building_project']=='building_project5'){echo 'selected';} ?>>Каркасно панельный</option>-->
                                <option value="building_project6" <?php if(isset($attributes['building_project']) && $attributes['building_project']=='building_project6'){echo 'selected';} ?>>Бадалян</option>
                                <option value="building_project7" <?php if(isset($attributes['building_project']) && $attributes['building_project']=='building_project7'){echo 'selected';} ?>>Московский ДСК</option>
                                <option value="building_project8" <?php if(isset($attributes['building_project']) && $attributes['building_project']=='building_project8'){echo 'selected';} ?>>Ереванский ДСК</option>
                                <option value="building_project9" <?php if(isset($attributes['building_project']) && $attributes['building_project']=='building_project9'){echo 'selected';} ?>>Грузинский</option>
                                <option value="building_project10" <?php if(isset($attributes['building_project']) && $attributes['building_project']=='building_project10'){echo 'selected';} ?>>Ленточный</option>
                                <option value="building_project11" <?php if(isset($attributes['building_project']) && $attributes['building_project']=='building_project11'){echo 'selected';} ?>>Монолитый сейсмоустойчивый</option>
                                <option value="building_project12" <?php if(isset($attributes['building_project']) && $attributes['building_project']=='building_project12'){echo 'selected';} ?>>Трехъярусный</option>
                                <option value="building_project13" <?php if(isset($attributes['building_project']) && $attributes['building_project']=='building_project13'){echo 'selected';} ?>>Четырехъярусный</option>
                                <option value="building_project14" <?php if(isset($attributes['building_project']) && $attributes['building_project']=='building_project14'){echo 'selected';} ?>>элитарная монолитная новостройка</option>
                                <option value="building_project16" <?php if(isset($attributes['building_project']) && $attributes['building_project']=='building_project16'){echo 'selected';} ?>>чешский проект</option>
                                <option value="building_project15" <?php if(isset($attributes['building_project']) && $attributes['building_project']=='building_project15'){echo 'selected';} ?>>Спецпроект</option>
                                <option value="building_project17" <?php if(isset($attributes['building_project']) && $attributes['building_project']=='building_project17'){echo 'selected';} ?>>Югославский</option>
                                <option value="building_project18" <?php if(isset($attributes['building_project']) && $attributes['building_project']=='building_project18'){echo 'selected';} ?>>Финский</option>
                                <option value="" <?php if(!isset($attributes['building_project']) || $attributes['building_project']==''){echo 'selected';} ?>>-</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="col-md-8">Тип здания</div>
                        <div class="col-md-11">
                            <select name="ap_building_type" class="apartment_attr ap_building_type form-control">
                                <option value="building_type1" <?php if(isset($attributes['building_type']) && $attributes['building_type']=='building_type1'){echo 'selected';} ?>>монолитный</option>
                                <option value="building_type2" <?php if(isset($attributes['building_type']) && $attributes['building_type']=='building_type2'){echo 'selected';} ?>>каменный</option>
                                <option value="building_type3" <?php if(isset($attributes['building_type']) && $attributes['building_type']=='building_type3'){echo 'selected';} ?>>крупнопанельный</option>
                                <option value="building_type4" <?php if(isset($attributes['building_type']) && $attributes['building_type']=='building_type4'){echo 'selected';} ?>>каркаснопанельный</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-5">
                        <div class="col-md-12">Расположение здания</div>
                        <div class="col-md-11">
                            <select name="ap_building_position" class="apartment_attr ap_building_position form-control">
                            <option value="building_position1" <?php if(isset($attributes['building_position']) && $attributes['building_position']=='building_position1'){echo 'selected';} ?>>Первая  линия</option>
                            <option value="building_position2" <?php if(isset($attributes['building_position']) && $attributes['building_position']=='building_position2'){echo 'selected';} ?>>Вторая  линия</option>
                            <option value="" <?php if(isset($attributes['building_position']) && $attributes['building_position']==''){echo 'selected';} ?>>-</option>
                                
                            </select>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="col-md-12">Тип покрытия</div>
                        <div class="col-md-11">
                            <select name="ap_housetop" class="apartment_attr ap_housetop form-control">
                                <option value="housetop1" <?php if(isset($attributes['housetop']) && $attributes['housetop']=='housetop1'){echo 'selected';} ?>>Деревянный</option>
                                <option value="housetop2" <?php if(isset($attributes['housetop']) && $attributes['housetop']=='housetop2'){echo 'selected';} ?>>Панельный</option>
                                <option value="housetop3" <?php if(isset($attributes['housetop']) && $attributes['housetop']=='housetop3'){echo 'selected';} ?>>Монолитный бетон</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="height: 55px;"></div>
            </div>
            <div class="col-md-6" style="background: #F9FAFC;padding: 23px 25px;">
                <div class="col-md-12" style="height: 190px;">
                    <div class="col-md-3">
                        <div class="col-md-12">Название этажа</div>
                        <div class="col-md-11" style="height: 165px;overflow: hidden;position: absolute;z-index: 1003;line-height: 10px;margin-top: 24px;background: #fff url(https://user.alex-r.am/images/select.png) no-repeat 96% 10px;border: 1px solid #D2D6DE;">
                            <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                <input type="checkbox" name="ap_floor_name" <?php if(isset($attributes['floor_name']) && $attributes['floor_name']=='basement'){echo 'checked';} ?> class="apartment_attr ap_floor_name" value="basement" style="float: left"><span style="float: left;margin-top: 6px;">подвал</span>
                            </div>
                            <div style="display: inline-block;width: 100%;padding: 0 1%;">
                                <input type="checkbox" name="ap_floor_name" <?php if(isset($attributes['floor_name']) && $attributes['floor_name']=='semibasement'){echo 'checked';} ?> class="apartment_attr ap_floor_name" value="semibasement" style="float: left"><span style="float: left;margin-top: 6px;">полуподвал</span>
                            </div>
                            <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                <input type="checkbox" name="ap_floor_name" <?php if(isset($attributes['floor_name']) && $attributes['floor_name']=='first'){echo 'checked';} ?> class="apartment_attr ap_floor_name" value="first" style="float: left"><span style="float: left;margin-top: 6px;">первый</span>
                            </div>
                            <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                <input type="checkbox" name="ap_floor_name" <?php if(isset($attributes['floor_name']) && $attributes['floor_name']=='high_first'){echo 'checked';} ?> class="apartment_attr ap_floor_name" value="high_first" style="float: left"><span style="float: left;margin-top: 6px;">белэтаж</span>
                            </div>
                            <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                <input type="checkbox" name="ap_floor_name" <?php if(isset($attributes['floor_name']) && $attributes['floor_name']=='average'){echo 'checked';} ?> class="apartment_attr ap_floor_name" value="average" style="float: left"><span style="float: left;margin-top: 6px;">средний</span>
                            </div>
                            <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                <input type="checkbox" name="ap_floor_name" <?php if(isset($attributes['floor_name']) && $attributes['floor_name']=='high_'){echo 'checked';} ?> class="apartment_attr ap_floor_name" value="high_" style="float: left"><span style="float: left;margin-top: 6px;">высокий</span>
                            </div>
                            <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                <input type="checkbox" name="ap_floor_name" <?php if(isset($attributes['floor_name']) && $attributes['floor_name']=='last'){echo 'checked';} ?> class="apartment_attr ap_floor_name" value="last" style="float: left"><span style="float: left;margin-top: 6px;">последний</span>
                            </div>
                            <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                <input type="checkbox" name="ap_floor_name" <?php if(isset($attributes['floor_name']) && $attributes['floor_name']=='duplex'){echo 'checked';} ?> class="apartment_attr ap_floor_name" value="duplex" style="float: left"><span style="float: left;margin-top: 6px;">дуплекс</span>
                            </div>
                            <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                <input type="checkbox" name="ap_floor_name" <?php if(isset($attributes['floor_name']) && $attributes['floor_name']=='mansard'){echo 'checked';} ?> class="apartment_attr ap_floor_name" value="mansard" style="float: left"><span style="float: left;margin-top: 6px;">мансард</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-8">Отделка</div>
                        <?php
                        if(isset($attributes['repair'])){
                            $repair = json_decode($attributes['repair'],true);
                        }
                        ?>
                        <div class="col-md-9 a_hide4" style="height: 165px;overflow: hidden;position: absolute;z-index: 1003;line-height: 10px;margin-top: 24px;background: #fff url(https://user.alex-r.am/images/select.png) no-repeat 96% 10px;border: 1px solid #D2D6DE;">
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="ap_repair" class="apartment_attr ap_repair" value="repair1" <?php if(isset($repair['repair']) && $repair['repair']=='repair1'){echo 'checked';} ?>></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Нулевое состояние</div>
                                <div class="col-md-3" style="margin-top: 1px"></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="ap_repair" class="apartment_attr ap_repair" value="repair2" <?php if(isset($repair['repair']) && $repair['repair']=='repair2'){echo 'checked';} ?>></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Гос. состояние</div>
                                <div class="col-md-3" style="margin-top: 1px"></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="ap_repair" class="apartment_attr ap_repair" value="repair3" <?php if(isset($repair['repair']) && $repair['repair']=='repair3'){echo 'checked';} ?>></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Капитальный ремонт</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" placeholder="год" style="height: 20px" name="repair3_dop" class="apartment_attr ap_repair_dop form-control" <?php if(isset($repair['repair']) && $repair['repair']=='repair3' && isset($repair['repair_dop'])){echo 'value="'.$repair['repair_dop'].'"';} ?>></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="ap_repair" class="apartment_attr ap_repair" value="repair4" <?php if(isset($repair['repair']) && $repair['repair']=='repair4'){echo 'checked';} ?>></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Стильный ремонт</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" placeholder="год" style="height: 20px" name="repair4_dop" class="apartment_attr ap_repair_dop form-control" <?php if(isset($repair['repair']) && $repair['repair']=='repair4' && isset($repair['repair_dop'])){echo 'value="'.$repair['repair_dop'].'"';} ?>></div>
                            </div>
                            <div class="col-md-12" style="margin: 0"><div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="ap_repair" class="apartment_attr ap_repair" value="repair5" <?php if(isset($repair['repair']) && $repair['repair']=='repair5'){echo 'checked';} ?>></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Среднее состояние</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" placeholder="год" style="height: 20px" name="repair5_dop" class="apartment_attr ap_repair_dop form-control" <?php if(isset($repair['repair']) && $repair['repair']=='repair5' && isset($repair['repair_dop'])){echo 'value="'.$repair['repair_dop'].'"';} ?>></div>
                            </div>
							<div class="col-md-12" style="margin: 0"><div class="col-md-1" style="margin-left: 2px;margin-top:0"><input type="checkbox" name="ap_repair" class="apartment_attr ap_repair" value="repair6" <?php if(isset($repair['repair']) && $repair['repair']=='repair6'){echo 'checked';} ?>></div>
								<div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Косметический ремонт</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" placeholder="год" style="height: 20px" name="repair6_dop" class="apartment_attr ap_repair_dop form-control" <?php if(isset($repair['repair']) && $repair['repair']=='repair6' && isset($repair['repair_dop'])){echo 'value="'.$repair['repair_dop'].'"';} ?>></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="ap_repair" class="apartment_attr ap_repair" value="repair7" <?php if(isset($repair['repair']) && $repair['repair']=='repair7'){echo 'checked';} ?>></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">В процессе ремонта</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="repair7_dop" class="apartment_attr ap_repair_dop form-control" placeholder="год" style="height: 20px" <?php if(isset($repair['repair']) && $repair['repair']=='repair7' && isset($repair['repair_dop'])){echo 'value="'.$repair['repair_dop'].'"';} ?>></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="ap_repair" class="apartment_attr ap_repair" value="repair8" <?php if(isset($repair['repair']) && $repair['repair']=='repair8'){echo 'checked';} ?>></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Частичный ремонт</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="repair8_dop" class="apartment_attr ap_repair_dop form-control" placeholder="год" style="height: 20px" <?php if(isset($repair['repair']) && $repair['repair']=='repair8' && isset($repair['repair_dop'])){echo 'value="'.$repair['repair_dop'].'"';} ?>></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="height: 55px;">
                    <div class="col-md-3">
                        <div class="col-md-8">Вода</div>
                        <div class="col-md-11">
                            <select name="ap_water" class="apartment_attr ap_water form-control"><option value="ap_water1" <?php if(isset($attributes['water']) && $attributes['water']=='ap_water1'){echo 'selected';} ?>>Постоянная</option>
                                <option value="ap_water2" <?php if(isset($attributes['water']) && $attributes['water']=='ap_water2'){echo 'selected';} ?>>Временная</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="col-md-8">Газ</div>
                        <div class="col-md-11">
                            <select name="ap_gas" class="apartment_attr ap_gas form-control">
                                <option value="ap_gas1" <?php if(isset($attributes['gas']) && $attributes['gas']=='ap_gas1'){echo 'selected';} ?>>В здании</option>
                                <option value="ap_gas2" <?php if(isset($attributes['gas']) && $attributes['gas']=='ap_gas2'){echo 'selected';} ?>>В квартире</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="height: 115px;">
                    <div class="col-md-6">
                        <div class="col-md-12">Балкон</div>
                        <?php
                        if(isset($attributes['balcony'])){
                            $balcony_array = json_decode($attributes['balcony'],true);
                            $arr_b = array();
                            $array_ba = array();
                            foreach($balcony_array as $balc){
                                array_push($arr_b,$balc['balcony']);
                                $array_ba[$balc['balcony']] = $balc['balcony_count'];
                            }
                        }
                        ?>
                        <div class="col-md-11 a_hide2" style="height: 87px;overflow: hidden;position: absolute;z-index: 1001;line-height: 10px;margin-top: 24px;background: #fff url(https://user.alex-r.am/images/select.png) no-repeat 96% 10px;border: 1px solid #D2D6DE;">
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="ap_balcony[]" class="apartment_attr ap_balcony" value="balcony1" <?php if(isset($arr_b) && in_array('balcony1',$arr_b)){echo 'checked';} ?>></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Висячий</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="balcony1_dop" placeholder="шт." style="height: 20px" class="apartment_attr ap_balcony_dop form-control" <?php if(isset($arr_b) && in_array('balcony1',$arr_b) && isset($array_ba['balcony1'])){echo 'value="'.$array_ba['balcony1'].'"';} ?>></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="ap_balcony[]" class="apartment_attr ap_balcony" value="balcony2" <?php if(isset($arr_b) && in_array('balcony2',$arr_b)){echo 'checked';} ?>></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Француский</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="balcony2_dop" placeholder="шт." style="height: 20px" class="apartment_attr ap_balcony_dop form-control" <?php if(isset($arr_b) && in_array('balcony2',$arr_b) && isset($array_ba['balcony2'])){echo 'value="'.$array_ba['balcony2'].'"';} ?>></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="ap_balcony[]" class="apartment_attr ap_balcony" value="balcony3" <?php if(isset($arr_b) && in_array('balcony3',$arr_b)){echo 'checked';} ?>></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Открытая Лоджия</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="balcony3_dop" placeholder="шт." style="height: 20px" class="apartment_attr ap_balcony_dop form-control" <?php if(isset($arr_b) && in_array('balcony3',$arr_b) && isset($array_ba['balcony3'])){echo 'value="'.$array_ba['balcony3'].'"';} ?>></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="ap_balcony[]" class="apartment_attr ap_balcony" value="balcony4" <?php if(isset($arr_b) && in_array('balcony4',$arr_b)){echo 'checked';} ?>></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Закрытая Лоджия</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="balcony4_dop" placeholder="шт." style="height: 20px" class="apartment_attr ap_balcony_dop form-control" <?php if(isset($arr_b) && in_array('balcony4',$arr_b) && isset($array_ba['balcony4'])){echo 'value="'.$array_ba['balcony4'].'"';} ?>></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="col-md-12">Гараж</div>
                        <div class="col-md-11 a_hide3" style="height: 40px;overflow: hidden;position: absolute;z-index: 1001;line-height: 10px;margin-top: 24px;background: #fff url(https://user.alex-r.am/images/select.png) no-repeat 96% 10px;border: 1px solid #D2D6DE;">
                            <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                <input type="checkbox" name="ap_garage" <?php if(isset($attributes['garage']) && $attributes['garage']=='ap_garage1'){echo 'checked';} ?> class="apartment_attr ap_garage" value="ap_garage1" style="float: left"><span style="float: left;margin-top: 6px;">Каменный</span>
                            </div>
                            <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                <input type="checkbox" name="ap_garage" <?php if(isset($attributes['garage']) && $attributes['garage']=='ap_garage2'){echo 'checked';} ?> class="apartment_attr ap_garage" value="ap_garage2" style="float: left"><span style="float: left;margin-top: 6px;">Железный</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="col-md-12">Подвал</div>
                        <div class="col-md-9">
                            <div class="col-md-12">
                                <div class="col-md-3"><input type="checkbox" <?php if(isset($attributes['basement']) && $attributes['basement']=='ap_basement1'){echo 'checked';} ?> name="ap_basement" value="ap_basement1" class="apartment_attr ap_basement"></div>
                                <div class="col-md-9">Есть</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            break;
        }
        case '6':{
            ?>
            <div class="col-md-4" style="background: #F9FAFC;padding: 23px 25px;">
                <div class="col-md-12" style="height: 73px;">
                    <div class="col-md-5">
                        <div class="col-md-8">Общая площадь</div>
                        <div class="col-md-8">
                            <input type="text" name="hm_total_area" class="home_attr hm_total_area form-control" <?php if(isset($attributes['total_area'])){echo 'value="'.$attributes['total_area'].'"';} ?>>
                        </div>
                        <div class="col-md-4">m2</div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-8">Площадь дома</div>
                        <div class="col-md-8">
                            <input type="text" name="hm_home_area" class="home_attr hm_home_area form-control" <?php if(isset($attributes['home_area'])){echo 'value="'.$attributes['home_area'].'"';} ?>>
                        </div>
                        <div class="col-md-4">m2</div>
                    </div>
                    <div class="col-md-3">
                        <div class="col-md-12">Количество комнат</div>
                        <div class="col-md-11">
                            <select name="hm_rooms" class="home_attr hm_rooms form-control">
                                <?php
                                for($i=1;$i<=10;$i++){
                                    ?>
                                    <option value="<?php echo $i ?>" <?php if(isset($attributes['rooms']) && $attributes['rooms']==$i){echo 'selected';} ?>><?php echo ($i<10)?$i:'10+'; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="height: 125px">
                    <div class="col-md-6">
                        <div class="col-md-8">Этажность</div>
                        <div class="col-md-11">
                            <select name="hm_floorer" class="home_attr hm_floorer form-control">
                                <?php
                                for($i=1;$i<6;$i++){
                                    ?>
                                    <option value="<?php echo $i ?>" <?php if(isset($attributes['floorer']) && $attributes['floorer']==$i){echo 'selected';} ?>><?php echo $i ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12">Продаваемый этаж</div>
                        <div class="col-md-11 h_hide" style="height: 95px;overflow: hidden;position: absolute;z-index: 1001;line-height: 10px;margin-top: 24px;background: #fff url(https://user.alex-r.am/images/select.png) no-repeat 96% 10px;border: 1px solid #D2D6DE;">
                            <?php
                            for($i=1;$i<6;$i++){
                                ?>
                                <div style="display: inline-block;width: 80%;padding: 0 10%;">
                                    <input type="checkbox" onclick="change_position_in_list(this)" name="hm_floor" <?php if(isset($attributes['floor']) && $attributes['floor']==$i){echo 'checked';} ?> class="home_attr hm_floor" value="<?php echo $i ?>" style="float: left"><span style="float: left;margin-top: 6px;margin-left: 6px;"><?php echo $i ?></span>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="height: 104px;">
                    <div class="col-md-6">
                        <div class="col-md-8">Тип здания</div>
                        <div class="col-md-11">
                            <select name="hm_building_type" class="home_attr hm_building_type form-control">
                                <option value="building_type2" <?php if(isset($attributes['building_type']) && $attributes['building_type']=='building_type2'){echo 'selected';} ?>>каменный</option>
                                <option value="building_type1" <?php if(isset($attributes['building_type']) && $attributes['building_type']=='building_type1'){echo 'selected';} ?>>монолитный</option>
                                <option value="building_type3" <?php if(isset($attributes['building_type']) && $attributes['building_type']=='building_type3'){echo 'selected';} ?>>крупнопанельный</option>
                                <option value="building_type4" <?php if(isset($attributes['building_type']) && $attributes['building_type']=='building_type4'){echo 'selected';} ?>>каркаснопанельный</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12">Положение здания</div>
                        <div class="col-md-11">
                            <select name="hm_building_position" class="home_attr hm_building_position form-control">
                                <option <?php if(isset($attributes['building_position']) && $attributes['building_position']=='building_position1'){echo 'selected';} ?> value="building_position1">Первая линия</option>
                                <option <?php if(isset($attributes['building_position']) && $attributes['building_position']=='building_position2'){echo 'selected';} ?> value="building_position2">Вторая линия</option>
                                <option <?php if(isset($attributes['building_position']) && $attributes['building_position']=='building_position3'){echo 'selected';} ?> value="building_position3">Третья линия</option>
                                <option <?php if(isset($attributes['building_position']) && $attributes['building_position']=='building_position4'){echo 'selected';} ?> value="building_position4">Четвертая линия</option>
                                <option <?php if(!isset($attributes['building_position']) || $attributes['building_position']==''){echo 'selected';} ?> value="">-</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8" style="background: #F9FAFC;padding: 23px 25px;">
                <div class="col-md-12" style="height: 115px;">
                    <div class="col-md-4">
                        <div class="col-md-8">Балкон</div>
                        <?php
                        $balcony = json_decode($attributes['balcony'],true);
                        ?>
                        <div class="col-md-11 h_hide1" style="height: 87px;overflow: hidden;position: absolute;z-index: 1001;line-height: 10px;margin-top: 24px;background: #fff url(https://user.alex-r.am/images/select.png) no-repeat 96% 10px;border: 1px solid #D2D6DE;">
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_balcony" class="home_attr hm_balcony" value="balcony1" <?php if(isset($balcony['balcony']) && $balcony['balcony']=='balcony1'){echo 'checked';} ?>></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Висячий</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" placeholder="шт." style="height: 20px" name="balcony1_dop" class="home_attr hm_balcony_dop form-control" <?php if(isset($balcony['balcony']) && $balcony['balcony']=='balcony1' && isset($balcony['balcony_count'])){echo 'value="'.$balcony['balcony_count'].'"';} ?>></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_balcony" class="home_attr hm_balcony" value="balcony2" <?php if(isset($balcony['balcony']) && $balcony['balcony']=='balcony2'){echo 'checked';} ?>></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Француский</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" placeholder="шт." style="height: 20px" name="balcony2_dop" class="home_attr hm_balcony_dop form-control" <?php if(isset($balcony['balcony']) && $balcony['balcony']=='balcony2' && isset($balcony['balcony_count'])){echo 'value="'.$balcony['balcony_count'].'"';} ?>></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_balcony" class="home_attr hm_balcony" value="balcony3" <?php if(isset($balcony['balcony']) && $balcony['balcony']=='balcony3'){echo 'checked';} ?>></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Открытая Лоджия</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" placeholder="шт." style="height: 20px" name="balcony3_dop" class="home_attr hm_balcony_dop form-control" <?php if(isset($balcony['balcony']) && $balcony['balcony']=='balcony3' && isset($balcony['balcony_count'])){echo 'value="'.$balcony['balcony_count'].'"';} ?>></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_balcony" class="home_attr hm_balcony" value="balcony4" <?php if(isset($balcony['balcony']) && $balcony['balcony']=='balcony4'){echo 'checked';} ?>></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Закрытая Лоджия</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" placeholder="шт." style="height: 20px" name="balcony4_dop" class="home_attr hm_balcony_dop form-control" <?php if(isset($balcony['balcony']) && $balcony['balcony']=='balcony4' && isset($balcony['balcony_count'])){echo 'value="'.$balcony['balcony_count'].'"';} ?>></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-9">Гараж</div>
                        <?php
                        $garage = json_decode($attributes['garage'],true);
                        ?>
                        <div class="col-md-11" style="height: 67px;overflow: hidden;position: absolute;z-index: 1001;line-height: 10px;margin-top: 24px;background: #fff url(https://user.alex-r.am/images/select.png) no-repeat 96% 10px;border: 1px solid #D2D6DE;">
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_garage" class="home_attr hm_garage" value="garage1" <?php if(isset($garage['garage_type']) && $garage['garage_type']=='garage1'){echo 'checked';} ?>></div>
                                <div class="col-md-6" style="margin-top: 6px;margin-left: 5px;">Пристройка</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" style="height: 20px" name="garage1_dop2" class="home_attr hm_garage_dop1 form-control" <?php if(isset($garage['garage_type']) && $garage['garage_type']=='garage1' && isset($garage['garage_area'])){echo 'value="'.$garage['garage_area'].'"';} ?>></div>
                                <div class="col-md-1 no-padding" style="margin-top: 6px">m2</div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_garage" class="home_attr hm_garage" value="garage2" <?php if(isset($garage['garage_type']) && $garage['garage_type']=='garage2'){echo 'checked';} ?>></div>
                                <div class="col-md-6" style="margin-top: 6px;margin-left: 5px;">Под зданием</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" style="height: 20px" name="garage2_dop2" class="home_attr hm_garage_dop1 form-control" <?php if(isset($garage['garage_type']) && $garage['garage_type']=='garage2' && isset($garage['garage_area'])){echo 'value="'.$garage['garage_area'].'"';} ?>></div>
                                <div class="col-md-1 no-padding" style="margin-top: 6px">m2</div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_garage" class="home_attr hm_garage" value="garage3" <?php if(isset($garage['garage_type']) && $garage['garage_type']=='garage3'){echo 'checked';} ?>></div>
                                <div class="col-md-6" style="margin-top: 6px;margin-left: 5px;">Отдельный</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" style="height: 20px" name="garage3_dop2" class="home_attr hm_garage_dop1 form-control" <?php if(isset($garage['garage_type']) && $garage['garage_type']=='garage3' && isset($garage['garage_area'])){echo 'value="'.$garage['garage_area'].'"';} ?>></div>
                                <div class="col-md-1 no-padding" style="margin-top: 6px">m2</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-8">Подвал</div>
                        <?php
                        $basement = json_decode($attributes['basement'],true);
                        ?>
                        <div class="col-md-11">
                            <div class="col-md-12">
                                <div class="col-md-1" style="margin-top: 1px;"><input type="checkbox" name="hm_basement" class="home_attr hm_basement" value="basement1" <?php if(isset($basement['basement']) && $basement['basement']=='basement1'){echo 'checked';} ?>></div>
                                <div class="col-md-3">Есть</div>
                                <div class="col-md-4">Площадь</div>
                                <div class="col-md-3"><input type="text" style="height: 20px" name="basement1_dop" class="home_attr hm_basement_dop form-control" <?php if(isset($basement['basement']) && $basement['basement']=='basement1' && isset($basement['basement_area'])){echo 'value="'.$basement['basement_area'].'"';} ?>></div>
                                <div class="col-md-1">м2</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="height: 190px;">
                    <div class="col-md-6">
                        <div class="col-md-8">Отделка</div>
                        <?php
                        $repair = json_decode($attributes['repair'],true);
                        ?>
                        <div class="col-md-11" style="height: 165px;overflow: hidden;position: absolute;z-index: 1003;line-height: 10px;margin-top: 24px;background: #fff url(https://user.alex-r.am/images/select.png) no-repeat 96% 10px;border: 1px solid #D2D6DE;">
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_repair" class="home_attr hm_repair" value="repair1" <?php if(isset($repair['repair']) && $repair['repair']=='repair1'){echo 'checked';} ?>></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Нулевое состояние</div>
                                <div class="col-md-3" style="margin-top: 1px"></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_repair" class="home_attr hm_repair" value="repair2" <?php if(isset($repair['repair']) && $repair['repair']=='repair2'){echo 'checked';} ?>></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Гос. состояние</div>
                                <div class="col-md-3" style="margin-top: 1px"></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_repair" class="home_attr hm_repair" value="repair3" <?php if(isset($repair['repair']) && $repair['repair']=='repair3'){echo 'checked';} ?>></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Капитальный ремонт</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="repair3_dop" placeholder="год" style="height: 20px" class="home_attr hm_repairr_dop form-control" <?php if(isset($repair['repair']) && $repair['repair']=='repair3' && isset($repair['repair_dop'])){echo 'value="'.$repair['repair_dop'].'"';} ?>></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_repair" class="home_attr hm_repair" value="repair4" <?php if(isset($repair['repair']) && $repair['repair']=='repair4'){echo 'checked';} ?>></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Стильный ремонт</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="repair4_dop" placeholder="год" style="height: 20px" class="home_attr hm_repair_dop form-control" <?php if(isset($repair['repair']) && $repair['repair']=='repair4' && isset($repair['repair_dop'])){echo 'value="'.$repair['repair_dop'].'"';} ?>></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_repair" class="home_attr hm_repair" value="repair5" <?php if(isset($repair['repair']) && $repair['repair']=='repair5'){echo 'checked';} ?>></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Среднее состояние</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="repair5_dop" placeholder="год" style="height: 20px" class="home_attr hm_repair_dop form-control" <?php if(isset($repair['repair']) && $repair['repair']=='repair5' && isset($repair['repair_dop'])){echo 'value="'.$repair['repair_dop'].'"';} ?>></div>
							</div>
							<div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top:0"><input type="checkbox" name="hm_repair" class="home_attr hm_repair" value="repair6" <?php if(isset($repair['repair']) && $repair['repair']=='repair6'){echo 'checked';} ?>></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Косметический ремонт</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="repair6_dop" placeholder="год" style="height: 20px" class="home_attr hm_repair_dop form-control" <?php if(isset($repair['repair']) && $repair['repair']=='repair6' && isset($repair['repair_dop'])){echo 'value="'.$repair['repair_dop'].'"';} ?>></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_repair" class="home_attr hm_repair" value="repair7" <?php if(isset($repair['repair']) && $repair['repair']=='repair7'){echo 'checked';} ?>></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">В процессе ремонта</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="repair7_dop" placeholder="год" style="height: 20px" class="home_attr hm_repair_dop form-control" <?php if(isset($repair['repair']) && $repair['repair']=='repair7' && isset($repair['repair_dop'])){echo 'value="'.$repair['repair_dop'].'"';} ?>></div>
                            </div>
                            <div class="col-md-12" style="margin: 0">
                                <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="checkbox" name="hm_repair" class="home_attr hm_repair" value="repair8" <?php if(isset($repair['repair']) && $repair['repair']=='repair8'){echo 'checked';} ?>></div>
                                <div class="col-md-7" style="margin-top: 6px;margin-left: 5px;">Частичный ремонт</div>
                                <div class="col-md-3" style="margin-top: 1px"><input type="text" name="repair8_dop" class="home_attr hm_repair_dop form-control" placeholder="год" style="height: 20px" <?php if(isset($repair['repair']) && $repair['repair']=='repair8' && isset($repair['repair_dop'])){echo 'value="'.$repair['repair_dop'].'"';} ?>></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="col-md-12">Вода</div>
                        <div class="col-md-11">
                            <select name="hm_water" class="home_attr hm_water form-control">
                                <option value="hm_water1" <?php if(isset($attributes['water']) && $attributes['water']=='hm_water1'){echo 'selected';} ?>>Постоянный</option>
                                <option value="hm_water2" <?php if(isset($attributes['water']) && $attributes['water']=='hm_water2'){echo 'selected';} ?>>Временный</option>
								<option value="hm_water3" <?php if(isset($attributes['water']) && $attributes['water']=='hm_water3'){echo 'selected';} ?>>Нет</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="col-md-12">Газ</div>
                        <div class="col-md-11">
                            <select name="hm_gas" class="home_attr hm_gas form-control">
                                <option value="hm_gas1" <?php if(isset($attributes['gas']) && $attributes['gas']=='hm_gas1'){echo 'selected';} ?>>Есть</option>
                                <option value="hm_gas2" <?php if(isset($attributes['gas']) && $attributes['gas']=='hm_gas2'){echo 'selected';} ?>>Возможность</option>
                                <option value="hm_gas3" <?php if(isset($attributes['gas']) && $attributes['gas']=='hm_gas3'){echo 'selected';} ?>>Нет</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="col-md-12">Система отопления</div>
                        <div class="col-md-11">
                            <select name="hm_heating_system" class="home_attr hm_heating_system form-control">
                                <option value="hm_heating_system1" <?php if(isset($attributes['heating_system']) && $attributes['heating_system']=='hm_heating_system1'){echo 'selected';} ?>>Есть</option>
                                <option value="hm_heating_system2" <?php if(isset($attributes['heating_system']) && $attributes['heating_system']=='hm_heating_system2'){echo 'selected';} ?>>Нет</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            break;
        }
        case '7':{
            ?>
            <div class="col-md-6" style="background: #F9FAFC;padding: 23px 25px;">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="col-md-12">Тип</div>
                        <div class="col-md-11">
                            <select name="cm_type" class="commercial_attr cm_type form-control">
                                <option value="cm_type2" <?php if(isset($attributes['type']) && $attributes['type']=='cm_type2'){echo 'selected';} ?>>гостиница</option>
                                <option value="cm_type1" <?php if(isset($attributes['type']) && $attributes['type']=='cm_type1'){echo 'selected';} ?>>оффис</option>
                                <option value="cm_type3" <?php if(isset($attributes['type']) && $attributes['type']=='cm_type3'){echo 'selected';} ?>>развлекательный</option>
                                <option value="cm_type4" <?php if(isset($attributes['type']) && $attributes['type']=='cm_type4'){echo 'selected';} ?>>магазин</option>
                                <option value="cm_type5" <?php if(isset($attributes['type']) && $attributes['type']=='cm_type5'){echo 'selected';} ?>>парикмахерская</option>
                                <option value="cm_type6" <?php if(isset($attributes['type']) && $attributes['type']=='cm_type6'){echo 'selected';} ?>>автотехобслуживание</option>
                                <option value="cm_type7" <?php if(isset($attributes['type']) && $attributes['type']=='cm_type7'){echo 'selected';} ?>>склад</option>
                                <option value="cm_type8" <?php if(isset($attributes['type']) && $attributes['type']=='cm_type8'){echo 'selected';} ?>>сельскохозяйственный</option>
                                <option value="cm_type9" <?php if(isset($attributes['type']) && $attributes['type']=='cm_type9'){echo 'selected';} ?>>бизнес центр</option>
                                <option value="cm_type10" <?php if(isset($attributes['type']) && $attributes['type']=='cm_type10'){echo 'selected';} ?>>производственный</option>
                                <option value="cm_type11" <?php if(isset($attributes['type']) && $attributes['type']=='cm_type11'){echo 'selected';} ?>>учебный</option>
                                <option value="cm_type12" <?php if(isset($attributes['type']) && $attributes['type']=='cm_type12'){echo 'selected';} ?>>медицинские заведения</option>
                                <option value="cm_type13" <?php if(isset($attributes['type']) && $attributes['type']=='cm_type13'){echo 'selected';} ?>>универсальный</option>
                                <option value="cm_type14" <?php if(isset($attributes['type']) && $attributes['type']=='cm_type14'){echo 'selected';} ?>>другое</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12">Тип здания</div>
                        <div class="col-md-11">
                            <select name="ap_building_type" class="apartment_attr ap_building_type form-control">
                                <option value="building_type1" <?php if(isset($attributes['building_type']) && $attributes['building_type']=='building_type1'){echo 'selected';} ?>>монолитный</option>
                                <option value="building_type2" <?php if(isset($attributes['building_type']) && $attributes['building_type']=='building_type2'){echo 'selected';} ?>>каменный</option>
                                <option value="building_type3" <?php if(isset($attributes['building_type']) && $attributes['building_type']=='building_type3'){echo 'selected';} ?>>крупнопанельный</option>
                                <option value="building_type4" <?php if(isset($attributes['building_type']) && $attributes['building_type']=='building_type4'){echo 'selected';} ?>>каркаснопанельный</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12">Расположение здания</div>
                        <div class="col-md-11">
                            <select name="ap_building_position" class="apartment_attr ap_building_position form-control">
                                <option value="building_position1" <?php if(isset($attributes['building_position']) && $attributes['building_position']=='building_position1'){echo 'selected';} ?>>Первая линия</option>
                                <option value="building_position2" <?php if(isset($attributes['building_position']) && $attributes['building_position']=='building_position2'){echo 'selected';} ?>>Вторая линия</option>
                                <option value="building_position3" <?php if(isset($attributes['building_position']) && $attributes['building_position']=='building_position3'){echo 'selected';} ?>>Третья линия</option>
                                <option value="building_position4" <?php if(isset($attributes['building_position']) && $attributes['building_position']=='building_position4'){echo 'selected';} ?>>Четвертая линия</option>
                                <option value="building_position5" <?php if(isset($attributes['building_position']) && $attributes['building_position']=='building_position5'){echo 'selected';} ?>>Другое</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="height: 140px">
                    <div class="col-md-3">
                        <div class="col-md-12">Площадь</div>
                        <div class="col-md-9">
                            <input type="text" name="cm_area" class="commercial_attr cm_area form-control" value="<?php if(isset($attributes['area'])){echo $attributes['area'];} ?>">
                        </div>
                        <div class="col-md-3">m2</div>
                    </div>
                    <div class="col-md-2">
                        <div class="col-md-12">Комнаты</div>
                        <div class="col-md-11">
                            <select name="cm_rooms" class="commercial_attr cm_rooms form-control">
                                <?php
                                for($i=1;$i<10;$i++){
                                    ?>
                                    <option value="<?php echo $i ?>" <?php if(isset($attributes['rooms']) && $attributes['rooms']==$i){echo 'selected';} ?>><?php echo $i ?></option>
                                    <?php
                                }
                                ?>
                                <option value="10">10+</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2" >
                        <div class="col-md-12">Этажность</div>
                        <div class="col-md-11">
                            <select name="cm_floorer" class="commercial_attr cm_floor form-control">
                                <?php
                                for($i=1;$i<=20;$i++){
                                    ?>
                                    <option value="<?php echo $i ?>" <?php if(isset($attributes['floorer']) && $attributes['floorer']==$i){echo 'selected';} ?>><?php echo $i ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="col-md-12">Этаж</div>
                        <div class="col-md-11 com_etaj" style="height: 130px;overflow: hidden;position: absolute;z-index: 1003;line-height: 10px;margin-top: 24px;background: #fff url(https://user.alex-r.am/images/select.png) no-repeat 96% 10px;border: 1px solid #D2D6DE;">
                            <?php
                            for($i=-3;$i<11;$i++){
                                ?>
                                <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                    <input type="checkbox" name="cm_floor" <?php if(isset($attributes['floor']) && $attributes['floor']==$i){echo 'checked';} ?> class="commercial_attr cm_floor" value="<?php echo $i; ?>" style="float: left"><span style="float: left;margin-top: 6px;"><?php echo $i; ?></span>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="col-md-12">Название этажa</div>
                        <div class="col-md-11 a_hide1" style="height: 95px;overflow: hidden;position: absolute;z-index: 1003;line-height: 10px;margin-top: 24px;background: #fff url(https://user.alex-r.am/images/select.png) no-repeat 96% 10px;border: 1px solid #D2D6DE;">
                            <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                <input type="checkbox" name="cm_floor_name" <?php if(isset($attributes['floor_name']) && $attributes['floor_name']=='basement'){echo 'checked';} ?> class="commercial_attr cm_floor_name" value="basement" style="float: left"><span style="float: left;margin-top: 6px;">подвал</span>
                            </div>
                            <div style="display: inline-block;width: 100%;padding: 0 1%;">
                                <input type="checkbox" name="cm_floor_name" <?php if(isset($attributes['floor_name']) && $attributes['floor_name']=='semibasement'){echo 'checked';} ?> class="commercial_attr cm_floor_name" value="semibasement" style="float: left"><span style="float: left;margin-top: 6px;">полуподвал</span>
                            </div>
                            <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                <input type="checkbox" name="cm_floor_name" <?php if(isset($attributes['floor_name']) && $attributes['floor_name']=='first'){echo 'checked';} ?> class="commercial_attr cm_floor_name" value="first" style="float: left"><span style="float: left;margin-top: 6px;">первый</span>
                            </div>
                            <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                <input type="checkbox" name="cm_floor_name" <?php if(isset($attributes['floor_name']) && $attributes['floor_name']=='average'){echo 'checked';} ?> class="commercial_attr cm_floor_name" value="average" style="float: left"><span style="float: left;margin-top: 6px;">средний</span>
                            </div>
                            <div style="display: inline-block;width: 98%;padding: 0 1%;">
                                <input type="checkbox" name="cm_floor_name" <?php if(isset($attributes['floor_name']) && $attributes['floor_name']=='last'){echo 'checked';} ?> class="commercial_attr cm_floor_name" value="last" style="float: left"><span style="float: left;margin-top: 6px;">последний</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6" style="background: #F9FAFC;padding: 23px 25px;">
                <div class="col-md-12" style="height: 198px;">
                    <div class="col-md-4">
                        <div class="col-md-12">Наличие витрин</div>
                        <div class="col-md-12">
                            <div class="col-md-1" style="margin-top: 1px"><input type="radio" name="cm_showcases" class="commercial_attr cm_showcases" value="cm_showcases1" <?php if(isset($attributes['showcases']) && $attributes['showcases']=='cm_showcases1'){echo 'checked';} ?>></div>
                            <div class="col-md-10" style="margin-left: 5px">Есть</div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-1" style="margin-top: 1px"><input type="radio" name="cm_showcases" class="commercial_attr cm_showcases" value="cm_showcases2" <?php if(isset($attributes['showcases']) && $attributes['showcases']=='cm_showcases2'){echo 'checked';} ?>></div>
                            <div class="col-md-10" style="margin-left: 5px">Нет</div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12">Вход</div>
                        <div class="col-md-10">
                            <select name="cm_entrance" class="commercial_attr cm_entrance form-control">
                                <option value="cm_entrance1" <?php if(isset($attributes['entrance']) && $attributes['entrance']=='cm_entrance1'){echo 'selected';} ?>>С улицы</option>
                                <option value="cm_entrance2" <?php if(isset($attributes['entrance']) && $attributes['entrance']=='cm_entrance2'){echo 'selected';} ?>>Со двора</option>
                                <option value="cm_entrance3" <?php if(isset($attributes['entrance']) && $attributes['entrance']=='cm_entrance3'){echo 'selected';} ?>>Главный вход</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12">Состояние</div>
                        <div class="col-md-12">
                            <div class="col-md-1" style="margin-top: 1px"><input type="radio" name="cm_repair" class="commercial_attr cm_repair" value="cm_repair1" <?php if(isset($attributes['repair']) && $attributes['repair']=='cm_repair1'){echo 'checked';} ?>></div>
                            <div class="col-md-10" style="margin-left: 5px">Отремонтированный</div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-1" style="margin-top: 1px"><input type="radio" name="cm_repair" class="commercial_attr cm_repair" value="cm_repair2" <?php if(isset($attributes['repair']) && $attributes['repair']=='cm_repair2'){echo 'checked';} ?>></div>
                            <div class="col-md-10" style="margin-left: 5px">Не отремонтировано</div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            break;
        }
        case '8':{
            ?>
        <div class="col-md-4" style="background: #F9FAFC;padding: 23px 25px;">
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="col-md-12">Размеры участок</div>
                    <div class="col-md-8">
                        <input type="text" name="ld_area" class="land_attr ld_area form-control" value="<?php if(isset($attributes['area'])){echo $attributes['area'];} ?>">
                    </div>
                    <div class="col-md-4">m2</div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-12">Наличие постройки</div>
                    <div class="col-md-8">
                        <input type="text" name="ld_buildings" class="land_attr ld_buildings form-control" value="<?php if(isset($attributes['buildings'])){echo $attributes['buildings'];} ?>">
                    </div>
                    <div class="col-md-4">m2</div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="col-md-12">Вода (питьевая)</div>
                    <div class="col-md-11">
                        <select name="ld_water_drink" class="land_attr ld_water_drink form-control">
                            <option value="water_drink1" <?php if(isset($attributes['water_drink']) && $attributes['water_drink']=='water_drink1'){echo 'selected';} ?>>Есть</option>
                            <option value="water_drink2" <?php if(isset($attributes['water_drink']) && $attributes['water_drink']=='water_drink2'){echo 'selected';} ?>>Нет</option>
                            <option value="water_drink3" <?php if(isset($attributes['water_drink']) && $attributes['water_drink']=='water_drink3'){echo 'selected';} ?>>Возможность</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-12">Вода (ирригационная)</div>
                    <div class="col-md-11">
                        <select name="ld_water_irrigation" class="land_attr ld_water_irrigation form-control">
                            <option value="water_irrigation1" <?php if(isset($attributes['water_irrigation']) && $attributes['water_irrigation']=='water_irrigation1'){echo 'selected';} ?>>Есть</option>
                            <option value="water_irrigation2" <?php if(isset($attributes['water_irrigation']) && $attributes['water_irrigation']=='water_irrigation2'){echo 'selected';} ?>>Нет</option>
                            <option value="water_irrigation3" <?php if(isset($attributes['water_irrigation']) && $attributes['water_irrigation']=='water_irrigation3'){echo 'selected';} ?>>Возможность</option>
                        </select>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="col-md-12">Газ</div>
                    <div class="col-md-11">
                        <select name="ld_gas" class="land_attr ld_gas form-control">
                            <option value="ld_gas1" <?php if(isset($attributes['gas']) && $attributes['gas']=='ld_gas1'){echo 'selected';} ?>>Есть</option>
                            <option value="ld_gas2" <?php if(isset($attributes['gas']) && $attributes['gas']=='ld_gas2'){echo 'selected';} ?>>Нет</option>
                            <option value="ld_gas3" <?php if(isset($attributes['gas']) && $attributes['gas']=='ld_gas3'){echo 'selected';} ?>>Возможность</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-12">Ток</div>
                    <div class="col-md-11">
                        <select name="ld_electricity" class="land_attr ld_electricity form-control">
                            <option value="ld_electricity1" <?php if(isset($attributes['electricity']) && $attributes['electricity']=='ld_electricity1'){echo 'selected';} ?>>Есть</option>
                            <option value="ld_electricity2" <?php if(isset($attributes['electricity']) && $attributes['electricity']=='ld_electricity2'){echo 'selected';} ?>>Нет</option>
                            <option value="ld_electricity3" <?php if(isset($attributes['electricity']) && $attributes['electricity']=='ld_electricity3'){echo 'selected';} ?>>Возможность</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8" style="background: #F9FAFC;padding: 23px 25px;">
            <div class="col-md-12" style="height: 95px">
                <div class="col-md-6">
                    <div class="col-md-12">Высоковольтные столбы</div>
                    <?php
                    $high_voltage_pillars = json_decode($attributes['high_voltage_pillars'],true);
                    ?>
                    <div class="col-md-11" style="height: 45px;overflow: hidden;position: absolute;z-index: 1003;line-height: 10px;margin-top: 24px;background: #fff url(https://user.alex-r.am/images/select.png) no-repeat 96% 10px;border: 1px solid #D2D6DE;">
                        <div class="col-md-12" style="margin: 0">
                            <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="radio" name="ld_high_voltage_pillars" class="land_attr ld_high_voltage_pillars" value="high_voltage_pillars1" <?php if(isset($high_voltage_pillars['high_voltage_pillars_exists']) && $high_voltage_pillars['high_voltage_pillars_exists']=='high_voltage_pillars1'){echo 'checked';} ?>></div>
                            <div class="col-md-2" style="margin-top: 6px;margin-left: 5px;">Есть</div>
                            <div class="col-md-4" style="margin-top: 6px;margin-left: 5px;">Количество</div>
                            <div class="col-md-3" style="margin-top: 1px"><input type="text" name="high_voltage_pillars1_dop" style="height: 20px" class="home_attr ld_high_voltage_pillars_dop form-control" <?php if(isset($high_voltage_pillars['high_voltage_pillars_exists']) && $high_voltage_pillars['high_voltage_pillars_exists']=='high_voltage_pillars1' && isset($high_voltage_pillars['high_voltage_pillars_dop'])){echo 'value="'.$high_voltage_pillars['high_voltage_pillars_dop'].'"';} ?>></div>
                            <div class="col-md-1" style="margin-top: 6px;margin-left: 5px;">шт.</div>
                        </div>
                        <div class="col-md-12" style="margin: 0">
                            <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="radio" name="ld_high_voltage_pillars" class="home_attr ld_high_voltage_pillars" value="high_voltage_pillars2" <?php if(isset($high_voltage_pillars['high_voltage_pillars_exists']) && $high_voltage_pillars['high_voltage_pillars_exists']=='high_voltage_pillars2'){echo 'checked';} ?>></div>
                            <div class="col-md-2" style="margin-top: 6px;margin-left: 5px;">Нет</div>
                            <div class="col-md-4" style="margin-top: 6px;margin-left: 5px;">Расстояние</div>
                            <div class="col-md-3" style="margin-top: 1px"><input type="text" name="high_voltage_pillars2_dop" style="height: 20px" class="home_attr ld_high_voltage_pillars_dop form-control" <?php if(isset($high_voltage_pillars['high_voltage_pillars_exists']) && $high_voltage_pillars['high_voltage_pillars_exists']=='high_voltage_pillars2' && isset($high_voltage_pillars['high_voltage_pillars_dop'])){echo 'value="'.$high_voltage_pillars['high_voltage_pillars_dop'].'"';} ?>></div>
                            <div class="col-md-1" style="margin-top: 6px;margin-left: 5px;">м</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-12">Канализация</div>
                    <?php
                    $sewage = json_decode($attributes['sewage'],true);
                    ?>
                    <div class="col-md-11" style="height: 60px;overflow: hidden;position: absolute;z-index: 1003;line-height: 10px;margin-top: 24px;background: #fff url(https://user.alex-r.am/images/select.png) no-repeat 96% 10px;border: 1px solid #D2D6DE;">
                        <div class="col-md-12" style="margin: 0">
                            <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="radio" name="ld_sewage" class="land_attr ld_sewage" value="sewage1" <?php if(isset($sewage['sewage_status']) && $sewage['sewage_status']=='sewage1'){echo 'checked';} ?>></div>
                            <div class="col-md-4" style="margin-top: 6px;margin-left: 5px;">Есть</div>
                            <div class="col-md-3" style="margin-top: 1px"></div>
                            <div class="col-md-2" style="margin-top: 1px"></div>
                            <div class="col-md-1" style="margin-top: 6px;margin-left: 5px;"></div>
                        </div>
                        <div class="col-md-12" style="margin: 0">
                            <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="radio" name="ld_sewage" class="home_attr ld_sewage" value="sewage2" <?php if(isset($sewage['sewage_status']) && $sewage['sewage_status']=='sewage2'){echo 'checked';} ?>></div>
                            <div class="col-md-4" style="margin-top: 6px;margin-left: 5px;">Нет</div>
                            <div class="col-md-3" style="margin-top: 1px"></div>
                            <div class="col-md-2" style="margin-top: 1px"></div>
                            <div class="col-md-1" style="margin-top: 6px;margin-left: 5px;"></div>
                        </div>
                        <div class="col-md-12" style="margin: 0">
                            <div class="col-md-1" style="margin-left: 2px;margin-top: 0"><input type="radio" name="ld_sewage" class="home_attr ld_sewage" value="sewage3" <?php if(isset($sewage['sewage_status']) && $sewage['sewage_status']=='sewage3'){echo 'checked';} ?>></div>
                            <div class="col-md-4" style="margin-top: 6px;margin-left: 5px;">Возможность</div>
                            <div class="col-md-3" style="margin-top: 1px">Расстояние</div>
                            <div class="col-md-2" style="margin-top: 1px"><input type="text" name="sewage3_dop" style="height: 20px" class="home_attr ld_sewage_dop form-control" <?php if(isset($sewage['sewage_status']) && $sewage['sewage_status']=='sewage3' && isset($sewage['sewage_dop'])){echo 'value="'.$sewage['sewage_dop'].'"';} ?>></div>
                            <div class="col-md-1" style="margin-top: 6px;margin-left: 5px;">м</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="height: 73px">
                <div class="col-md-6">
                    <div class="col-md-12">Ограждение</div>
                    <div class="col-md-11">
                        <select name="ld_fence" class="land_attr ld_fence form-control">
                            <option value="ld_fence1" <?php if(isset($attributes['fence']) && $attributes['fence']=='ld_fence1'){echo 'selected';} ?>>Камень</option>
                            <option value="ld_fence2" <?php if(isset($attributes['fence']) && $attributes['fence']=='ld_fence2'){echo 'selected';} ?>>Проволочные сетки</option>
                            <option value="ld_fence3" <?php if(isset($attributes['fence']) && $attributes['fence']=='ld_fence3'){echo 'selected';} ?>>Нет</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-12">Плодовые деревья</div>
                    <div class="col-md-1"><input type="radio" name="fruit_trees" class="land_attr fruit_trees" value="fruit_trees1" <?php if(isset($sewage['fruit_trees']) && $sewage['fruit_trees']=='fruit_trees1'){echo 'checked';} ?>></div>
                    <div class="col-md-3" style="margin-top: 5px">Есть</div>
                    <div class="col-md-1"><input type="radio" name="fruit_trees" class="land_attr fruit_trees" value="fruit_trees2" <?php if(isset($sewage['fruit_trees']) && $sewage['fruit_trees']=='fruit_trees2'){echo 'checked';} ?>></div>
                    <div class="col-md-3" style="margin-top: 5px">Нет</div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </div>
            <?php
            break;
        }
        case '9':{
            ?>
            <div class="col-md-6" style="background: #F9FAFC;padding: 23px 25px;">
                <div class="col-md-6">
                    <div class="col-md-12">Площадь</div>
                    <div class="col-md-8">
                        <input type="text" name="gg_area" class="garage_attr gg_area form-control" value="<?php if(isset($attributes['area'])){echo $attributes['area'];} ?>">
                    </div>
                    <div class="col-md-4">m2</div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-12">Тип здания</div>
                    <div class="col-md-1"><input type="radio" name="gg_building_type" class="garage_attr gg_building_type" value="gg_building1" <?php if(isset($attributes['building_type']) && $attributes['building_type']=='gg_building1'){echo 'checked';} ?>></div>
                    <div class="col-md-3">Камень</div>
                    <div class="col-md-1"><input type="radio" name="gg_building_type" class="garage_attr gg_building_type" value="gg_building2" <?php if(isset($attributes['building_type']) && $attributes['building_type']=='gg_building2'){echo 'checked';} ?>></div>
                    <div class="col-md-3">Металл</div>
                    <div class="col-md-1"></div>
                </div>
            </div>
            <div class="col-md-6" style="background: #F9FAFC;padding: 50px 25px;">

            </div>
            <?php
            break;
        }
    }
    ?>
</div>
<style>th{text-align: center}</style>
<table class="table table-hover table-bordered" style="text-align:center">
    <tr>
        <th>На армянском</th>
        <th>На русском</th>
        <th>На английском</th>
    </tr>
    <tr>
        <td><input type="text" name="title_hy" placeholder="Название" class="form-control" value="<?php echo $post->post_title; ?>" style="font-size: 14px;"></td>
        <td><input type="text" name="title_ru" placeholder="Название" class="form-control" value="<?php echo $post->post_title_ru; ?>" style="font-size: 14px;"></td>
        <td><input type="text" name="title_en" placeholder="Название" class="form-control" value="<?php echo $post->post_title_en; ?>" style="font-size: 14px;"></td>
    </tr>
    <tr>
        <td><textarea name="content_hy" placeholder="Описание" class="form-control" style="height: 100px;font-size: 14px;resize: none;"><?php echo $post->post_content; ?></textarea></td>
        <td><textarea name="content_ru" placeholder="Описание" class="form-control" style="height: 100px;font-size: 14px;resize: none;"><?php echo $post->post_content_ru; ?></textarea></td>
        <td><textarea name="content_en" placeholder="Описание" class="form-control" style="height: 100px;font-size: 14px;resize: none;"><?php echo $post->post_content_en; ?></textarea></td>
    </tr>
    <tr>
        <td><textarea name="keywords_hy" placeholder="Keywords" class="form-control" style="font-size: 14px;resize: none;"><?php echo $post->post_keyword; ?></textarea></td>
        <td><textarea name="keywords_ru" placeholder="Keywords" class="form-control" style="font-size: 14px;resize: none;"><?php echo $post->post_keyword_ru; ?></textarea></td>
        <td><textarea name="keywords_en" placeholder="Keywords" class="form-control" style="font-size: 14px;resize: none;"><?php echo $post->post_keyword_en; ?></textarea></td>
    </tr>
</table>
<?php global $user_level; if($user_level==1){ ?>
<div class="col-md-12" style="margin-top: 10px">
	<div class="col-md-4">
		<select name="pst_user_id_c" class="form-control">
			<?php
			$result_users = mwdb_select("SELECT user_id,user_name FROM users WHERE user_type!=3 ORDER BY user_type",array());
			foreach($result_users as $row_user){
				?>
				<option <?php if($post->post_user==$row_user->user_id){echo 'selected';} ?> value="<?=$row_user->user_id?>"><?=$row_user->user_name?></option>
				<?php
			}
			?>
		</select>
	</div>
</div>
<?php } ?>
<div class="col-md-12" style="margin-top: 20px">
	<input type="checkbox" name="published" value="1" <?php if($post->published==1){echo 'checked';} ?>><label>Опубликован</label>
</div>
<div class="col-md-12" style="text-align: center;margin-top: 20px">
    <input type="submit" name="add_estate" value="Сохранить">
    <a href="https://alex-r.am/estate/<?php echo $post->post_id; ?>/?user_id=<?php echo $_SESSION['user_id']; ?>" class="btn btn-default" target="_blank" style="background: #e6c924">
        <i class="fa fa-eye" data-toggle="tooltip" title="" data-original-title="Просмотр"></i>
    </a>
</div>
</form>
</div>
</div>
</div>
</div>
</section>
<script>
    var page = 'add_post';
    var edit_region = '<?php echo $post->post_region; ?>';
    var edit_city = '<?php echo $post->post_city; ?>';
    var edit_area = '<?php echo $post->post_area; ?>';
    var edit_microarea = '<?php echo $post->post_microarea; ?>';
    var edit_street = '<?php echo $post->post_street; ?>';
    var subpage = 'view';
</script>