<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZ2D02mcYa-GES5qXKTrVOHIabQirrDi4&libraries=places"></script>
<style>
    .a_hide:hover{cursor: pointer;height: 455px !important;}
    .a_hide1:hover{cursor: pointer;height: 95px !important;}
    .a_hide2:hover{cursor: pointer;height: 87px !important;}
    .a_hide3:hover{cursor: pointer;height: 40px !important;}
    .a_hide4:hover{cursor: pointer;height: 165px !important;}
    .h_hide:hover{cursor: pointer}
    .com_etaj:hover{height: 255px !important;}
</style>
<link rel="stylesheet" href="<?php echo base_url() ?>css/chosen.css">
<?php
if(isset($_POST['add_estate']) && $_POST['add_estate']=='Добавить'){
    $owner = new PostOwner(0,$_POST['owner_name'],$_POST['owner_phone1'],$_POST['owner_phone2'],$_POST['owner_phone3'],$_POST['owner_email'],$_POST['owner_details'],$_POST['owner_skype']);
    $post_owner = $owner->insert2();
    switch($_POST['category']){
        case '5':{
            if(isset($_POST['ap_repair'])){
                $rep_type = $_POST['ap_repair'];
                $dop_index = $rep_type.'_dop';
                if(isset($_POST[$dop_index])){
                    $repair_dop = $_POST[$dop_index];
                }
                else{
                    $repair_dop = '';
                }
                $repair = json_encode(array("repair"=>$rep_type,"repair_dop"=>$repair_dop));
            }
            else{
                $repair = '';
            }
            if(isset($_POST['ap_balcony'])){
                $array_balc = array();
                foreach($_POST['ap_balcony'] as $balcony_type) {
                    $dop_index = $balcony_type . '_dop';
                    if (isset($_POST[$dop_index])) {
                        $balcony_dop = $_POST[$dop_index];
                    } else {
                        $balcony_dop = '';
                    }
                    array_push($array_balc,array("balcony" => $balcony_type, "balcony_count" => $balcony_dop));
                }
                $balcony = json_encode($array_balc);
            }
            else{
                $balcony = '';
            }
            $sett_array = array(
                "area"=>isset($_POST['ap_area'])?$_POST['ap_area']:'',
                "rooms"=>isset($_POST['ap_rooms'])?$_POST['ap_rooms']:'',
                "floorer"=>isset($_POST['ap_floorer'])?$_POST['ap_floorer']:'',
                "floor"=>isset($_POST['ap_floor'])?$_POST['ap_floor']:'',
                "floor_name"=>isset($_POST['ap_floor_name'])?$_POST['ap_floor_name']:'',
                "repair"=>$repair,
                "balcony"=>$balcony,
                "garage"=>isset($_POST['ap_garage'])?$_POST['ap_garage']:'',
                "basement"=>isset($_POST['ap_basement'])?$_POST['ap_basement']:'',
                "water"=>isset($_POST['ap_water'])?$_POST['ap_water']:'',
                "gas"=>isset($_POST['ap_gas'])?$_POST['ap_gas']:'',
                "building_project"=>isset($_POST['ap_building_project'])?$_POST['ap_building_project']:'',
                "building_position"=>isset($_POST['ap_building_position'])?$_POST['ap_building_position']:'',
                "building_type"=>isset($_POST['ap_building_type'])?$_POST['ap_building_type']:'',
                "housetop"=>isset($_POST['ap_housetop'])?$_POST['ap_housetop']:''
            );
            $settings = json_encode($sett_array);
            break;
        }
        case '6':{
            if(isset($_POST['hm_repair'])){
                $rep_type = $_POST['hm_repair'];
                $dop_index = $rep_type.'_dop';
                if(isset($_POST[$dop_index])){
                    $repair_dop = $_POST[$dop_index];
                }
                else{
                    $repair_dop = '';
                }
                $repair = json_encode(array("repair"=>$rep_type,"repair_dop"=>$repair_dop));
            }
            else{
                $repair = '';
            }
            if(isset($_POST['hm_balcony'])){
                $balcony_type = $_POST['hm_balcony'];
                $dop_index = $balcony_type.'_dop';
                if(isset($_POST[$dop_index])){
                    $balcony_dop = $_POST[$dop_index];
                }
                else{
                    $balcony_dop = '';
                }
                $balcony = json_encode(array("balcony"=>$balcony_type,"balcony_count"=>$balcony_dop));
            }
            else{
                $balcony = '';
            }
            if(isset($_POST['hm_garage'])){
                $garage_type = $_POST['hm_garage'];
                $dop_index1 = $rep_type.'_dop1';
                $dop_index2 = $rep_type.'_dop2';
                if(isset($_POST[$dop_index1])){
                    $garage_count = $_POST[$dop_index1];
                }
                else{
                    $garage_count = 1;
                }
                if(isset($_POST[$dop_index2])){
                    $garage_area = $_POST[$dop_index2];
                }
                else{
                    $garage_area = '';
                }
                $garage = json_encode(array("garage_type"=>$garage_type,"garage_count"=>$garage_count,"garage_area"=>$garage_area));
            }
            else{
                $garage = '';
            }
            if(isset($_POST['hm_basement'])){
                $basement_status = $_POST['hm_basement'];
                $dop_index = $basement_status.'_dop';
                $basement = json_encode(array("basement"=>$basement_status,"basement_area"=>$_POST[$dop_index]));
            }
            else{
                $basement = '';
            }
            $sett_array = array(
                "total_area"=>isset($_POST['hm_total_area'])?$_POST['hm_total_area']:'',
                "home_area"=>isset($_POST['hm_home_area'])?$_POST['hm_home_area']:'',
                "rooms"=>isset($_POST['hm_rooms'])?$_POST['hm_rooms']:'',
                "floorer"=>isset($_POST['hm_floorer'])?$_POST['hm_floorer']:'',
                "floor"=>isset($_POST['hm_floor'])?$_POST['hm_floor']:'',
                "repair"=>$repair,
                "balcony"=>$balcony,
                "garage"=>$garage,
                "basement"=>$basement,
                "water"=>isset($_POST['hm_water'])?$_POST['hm_water']:'',
                "gas"=>isset($_POST['hm_gas'])?$_POST['hm_gas']:'',
                "heating_system"=>isset($_POST['hm_heating_system'])?$_POST['hm_heating_system']:'',
                "building_position"=>isset($_POST['hm_building_position'])?$_POST['hm_building_position']:'',
                "building_type"=>isset($_POST['hm_building_type'])?$_POST['hm_building_type']:''
            );
            $settings = json_encode($sett_array);
            break;
        }
        case '7':{
            $sett_array = array(
                "type"=>isset($_POST['cm_type'])?$_POST['cm_type']:'',
                "area"=>isset($_POST['cm_area'])?$_POST['cm_area']:'',
                "rooms"=>isset($_POST['cm_rooms'])?$_POST['cm_rooms']:'',
                "floor"=>isset($_POST['cm_floor'])?$_POST['cm_floor']:'',
                "floorer"=>isset($_POST['cm_floorer'])?$_POST['cm_floorer']:'',
                "floor_name"=>isset($_POST['cm_floor_name'])?$_POST['cm_floor_name']:'',
                "showcases"=>isset($_POST['cm_showcases'])?$_POST['cm_showcases']:'',
                "entrance"=>isset($_POST['cm_entrance'])?$_POST['cm_entrance']:'',
                "repair"=>isset($_POST['cm_repair'])?$_POST['cm_repair']:'',
                "building_position"=>isset($_POST['ap_building_position'])?$_POST['ap_building_position']:'',
                "building_type"=>isset($_POST['ap_building_type'])?$_POST['ap_building_type']:'',
            );
            $settings = json_encode($sett_array);
            break;
        }
        case '8':{
            if(isset($_POST['ld_high_voltage_pillars'])){
                $high_voltage_pillars_exists = $_POST['ld_high_voltage_pillars'];
                $high_voltage_pillars_dop = $_POST[$high_voltage_pillars_exists.'_dop'];
                $high_voltage_pillars = json_encode(array("high_voltage_pillars_exists"=>$high_voltage_pillars_exists,"high_voltage_pillars_dop"=>$high_voltage_pillars_dop));
            }
            else{
                $high_voltage_pillars = '';
            }
            if(isset($_POST['ld_sewage'])){
                $sewage_status = $_POST['ld_sewage'];
                if(isset($_POST[$sewage_status.'_dop'])){
                    $sewage_dop = $_POST[$sewage_status.'_dop'];
                }
                else{
                    $sewage_dop = '';
                }
                $sewage = json_encode(array("sewage_status"=>$sewage_status,"sewage_dop"=>$sewage_dop));
            }
            else{
                $sewage = '';
            }
            $sett_array = array(
                "area"=>isset($_POST['ld_area'])?$_POST['ld_area']:'',
                "buildings"=>isset($_POST['ld_buildings'])?$_POST['ld_buildings']:'',
                "water_drink"=>isset($_POST['ld_water_drink'])?$_POST['ld_water_drink']:'',
                "water_irrigation"=>isset($_POST['ld_water_irrigation'])?$_POST['ld_water_irrigation']:'',
                "gas"=>isset($_POST['ld_gas'])?$_POST['ld_gas']:'',
                "electricity"=>isset($_POST['ld_electricity'])?$_POST['ld_electricity']:'',
                "high_voltage_pillars"=>$high_voltage_pillars,
                "sewage"=>$sewage,
                "fence"=>isset($_POST['ld_fence'])?$_POST['ld_fence']:'',
                "fruit_trees"=>isset($_POST['fruit_trees'])?$_POST['fruit_trees']:''
            );
            $settings = json_encode($sett_array);
            break;
        }
        case '9':{
            $sett_array = array(
                "area"=>isset($_POST['gg_area'])?$_POST['gg_area']:'',
                "building_type"=>isset($_POST['gg_building_type'])?$_POST['gg_building_type']:''
            );
            $settings = json_encode($sett_array);
            break;
        }
        case '10':{
            $settings = '{}';
            break;
        }
    }
    $images_array = array();
    if(isset($_POST['image_u'])){
        $mh = 0;
        foreach($_POST['image_u'] as $key=>$val){
            if(isset($_POST['image_hide'][$key]) && $_POST['image_hide'][$key]=='1'){
                $h = '1';
            }
            else{
                $h = '0';
            }
            $images_array[$mh] = array("img"=>$val,"order"=>$_POST['image_order'][$key],"hide"=>$h);
            $mh++;
        }
    }
    $images = json_encode($images_array);
    $meta_array = array();
    if(isset($_POST['sale']) && $_POST['sale']=='1'){
        $meta_array['sale'] = 1;
        $meta_array['price_type'] = $_POST['price_type'];
        if($meta_array['price_type']=='1'){
            $meta_array['price_number'] = $_POST['price_number'];
            $meta_array['price_currency'] = $_POST['price_currency'];
        }
        if(isset($_POST['obmen'])){
            $meta_array['obmen'] = $_POST['obmen'];
        }
        else{
            $meta_array['obmen'] = 0;
        }
    }
    else{
        $meta_array['sale'] = 0;
    }
    if(isset($_POST['rent']) && $_POST['rent']=='1'){
        $meta_array['rent'] = 1;
        if(isset($_POST['rent_type1']) && $_POST['rent_type1']=='1'){
            $meta_array['rent_type1'] = 1;
            $meta_array['rent_cost1'] = $_POST['rent_cost1'];
            $meta_array['rent_currency1'] = $_POST['rent_currency1'];
        }
        else{
            $meta_array['rent_type1'] = 0;
        }
        if(isset($_POST['rent_type2']) && $_POST['rent_type2']=='1'){
            $meta_array['rent_type2'] = 1;
            $meta_array['rent_cost2'] = $_POST['rent_cost2'];
            $meta_array['rent_currency2'] = $_POST['rent_currency2'];
        }
        else{
            $meta_array['rent_type2'] = 0;
        }
        if(isset($_POST['rent_type3']) && $_POST['rent_type3']=='1'){
            $meta_array['rent_type3'] = 1;
            $meta_array['rent_cost3'] = $_POST['rent_cost3'];
            $meta_array['rent_currency3'] = $_POST['rent_currency3'];
        }
        else{
            $meta_array['rent_type3'] = 0;
        }
    }
    else{
        $meta_array['rent'] = 0;
    }
    $meta = json_encode($meta_array);
    if($_POST['app_number']!=''){
        $app_number = ' кв. '.$_POST['app_number'];
    }
    else{
        $app_number = '';
    }
	if(isset($_POST['pst_user_id_c']) && (int)$_POST['pst_user_id_c']>0){
		$pst_user_id_c = (int)$_POST['pst_user_id_c'];
	}
	else{
		$pst_user_id_c = $_SESSION['user_id'];
	}
    $ad = new Post('hy',0,$_POST['code'],$_POST['title_hy'],$_POST['content_hy'],$_POST['keywords_hy'],$_POST['title_ru'],$_POST['content_ru'],$_POST['keywords_ru'],$_POST['title_en'],$_POST['content_en'],$_POST['keywords_en'],$_POST['category'],$_POST['region'],$_POST['city'],$_POST['area'],$_POST['microarea'],$_POST['street'],$_POST['address'].$app_number,$pst_user_id_c,$post_owner,$settings,$images,$meta,$_POST['map_address'],$_POST['status_ad'],$_POST['post_details']);
    $ad->insert();
	mwdb_query("UPDATE posts SET post_arajarkam='{var}' WHERE post_id={var}",array($_POST['arajark'],$ad->post_id));
    echo '<script>document.location.href="https://user.alex-r.am/index.php?action=post&subaction=all"</script>';
}
if(isset($_POST['add_estate']) && $_POST['add_estate']=='Сохранить'){
    $owner = new PostOwner(0,$_POST['owner_name'],$_POST['owner_phone1'],$_POST['owner_phone2'],$_POST['owner_phone3'],$_POST['owner_email'],$_POST['owner_details'],$_POST['owner_skype']);
    $post_owner = $owner->insert();
    switch($_POST['category']){
        case '5':{
            if(isset($_POST['ap_repair'])){
                $rep_type = $_POST['ap_repair'];
                $dop_index = $rep_type.'_dop';
                if(isset($_POST[$dop_index])){
                    $repair_dop = $_POST[$dop_index];
                }
                else{
                    $repair_dop = '';
                }
                $repair = json_encode(array("repair"=>$rep_type,"repair_dop"=>$repair_dop));
            }
            else{
                $repair = '';
            }
            if(isset($_POST['ap_balcony'])){
                $array_balc = array();
                foreach($_POST['ap_balcony'] as $balcony_type) {
                    $dop_index = $balcony_type . '_dop';
                    if (isset($_POST[$dop_index])) {
                        $balcony_dop = $_POST[$dop_index];
                    } else {
                        $balcony_dop = '';
                    }
                    array_push($array_balc,array("balcony" => $balcony_type, "balcony_count" => $balcony_dop));
                }
                $balcony = json_encode($array_balc);
            }
            else{
                $balcony = '';
            }
            $sett_array = array(
                "area"=>isset($_POST['ap_area'])?$_POST['ap_area']:'',
                "rooms"=>isset($_POST['ap_rooms'])?$_POST['ap_rooms']:'',
                "floorer"=>isset($_POST['ap_floorer'])?$_POST['ap_floorer']:'',
                "floor"=>isset($_POST['ap_floor'])?$_POST['ap_floor']:'',
                "floor_name"=>isset($_POST['ap_floor_name'])?$_POST['ap_floor_name']:'',
                "repair"=>$repair,
                "balcony"=>$balcony,
                "garage"=>isset($_POST['ap_garage'])?$_POST['ap_garage']:'',
                "basement"=>isset($_POST['ap_basement'])?$_POST['ap_basement']:'',
                "water"=>isset($_POST['ap_water'])?$_POST['ap_water']:'',
                "gas"=>isset($_POST['ap_gas'])?$_POST['ap_gas']:'',
                "building_project"=>isset($_POST['ap_building_project'])?$_POST['ap_building_project']:'',
                "building_position"=>isset($_POST['ap_building_position'])?$_POST['ap_building_position']:'',
                "building_type"=>isset($_POST['ap_building_type'])?$_POST['ap_building_type']:'',
                "housetop"=>isset($_POST['ap_housetop'])?$_POST['ap_housetop']:''
            );
            $settings = json_encode($sett_array);
            break;
        }
        case '6':{
            if(isset($_POST['hm_repair'])){
                $rep_type = $_POST['hm_repair'];
                $dop_index = $rep_type.'_dop';
                if(isset($_POST[$dop_index])){
                    $repair_dop = $_POST[$dop_index];
                }
                else{
                    $repair_dop = '';
                }
                $repair = json_encode(array("repair"=>$rep_type,"repair_dop"=>$repair_dop));
            }
            else{
                $repair = '';
            }
            if(isset($_POST['hm_balcony'])){
                $balcony_type = $_POST['hm_balcony'];
                $dop_index = $balcony_type.'_dop';
                if(isset($_POST[$dop_index])){
                    $balcony_dop = $_POST[$dop_index];
                }
                else{
                    $balcony_dop = '';
                }
                $balcony = json_encode(array("balcony"=>$balcony_type,"balcony_count"=>$balcony_dop));
            }
            else{
                $balcony = '';
            }
            if(isset($_POST['hm_garage'])){
                $garage_type = $_POST['hm_garage'];
                $dop_index1 = $rep_type.'_dop1';
                $dop_index2 = $rep_type.'_dop2';
                if(isset($_POST[$dop_index1])){
                    $garage_count = $_POST[$dop_index1];
                }
                else{
                    $garage_count = 1;
                }
                if(isset($_POST[$dop_index2])){
                    $garage_area = $_POST[$dop_index2];
                }
                else{
                    $garage_area = '';
                }
                $garage = json_encode(array("garage_type"=>$garage_type,"garage_count"=>$garage_count,"garage_area"=>$garage_area));
            }
            else{
                $garage = '';
            }
            if(isset($_POST['hm_basement'])){
                $basement_status = $_POST['hm_basement'];
                $dop_index = $basement_status.'_dop';
                $basement = json_encode(array("basement"=>$basement_status,"basement_area"=>$_POST[$dop_index]));
            }
            else{
                $basement = '';
            }
            $sett_array = array(
                "total_area"=>isset($_POST['hm_total_area'])?$_POST['hm_total_area']:'',
                "home_area"=>isset($_POST['hm_home_area'])?$_POST['hm_home_area']:'',
                "rooms"=>isset($_POST['hm_rooms'])?$_POST['hm_rooms']:'',
                "floorer"=>isset($_POST['hm_floorer'])?$_POST['hm_floorer']:'',
                "floor"=>isset($_POST['hm_floor'])?$_POST['hm_floor']:'',
                "repair"=>$repair,
                "balcony"=>$balcony,
                "garage"=>$garage,
                "basement"=>$basement,
                "water"=>isset($_POST['hm_water'])?$_POST['hm_water']:'',
                "gas"=>isset($_POST['hm_gas'])?$_POST['hm_gas']:'',
                "heating_system"=>isset($_POST['hm_heating_system'])?$_POST['hm_heating_system']:'',
                "building_position"=>isset($_POST['hm_building_position'])?$_POST['hm_building_position']:'',
                "building_type"=>isset($_POST['hm_building_type'])?$_POST['hm_building_type']:''
            );
            $settings = json_encode($sett_array);
            break;
        }
        case '7':{
            $sett_array = array(
                "type"=>isset($_POST['cm_type'])?$_POST['cm_type']:'',
                "area"=>isset($_POST['cm_area'])?$_POST['cm_area']:'',
                "rooms"=>isset($_POST['cm_rooms'])?$_POST['cm_rooms']:'',
                "floor"=>isset($_POST['cm_floor'])?$_POST['cm_floor']:'',
                "floorer"=>isset($_POST['cm_floorer'])?$_POST['cm_floorer']:'',
                "floor_name"=>isset($_POST['cm_floor_name'])?$_POST['cm_floor_name']:'',
                "showcases"=>isset($_POST['cm_showcases'])?$_POST['cm_showcases']:'',
                "entrance"=>isset($_POST['cm_entrance'])?$_POST['cm_entrance']:'',
                "repair"=>isset($_POST['cm_repair'])?$_POST['cm_repair']:'',
                "building_position"=>isset($_POST['ap_building_position'])?$_POST['ap_building_position']:'',
                "building_type"=>isset($_POST['ap_building_type'])?$_POST['ap_building_type']:'',
            );
            $settings = json_encode($sett_array);
            break;
        }
        case '8':{
            if(isset($_POST['ld_high_voltage_pillars'])){
                $high_voltage_pillars_exists = $_POST['ld_high_voltage_pillars'];
                $high_voltage_pillars_dop = $_POST[$high_voltage_pillars_exists.'_dop'];
                $high_voltage_pillars = json_encode(array("high_voltage_pillars_exists"=>$high_voltage_pillars_exists,"high_voltage_pillars_dop"=>$high_voltage_pillars_dop));
            }
            else{
                $high_voltage_pillars = '';
            }
            if(isset($_POST['ld_sewage'])){
                $sewage_status = $_POST['ld_sewage'];
                if(isset($_POST[$sewage_status.'_dop'])){
                    $sewage_dop = $_POST[$sewage_status.'_dop'];
                }
                else{
                    $sewage_dop = '';
                }
                $sewage = json_encode(array("sewage_status"=>$sewage_status,"sewage_dop"=>$sewage_dop));
            }
            else{
                $sewage = '';
            }
            $sett_array = array(
                "area"=>isset($_POST['ld_area'])?$_POST['ld_area']:'',
                "buildings"=>isset($_POST['ld_buildings'])?$_POST['ld_buildings']:'',
                "water_drink"=>isset($_POST['ld_water_drink'])?$_POST['ld_water_drink']:'',
                "water_irrigation"=>isset($_POST['ld_water_irrigation'])?$_POST['ld_water_irrigation']:'',
                "gas"=>isset($_POST['ld_gas'])?$_POST['ld_gas']:'',
                "electricity"=>isset($_POST['ld_electricity'])?$_POST['ld_electricity']:'',
                "high_voltage_pillars"=>$high_voltage_pillars,
                "sewage"=>$sewage,
                "fence"=>isset($_POST['ld_fence'])?$_POST['ld_fence']:'',
                "fruit_trees"=>isset($_POST['fruit_trees'])?$_POST['fruit_trees']:''
            );
            $settings = json_encode($sett_array);
            break;
        }
        case '9':{
            $sett_array = array(
                "area"=>isset($_POST['gg_area'])?$_POST['gg_area']:'',
                "building_type"=>isset($_POST['gg_building_type'])?$_POST['gg_building_type']:''
            );
            $settings = json_encode($sett_array);
            break;
        }
        case '10':{
            $settings = '{}';
            break;
        }
    }
    $images_array = array();
    if(isset($_POST['image_u'])){
        $mh = 0;
        foreach($_POST['image_u'] as $key=>$val){
            if(isset($_POST['image_hide'][$key]) && $_POST['image_hide'][$key]=='1'){
                $h = '1';
            }
            else{
                $h = '0';
            }
            $images_array[$mh] = array("img"=>$val,"order"=>$_POST['image_order'][$key],"hide"=>$h);
            $mh++;
        }
    }
    $images = json_encode($images_array);
    $meta_array = array();
    if(isset($_POST['sale']) && $_POST['sale']=='1'){
        $meta_array['sale'] = 1;
        $meta_array['price_type'] = $_POST['price_type'];
        if($meta_array['price_type']=='1'){
            $meta_array['price_number'] = $_POST['price_number'];
            $meta_array['price_currency'] = $_POST['price_currency'];
        }
        if(isset($_POST['obmen'])){
            $meta_array['obmen'] = $_POST['obmen'];
        }
        else{
            $meta_array['obmen'] = 0;
        }
    }
    else{
        $meta_array['sale'] = 0;
    }
    if(isset($_POST['rent']) && $_POST['rent']=='1'){
        $meta_array['rent'] = 1;
        if(isset($_POST['rent_type1']) && $_POST['rent_type1']=='1'){
            $meta_array['rent_type1'] = 1;
            $meta_array['rent_cost1'] = $_POST['rent_cost1'];
            $meta_array['rent_currency1'] = $_POST['rent_currency1'];
        }
        else{
            $meta_array['rent_type1'] = 0;
        }
        if(isset($_POST['rent_type2']) && $_POST['rent_type2']=='1'){
            $meta_array['rent_type2'] = 1;
            $meta_array['rent_cost2'] = $_POST['rent_cost2'];
            $meta_array['rent_currency2'] = $_POST['rent_currency2'];
        }
        else{
            $meta_array['rent_type2'] = 0;
        }
        if(isset($_POST['rent_type3']) && $_POST['rent_type3']=='1'){
            $meta_array['rent_type3'] = 1;
            $meta_array['rent_cost3'] = $_POST['rent_cost3'];
            $meta_array['rent_currency3'] = $_POST['rent_currency3'];
        }
        else{
            $meta_array['rent_type3'] = 0;
        }
    }
    else{
        $meta_array['rent'] = 0;
    }
    $meta = json_encode($meta_array);
    if($_POST['app_number']!=''){
        $app_number = ' кв. '.$_POST['app_number'];
    }
    else{
        $app_number = '';
    }
	if(isset($_POST['pst_user_id_c']) && (int)$_POST['pst_user_id_c']>0){
		$pst_user_id_c = (int)$_POST['pst_user_id_c'];
	}
	else{
		$pst_user_id_c = $_SESSION['user_id'];
	}
    $ad = new Post('hy',0,$_POST['code'],$_POST['title_hy'],$_POST['content_hy'],$_POST['keywords_hy'],$_POST['title_ru'],$_POST['content_ru'],$_POST['keywords_ru'],$_POST['title_en'],$_POST['content_en'],$_POST['keywords_en'],$_POST['category'],$_POST['region'],$_POST['city'],$_POST['area'],$_POST['microarea'],$_POST['street'],$_POST['address'].$app_number,$pst_user_id_c,$post_owner,$settings,$images,$meta,$_POST['map_address'],$_POST['status_ad'],$_POST['post_details']);
    $ad->insert();
	mwdb_query("UPDATE posts SET post_arajarkam='{var}' WHERE post_id={var}",array($_POST['arajark'],$ad->post_id));
	mwdb_query("UPDATE posts SET post_publish='{var}' WHERE post_id={var}",array(0,$ad->post_id));
    echo '<script>document.location.href="https://user.alex-r.am/index.php?action=post&subaction=all"</script>';
}
?>
<section class="content-header">
    <h1>Добавить недвижимость</h1>
</section>
<style>
    .dz_home_ch {
        position: absolute;
        z-index: 100000;
        top: 0px;
        right: 0px;
        padding: 5px;
        background-color: rgba(255, 255, 255, 0.65098);
    }
    .dz_home_ch input {
        width: 76px;
        height: 20px;
        float: left;
    }
</style>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body table-responsive">
                    <form action="" method="post" enctype="multipart/form-data">
                    <div class="col-md-12" style="margin-bottom: 0px">
                        <label style="float: left;    margin-top: 5px;">Код:</label>
                        <div class="col-md-1"><input type="text" name="code" class="form-control"></div>
                        <div style="float: right">
                            <div style="float: left;margin: 7px;">Статус:</div>
                            <div style="float: left">
                                <select name="status_ad" class="form-control">
                                    <option value="0">Обычный</option>
                                    <option value="1">Первичный</option>
                                    <option value="2">TOP</option>
                                </select>
                            </div>
                        </div>
                    </div>

                        <div class="col-md-5" style="margin-bottom: 10px">
                                <div class="col-md-12"><input style="width: 17px;height: 17px" type="checkbox" name="sale" class="sale" value="1"> Продажа</div>
                                <div class="col-md-8 sale_options" style="display: none">
                                    <div class="col-md-12">
                                          <div class="col-md-12" style="margin-bottom:9px;">
                                            <div class="col-md-1"><input type="radio" name="price_type" value="1" checked style="width: 17px;height: 17px"></div>
                                            <div class="col-md-7"><input type="number" name="price_number" class="form-control"></div>
                                            <div class="col-md-3">
                                                <select class="form-control" name="price_currency">
                                                    <option value="USD">USD</option>
                                                    <option value="AMD">AMD</option>
                                                    <option value="EUR">EUR</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                            <div class="col-md-1 no-padding"><input type="radio" name="price_type" value="2" style="width: 17px;height: 17px"></div>
                                            <div class="col-md-11">Договорная</div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-1 no-padding"><input type="checkbox" name="obmen" value="1" style="width: 17px;height: 17px"></div>
                                            <div class="col-md-11">Обмен</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div class="col-md-7" style="margin-bottom: 10px">
                                <div class="col-md-2"><input style="width: 17px;height: 17px" type="checkbox" name="rent" class="rent" value="1"> Аренда</div>
                                <div class="col-md-10 rent_options" style="display: none">
                                    <div class="col-md-12" style="margin-bottom: 10px">
                                        <div class="col-md-1"><input type="checkbox" name="rent_type1" value="1" style="width: 17px;height: 17px"></div>
                                        <div class="col-md-2 no-padding">длительный</div>
                                        <div class="col-md-3">
                                            <select name="rent_time1" class="rent_time1 form-control">
                                                <option value="6m">6 месяцев</option>
                                                <option value="7m">7 месяцев</option>
                                                <option value="8m">8 месяцев</option>
                                                <option value="9m">9 месяцев</option>
                                                <option value="10m">10 месяцев</option>
                                                <option value="11m">11 месяцев</option>
                                                <option value="12m">12 месяцев</option>
                                                <option value="13m">12+ месяцев</option>
                                            </select>
                                        </div>

                                        <div class="col-md-3">
                                            <input type="number" name="rent_cost1" class="form-control" placeholder="Цена">
                                        </div>
                                        <div class="col-md-2">
                                            <select class="form-control" name="rent_currency1">
                                                <option value="USD">USD</option>
                                                <option value="AMD">AMD</option>
                                                <option value="EUR">EUR</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="margin-bottom: 10px">
                                        <div class="col-md-1"><input type="checkbox" name="rent_type2" value="1" style="width: 17px;height: 17px"></div>
                                        <div class="col-md-2 no-padding">кратковременный</div>
                                        <div class="col-md-3">
                                            <select name="rent_time2" class="rent_time2 form-control">
                                                <option value="1m">1 месяц</option>
                                                <option value="2m">2 месяца</option>
                                                <option value="3m">3 месяца</option>
                                                <option value="4m">4 месяца</option>
                                                <option value="5m">5 месяцев</option>
                                                <option value="6m">6 месяцев</option>
                                            </select>
                                        </div>

                                        <div class="col-md-3">
                                            <input type="number" name="rent_cost2" class="form-control" placeholder="Цена">
                                        </div>
                                        <div class="col-md-2">
                                            <select class="form-control" name="rent_currency2">
                                                <option value="USD">USD</option>
                                                <option value="AMD">AMD</option>
                                                <option value="EUR">EUR</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="margin-bottom: 10px">
                                        <div class="col-md-1"><input type="checkbox" name="rent_type3" value="1" style="width: 17px;height: 17px"></div>
                                        <div class="col-md-2 no-padding">посуточнo</div>
                                        <div class="col-md-3">
                                            <select name="rent_time3" class="rent_time3 form-control">
                                                <option value="1d">1 день</option>
                                                <option value="2d">2 дня</option>
                                                <option value="3d">3 дня</option>
                                                <option value="4d">4 дня</option>
                                                <option value="5d">5 дней</option>
                                                <option value="6d">6 дней</option>
                                                <option value="7d">7 дней</option>
                                                <option value="8d">8 дней</option>
                                                <option value="9d">9 дней</option>
                                                <option value="10d">10 дней</option>
                                                <option value="11d">11 дней</option>
                                                <option value="12d">12 дней</option>
                                                <option value="13d">13 дней</option>
                                                <option value="14d">14 дней</option>
                                                <option value="15d">15 дней</option>
                                                <option value="16d">16 дней</option>
                                                <option value="17d">17 дней</option>
                                                <option value="18d">18 дней</option>
                                                <option value="19d">19 дней</option>
                                                <option value="20d">20 дней</option>
                                                <option value="21d">21 дней</option>
                                                <option value="22d">22 дней</option>
                                                <option value="23d">23 дней</option>
                                                <option value="24d">24 дней</option>
                                                <option value="25d">25 дней</option>
                                                <option value="26d">26 дней</option>
                                                <option value="27d">27 дней</option>
                                                <option value="28d">28 дней</option>
                                                <option value="29d">29 дней</option>
                                                <option value="30d">30 дней</option>
                                            </select>
                                        </div>

                                        <div class="col-md-3">
                                            <input type="number" name="rent_cost3" class="form-control" placeholder="Цена">
                                        </div>
                                        <div class="col-md-2">
                                            <select class="form-control" name="rent_currency3">
                                                <option value="USD">USD</option>
                                                <option value="AMD">AMD</option>
                                                <option value="EUR">EUR</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                     <div class="col-md-12">
                         <div class="col-md-4">
                        <select name="category" class="category form-control" onchange="get_category_attributes(this);" style="    background: #C4EBFF;">
                            <option value="0">-- Категория --</option>
                            <?php
                            $cats = mwdb_select("SELECT category_id,category_title_ru FROM category WHERE {var} ORDER BY category_order",array(1));
                            foreach($cats as $cat){
                                ?>
                                <option value="<?php echo $cat->category_id; ?>"><?php echo $cat->category_title_ru; ?></option>
                                <?php
                            }
                            ?>
                        </select>

                            <div class="col-md-12">
                                <select name="region" class="region form-control" onchange="region_change(this)">
                                    <option value="0">-- Регион --</option>
                                </select>
                            </div>
                            <div class="col-md-12">
                                <select name="city" class="city form-control" onchange="city_change(this)">
                                    <option value="0">-- Город/община --</option>
                                </select>
                            </div>
                            <div class="col-md-12">
                                <select name="area" class="area form-control" onchange="area_change(this)">
                                    <option value="0">-- Район --</option>
                                </select>
                            </div>


                            <div class="col-md-12">
                                <select name="microarea" class="microarea form-control" onchange="microarea_change(this)">
                                    <option value="0">-- Микрорайон --</option>
                                </select>
                            </div>
                            <div class="col-md-12">
                                <select name="street" class="street_selecton street form-control" data-placeholder="-- Улица --" tabindex="2">
                                </select>
                            </div>
                            <div class="col-md-6">
                                <input type="text" name="address" class="address form-control" placeholder="Дом" autocomplete="off">
                            </div>
                            <div class="col-md-6">
                                <input type="text" name="app_number" class="app_number form-control" placeholder="Квартира" autocomplete="off">
                            </div>



                             <div style="display: inline-block;width: 100%;border-top: 3px solid #F00;border-bottom: 3px solid #FF0000;">
                                 <div class="col-md-6">
                                     <input type="text" name="owner_name" class="owner_name form-control" placeholder="Имя " style="margin-bottom: 3px;">
                                     <input type="text" name="owner_phone1" class="owner_phone1 form-control" placeholder="Телефон1 " style="    margin-bottom: 3px;">
                                     <input type="text" name="owner_phone2" class="owner_phone2 form-control" placeholder="Телефон2 " style="    margin-bottom: 3px;">
                                     <input type="text" name="arajark" class="owner_phone3 form-control" placeholder="Arajark.am " style="    margin-bottom: 3px;">
                                     <input type="email" name="owner_email" class="owner_email form-control" placeholder="E-mail " style="    margin-bottom: 3px;">
                                     <input type="skype" name="owner_skype" class="owner_skype form-control" placeholder="Skype ">
                                 </div>
                                 <div class="col-md-6">
                                     <textarea name="owner_details" class="owner_details form-control" placeholder="Иные детали " style="height: 100px;"></textarea>
                                 </div>
                                 <div class="col-md-6">
                                     <textarea name="post_details" class="post_details form-control" placeholder="Дополнительно" style="height: 100px;    background: #FFFED2;"></textarea>
                                 </div>
                             </div>


                         </div>
                         <div class="col-md-8">
                    <style>
                        #map{
                            height: 425px;
                            width: 100%;
                        }
                    </style>
                    <div id="map" class="md-col-12"></div>
                    <input type="hidden" name="map_address" class="map_address" value="">
                    <script type="text/javascript">
                        function initialize() {
                            var mapOptions = {
                                center: { lat: 40.177134, lng: 44.515690},
                                zoom: 13
                            };
                            var map = new google.maps.Map(document.getElementById('map'),
                                mapOptions);
                            var marker = new google.maps.Marker({
                                map: map,
                                draggable: true,
                                position: mapOptions.center
                            });
                            google.maps.event.addListener(marker, 'dragend', function() {
                                var position = marker.position;
                                var lat = position.lat();
                                var lng = position.lng();
                                $(".map_address").val(lat+','+lng);
                            });
                        }
                        google.maps.event.addDomListener(window, 'load', initialize);
                    </script>
                             </div>
                         </div>

                        <div class="add_images">
                            <div class="add_images_center">
                                <div id="mydropzone" class="dropzone"></div>
                            </div>
                        </div>
                        <div class="category_attributes" style="margin-top: 10px"></div>
                    <style>th{text-align: center}</style>
                    <table class="table table-hover table-bordered" style="text-align:center">
                        <tr>
                            <th>На армянском</th>
                            <th>На русском</th>
                            <th>На английском</th>
                        </tr>
                        <tr>
                            <td><input type="text" name="title_hy" placeholder="Название" class="form-control" style="font-size: 14px;"></td>
                            <td><input type="text" name="title_ru" placeholder="Название" class="form-control" style="font-size: 14px;"></td>
                            <td><input type="text" name="title_en" placeholder="Название" class="form-control" style="font-size: 14px;"></td>
                        </tr>
                        <tr>
                            <td><textarea name="content_hy" placeholder="Описание" class="form-control" style="height: 100px;font-size: 14px;resize: none;"></textarea></td>
                            <td><textarea name="content_ru" placeholder="Описание" class="form-control" style="height: 100px;font-size: 14px;resize: none;"></textarea></td>
                            <td><textarea name="content_en" placeholder="Описание" class="form-control" style="height: 100px;font-size: 14px;resize: none;"></textarea></td>
                        </tr>
                        <tr>
                            <td><textarea name="keywords_hy" placeholder="Keywords" class="form-control" style="font-size: 14px;resize: none;"></textarea></td>
                            <td><textarea name="keywords_ru" placeholder="Keywords" class="form-control" style="font-size: 14px;resize: none;"></textarea></td>
                            <td><textarea name="keywords_en" placeholder="Keywords" class="form-control" style="font-size: 14px;resize: none;"></textarea></td>
                        </tr>
                    </table>
					<?php global $user_level; if($user_level==1){ ?>
					<div class="col-md-12" style="margin-top: 10px">
						<div class="col-md-4">
							<select name="pst_user_id_c" class="form-control">
								<?php
								$result_users = mwdb_select("SELECT user_id,user_name FROM users WHERE user_type!=3 ORDER BY user_type",array());
								foreach($result_users as $row_user){
									?>
									<option value="<?=$row_user->user_id?>"><?=$row_user->user_name?></option>
									<?php
								}
								?>
							</select>
						</div>
					</div>
					<?php } ?>
                    <div class="col-md-12" style="text-align: center;margin-top: 10px">
                        <input type="submit" name="add_estate" value="Добавить">
						<input type="submit" name="add_estate" value="Сохранить">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    var page = 'add_post';
    var edit_region = 8;
</script>