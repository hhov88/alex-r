<section class="content-header">
    <h1>Поиск недвижимости</h1>
    <div class="serach_block">
        <form name="serach" class="search_form" action="" method="get">
            <input type="search" name="word" class="word">
            <input type="hidden" name="action" value="post">
            <input type="hidden" name="subaction" value="search">
            <select name="type" class="search_type">
                <option value="id" selected>id</option>
                <option value="phone">phone/name</option>								<option value="arajark">Arajark.am ID</option>
            </select>
            <input type="submit" name="s" value="Search">
        </form>
    </div>
    <div style="float:right">
        <span style="background: rgba(210, 236, 161, 0.45);display: inline-block;width: 20px;height: 20px"></span>TOP
        <span style="background: rgba(255, 255, 0, 0.15);display: inline-block;width: 20px;height: 20px;margin-left: 40px"></span>Первичный
    </div>
</section>
<style>th{text-align: center}</style>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body table-responsive">
                    <table class="table table-hover table-bordered" style="text-align:center">
                        <tr>
                            <th>ID</th>
                            <th>Название</th>
                            <th>Площадь</th>
                            <th>Этаж</th>
                            <th>Цена</th>
                            <th>Адрес</th>
                            <th>Владелец</th>
							<th style="width:10%">Доп. инфо</th>
							<th style="width:7%">Иные детали</th>
                            <th>Изображение</th>

                            <th>Дествия</th>
                            <?php if($user_level=='1'){ ?>
                                <th>Оператор</th>
                            <?php } ?>
                        </tr>
                        <?php
                        global $user_level;
                        if($user_level=='1'){
                            $query_part = '1=1';
                        }
                        else{
                            $u_id = $_SESSION['user_id'];
                            $query_part = "post_user=$u_id ";
                        }
                        if(isset($_GET['s']) && isset($_GET["type"]) && isset($_GET["word"]) && $_GET["type"] != "" && $_GET["word"] != ""){
                            if($_GET["type"]=="id"){
                                $query_part .= " AND post_code='".$_GET["word"]."'";
                            }
                            if($_GET["type"]=="phone"){
                                $query_part2 = " (owner_name LIKE '%".$_GET["word"]."%' OR owner_phone1 LIKE '%".$_GET["word"]."%' OR owner_phone2 LIKE '%".$_GET["word"]."%')";
                                $post_oweners_id = mwdb_select("SELECT owner_id FROM post_owner WHERE $query_part2",array());
                                $ids = '';
                                foreach($post_oweners_id as $id){
                                    $ids .= $id->owner_id.",";
                                }
                                $owners_id = substr($ids,0,-1);
                                $query_part .=  " AND post_owner IN (".$owners_id.")";
                            }							if($_GET["type"]=="arajark"){								$query_part .= " AND post_arajarkam='".$_GET["word"]."'";							}
                        }
                        if(isset($_GET['page'])){
                            $page = (int)$_GET['page'];
                            if($page<2){
                                $page = 1;
                            }
                        }
                        else{
                            $page = 1;
                        }
                        $offset = ($page-1)*20;
						//echo $query_part;
                        $result_posts = mwdb_select("SELECT post_id FROM posts WHERE $query_part ORDER BY post_status DESC,post_id DESC LIMIT {var},20",array($offset));
                        foreach($result_posts as $row_post){
                            $post_class = new Post('ru',$row_post->post_id);
                            $post = $post_class->get();
                            $location = '';
                            /*if($post->post_region>0){
                                $location .= mwdb_get_var("SELECT region_ru FROM region WHERE region_id={var}",array($post->post_region)).', ';
                            }*/
                            if($post->post_city>0){
                                $location .= mwdb_get_var("SELECT city_ru FROM city WHERE city_id={var}",array($post->post_city)).', ';
                            }
                            /*if($post->post_area>0){
                                $location .= mwdb_get_var("SELECT area_ru FROM area WHERE area_id={var}",array($post->post_area)).', ';
                            }*/
                            if($post->post_microarea>0){
                                $location .= mwdb_get_var("SELECT area_ru FROM microarea WHERE area_id={var}",array($post->post_microarea)).', ';
                            }
                            if($post->post_street>0){
                                $location .= mwdb_get_var("SELECT street_ru FROM streets WHERE street_id={var}",array($post->post_street)).' ';
                            }
                            $location .= $post->post_address;
                            $owner = json_decode($post->post_owner);
                            $owner_show = ''.$owner->owner_name.'<br> '.$owner->owner_phone1.'<br> '.($owner->owner_phone2!=''?$owner->owner_phone2.'<br> ':'').($owner->owner_phone3!=''?$owner->owner_phone3.'<br> ':'').($owner->owner_email!=''?$owner->owner_email.'<br>':'').($post->arajarkam!=''?'Arajark.am: '.$post->arajarkam:'');
                            $images = json_decode($post->post_images,true);
                            if(isset($images[0]['img']) && $images[0]['img']!=''){
                                $image_primary = 'http://user.alex-r.am/images/100/'.$images[0]['img'];
                            }
                            else{
                                $image_primary = 'http://user.alex-r.am/LOGO.png';
                            }
                            if($post->post_status=='1'){
                                $style_row = 'style="background:rgba(255, 255, 0, 0.15)"';
                            }
                            elseif($post->post_status=='2'){
                                $style_row = 'style="background:rgba(210, 236, 161, 0.45);"';
                            }
							elseif($post->post_status=='6' || $post->post_status=='5'){
                                $style_row = 'style="background: rgba(255, 0, 0, 0.15);"';
                            }
                            else{
                                $style_row = '';
                            }
                            $settings = json_decode($post->post_meta);
                            if(isset($settings->sale) && $settings->sale=='1'){
                                $price = $settings->price_number.' '.$settings->price_currency;
                            }
                            if(isset($settings->rent) && $settings->rent=='1'){
                                $price .= '<br>Аренда: ';
                                if(isset($settings->rent_cost1) && (double)$settings->rent_cost1>0){
                                    $price .= $settings->rent_cost1.' '.$settings->rent_currency1;
                                }
                                elseif(isset($settings->rent_cost2) && (double)$settings->rent_cost2>0){
                                    $price .= $settings->rent_cost2.' '.$settings->rent_currency2;
                                }
                                elseif(isset($settings->rent_cost3) && (double)$settings->rent_cost3>0){
                                    $price .= $settings->rent_cost3.' '.$settings->rent_currency3;
                                }
                            }
                            $attributes = json_decode($post->settings);
                            if(isset($attributes->area)){
                                $area = $attributes->area;
                            }
                            elseif($attributes->home_area){
                                $area = $attributes->home_area;
                            }
                            else{
                                $area = '';
                            }
                            $fl = '';
                            if(isset($attributes->floor) && $attributes->floor!=''){
                                $fl .= $attributes->floor;
                            }
                            if(isset($attributes->floorer) && $attributes->floorer!=''){
                                $fl .= '/'.$attributes->floorer;
                            }
                            ?>
                            <tr <?php echo $style_row; ?>>
                                <td><?php echo $post->post_id; ?></td>
                                <td><?php echo $post->post_title_ru; ?></td>
                                <td><?php echo $area; ?> м2</td>
                                <td><?php echo $fl; ?></td>
                                <td><?php echo $price; ?></td>
                                <td><?php echo $location; ?></td>
                                <td><?php echo $owner_show; ?></td>
								<td><?php echo nl2br($post->post_dop); ?></td>
								<td><?php echo nl2br($owner->owner_settings); ?></td>
                                <td><img src="<?php echo $image_primary; ?>" width="100"></td>

                                <td>
                                    <div class="btn-group">
                                        <a href="http://alex-r.am/estate/<?php echo $post->post_id; ?>/?user_id=<?php echo $_SESSION['user_id']; ?>" class="btn btn-default" target="_blank" style="background: #e6c924">
                                            <i class="fa fa-eye" data-toggle="tooltip" title="" data-original-title="Просмотр"></i>
                                        </a>
                                        <a href="http://user.alex-r.am/index.php?action=post&subaction=view&id=<?php echo $post->post_id; ?>" class="btn btn-default" style="background: #59e614">
                                            <i class="fa fa-edit" data-toggle="tooltip" title="" data-original-title="Редактировать"></i>
                                        </a>
                                        <a href="http://user.alex-r.am/index.php?action=post&subaction=find_client&id=<?php echo $post->post_id; ?>" class="btn btn-default">
                                            <i class="fa fa-search" data-toggle="tooltip" title="" data-original-title="Найти покупателя"></i>
                                        </a>
                                        <a href="http://user.alex-r.am/index.php?action=post&subaction=offer_client&id=<?php echo $post->post_id; ?>" class="btn btn-default">
                                            <i class="fa fa-reply" data-toggle="tooltip" title="" data-original-title="Предложить покупателю"></i>
                                        </a>
                                        <a href="http://user.alex-r.am/index.php?action=post&subaction=sold&id=<?php echo $post->post_id; ?>" class="btn btn-default">
                                            <i class="fa fa-usd" data-toggle="tooltip" title="" data-original-title="Продано"></i>
                                        </a>
                                        <a onclick="return confirm('Вы уверены?')" href="http://user.alex-r.am/index.php?action=post&subaction=delete&id=<?php echo $post->post_id; ?>" class="btn btn-danger">
                                            <i class="fa fa-trash-o" data-toggle="tooltip" title="" data-original-title="Удалить"></i> </a>
                                    </div>
                                </td>
                                <?php if($user_level=='1'){ ?>
                                    <td><?php echo mwdb_get_var("SELECT user_name FROM users WHERE user_id={var}",array($post->post_user)); ?></td>
                                <?php } ?>
                            </tr>
                            <?php
                            unset($price);
                        }
                        ?>
                    </table>
                    <div class="col-md-12">
                        <?php
                        $count_posts = mwdb_get_var("SELECT COUNT(*) FROM posts WHERE {var}",array($query_part));
                        if($count_posts%20==0){
                            $page_count = (int)($count_posts/20);
                        }
                        else{
                            $page_c = (int)($count_posts/20);
                            $page_count = $page_c+1;
                        }
                        if($page_count>1){
                            ?>
                            <a href="http://user.alex-r.am/index.php?action=post&subaction=all" class="pager <?php if($page==1){echo 'current';} ?>">1</a>
                            <?php
                            for($i=2;$i<=$page_count;$i++){
                                ?>
                                <a href="http://user.alex-r.am/index.php?action=post&subaction=all&page=<?php echo $i; ?>" class="pager <?php if($page==$i){echo 'current';} ?>"><?php echo $i; ?></a>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>