<?php
if(isset($_POST['is_submit']) && $_POST['is_submit']=='1'){
	$_SESSION['post'] = $_POST;
}
else{
	if(isset($_GET['page']) && (int)$_GET['page']>0){}else{
		unset($_SESSION['post']);
	}
}
?>
<section class="content-header">
    <h1>Все недвижимости</h1>
	<div class="serach_block">
        <form name="serach" class="search_form" action="" method="get">
            <input type="search" name="word" class="word">
            <input type="hidden" name="action" value="post">
            <input type="hidden" name="subaction" value="search">
            <select name="type" class="search_type">
                <option value="id" selected>id</option>
                <option value="phone">phone/name</option>
				<option value="arajark">Arajark.am ID</option>
            </select>
            <input type="submit" name="s" value="search">
        </form>
    </div>
</section>
<style>th{text-align: center}</style>
 <form action="" method="post">
<div class="col-md-12" style="background: #fff;padding:10px 20px;position: relative">
<div class="col-md-5">
                <div class="col-md-12">
                    <div class="col-md-5">
                        <select name="f_region" class="form-control f_region" style="height: 20px" onchange="get_f_city(this)">
                            <?php
                            $result_region = mwdb_select("SELECT region_id,region_ru FROM region WHERE 1 ORDER BY region_order",array());
                            foreach($result_region as $row_region){
                                ?>
                                <option <?php if(isset($_SESSION['post']['f_region']) && $_SESSION['post']['f_region']==$row_region->region_id){echo 'selected';} ?> value="<?php echo $row_region->region_id; ?>"><?php echo $row_region->region_ru; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-5">
                        <select name="f_city" class="form-control f_city" style="height: 20px" onchange="get_f_area(this)">
                            <option value="0">-- Город/община --</option>
                            <?php
                            $result_cities = mwdb_select("SELECT city_id,city_ru FROM city WHERE region={var} ORDER BY city_order",array(8));
                            foreach ($result_cities as $result_city) {
                                ?>
                                <option <?php if(isset($_SESSION['post']['f_city']) && $_SESSION['post']['f_city']==$result_city->city_id){echo 'selected';} ?> value="<?php echo $result_city->city_id; ?>"><?php echo $result_city->city_ru; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-5">
                        <select name="f_area" class="form-control f_area" style="height: 20px" onchange="get_f_microarea(this)">
                            <option value="0">-- Район --</option>
                            <?php
                            if(isset($_SESSION['post']['f_city']) && $_SESSION['post']['f_city']>0){
                                $result_areas = mwdb_select("SELECT area_id,area_ru FROM area WHERE city={var} ORDER BY area_order",array($_SESSION['post']['f_city']));
                                foreach($result_areas as $row_area){
                                    ?>
                                    <option <?php if(isset($_SESSION['post']['f_area']) && $_SESSION['post']['f_area']==$row_area->area_id){echo 'selected';} ?> value="<?php echo $row_area->area_id; ?>"><?php echo $row_area->area_ru; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-5">
                        <select name="f_microarea" class="form-control f_microarea" style="height: 20px">
                            <option value="0">-- Микрорайон --</option>
                            <?php
                            if(isset($_SESSION['post']['f_area']) && $_SESSION['post']['f_area']>0){
                                $result_areas = mwdb_select("SELECT area_id,area_ru FROM microarea WHERE area={var} ORDER BY area_order",array($_SESSION['post']['f_area']));
                                foreach($result_areas as $row_area){
                                    ?>
                                    <option <?php if(isset($_SESSION['post']['f_microarea']) && $_SESSION['post']['f_microarea']==$row_area->area_id){echo 'selected';} ?> value="<?php echo $row_area->area_id; ?>"><?php echo $row_area->area_ru; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-5">
                    <input type="text" name="f_street_input" class="form-control f_street_input" oninput="get_f_streets(this);" style="height: 20px" placeholder="Улица" value="<?php if(isset($_SESSION['post']['f_street_input'])){echo $_SESSION['post']['f_street_input'];} ?>">
                    <div class="f_street_res" style="position: absolute;z-index: 100;background: #fff;padding: 0px 10px;border: 1px solid #ccc;width: 100%;border-top: none;max-height: 200px;overflow: auto;"></div>
                    <input type="hidden" name="f_street" value="<?php if(isset($_SESSION['post']['f_street'])){echo $_SESSION['post']['f_street'];} ?>" class="f_street">
                </div>
				<div class="col-md-5">
                    <input type="text" name="f_addrss_input" class="form-control f_addrss_input" style="height: 20px" placeholder="Адрес" value="<?php if(isset($_SESSION['post']['f_addrss_input'])){echo $_SESSION['post']['f_addrss_input'];} ?>">
                </div>
               
           
			<input type="hidden" name="is_submit" value="1">
            <div style="width: 95px;margin-top:20px">
                <button type="submit" class="btn btn-block btn-success btn-social" style="padding: 5px;font-size: 13px;">
                    <i class="fa fa-search" style="font-size: 13px !important;line-height: 28px !important;"></i> Найти
                </button>
				<a href="https://user.alex-r.am/index.php?action=post&amp;subaction=archive" class="btn btn-danger" style="position: absolute;bottom: -1px;left: 115px;padding: 5.5px 10px;">Մաքրել</a>
            </div>
			</div> </div></form>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body table-responsive">
                    <?php
                    if(isset($_GET['k']) && $_GET['k']=='delete_all'){
                        $id_delt = (int)$_GET['id'];
                        mwdb_query("DELETE FROM posts WHERE post_id={var}",array($id_delt));
                    }
                    ?>
                    <table class="table table-hover table-bordered" style="text-align:center">
                        <tr>
                            <th>ID</th>
                            <th>Название</th>
                            <?php if($user_level=='1'){ ?>
                                <th>Оператор</th>
                            <?php } ?>
                            <th>Адрес</th>
                            <th>Владелец</th>
                            <th>Изображение</th>
                            <th>Категория</th>
                            <th>Дествия</th>
                        </tr>
                        <?php
                        global $user_level;
                        if($user_level=='1'){
                            $query_part = 'post_status=6';
                        }
                        else{
                            $u_id = $_SESSION['user_id'];
                            $query_part = "post_user=$u_id AND post_status=6";
                        }
						if(isset($_GET['s']) && isset($_GET["type"]) && isset($_GET["word"]) && $_GET["type"] != "" && $_GET["word"] != ""){
                            if($_GET["type"]=="id"){
                                $query_part .= " AND post_code='".$_GET["word"]."'";
                            }
                            if($_GET["type"]=="phone"){
                                $query_part2 = " (owner_name LIKE '%".$_GET["word"]."%' OR owner_phone1 LIKE '%".$_GET["word"]."%' OR owner_phone2 LIKE '%".$_GET["word"]."%')";
                                $post_oweners_id = mwdb_select("SELECT owner_id FROM post_owner WHERE $query_part2",array());
                                $ids = '';
                                foreach($post_oweners_id as $id){
                                    $ids .= $id->owner_id.",";
                                }
                                $owners_id = substr($ids,0,-1);
                                $query_part .=  " AND post_owner IN (".$owners_id.")";
                            }							if($_GET["type"]=="arajark"){								$query_part .= " AND post_arajarkam='".$_GET["word"]."'";							}
                        }
                        if(isset($_GET['page'])){
                            $page = (int)$_GET['page'];
                            if($page<2){
                                $page = 1;
                            }
                        }
                        else{
                            $page = 1;
                        }
						if(isset($_SESSION['post']['f_region']) && $_SESSION['post']['f_region']>0){
                                $query_part .= " AND post_region=". $_SESSION['post']['f_region']." ";
                            }
                            if(isset($_SESSION['post']['f_city']) && $_SESSION['post']['f_city']>0){
                                $query_part .= " AND post_city=". $_SESSION['post']['f_city']." ";
                            }
                            if(isset($_SESSION['post']['f_area']) && $_SESSION['post']['f_area']>0){
                                $query_part .= " AND post_area=". $_SESSION['post']['f_area']." ";
                            }
                            if(isset($_SESSION['post']['f_microarea']) && $_SESSION['post']['f_microarea']>0){
                                $query_part .= " AND post_microarea=". $_SESSION['post']['f_microarea']." ";
                            }
                            if(isset($_SESSION['post']['f_street']) && $_SESSION['post']['f_street']!=''){
                                $query_part .= " AND post_street IN (". $_SESSION['post']['f_street'].") ";
                            }
							if(isset($_SESSION['post']['f_addrss_input']) && $_SESSION['post']['f_addrss_input']!=''){
                                $query_part .= " AND post_address LIKE '".$_SESSION['post']['f_addrss_input']."%' ";
                            }
                        $offset = ($page-1)*20;
                        $result_posts = mwdb_select("SELECT post_id FROM posts WHERE $query_part ORDER BY post_id DESC LIMIT {var},20",array($offset));
                        foreach($result_posts as $row_post){
                            $post_class = new Post('ru',$row_post->post_id);
                            $post = $post_class->get();
                            $location = '';
                            if($post->post_region>0){
                                $location .= mwdb_get_var("SELECT region_ru FROM region WHERE region_id={var}",array($post->post_region)).', ';
                            }
                            if($post->post_city>0){
                                $location .= mwdb_get_var("SELECT city_ru FROM city WHERE city_id={var}",array($post->post_city)).', ';
                            }
                            if($post->post_area>0){
                                $location .= mwdb_get_var("SELECT area_ru FROM area WHERE area_id={var}",array($post->post_area)).', ';
                            }
                            if($post->post_microarea>0){
                                $location .= mwdb_get_var("SELECT area_ru FROM microarea WHERE area_id={var}",array($post->post_microarea)).', ';
                            }
                            if($post->post_street>0){
                                $location .= mwdb_get_var("SELECT street_ru FROM streets WHERE street_id={var}",array($post->post_street)).' ';
                            }
                            $location .= $post->post_address;
                            $owner = json_decode($post->post_owner);
                            $owner_show = 'Имя: '.$owner->owner_name.'<br>Телефон1: '.$owner->owner_phone1.'<br>Телефон2: '.$owner->owner_phone2.'<br>E-mail: '.$owner->owner_email;
                            $images = json_decode($post->post_images,true);
                            if(isset($images[0]['img']) && $images[0]['img']!=''){
                                $image_primary = 'https://user.alex-r.am/images/100/'.$images[0]['img'];
                            }
                            else{
                                $image_primary = 'https://user.alex-r.am/LOGO.png';
                            }
                            ?>
                            <tr>
                                <td><?php echo $post->post_id; ?></td>
                                <td><?php echo $post->post_title_ru; ?></td>
                                <?php if($user_level=='1'){ ?>
                                    <td><?php echo mwdb_get_var("SELECT user_name FROM users WHERE user_id={var}",array($post->post_user)); ?></td>
                                <?php } ?>
                                <td><?php echo $location; ?></td>
                                <td><?php echo $owner_show; ?></td>
                                <td><img src="<?php echo $image_primary; ?>" width="100"></td>
                                <td><?php echo mwdb_get_var("SELECT category_title_ru FROM category WHERE category_id={var}",array($post->post_category)); ?></td>
                                <td>
                                    <div class="btn-group">
                                        <a href="https://user.alex-r.am/index.php?action=post&subaction=view&id=<?php echo $post->post_id; ?>" class="btn btn-default">
                                            <i class="fa fa-eye" data-toggle="tooltip" title="" data-original-title="Просмотр"></i>
                                        </a>
                                        <a onclick="return confirm('Вы уверены?')" href="https://user.alex-r.am/index.php?action=post&subaction=restore&id=<?php echo $post->post_id; ?>" class="btn btn-default" style="background: #59e614">
                                            <i class="fa fa-reply" data-toggle="tooltip" title="" data-original-title="Восстоновить"></i>
                                        </a>
                                        <a onclick="return confirm('Вы уверены?')" href="https://user.alex-r.am/index.php?action=post&subaction=archive&k=delete_all&id=<?php echo $post->post_id; ?>" class="btn btn-danger">
                                            <i class="fa fa-trash-o" data-toggle="tooltip" title="" data-original-title="Удалить навсегда"></i> </a>
                                    </div>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>
                    <div class="col-md-12">
                        <?php
                        $count_posts = mwdb_get_var("SELECT COUNT(*) FROM posts WHERE {var}",array($query_part));
                        if($count_posts%20==0){
                            $page_count = (int)($count_posts/20);
                        }
                        else{
                            $page_c = (int)($count_posts/20);
                            $page_count = $page_c+1;
                        }
                        if($page_count>1){
                            ?>
                            <a href="https://user.alex-r.am/index.php?action=post&subaction=archive&page=1" class="pager <?php if($page==1){echo 'current';} ?>">1</a>
                            <?php
                            for($i=2;$i<=$page_count;$i++){
                                ?>
                                <a href="https://user.alex-r.am/index.php?action=post&subaction=archive&page=<?php echo $i; ?>" class="pager <?php if($page==$i){echo 'current';} ?>"><?php echo $i; ?></a>
                            <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>