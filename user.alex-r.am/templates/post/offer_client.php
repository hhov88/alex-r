<section class="content-header">
    <h1>Предложить покупателю</h1>
</section>
<style>th{text-align: center}</style>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body table-responsive">
                    <?php
                    if(isset($_GET['id']) && (int)$_GET['id']>0){
                        $id_post = (int)$_GET['id'];
                    }
                    ?>
                    <div class="col-md-4">
                        <input type="text" name="client_name_search" class="form-control client_name_search" placeholder="Имя клиента" onkeyup="search_client_by_name(this,<?php echo $user_level; ?>,<?php echo $id_post; ?>)">
                    </div>
                    <div class="col-md-4">
                        <input type="text" name="client_phone_search" class="form-control client_phone_search" placeholder="Телефон клиента" onkeyup="search_client_by_phone(this,<?php echo $user_level; ?>,<?php echo $id_post; ?>)">
                    </div>
                    <div class="col-md-4">
                        <input type="text" name="client_id_search" class="form-control client_id_search" placeholder="ID клиента" onkeyup="search_client_by_id(this,<?php echo $user_level; ?>,<?php echo $id_post; ?>)">
                    </div>
                    <div class="col-md-12 search_result_clients"></div>
                </div>
            </div>
        </div>
    </div>
</section>