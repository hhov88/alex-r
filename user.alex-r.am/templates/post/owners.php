<section class="content-header">
    <h1>Все продавцы</h1>
	<div class="serach_block">
        <form name="serach" class="search_form" action="" method="get">
            <input type="search" name="word" class="word" placeholder="ID недвижимости" style="outline:none">
            <input type="hidden" name="action" value="post">
            <input type="hidden" name="subaction" value="owners">
            <input type="submit" name="s" value="search">
        </form>
    </div>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body table-responsive">
                    <table class="table table-hover table-bordered" style="text-align:center">
                        <tr>
							<th style="text-align:center">ID</th>
							<th style="text-align:center">Имя</th>
							<th style="text-align:center">Телефон 1</th>
							<th style="text-align:center">Телефон 2</th>
							<th style="text-align:center">E-mail</th>
							<th style="text-align:center">Skype</th>
							<th style="text-align:center">Другие детали</th>
							<th style="text-align:center">ID недвижимости</th>
							<th style="text-align:center"></th>
						</tr>
						<tr>
							<td></td>
							<td>
								<input type="text" name="o_name" class="o_name form-control">
							</td>
							<td>
								<input type="text" name="o_phone1" class="o_phone1 form-control">
							</td>
							<td>
								<input type="text" name="o_phone2" class="o_phone2 form-control">
							</td>
							<td>
								<input type="text" name="o_email" class="o_email form-control">
							</td>
							<td>
								<input type="text" name="o_skype" class="o_skype form-control">
							</td>
							<td>
								<input type="text" name="o_other" class="o_other form-control">
							</td>
							<td>
								<select name="o_estate" class="form-control o_estate">
									<option value="0">-- Выбрать --</option>
									<?php
									$result_estates = mwdb_select("SELECT post_id FROM posts WHERE 1 ORDER BY post_id DESC",array());
									foreach($result_estates as $row_estate){
										?>
										<option value="<?=$row_estate->post_id?>"><?=$row_estate->post_id?></option>
										<?php
									}
									?>
								</select>
							</td>
							<td>
								<div class="btn-group">
									<a onclick="add_owner_vnez(this)" class="btn btn-default" style="background: #59e614">
										<i class="fa fa-check" data-toggle="tooltip" title="" data-original-title="Добавить"></i>
									</a>
								</div>
							</td>
						</tr>
						<?php
						if(isset($_GET['page']) && $_GET['page']>1){
							$page = (int)$_GET['page'];
						}
						else{
							$page = 1;
						}
						$limit = 50;
						$offset = ($page-1)*$limit;
						if(isset($_GET['word']) && (int)$_GET['word']>0){
							$owner_id = mwdb_get_var("SELECT post_owner FROM posts WHERE post_id={var}",array((int)$_GET['word']));
							$result_owners = mwdb_select("SELECT * FROM post_owner WHERE owner_id={var} ORDER BY owner_id DESC LIMIT $offset,$limit",array($owner_id));
						}
						else{
							$result_owners = mwdb_select("SELECT * FROM post_owner WHERE 1 ORDER BY owner_id DESC LIMIT $offset,$limit",array());
						}
						foreach($result_owners as $row_owner){
							?>
							<tr>
								<td><?=$row_owner->owner_id?></td>
								<td><?=$row_owner->owner_name?></td>
								<td><?=$row_owner->owner_phone1?></td>
								<td><?=$row_owner->owner_phone2?></td>
								<td><?=$row_owner->owner_email?></td>
								<td><?=$row_owner->owner_skype?></td>
								<td><?=$row_owner->owner_settings?></td>
								<td>
								<?php
								$result_post_ids = mwdb_select("SELECT post_id FROM posts WHERE post_owner={var}",array($row_owner->owner_id));
								$pppoo = '';
								foreach($result_post_ids as $one_post_id){
									$pppoo .= $one_post_id->post_id.', ';
								}
								echo substr($pppoo,0,-2);
								?>
								</td>
								<td>
								
							</td>
							</tr>
							<?php
						}
						?>
					</table>
					<div class="col-md-12">
						<?php
						if(isset($_GET['word']) && (int)$_GET['word']>0){
							$count_all = 1;
						}
						else{
							$count_all = mwdb_get_var("SELECT COUNT(*) FROM post_owner WHERE 1",array());
						}
						if($count_all%$limit==0){
							$page_count = (int)($count_all/$limit);
						}
						else{
							$page_count = (int)($count_all/$limit);
							$page_count = $page_count+1;
						}
						for($i=1;$i<=$page_count;$i++){
							?>
							<a class="btn <?=($i==$page?'btn-primary':'btn-default')?>" href="http://user.alex-r.am/index.php?action=post&subaction=owners<?=($i==1?'':'&page='.$i)?>"><?=$i?></a>
							<?php
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>