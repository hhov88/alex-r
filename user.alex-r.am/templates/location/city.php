<section class="content-header">
    <h1>Города</h1>
</section>
<?php
if(isset($_POST['backend_ad_user']) && $_POST['backend_ad_user']='Добавить'){
    $city_hy = (string)$_POST['city_hy'];
    $city_ru = (string)$_POST['city_ru'];
    $city_en = (string)$_POST['city_en'];
    $region = (int)$_POST['region_area'];
    $city_order = (int)$_POST['city_order'];
    $city = new City(0,$city_hy,$city_ru,$city_en,$region);
    $city->insert($city_order);
    echo 'Город добавлен';
}
?>
111
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Добавить Город</h3>
                    <div class="pull-right box-tools">
                        <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Скрыть" style="margin-right: 5px;"><i class="fa fa-minus"></i></button>
                    </div><!-- /. tools -->
                </div>
                <div class="box-body">
                    <form action="" method="post" enctype="multipart/form-data" autocomplete="off">
                        <table class="table table-hover table-bordered" style="text-align: center">
                            <tr>
                                <th>На армянском</th>
                                <th>На русском</th>
                                <th>На английском</th>
                            </tr>
                            <tr>
                                <td><input type="text" name="city_hy" class="form-control" placeholder="Название района"></td>
                                <td><input type="text" name="city_ru" class="form-control" placeholder="Название района"></td>
                                <td><input type="text" name="city_en" class="form-control" placeholder="Название района"></td>
                            </tr>
                        </table>
                        <select class="form-control" name="region_area" style="margin: 20px 0">
                            <option value="0">-- Выберите Регион --</option>
                            <?php
                            $result_region = mwdb_select("SELECT region_id,region_ru FROM region ORDER BY region_order",array(8));
                            foreach($result_region as $region){
                                ?>
                                <option value="<?php echo $region->region_id ?>"><?php echo $region->region_ru ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <input type="number" class="form-control" name="city_order" placeholder="Порядок" style="margin-bottom: 20px">
                        <input type="submit" name="backend_ad_user" value="Добавить" class="backend_ad_user btn btn-primary">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body table-responsive">
                    <style>
                        th{text-align: center}
                        .my_hide{display: none}
                    </style>
                    <div class="col-md-3" style="float: right;margin-bottom: 20px">
                        <select name="region_filter" class="form-control" onchange="document.location.href='http://user.alex-r.am/index.php?action=locations&subaction=city&region='+$(this).val()">
                        <?php
                        $result_regions = mwdb_select("SELECT region_id,region_ru FROM region WHERE 1",array());
                        foreach($result_regions as $row_region){
                            ?>
                            <option value="<?php echo $row_region->region_id; ?>" <?php if(isset($_GET['region']) && $_GET['region']==$row_region->region_id){echo 'selected';} ?>><?php echo $row_region->region_ru; ?></option>
                            <?php
                        }
                        ?>
                        </select>
                    </div>
                    <table class="table table-hover table-bordered" style="text-align: center">
                        <tr>
                            <th>ID</th>
                            <th>На армянском</th>
                            <th>На русском</th>
                            <th>На английском</th>
                            <th>Регион</th>
                            <th>Дествия</th>
                        </tr>
                        <?php
                        if(isset($_GET['region']) && (int)$_GET['region']>0){
                            $reg_filter = (int)$_GET['region'];
                            $qu = "region=".$reg_filter;
                        }
                        else{
                            $qu = 1;
                        }
                        $result_cities = mwdb_select("SELECT city_id FROM city WHERE {var} ORDER BY region,city_order",array($qu));
                        foreach($result_cities as $row_city){
                            $i = $row_city->city_id;
                            $city = new City($i);
                            $city->get();
                            ?>
                            <tr>
                                <td><?=$city->id; ?></td>
                                <td>
                                    <span class="my_show"><?=$city->hy; ?></span>
                                    <span class="my_hide" style="width: 100%;"><input type="text" name="hy" class="hy form-control" value="<?=$city->hy; ?>"></span>
                                </td>
                                <td>
                                    <span class="my_show"><?=$city->ru; ?></span>
                                    <span class="my_hide" style="width: 100%;"><input type="text" name="ru" class="ru form-control" value="<?=$city->ru; ?>"></span>
                                </td>
                                <td>
                                    <span class="my_show"><?=$city->en; ?></span>
                                    <span class="my_hide" style="width: 100%;"><input type="text" name="en" class="en form-control" value="<?=$city->en; ?>"></span>
                                </td>
                                <td>
                                    <?php
                                    $region = new Region($city->region);
                                    $region->get();
                                    echo $region->ru;
                                    ?>
                                </td>
                                <td>
                                    <div class="btn-group my_show">
                                        <a class="btn btn-default" onclick="edit_regions(this)"><i class="fa fa-edit"></i>Изменить</a>
                                        <a onclick="delete_city(this)" data-id="<?=$city->id; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i> Удалить</a>
                                    </div>
                                    <div class="btn-group my_hide">
                                        <a data-id="<?=$city->id; ?>" class="btn btn-success" onclick="save_city(this,<?php echo $_GET['region'] ?>)"><i class="fa fa-check"></i>Сохранить</a>
                                    </div>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>