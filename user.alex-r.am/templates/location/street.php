<section class="content-header">
    <h1>Улицы</h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body table-responsive">
                    <style>
                        th{text-align: center}
                        .my_hide{display: none}
                    </style>
                    <div class="col-md-12" style="margin-bottom: 20px">
                        <div class="col-md-3">
                            <select name="region_filter" class="form-control" onchange="document.location.href='http://user.alex-r.am/index.php?action=locations&subaction=street&region='+$(this).val()">
                                <?php
                                $result_regions = mwdb_select("SELECT region_id,region_ru FROM region WHERE 1",array());
                                foreach($result_regions as $row_region){
                                    ?>
                                    <option value="<?php echo $row_region->region_id; ?>" <?php if(isset($_GET['region']) && $_GET['region']==$row_region->region_id){echo 'selected';} ?>><?php echo $row_region->region_ru; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select name="city_filter" class="form-control" onchange="document.location.href='http://user.alex-r.am/index.php?action=locations&subaction=street&region='+$('select[name=region_filter]').val()+'&city='+$(this).val()">
                                <option value="">Все Города</option>
                                <?php
                                $in = '';
                                $region = (int)$_GET['region'];
                                $result_cities = mwdb_select("SELECT city_id,city_ru FROM city WHERE region={var}",array($region));
                                foreach($result_cities as $row_city){
                                    ?>
                                    <option value="<?php echo $row_city->city_id; ?>" <?php if(isset($_GET['city']) && $_GET['city']==$row_city->city_id){echo 'selected';} ?>><?php echo $row_city->city_ru; ?></option>
                                    <?php
                                    $in .= $row_city->city_id.',';
                                }
                                $in = substr($in,0,-1);
                                ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select name="area_filter" class="form-control" onchange="document.location.href='http://user.alex-r.am/index.php?action=locations&subaction=street&region='+$('select[name=region_filter]').val()+'&city='+$('select[name=city_filter]').val()+'&area='+$(this).val()">
                                <option value="">Все Районы</option>
                                <?php
                                $in1 = '';
                                $city = (int)$_GET['city'];
                                $result_areas = mwdb_select("SELECT area_id,area_ru FROM area WHERE city={var}",array($city));
                                foreach($result_areas as $row_area){
                                    ?>
                                    <option value="<?php echo $row_area->area_id; ?>" <?php if(isset($_GET['area']) && $_GET['area']==$row_area->area_id){echo 'selected';} ?>><?php echo $row_area->area_ru; ?></option>
                                    <?php
                                    $in1 .= $row_area->area_id.',';
                                }
                                $in1 = substr($in,0,-1);
                                ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select name="microarea_filter" class="form-control" onchange="document.location.href='http://user.alex-r.am/index.php?action=locations&subaction=street&region='+$('select[name=region_filter]').val()+'&city='+$('select[name=city_filter]').val()+'&area='+$('select[name=area_filter]').val()+'&microarea='+$(this).val()">
                                <option value="">Все Микрорайоны</option>
                                <?php
                                $in2 = '';
                                $area = (int)$_GET['area'];
                                $result_microareas = mwdb_select("SELECT area_id,area_ru FROM microarea WHERE area={var}",array($area));
                                foreach($result_microareas as $row_microarea){
                                    ?>
                                    <option value="<?php echo $row_microarea->area_id; ?>" <?php if(isset($_GET['microarea']) && $_GET['microarea']==$row_microarea->area_id){echo 'selected';} ?>><?php echo $row_microarea->area_ru; ?></option>
                                    <?php
                                    $in2 .= $row_microarea->area_id.',';
                                }
                                $in2 = substr($in,0,-1);
                                ?>
                            </select>
                        </div>
                    </div>
                    <table class="table table-hover table-bordered" style="text-align: center">
                        <tr>
                            <th>ID</th>
                            <th>На армянском</th>
                            <th>На русском</th>
                            <th>На английском</th>
                            <th>Регион</th>
                            <th>Город</th>
                            <th>Район</th>
                            <th>Микрорайон</th>
                            <th style="width: 220px;">Дествия</th>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="text" name="street_hy" class="form-control">
                            </td>
                            <td>
                                <input type="text" name="street_ru" class="form-control">
                            </td>
                            <td>
                                <input type="text" name="street_en" class="form-control">
                            </td>
                            <td>
                                <select name="region" class="region form-control" onchange="region_change(this)">
                                    <option value="0">-- Регион --</option>
                                </select>
                            </td>
                            <td>
                                <select name="city" class="city form-control" onchange="city_change(this)">
                                    <option value="0">-- Город --</option>
                                </select>
                            </td>
                            <td>
                                <select name="area" class="area form-control" onchange="area_change(this)">
                                    <option value="0">-- Район --</option>
                                </select>
                            </td>
                            <td>
                                <select name="microarea" class="microarea form-control" onchange="microarea_change(this)">
                                    <option value="0">-- Микрорайон --</option>
                                </select>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <a class="btn btn-success" onclick="add_street(this,<?php echo $_GET['region'] ?>)"><i class="fa fa-check"></i> Добавить</a>
                                </div>
                            </td>
                        </tr>
                        <?php
                        if(isset($_GET['microarea']) && (int)$_GET['microarea']>0){
                            $reg_filter = (int)$_GET['microarea'];
                            $qu = "microarea=".$reg_filter;
                            $paging_ = false;
                        }
                        elseif(isset($_GET['area']) && (int)$_GET['area']>0){
                            $reg_filter = (int)$_GET['area'];
                            $qu = "area=".$reg_filter;
                            $paging_ = false;
                        }
                        elseif(isset($_GET['city']) && (int)$_GET['city']>0){
                            $reg_filter = (int)$_GET['city'];
                            $qu = "city=".$reg_filter;
                            $paging_ = false;
                        }
                        elseif(isset($_GET['region']) && (int)$_GET['region']>0){
                            $reg_filter = (int)$_GET['region'];
                            $qu = "region=".$reg_filter;
                            $paging_ = true;
                        }
                        else{
                            $qu = 1;
                            $paging_ = true;
                        }
                        if($paging_){
                            if(isset($_GET['page']) && $_GET['page']>1){
                                $pager = (int)$_GET['page'];
                            }
                            else{
                                $pager = 1;
                            }
                            $offset = ($pager-1)*30;
                            $result_streets = mwdb_select("SELECT street_id FROM streets WHERE {var} ORDER BY region,city,area,microarea,street_hy LIMIT $offset,30",array($qu));
                            $count_streets = mwdb_get_var("SELECT COUNT(*) FROM streets WHERE {var}",array($qu));
                        }
                        else{
                            $result_streets = mwdb_select("SELECT street_id FROM streets WHERE {var} ORDER BY region,city,area,microarea,street_hy",array($qu));
                            $pager = 1;
                        }
                        foreach($result_streets as $row_streets){
                            $i = $row_streets->street_id;
                            $street = new Street($i);
                            $street->get();
                            ?>
                            <tr>
                                <td><?=$street->id; ?></td>
                                <td>
                                    <span class="my_show"><?=$street->hy; ?></span>
                                    <span class="my_hide" style="width: 100%;"><input type="text" name="hy" class="hy form-control" value="<?=$street->hy; ?>"></span>
                                </td>
                                <td>
                                    <span class="my_show"><?=$street->ru; ?></span>
                                    <span class="my_hide" style="width: 100%;"><input type="text" name="ru" class="ru form-control" value="<?=$street->ru; ?>"></span>
                                </td>
                                <td>
                                    <span class="my_show"><?=$street->en; ?></span>
                                    <span class="my_hide" style="width: 100%;"><input type="text" name="en" class="en form-control" value="<?=$street->en; ?>"></span>
                                </td>
                                <td>
                                    <span class="my_show">
                                        <?php
                                        $reg = new Region($street->region);
                                        $reg->get();
                                        echo $reg->ru;
                                        ?>
                                    </span>
                                    <span data-id="<?=$street->region ?>" class="my_hide region_select" style="width: 100%;" >
                                        <select name="region" class="region form-control" onchange="region_change(this)">
                                            <option value="0">-- Город --</option>
                                        </select>
                                    </span>
                                </td>
                                <td>
                                    <span class="my_show">
                                        <?php
                                        $reg = new City($street->city);
                                        $reg->get();
                                        echo $reg->ru;
                                        ?>
                                    </span>
                                    <span class="my_hide city_select" style="width: 100%;" data-id="<?=$street->city ?>">
                                        <select name="city"  class="city form-control" onchange="city_change(this)">
                                            <option value="0">-- Город --</option>
                                        </select>
                                    </span>
                                </td>
                                <td>
                                    <span class="my_show">
                                        <?php
                                        if($street->area>0){
                                            $reg = new Area($street->area);
                                            $reg->get();
                                            echo $reg->ru;
                                        }
                                        ?>
                                    </span>
                                    <span class="my_hide area_select" style="width: 100%;" data-id="<?=$street->area ?>">
                                        <select name="area" class="area form-control" onchange="area_change(this)">
                                            <option value="0">-- Район --</option>
                                        </select>
                                    </span>
                                </td>
                                <td>
                                    <span class="my_show">
                                        <?php
                                        if($street->microarea>0){
                                            $reg = new Microarea($street->microarea);
                                            $reg->get();
                                            echo $reg->ru;
                                        }
                                        ?>
                                    </span>
                                    <span class="my_hide microarea_select" style="width: 100%;" data-id="<?=$street->microarea ?>">
                                        <select name="microarea" class="microarea form-control" onchange="microarea_change(this)">
                                            <option value="0">-- Микрорайон --</option>
                                        </select>
                                    </span>
                                </td>
                                <td>
                                    <div class="btn-group my_show">
                                        <a class="btn btn-default" onclick="edit_regions(this)"><i class="fa fa-edit"></i> Изменить</a>
                                        <a onclick="delete_street(this,<?php echo $_GET['region'] ?>)" data-id="<?=$street->id; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i> Удалить</a>
                                    </div>
                                    <div class="btn-group my_hide">
                                        <a data-id="<?=$street->id; ?>" class="btn btn-success" onclick="save_street(this,<?php echo $_GET['region'] ?>,window.location.href)"><i class="fa fa-check"></i> Сохранить</a>
                                    </div>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>
                    <?php if($paging_){ ?>
                    <div class="col-md-12">
                        <?php
                        if($count_streets%30==0){
                            $page_count = (int)($count_streets/30);
                        }
                        else{
                            $page_c = (int)($count_streets/30);
                            $page_count = $page_c+1;
                        }
                        $get_vars = '';
                        foreach($_GET as $key=>$val){
                            if($key!='page'){
                                $get_vars .= $key.'='.$val.'&';
                            }
                        }
                        $get_vars = substr($get_vars,0,-1);
                        if($page_count>1){
                            ?>
                            <a href="http://user.alex-r.am/index.php?<?php echo $get_vars; ?>" class="pager <?php if(isset($pager) && $pager==1){echo 'current';} ?>">1</a>
                            <?php
                            for($i=2;$i<=$page_count;$i++){
                                ?>
                                <a href="http://user.alex-r.am/index.php?<?php echo $get_vars; ?>&page=<?php echo $i; ?>" class="pager <?php if(isset($pager) && $pager==$i){echo 'current';} ?>"><?php echo $i; ?></a>
                            <?php
                            }
                        }
                        ?>
                    </div>
                <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    var page = 'add_post';
    var edit_region = 8;
</script>