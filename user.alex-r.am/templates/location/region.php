<section class="content-header">
    <h1>Регионы</h1>
</section>
<?php
if(isset($_POST['backend_ad_user']) && $_POST['backend_ad_user']='Добавить'){
    $region_hy = (string)$_POST['region_hy'];
    $region_ru = (string)$_POST['region_ru'];
    $region_en = (string)$_POST['region_en'];
    $region_order = (int)$_POST['region_order'];
    $region = new Region(0,$region_hy,$region_ru,$region_en);
    $region->insert($region_order);
    echo 'Регион добавлен';
}
?>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Добавить Регион</h3>
                    <div class="pull-right box-tools">
                        <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Скрыть" style="margin-right: 5px;"><i class="fa fa-minus"></i></button>
                    </div><!-- /. tools -->
                </div>
                <div class="box-body">
                    <form action="" method="post" enctype="multipart/form-data" autocomplete="off">
                        <table class="table table-hover table-bordered" style="text-align: center">
                            <tr>
                                <th>На армянском</th>
                                <th>На русском</th>
                                <th>На английском</th>
                            </tr>
                            <tr>
                                <td><input type="text" name="region_hy" class="form-control" placeholder="Название района"></td>
                                <td><input type="text" name="region_ru" class="form-control" placeholder="Название района"></td>
                                <td><input type="text" name="region_en" class="form-control" placeholder="Название района"></td>
                            </tr>
                        </table>
                        <input type="number" class="form-control" name="region_order" placeholder="Порядок" style="margin-bottom: 20px">
                        <input type="submit" name="backend_ad_user" value="Добавить" class="backend_ad_user btn btn-primary">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body table-responsive">
                    <style>
                        th{text-align: center}
                        .my_hide{display: none}
                    </style>
                    <table class="table table-hover table-bordered" style="text-align: center">
                        <tr>
                            <th>ID</th>
                            <th>На армянском</th>
                            <th>На русском</th>
                            <th>На английском</th>
                            <th>Дествия</th>
                        </tr>
                        <?php
                        $result_regions = mwdb_select("SELECT region_id FROM region WHERE {var} ORDER BY region_order",array(1));
                        foreach($result_regions as $row_region){
                            $i = $row_region->region_id;
                            $region = new Region($i);
                            $region->get();
                            ?>
                        <tr>
                            <td><?=$region->id; ?></td>
                            <td>
                                <span class="my_show"><?=$region->hy; ?></span>
                                <span class="my_hide" style="width: 100%;"><input type="text" name="hy" class="hy form-control" value="<?=$region->hy; ?>"></span>
                            </td>
                            <td>
                                <span class="my_show"><?=$region->ru; ?></span>
                                <span class="my_hide" style="width: 100%;"><input type="text" name="ru" class="ru form-control" value="<?=$region->ru; ?>"></span>
                            </td>
                            <td>
                                <span class="my_show"><?=$region->en; ?></span>
                                <span class="my_hide" style="width: 100%;"><input type="text" name="en" class="en form-control" value="<?=$region->en; ?>"></span>
                            </td>
                            <td>
                                <div class="btn-group my_show">
                                        <a class="btn btn-default" onclick="edit_regions(this)"><i class="fa fa-edit"></i>Изменить</a>
                                        <a onclick="delete_region(this)" data-id="<?=$region->id; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i> Удалить</a>
                                </div>
                                <div class="btn-group my_hide">
                                        <a data-id="<?=$region->id; ?>" class="btn btn-success" onclick="save_region(this)"><i class="fa fa-check"></i>Сохранить</a>
                                </div>
                            </td>
                        </tr>
                            <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>