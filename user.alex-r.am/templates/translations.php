<?php
require( TEMPLATE_PATH."/header.php" );
?>
<section class="content-header">
    <h1>Переводы</h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body table-responsive no-padding">
    <style>th{text-align: center}</style>
    <?php
    if(isset($_POST['add_tr']) && $_POST['add_tr']=='Добавить'){
        $new_translation = new Translations(0,$_POST['tr_code'],$_POST['tr_hy'],$_POST['tr_ru'],$_POST['tr_en']);
        if($new_translation->insert()){
            echo '<div style="color:green">Перевод добавлен</div>';
        }
        else{
            echo '<div style="color:red">Перевод с таким кодом есть в базе. Перевод не добавлен!</div>';
        }
    }
    if(isset($_POST['add_tr']) && $_POST['add_tr']=='Сохранить'){
        $new_translation = new Translations($_POST['tr_id'],$_POST['tr_code'],$_POST['tr_hy'],$_POST['tr_ru'],$_POST['tr_en']);
        if($new_translation->update()){
            echo '<div style="color:green">Перевод сахранен</div>';
            echo '<script>setTimeout(document.location.href="http://user.alex-r.am/index.php?action=translations", 4000);</script>';
        }
        else{
            echo '<div style="color:red">Перевод с таким кодом есть в базе. Перевод не сахранен!</div>';
        }
    }
    if(isset($_GET['subaction']) && $_GET['subaction']=='delete'){
        $id = (int)$_GET['id'];
        $new_translation = new Translations($id);
        $new_translation->delete();
    }

    ?>
    <table class="table table-hover table-bordered" style="text-align:center">
        <tr>
            <th style="width: 20%">Код</th>
            <th style="width: 20%">Армянский</th>
            <th style="width: 20%">Русский</th>
            <th style="width: 20%">Английский</th>
            <th style="width: 20%"></th>
        </tr>
    </table>
    <form action="" method="post">
        <?php
        if(isset($_GET['subaction']) && $_GET['subaction']=='edit'){
            $id = (int)$_GET['id'];
            $new_translation = new Translations($id);
            $this_translation = $new_translation->get_translation_by_id();
            ?>
            <input type="hidden" name="tr_id" value="<?php echo $id; ?>">
            <?php
        }
        ?>
    <table class="table table-hover table-bordered" style="text-align:center">
        <tr>
            <td style="width: 20%"><input type="text" name="tr_code" class="form-control" <?php if(isset($_GET['subaction']) && $_GET['subaction']=='edit'){echo 'value="'.$this_translation->translation_code.'"';} ?>></td>
            <td style="width: 20%"><input type="text" name="tr_hy" class="form-control" <?php if(isset($_GET['subaction']) && $_GET['subaction']=='edit'){echo 'value="'.$this_translation->translation_hy.'"';} ?>></td>
            <td style="width: 20%"><input type="text" name="tr_ru" class="form-control" <?php if(isset($_GET['subaction']) && $_GET['subaction']=='edit'){echo 'value="'.$this_translation->translation_ru.'"';} ?>></td>
            <td style="width: 20%"><input type="text" name="tr_en" class="form-control" <?php if(isset($_GET['subaction']) && $_GET['subaction']=='edit'){echo 'value="'.$this_translation->translation_en.'"';} ?>></td>
            <td style="width: 20%"><input type="submit" name="add_tr" value="<?php if(isset($_GET['subaction']) && $_GET['subaction']=='edit'){echo 'Сохранить';}else{echo 'Добавить';} ?>"></td>
        </tr>
    </table>
    </form>
    <?php
    $translations = new Translations();
    $all_translations = $translations->get_translations();
    ?>
    <table class="table table-hover table-bordered" style="text-align:center">
        <?php
        foreach($all_translations as $translation){
            ?>
            <tr>
                <td style="width: 20%"><?php echo $translation->translation_code; ?></td>
                <td style="width: 20%"><?php echo $translation->translation_hy; ?></td>
                <td style="width: 20%"><?php echo $translation->translation_ru; ?></td>
                <td style="width: 20%"><?php echo $translation->translation_en; ?></td>
                <td style="width: 20%">
                    <div class="btn-group">
                        <a href="http://user.alex-r.am/index.php?action=translations&subaction=edit&id=<?php echo $translation->translation_id; ?>" class="btn btn-default">
                            <i class="fa fa-edit"></i>Редактировать
                        </a>
                        <a onclick="return confirm('Вы уверены?')" href="http://user.alex-r.am/index.php?action=translations&subaction=delete&id=<?php echo $translation->translation_id; ?>" class="btn btn-danger">
                            <i class="fa fa-trash-o"></i> Удалить</a>
                    </div>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
                    </div>
                </div>
            </div>
        </div>
</section>
<?php
require( TEMPLATE_PATH."/footer.php" );
?>