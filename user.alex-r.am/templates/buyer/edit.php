<?php
$id = (int)$_GET['id'];
$client = new Client($id);
$client->get();
if(isset($_POST['add_client']) && $_POST['add_client']=='Сохранить'){
    $client = new Client($id,$_POST['owner_name'],$_POST['owner_phone1'],$_POST['owner_phone2'],$_POST['owner_email'],$_POST['owner_details'],$_SESSION['user_id'],$_POST['owner_type']);
    $post_client = $client->update();
    echo '<script>document.location.href="http://user.alex-r.am/index.php?action=buyer&subaction=view&id='.$id.'"</script>';
}
?>
<section class="content-header">
    <h1>Редактировать Клиента</h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body table-responsive">
                    <form action="" method="post">
                        <div class="col-md-2" style="float: right">
                            <select name="owner_type" class="form-control">
                                <option value="0">Обычный клиент</option>
                                <option <?php if($client->client_type=='2'){echo 'selected';} ?> value="2">Первичный клиент</option>
                                <option <?php if($client->client_type=='1'){echo 'selected';} ?> value="1">Вторичный клиент</option>
                            </select>
                        </div>
                        <div style="margin-top: 20px;display: inline-block;width: 100%;border-top: 1px solid #93B1D1;padding: 20px 0;border-bottom: 1px solid #93B1D1;">
                            <div class="col-md-6">
                                <input type="text" name="owner_name" class="owner_name form-control" placeholder="Имя клиента" value="<?php echo $client->client_name; ?>"><br>
                                <input type="text" name="owner_phone1" class="owner_phone1 form-control" placeholder="Телефон1 клиента" value="<?php echo $client->client_phone1; ?>"><br>
                                <input type="text" name="owner_phone2" class="owner_phone2 form-control" placeholder="Телефон2 клиента" value="<?php echo $client->client_phone2; ?>"><br>
                                <input type="email" name="owner_email" class="owner_email form-control" placeholder="E-mail клиента" value="<?php echo $client->client_email; ?>">
                            </div>
                            <div class="col-md-6">
                                <textarea name="owner_details" class="owner_details form-control" placeholder="Другие детали клиента" style="height: 196px;"><?php echo $client->client_settings; ?></textarea>
                            </div>
                        </div>
                        <div class="col-md-12" style="text-align: center;margin-top: 20px">
                            <input type="submit" name="add_client" value="Сохранить" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>