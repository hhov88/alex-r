<section class="content-header">
    <h1>Архив клиентов</h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body table-responsive">
                    <style>
                        th{text-align: center}
                    </style>
                    <table class="table table-hover table-bordered" style="text-align: center">
                        <tr>
                            <th>ID</th>
                            <th>Имя клиента</th>
                            <th>Телефон</th>
                            <th>E-mail</th>
                            <th>Дата регистрации</th>
                            <th>Статус</th>
                            <th>Дествия</th>
                        </tr>
                        <?php
                        if(isset($_GET['page'])){
                            $page = (int)$_GET['page'];
                            if($page<2){
                                $page = 1;
                            }
                        }
                        else{
                            $page = 1;
                        }
                        $offset = ($page-1)*20;
                        global $user_level;
                        if($user_level=='1'){
                            $result_clients = mwdb_select("SELECT * FROM post_client WHERE client_status={var} ORDER BY client_type DESC,client_time DESC LIMIT {var},20",array(6,$offset));
                        }
                        else{
                            $result_clients = mwdb_select("SELECT * FROM post_client WHERE client_status={var} AND user_id={var} ORDER BY client_type DESC,client_time DESC LIMIT {var},20",array(6,$_SESSION['user_id'],$offset));
                        }
                        foreach($result_clients as $row_client){
                            $status = '<span class="label label-danger">Удалён</span>';
                            if($row_client->client_type=='1'){
                                $style_tr = 'style="background:rgba(255, 255, 0, 0.15)"';
                            }
                            else{
                                $style_tr = '';
                            }
                            ?>
                            <tr <?php echo $style_tr; ?>>
                                <td><?php echo $row_client->client_id; ?></td>
                                <td><?php echo $row_client->client_name; ?></td>
                                <td><?php echo $row_client->client_phone1;if(isset($row_client->client_phone2) && $row_client->client_phone2!=''){echo ', '.$row_client->client_phone2;} ?></td>
                                <td><?php echo $row_client->client_email; ?></td>
                                <td><?php echo $row_client->client_time; ?></td>
                                <td><?php echo $status; ?></td>
                                <td>
                                    <div class="btn-group">
                                        <a href="http://user.alex-r.am/index.php?action=buyer&subaction=view&id=<?php echo $row_client->client_id; ?>" class="btn btn-default">
                                            <i class="fa fa-eye"></i>Просмотр
                                        </a>
                                        <a href="http://user.alex-r.am/index.php?action=buyer&subaction=edit&id=<?php echo $row_client->client_id; ?>" class="btn btn-default">
                                            <i class="fa fa-edit"></i>Редактировать
                                        </a>
                                        <a href="http://user.alex-r.am/index.php?action=buyer&subaction=end&id=<?php echo $row_client->client_id; ?>" class="btn btn-success">
                                            <i class="fa fa-check"></i>Завершить
                                        </a>
                                        <!--<a onclick="return confirm('Вы уверены?')" href="http://user.alex-r.am/index.php?action=buyer&subaction=delete&id=<?php echo $row_client->client_id; ?>" class="btn btn-danger">
                                            <i class="fa fa-trash-o"></i> Удалить</a>-->
                                    </div>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>