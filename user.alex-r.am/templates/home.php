<?php
require( TEMPLATE_PATH."/header.php" );
?>
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?php echo mwdb_get_var("SELECT COUNT(*) FROM users WHERE user_type!={var}",array(1)); ?></h3>
                    <p>Операторов</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-stalker"></i>
                </div>
                <a href="https://user.alex-r.am/index.php?action=operators" class="small-box-footer">Больше информации <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><?php echo mwdb_get_var("SELECT COUNT(*) FROM posts WHERE post_status!={var}",array(6)); ?></h3>
                    <p>Недвижимостей</p>
                </div>
                <div class="icon">
                    <i class="ion ion-home"></i>
                </div>
                <a href="https://user.alex-r.am/index.php?action=post&subaction=all" class="small-box-footer">Больше информации <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><?php echo mwdb_get_var("SELECT COUNT(*) FROM post_client WHERE client_status!={var}",array(6)); ?></h3>
                    <p>Клиентов</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="https://user.alex-r.am/index.php?action=buyer&subaction=all" class="small-box-footer">Больше информации <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?php echo mwdb_get_var("SELECT COUNT(*) FROM client_offers WHERE offer_status={var}",array(2)); ?></h3>
                    <p>Завершенных заказов</p>
                </div>
                <div class="icon">
                    <i class="ion ion-checkmark"></i>
                </div>
                <a href="https://user.alex-r.am/index.php?action=buyer&subaction=all" class="small-box-footer">Больше информации <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div><!-- ./col -->
    </div>
<?php
require( TEMPLATE_PATH."/footer.php" );
?>