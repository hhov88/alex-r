<?php
require( TEMPLATE_PATH."/header.php" );
if(isset($_GET['subaction'])){
    switch($_GET['subaction']){
        case 'region':{
            require( TEMPLATE_PATH."/location/region.php" );
            break;
        }
        case 'city':{
            require( TEMPLATE_PATH."/location/city.php" );
            break;
        }
        case 'area':{
            require( TEMPLATE_PATH."/location/area.php" );
            break;
        }
        case 'microarea':{
            require( TEMPLATE_PATH."/location/microarea.php" );
            break;
        }
        case 'street':{
            require( TEMPLATE_PATH."/location/street.php" );
            break;
        }
    }
}
require( TEMPLATE_PATH."/footer.php" );
?>