<?php
require( TEMPLATE_PATH."/header.php" );
?>
<section class="content-header">
    <h1>Категории</h1>
</section>
<section class="content">
    <?php
    if(isset($_POST['backend_ad_user']) && $_POST['backend_ad_user']=='Добавить'){
        $category_title = (string)$_POST['category_title'];
        $category_content = (string)$_POST['category_content'];
        $category_keywords = (string)$_POST['category_keywords'];
        $category_title_ru = (string)$_POST['category_title_ru'];
        $category_content_ru = (string)$_POST['category_content_ru'];
        $category_keywords_ru = (string)$_POST['category_keywords_ru'];
        $category_title_en = (string)$_POST['category_title_en'];
        $category_content_en = (string)$_POST['category_content_en'];
        $category_keywords_en = (string)$_POST['category_keywords_en'];
        $category_slug = (string)$_POST['category_slug'];
        $category_order = (int)$_POST['category_order'];
        $ins = new Category(0,$category_title,$category_content,$category_slug,$category_order,$category_keywords,$category_title_ru,$category_content_ru,$category_keywords_ru,$category_title_en,$category_content_en,$category_keywords_en);
        $ins->insert();
    }
    if(isset($_POST['backend_ad_user']) && $_POST['backend_ad_user']=='Сохранить'){
        $id = (int)$_POST['category_ed_id'];
        $category_title = (string)$_POST['category_title'];
        $category_content = (string)$_POST['category_content'];
        $category_keywords = (string)$_POST['category_keywords'];
        $category_title_ru = (string)$_POST['category_title_ru'];
        $category_content_ru = (string)$_POST['category_content_ru'];
        $category_keywords_ru = (string)$_POST['category_keywords_ru'];
        $category_title_en = (string)$_POST['category_title_en'];
        $category_content_en = (string)$_POST['category_content_en'];
        $category_keywords_en = (string)$_POST['category_keywords_en'];
        $category_slug = (string)$_POST['category_slug'];
        $category_order = (int)$_POST['category_order'];
        $update = new Category($id,$category_title,$category_content,$category_slug,$category_order,$category_keywords,$category_title_ru,$category_content_ru,$category_keywords_ru,$category_title_en,$category_content_en,$category_keywords_en);
        $update->update();
    }
    /*if(isset($_GET['subaction']) && $_GET['subaction']=='delete'){
        $id = (int)$_GET['id'];
        $del_obj = new Category($id);
        $del_obj->delete();
    }*/
    if(isset($_GET['subaction']) && $_GET['subaction']=='edit'){
        $id = (int)$_GET['id'];
        $edit_obj = new Category($id);
        $edit = $edit_obj->get();
       ?>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Редактировать категория</h3>
                        <div class="pull-right box-tools">
                            <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Скрыть" style="margin-right: 5px;"><i class="fa fa-minus"></i></button>
                        </div><!-- /. tools -->
                    </div>
                    <div class="box-body">
                        <form action="" method="post">
                            <input type="hidden" name="category_ed_id" value="<?php echo $edit->category_id; ?>">
                            <table class="table table-hover table-bordered" style="text-align:center">
                                <tr>
                                    <th>На армянском</th>
                                    <th>На русском</th>
                                    <th>На английском</th>
                                </tr>
                                <tr>
                                    <td><input type="text" name="category_title" placeholder="Название категории" class="form-control" value="<?php echo $edit->category_title ?>"></td>
                                    <td><input type="text" name="category_title_ru" placeholder="Название категории" class="form-control" value="<?php echo $edit->category_title_ru ?>"></td>
                                    <td><input type="text" name="category_title_en" placeholder="Название категории" class="form-control" value="<?php echo $edit->category_title_en ?>"></td>
                                </tr>
                                <tr>
                                    <td><textarea name="category_content" placeholder="Описание категории" class="form-control"><?php echo $edit->category_content ?></textarea></td>
                                    <td><textarea name="category_content_ru" placeholder="Описание категории" class="form-control"><?php echo $edit->category_content_ru ?></textarea></td>
                                    <td><textarea name="category_content_en" placeholder="Описание категории" class="form-control"><?php echo $edit->category_content_en ?></textarea></td>
                                </tr>
                                <tr>
                                    <td><textarea name="category_keywords" placeholder="Keywords" class="form-control"><?php echo $edit->category_keywords ?></textarea></td>
                                    <td><textarea name="category_keywords_ru" placeholder="Keywords" class="form-control"><?php echo $edit->category_keywords_ru ?></textarea></td>
                                    <td><textarea name="category_keywords_en" placeholder="Keywords" class="form-control"><?php echo $edit->category_keywords_en ?></textarea></td>
                                </tr>
                            </table>
                            <input type="text" name="category_slug" placeholder="Сылка категории" class="form-control" value="<?php echo $edit->category_slug ?>"><br>
                            <input type="number" name="category_order" placeholder="Порядок категории" class="form-control" value="<?php echo $edit->category_order ?>"><br>
                            <input type="submit" name="backend_ad_user" value="Сохранить" class="backend_ad_user btn btn-primary">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    else{
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Добавить категория</h3>
                    <div class="pull-right box-tools">
                        <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Скрыть" style="margin-right: 5px;"><i class="fa fa-minus"></i></button>
                    </div><!-- /. tools -->
                </div>
                <div class="box-body">
                    <form action="" method="post">
                        <table class="table table-hover table-bordered" style="text-align:center">
                            <tr>
                                <th>На армянском</th>
                                <th>На русском</th>
                                <th>На английском</th>
                            </tr>
                            <tr>
                                <td><input type="text" name="category_title" placeholder="Название категории" class="form-control"></td>
                                <td><input type="text" name="category_title_ru" placeholder="Название категории" class="form-control"></td>
                                <td><input type="text" name="category_title_en" placeholder="Название категории" class="form-control"></td>
                            </tr>
                            <tr>
                                <td><textarea name="category_content" placeholder="Описание категории" class="form-control"></textarea></td>
                                <td><textarea name="category_content_ru" placeholder="Описание категории" class="form-control"></textarea></td>
                                <td><textarea name="category_content_en" placeholder="Описание категории" class="form-control"></textarea></td>
                            </tr>
                            <tr>
                                <td><textarea name="category_keywords" placeholder="Keywords" class="form-control"></textarea></td>
                                <td><textarea name="category_keywords_ru" placeholder="Keywords" class="form-control"></textarea></td>
                                <td><textarea name="category_keywords_en" placeholder="Keywords" class="form-control"></textarea></td>
                            </tr>
                        </table>
                        <input type="text" name="category_slug" placeholder="Сылка категории" class="form-control"><br>
                        <input type="number" name="category_order" placeholder="Порядок категории" class="form-control"><br>
                        <input type="submit" name="backend_ad_user" value="Добавить" class="backend_ad_user btn btn-primary">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <style>th{text-align: center !important;}</style>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Все категории</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover table-bordered" style="text-align:center">
                        <tr>
                            <th>Название категории</th>
                            <th>Порядок категории</th>
                            <th>Описание категории</th>
                            <th>действие</th>
                        </tr>
                        <?php
                        if(isset($_GET['page']) && $_GET['page']>1){
                            $page = (int)$_GET['page'];
                        }
                        else{
                            $page = 1;
                        }
                        $offset = ($page-1)*20;
                        $users = mwdb_select("SELECT * FROM category WHERE {var}",array(1));
                        if($users){
                            foreach($users as $user){
                                ?>
                                <tr>
                                    <td><?php echo $user->category_title; ?></td>
                                    <td><?php echo $user->category_order; ?></td>
                                    <td><?php echo $user->category_content; ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="http://user.alex-r.am/index.php?action=category&subaction=edit&id=<?php echo $user->category_id; ?>" class="btn btn-default"><i class="fa fa-edit"></i>Изменить</a>
                                            <!--<a onclick="return confirm('Вы уверены?')" href="http://user.alex-r.am/index.php?action=category&subaction=delete&id=<?php echo $user->category_id; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i> Удалить</a>-->
                                        </div>
                                    </td>
                                </tr>
                            <?php
                            }
                        }
                        ?>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>
<?php
require( TEMPLATE_PATH."/footer.php" );
?>