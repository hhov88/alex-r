</section>
</div>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.0
    </div>
    <strong>Copyright &copy; 2012-2015 <a href="https://masterweb.am">Masterweb LLC</a>.</strong> All rights reserved.
</footer>
<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <!--<li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
        <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>-->
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <!-- Home tab content -->
        <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class='control-sidebar-menu'>
                <li>
                    <a href='javascript::;'>
                        <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
                            <p>Will be 23 on April 24th</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href='javascript::;'>
                        <i class="menu-icon fa fa-user bg-yellow"></i>
                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>
                            <p>New phone +1(800)555-1234</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href='javascript::;'>
                        <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>
                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>
                            <p>nora@example.com</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href='javascript::;'>
                        <i class="menu-icon fa fa-file-code-o bg-green"></i>
                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>
                            <p>Execution time 5 seconds</p>
                        </div>
                    </a>
                </li>
            </ul><!-- /.control-sidebar-menu -->

            <h3 class="control-sidebar-heading">Tasks Progress</h3>
            <ul class='control-sidebar-menu'>
                <li>
                    <a href='javascript::;'>
                        <h4 class="control-sidebar-subheading">
                            Custom Template Design
                            <span class="label label-danger pull-right">70%</span>
                        </h4>
                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href='javascript::;'>
                        <h4 class="control-sidebar-subheading">
                            Update Resume
                            <span class="label label-success pull-right">95%</span>
                        </h4>
                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href='javascript::;'>
                        <h4 class="control-sidebar-subheading">
                            Laravel Integration
                            <span class="label label-waring pull-right">50%</span>
                        </h4>
                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href='javascript::;'>
                        <h4 class="control-sidebar-subheading">
                            Back End Framework
                            <span class="label label-primary pull-right">68%</span>
                        </h4>
                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                        </div>
                    </a>
                </li>
            </ul><!-- /.control-sidebar-menu -->

        </div><!-- /.tab-pane -->

        <!-- Settings tab content -->
        <div class="tab-pane" id="control-sidebar-settings-tab">
            <form method="post">
                <h3 class="control-sidebar-heading">General Settings</h3>
                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Report panel usage
                        <input type="checkbox" class="pull-right" checked />
                    </label>
                    <p>
                        Some information about this general settings option
                    </p>
                </div><!-- /.form-group -->

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Allow mail redirect
                        <input type="checkbox" class="pull-right" checked />
                    </label>
                    <p>
                        Other sets of options are available
                    </p>
                </div><!-- /.form-group -->

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Expose author name in posts
                        <input type="checkbox" class="pull-right" checked />
                    </label>
                    <p>
                        Allow the user to show his name in blog posts
                    </p>
                </div><!-- /.form-group -->

                <h3 class="control-sidebar-heading">Chat Settings</h3>

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Show me as online
                        <input type="checkbox" class="pull-right" checked />
                    </label>
                </div><!-- /.form-group -->

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Turn off notifications
                        <input type="checkbox" class="pull-right" />
                    </label>
                </div><!-- /.form-group -->

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Delete chat history
                        <a href="javascript::;" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
                    </label>
                </div><!-- /.form-group -->
            </form>
        </div><!-- /.tab-pane -->
    </div>
</aside>
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
            <td class="priority">{%=i+1%}</td>
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td class="priority">{%=i+1%}</td>
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<div class='control-sidebar-bg'></div>
</div>
<!-- jQuery 2.1.4 -->
<script src="<?php echo base_url(); ?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- jQuery UI 1.11.2 -->
<script src="https://code.jquery.com/ui/1.11.2/jquery-ui.min.js" type="text/javascript"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.2 JS -->
<script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Sparkline -->
<script src="<?php echo base_url(); ?>plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- jvectormap -->
<script src="<?php echo base_url(); ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url(); ?>plugins/knob/jquery.knob.js" type="text/javascript"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
<!-- datepicker -->
<script src="<?php echo base_url(); ?>plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url(); ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url(); ?>plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<!-- FastClick -->
<script src='<?php echo base_url(); ?>plugins/fastclick/fastclick.min.js'></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>dist/js/app.min.js" type="text/javascript"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url(); ?>dist/js/pages/dashboard.js" type="text/javascript"></script>

<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>dist/js/demo.js" type="text/javascript"></script>

<!-- Upload images -->
<script src="<?php echo base_url(); ?>js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="<?php echo base_url(); ?>js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="<?php echo base_url(); ?>js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="<?php echo base_url(); ?>js/canvas-to-blob.min.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
<!-- blueimp Gallery script -->
<script src="<?php echo base_url(); ?>js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?php echo base_url(); ?>js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?php echo base_url(); ?>js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="<?php echo base_url(); ?>js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="<?php echo base_url(); ?>js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="<?php echo base_url(); ?>js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="<?php echo base_url(); ?>js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="<?php echo base_url(); ?>js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="<?php echo base_url(); ?>js/jquery.fileupload-ui.js"></script>
<!-- The main application script -->
<script src="<?php echo base_url(); ?>js/main.js"></script>

<script src="<?php echo base_url(); ?>js/dropzone.js"></script>
<script src="<?php echo base_url(); ?>js/sweetalert.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/my_script.js"></script>
<script src="<?php echo base_url(); ?>js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>js/prism.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    var config = {
        '.street_selecton'           : {},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
        '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
        $(".street_selecton").trigger("chosen:updated");
    }
</script>
<?php
if(isset($_GET['action']) && $_GET['action']=='buyer'){
    ?>
    <script>
            var area_tree;
            //$(".area").hide();
            $(".microarea").hide();
            $(document).ready(function(){
                jQuery.post('https://user.alex-r.am/ajax.php', {
                    action 	: 'get_location_tree'
                },function(data){
                    area_tree = $.parseJSON(data);
                    area_tree.forEach(function(region, i, area_tree) {
                        if(window.edit_region !== undefined && edit_region==region.region.settings.region_id){
                            var sel = 'selected';
                        }
                        else{
                            var sel = '';
                        }
                        $(".region").append('<option value="'+region.region.settings.region_id+'" '+sel+'>'+region.region.settings.region_ru+'</option>');
                    });
                    region_change(".region");
                })
            })
            var regSelector = 0;
            function region_change(elm){
                var regId = $(elm).val();
                var citySelect = '';
                var streetSelect = '<option value="0">-- Улица --</option>';
                area_tree.forEach(function(region, i, area_tree) {
                    if(region.region.settings.region_id==regId){
                        var regStreets = region.region.streets;
                        regStreets.forEach(function(street,j,regStreets){
                            if(window.edit_street !== undefined && edit_street==street.street_id){
                                var sel = 'selected';
                            }
                            else{
                                var sel = '';
                            }
                            streetSelect += '<option value="'+street.street_id+'" '+sel+'>'+street.street_ru+'</option>';
                        })
                        regSelector = i;
                    }
                });
                var cities = area_tree[regSelector].region.cities;
                var counter = 0;
                cities.forEach(function(city, i, cities) {
                    if(window.edit_city !== undefined && $.inArray(city.settings.city_id, edit_city)>-1){
                        var sel = 'checked';
                    }
                    else{
                        var sel ='';
                    }
                    citySelect += '<div class="col-md-12"><input type="checkbox" name="city_val[]" class="city_val" onclick="city_change(this)" value="'+city.settings.city_id+'" '+sel+'>'+city.settings.city_ru+'</div>';
                    counter++;
                });
                $(".city").html(citySelect);
                $(".street").html(streetSelect);
                if(window.edit_city !== undefined && edit_city.length>0){
                    city_change(".city");
                }
            }
            var citySelector = 0;
            function city_change(elm){
                var areaSelect = '';
                var streetSelect = '<option value="0">-- Улица --</option>';
                $(".city_val").each(function(){
                    if($(this).is(":checked")){
                        var cityId = $(this).val();
                        var cities = area_tree[regSelector].region.cities;
                        mp = 0;
                        cities.forEach(function(city,i,cities){
                            //alert(city.settings.city_id+'-----'+cityId);
                            if(city.settings.city_id==cityId){
                                /*var cityStreets = city.streets;
                                cityStreets.forEach(function(street,j,cityStreets){
                                    if(window.edit_street !== undefined && edit_street==street.street_id){
                                        var sel = 'selected';
                                    }
                                    else{
                                        var sel = '';
                                    }
                                    streetSelect += '<option value="'+street.street_id+'" '+sel+'>'+street.street_ru+'</option>';
                                })*/
                                //alert(i+'----');
                                citySelector = mp;
                                var areas = cities[citySelector].areas;
                                var counter = 0;
                                areas.forEach(function(area, i, areas){
                                    if(window.edit_area !== undefined && $.inArray(area.settings.area_id,edit_area)>-1){
                                        var sel = 'checked';
                                    }
                                    else{
                                        var sel = '';
                                    }
                                    areaSelect += '<div class="col-md-12"><input type="checkbox" name="area_val[]" onclick="area_select(this,\'\')" class="area_val" value="'+area.settings.area_id+'" '+sel+'>'+area.settings.area_ru+'' +
                                        '<div class="col-md-12 for_microarea"></div>' +
                                        '</div>';
                                    counter++;
                                });
                                areaSelect += '<div class="col-md-12" style="border-bottom:1px solid #ccc"></div>';
                            }
                            mp++;
                        });

                        $(".street").html(streetSelect);
                    }
                })
                $(".area").html(areaSelect);
                if(window.edit_area !== undefined && edit_area.length>0){
                    $(".area_val").each(function(){
                        area_select(this,edit_microarea);
                    })
                }
            }
        function area_select(elm,micro){
            if($(elm).is(":checked")) {
                var area_id = $(elm).val();
                jQuery.post('https://user.alex-r.am/ajax.php', {
                    action: 'get_location_microares',
                    area_id: area_id,
                    micro : micro
                }, function (data) {
                    $(elm).parent().children(".for_microarea").html(data);
                })
            }
            else{
                $(elm).parent().children(".for_microarea").html('');
            }
        }
            $(document).ready(function(){
                if($(".search_results").length ) {
                    $(".overlay").show();
                    var user = $(".search_results").data("user");
                    jQuery.post('https://user.alex-r.am/ajax.php', {
                        action: 'get_client_search_result',
                        user: user,
                        operator: '<?php echo $_SESSION['user_id']; ?>'
                    }, function (data) {
                        $(".search_results").html(data);
                        $(".overlay").hide();
                        $('html,body').animate({
                                scrollTop: $(".search_results").offset().top
                            },
                            'slow');
                    })
                }
            })
    </script>
    <?php
}
?>
<style>
.scrollup {
    width: 86px;
    text-align: center;
    position: fixed;
    bottom: 50px;
    left: 80px;
    color: #fff;
    border-radius: 2px;
    display: none;
    /* text-indent: -9999px; */
    background: url('https://arajark.am/images/button_top.png');
    z-index: 9999999;
	    width: 62px;
    height: 62px;
	font-size:0px;
}

</style>
<a href="#" class="scrollup" style="display: inline;">Դեպի վեր</a>
<script type="text/javascript">
    $(document).ready(function () {

        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        });

        $('.scrollup').click(function () {
            $("html, body").animate({
                scrollTop: 0
            }, 600);
            return false;
        });

    });
</script>
<!-- CK Editor -->
<script src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor1');
        CKEDITOR.replace('editor2');
        CKEDITOR.replace('editor3');
    });
</script>

</body>
</html>