<?php
require( TEMPLATE_PATH."/header.php" );
?>

    <section class="content-header">
        <h1>Риэлторы</h1>
    </section>
    <section class="content">
        <?php
        if(isset($_POST['backend_ad_user']) && $_POST['backend_ad_user']=='Добавить'){
            $user_name = (string)$_POST['user_name'];
            $user_email = (string)$_POST['user_email'];
            $user_phone = (string)$_POST['user_phone'];
            $branch = (string)$_POST['user_branch'];
            $user_city = '';
            if(isset($_POST['user_city'])){
                foreach($_POST['user_city'] as $u_city){
                    $user_city .= $u_city.',';
                }
                $user_city = substr($user_city,0,-1);
            }
            $result_operatorka = mwdb_get_var("SELECT user_id FROM users WHERE user_name='{var}'",array($user_name));
            if($result_operatorka){
                echo '<div style="color:red">Риэлтор с таким логином уже есть!</div>';
            }
            else{
                $password = (string)$_POST['user_pass'];
                $pass = md5($password);
                if($user_name!='' && $password!=''){
                    mwdb_query("INSERT INTO users (user_name,user_password,user_email,user_type,user_phone,branch,city) VALUES ('{var}','{var}','{var}','{var}','{var}','{var}','{var}')",array($user_name,$pass,$user_email,3,$user_phone,$branch,$user_city));
                }
            }
        }
        if(isset($_POST['backend_ad_user']) && $_POST['backend_ad_user']=='Изменить'){
            $edited_user = (int)$_POST['edited_user'];
            $user_name = (string)$_POST['user_name'];
            $user_email = (string)$_POST['user_email'];
            $user_phone = (string)$_POST['user_phone'];
            $branch = (string)$_POST['user_branch'];
            $user_city = '';
            if(isset($_POST['user_city'])){
                foreach($_POST['user_city'] as $u_city){
                    $user_city .= $u_city.',';
                }
                $user_city = substr($user_city,0,-1);
            }
            $user_pass = (string)$_POST['user_pass'];
            $pass = md5($user_pass);
            $result_operatorka = mwdb_get_var("SELECT user_id FROM users WHERE user_name='{var}' AND user_id!={var}",array($user_name,$edited_user));
            if($result_operatorka){
                echo '<div style="color:red">Риэлтор с таким логином уже есть!</div>';
            }
            else{
                if($user_name!=''){
                    mwdb_query("UPDATE users SET user_name='{var}',user_email='{var}',user_phone='{var}',branch='{var}',city='{var}' WHERE user_id={var}",array($user_name,$user_email,$user_phone,$branch,$user_city,$edited_user));
                }
                if($user_pass!=''){
                    mwdb_query("UPDATE users SET user_password='{var}' WHERE user_id={var}",array($pass,$edited_user));
                }
            }
        }
        if(isset($_GET['subaction']) && $_GET['subaction']!=''){
            $subaction = (string)$_GET['subaction'];
        }
        else{
            $subaction = '';
        }
        switch($subaction){
            case 'block':{
                $id_of_subaction = (int)$_GET['id'];
                mwdb_query("UPDATE users SET user_status='0' WHERE user_id={var}",array($id_of_subaction));
                echo 'Риэлтор заблокирован';
                break;
            }
            case 'activate':{
                $id_of_subaction = (int)$_GET['id'];
                mwdb_query("UPDATE users SET user_status='1' WHERE user_id={var}",array($id_of_subaction));
                echo 'Риэлтор активирован';
                break;
            }
            case 'edit':{
                $id_of_subaction = (int)$_GET['id'];
                $user_settings = mwdb_get_row("SELECT * FROM users WHERE user_id={var}",array($id_of_subaction));
                $edit = true;
                ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title">Изменить Риэлтора</h3>
                                <div class="pull-right box-tools">
                                    <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Скрыть" style="margin-right: 5px;"><i class="fa fa-minus"></i></button>
                                </div><!-- /. tools -->
                            </div>
                            <div class="box-body">
                                <form action="" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="edited_user" value="<?php echo $id_of_subaction; ?>"/>
                                    <input type="text" name="user_name" placeholder="Имя, Фамилия" value="<?php echo $user_settings->user_name; ?>" class="form-control"><br>
                                    <input type="email" name="user_email" placeholder="E-mail" value="<?php echo $user_settings->user_email; ?>" class="form-control"><br>
                                    <input type="text" name="user_phone" placeholder="Телефон" value="<?php echo $user_settings->user_phone; ?>" class="form-control"><br>
                                    <input type="password" name="user_pass" placeholder="Пароль" class="form-control"><br>
                                    <select name="user_branch" class="form-control">
                                        <option value="0">-- Выберите офис --</option>
                                        <?php
                                        $result_ofices = mwdb_select("SELECT branch_id,branch_name_hy FROM branch WHERE branch_id>{var}",array(5));
                                        foreach ($result_ofices as $result_ofice) {
                                            ?>
                                            <option value="<?php echo $result_ofice->branch_id; ?>" <?php if($result_ofice->branch_id==$user_settings->branch){echo 'selected';} ?>><?php echo $result_ofice->branch_name_hy; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select><br>
                                    <select name="user_city[]" class="form-control" multiple style="height: 250px">
                                        <?php
                                        $result_cities = mwdb_select("SELECT user_id,user_name FROM users WHERE user_type={var}",array(2));
                                        foreach($result_cities as $row_city){
                                            ?>
                                            <option value="<?php echo $row_city->user_id; ?>" <?php if(in_array($row_city->user_id,explode(',',$user_settings->city))){echo 'selected';} ?>><?php echo $row_city->user_name; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select><br>
                                    <input type="submit" name="backend_ad_user" value="Изменить" class="backend_ad_user btn btn-primary">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                break;
            }
            case 'delete':{
                $id = (int)$_GET['id'];
                mwdb_query("DELETE FROM users WHERE user_id={var}",array($id));
                echo '<script>document.location.href="http://user.alex-r.am/index.php?action=brokers"</script>';
                break;
            }
        }
        ?>
        <?php
        if(!isset($edit)){
            ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Добавить Риэлтор</h3>
                            <div class="pull-right box-tools">
                                <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Скрыть" style="margin-right: 5px;"><i class="fa fa-minus"></i></button>
                            </div><!-- /. tools -->
                        </div>
                        <div class="box-body">
                            <form action="" method="post" enctype="multipart/form-data" autocomplete="off">
                                <input type="text" name="user_name" placeholder="Имя, Фамилия" class="form-control"><br>
                                <input type="email" name="user_email" placeholder="E-mail" class="form-control"><br>
                                <input type="text" name="user_phone" placeholder="Телефон" class="form-control"><br>
                                <input type="password" name="user_pass" placeholder="Пароль" class="form-control" autocomplete="off"><br>
                                <select name="user_branch" class="form-control">
                                    <option value="0">-- Выберите офис --</option>
                                    <?php
                                    $result_ofices = mwdb_select("SELECT branch_id,branch_name_hy FROM branch WHERE branch_id>{var}",array(5));
                                    foreach ($result_ofices as $result_ofice) {
                                        ?>
                                        <option value="<?php echo $result_ofice->branch_id; ?>"><?php echo $result_ofice->branch_name_hy; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select><br>
                                <select name="user_city[]" class="form-control" multiple style="height: 250px">
                                    <?php
                                    $result_cities = mwdb_select("SELECT user_id,user_name FROM users WHERE user_type={var}",array(2));
                                    foreach($result_cities as $row_city){
                                        ?>
                                        <option value="<?php echo $row_city->user_id; ?>"><?php echo $row_city->user_name; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select><br>
                                <input type="submit" name="backend_ad_user" value="Добавить" class="backend_ad_user btn btn-primary">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <style>th{text-align: center !important;}</style>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Все Риэлторы</h3>
                        <div class="box-tools">
                            <div class="input-group">
                                <input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                                <div class="input-group-btn">
                                    <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover table-bordered" style="text-align:center">
                            <tr>
                                <th>ID</th>
                                <th>E-mail</th>
                                <th>Name</th>
                                <th>Телефон</th>
                                <th>Дата регистрации</th>
                                <th>Статус</th>
                                <th>Online</th>
                                <th>действие</th>
                            </tr>
                            <?php
                            if(isset($_GET['page']) && $_GET['page']>1){
                                $page = (int)$_GET['page'];
                            }
                            else{
                                $page = 1;
                            }
                            $offset = ($page-1)*20;
                            $users = mwdb_select("SELECT * FROM users WHERE user_type={var}",array(3));
                            if($users){
                                foreach($users as $user){
                                    ?>
                                    <tr>
                                        <td><?php echo $user->user_id; ?></td>
                                        <td><?php echo $user->user_email; ?></td>
                                        <td><?php echo $user->user_name; ?></td>
                                        <td><?php echo $user->user_phone; ?></td>
                                        <td><?php echo $user->user_registered; ?></td>
                                        <td><?php if($user->user_status=='1'){echo '<span class="label label-success">активный</span>';} else{echo '<span class="label label-danger">неактивный</span>';} ?></td>
                                        <td>
                                            <?php
                                            $now = date("Y-m-d H:i:s",time()-300);
                                            if($user->user_last_login>$now){echo '<span style="color:green"><i class="fa fa-dot-circle-o text-green"></i> Yes</span>';} else{echo '<span style="color:red"><i class="fa fa-circle-o text-red"></i> No</span>';}
                                            ?>
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="http://user.alex-r.am/index.php?action=brokers&subaction=edit&id=<?php echo $user->user_id; ?>" class="btn btn-default"><i class="fa fa-edit"></i>Изменить</a>
                                                <a onclick="return confirm('Вы уверены?')" href="http://user.alex-r.am/index.php?action=brokers&subaction=<?php if($user->user_status=='1'){echo 'block';} else{echo 'activate';} ?>&id=<?php echo $user->user_id; ?>" class="btn btn-default"><?php if($user->user_status=='1'){echo '<i class="fa fa-ban"></i> Блокировать';} else{echo '<i class="fa fa-check"></i> Активировать';} ?></a>
                                                <a onclick="return confirm('Вы уверены?')" href="http://user.alex-r.am/index.php?action=brokers&subaction=delete&id=<?php echo $user->user_id; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i> Удалить</a>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
<?php
require( TEMPLATE_PATH."/footer.php" );
?>